//header files
//standard headers
#include <stdio.h>

//cuda headers
#include <cuda.h>

//global variables
const int SPS_iNumberOfArrayElements = 5;

float* SPS_hostInput1 = NULL;
float* SPS_hostInput2 = NULL;
float* SPS_hostOutput = NULL;

float* SPS_deviceInput1 = NULL;
float* SPS_deviceInput2 = NULL;
float* SPS_deviceOutput = NULL;

//CUDA kernel
__global__ void vecAddGPU(float* SPS_in1, float* SPS_in2, float* SPS_out, int SPS_len)
{
	//code
	int SPS_i = blockIdx.x * blockDim.x + threadIdx.x;

	if (SPS_i < SPS_len)
	{
		SPS_out[SPS_i] = SPS_in1[SPS_i] + SPS_in2[SPS_i];
	}
}

//entry-point function
int main(void)
{
	//function declarations
	void cleanup(void);

	//variable declarations
	int SPS_size = SPS_iNumberOfArrayElements * sizeof(float);
	cudaError_t result = cudaSuccess;

	//code
	//host memory allocation
	SPS_hostInput1 = (float*)malloc(SPS_size);
	if (SPS_hostInput1 == NULL)
	{
		printf("Host Memory allocation is failed for SPS_hostInput1 array. \n");
		cleanup();
		exit(EXIT_FAILURE);
	}

	SPS_hostInput2 = (float*)malloc(SPS_size);
	if (SPS_hostInput2 == NULL)
	{
		printf("Host Memory allocation is failed for SPS_hostInput2 array. \n");
		cleanup();
		exit(EXIT_FAILURE);
	}

	SPS_hostOutput = (float*)malloc(SPS_size);
	if (SPS_hostOutput == NULL)
	{
		printf("Host Memory allocation is failed for SPS_hostOutput array. \n");
		cleanup();
		exit(EXIT_FAILURE);
	}

	//filling values into host arrays
	SPS_hostInput1[0] = 101.0;
	SPS_hostInput1[1] = 102.0;
	SPS_hostInput1[2] = 103.0;
	SPS_hostInput1[3] = 104.0;
	SPS_hostInput1[4] = 105.0;

	SPS_hostInput2[0] = 201.0;
	SPS_hostInput2[1] = 202.0;
	SPS_hostInput2[2] = 203.0;
	SPS_hostInput2[3] = 204.0;
	SPS_hostInput2[4] = 205.0;

	//device memory allocation
	result = cudaMalloc((void**)&SPS_deviceInput1, SPS_size);
	if (result != cudaSuccess)
	{
		printf("Device Memory allocation is failed for SPS_deviceInput1 array.\n");
		cleanup();
		exit(EXIT_FAILURE);
	}

	result = cudaMalloc((void**)&SPS_deviceInput2, SPS_size);
	if (result != cudaSuccess)
	{
		printf("Device Memory allocation is failed for SPS_deviceInput2 array.\n");
		cleanup();
		exit(EXIT_FAILURE);
	}

	result = cudaMalloc((void**)&SPS_deviceOutput, SPS_size);
	if (result != cudaSuccess)
	{
		printf("Device Memory allocation is failed for SPS_deviceOutput array.\n");
		cleanup();
		exit(EXIT_FAILURE);
	}

	//copy data from host arrays into device arrays
	result = cudaMemcpy(SPS_deviceInput1, SPS_hostInput1, SPS_size, cudaMemcpyHostToDevice);
	if (result != cudaSuccess)
	{
		printf("Host to Device Data Copy is failed for SPS_deviceInput1 array.\n");
		cleanup();
		exit(EXIT_FAILURE);
	}

	result = cudaMemcpy(SPS_deviceInput2, SPS_hostInput2, SPS_size, cudaMemcpyHostToDevice);
	if (result != cudaSuccess)
	{
		printf("Host to Device Data Copy is failed for SPS_deviceInput2 array.\n");
		cleanup();
		exit(EXIT_FAILURE);
	}

	dim3 dimGrid = dim3(SPS_iNumberOfArrayElements, 1, 1);
	dim3 dimBlock = dim3(1, 1, 1);

	//CUDA kernel for Vector Addition
	vecAddGPU <<<dimGrid, dimBlock >>> (SPS_deviceInput1, SPS_deviceInput2, SPS_deviceOutput, SPS_iNumberOfArrayElements);

	//copy data from device array into host array
	result = cudaMemcpy(SPS_hostOutput, SPS_deviceOutput, SPS_size, cudaMemcpyDeviceToHost);
	if (result != cudaSuccess)
	{
		printf("Device to Host Data Copy is failed for SPS_hostOutput array.\n");
		cleanup();
		exit(EXIT_FAILURE);
	}

	//vector addition on host
	for (int SPS_i = 0; SPS_i < SPS_iNumberOfArrayElements; SPS_i++)
	{
		printf("%f + %f = %f\n", SPS_hostInput1[SPS_i], SPS_hostInput2[SPS_i], SPS_hostOutput[SPS_i]);
	}

	//cleanup
	cleanup();

	return(0);

}

void cleanup(void)
{
	//code
	if (SPS_deviceOutput)
	{
		cudaFree(SPS_deviceOutput);
		SPS_deviceOutput = NULL;
	}

	if (SPS_deviceInput2)
	{
		cudaFree(SPS_deviceInput2);
		SPS_deviceInput2 = NULL;
	}

	if (SPS_deviceInput1)
	{
		cudaFree(SPS_deviceInput1);
		SPS_deviceInput1 = NULL;
	}

	if (SPS_hostOutput)
	{
		free(SPS_hostOutput);
		SPS_hostOutput = NULL;
	}

	if (SPS_hostInput2)
	{
		free(SPS_hostInput2);
		SPS_hostInput2 = NULL;
	}

	if (SPS_hostInput1)
	{
		free(SPS_hostInput1);
		SPS_hostInput1 = NULL;
	}
}

