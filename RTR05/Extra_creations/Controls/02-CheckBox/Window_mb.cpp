// Windows header files
#include <windows.h>
#include <CommCtrl.h>

// Global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HWND hwnd;
HWND buttonHandle1;
HWND buttonHandle2;
HWND checkBox1Handle;
HWND checkBox2Handle;
HWND checkBox3Handle;
HWND checkBox4Handle;
HWND checkBox5Handle;

//UI related varibales
int spacing = 100;
int buttonX = 50;
int buttonY = 50;
int buttonWidth = 100;
int buttonHeight = 100;

// Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hprevInstance, LPSTR lpszCmdLine, int iCmdShow) //windows.h madhe '#pragma argused' ha macro ahe tyamule hprevInstance ani lpszCmdLine he vaparla nahi tari error ala nahi. Pan Microsoft backward compatibility sathi ajunhi te code madhe hprevInstance ani lpszCmdLine vapartat.
{
	// local variable declarations
	WNDCLASSEX wndclass;

	MSG msg;
	TCHAR szAppName[] = TEXT("Smita_Patil_chi_Window!"); //He process ch naav ahe




	// code
	// WNDCLASSEX declarations
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// Register WNDCLASSEX
	RegisterClassEx(&wndclass);

	// Create the window
	hwnd = CreateWindow(szAppName,
		TEXT("SMITA_PATIL"), //He window ch naav ahe
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	buttonHandle1 = CreateWindowEx(0,
		WC_BUTTON,
		TEXT("My Button 1"),
		WS_CHILD | WS_VISIBLE,
		buttonX, buttonY,
		buttonWidth, buttonHeight,
		hwnd,
		nullptr,
		nullptr,
		nullptr);

	buttonHandle2 = CreateWindowEx(0,
		WC_BUTTON,
		TEXT("My Button 2"),
		WS_CHILD | WS_VISIBLE,
		buttonX + spacing + buttonWidth, buttonY,
		buttonWidth, buttonHeight,
		hwnd,
		nullptr,
		nullptr,
		nullptr);

	checkBox1Handle = CreateWindowEx(0,
		WC_BUTTON,
		TEXT("My Checkbox 1"),
		WS_CHILD | BS_AUTOCHECKBOX | WS_VISIBLE,
		buttonX , 150 + buttonY,
		1.3 * buttonWidth, buttonHeight,
		hwnd,
		nullptr,
		nullptr,
		nullptr);

	checkBox2Handle = CreateWindowEx(0,
		WC_BUTTON,
		TEXT("My Checkbox 2"),
		WS_CHILD | BS_AUTO3STATE | WS_VISIBLE,
		buttonX + (spacing + buttonWidth), 150 + buttonY,
		1.3 * buttonWidth, buttonHeight,
		hwnd,
		nullptr,
		nullptr,
		nullptr);

	checkBox3Handle = CreateWindowEx(0,
		WC_BUTTON,
		TEXT("My Checkbox 3"),
		WS_CHILD | BS_CHECKBOX | WS_VISIBLE,
		buttonX + (spacing + buttonWidth) * 2, 150 + buttonY,
		1.3 * buttonWidth, buttonHeight,
		hwnd,
		nullptr,
		nullptr,
		nullptr);

	checkBox4Handle = CreateWindowEx(0,
		WC_BUTTON,
		TEXT("My Checkbox 4"),
		WS_CHILD | BS_AUTOCHECKBOX | BS_PUSHLIKE | WS_VISIBLE,
		buttonX + (spacing + buttonWidth) * 3, 150 + buttonY,
		1.3 * buttonWidth, buttonHeight,
		hwnd,
		nullptr,
		nullptr,
		nullptr);

	checkBox5Handle = CreateWindowEx(0,
		WC_BUTTON,
		TEXT("My Checkbox 5"),
		WS_CHILD | BS_AUTO3STATE | BS_PUSHLIKE | WS_VISIBLE,
		buttonX + (spacing + buttonWidth) * 4, 150 + buttonY,
		1.3 * buttonWidth, buttonHeight,
		hwnd,
		nullptr,
		nullptr,
		nullptr);

	// Show the window
	ShowWindow(hwnd, iCmdShow);

	// Paint/redraw the window
	UpdateWindow(hwnd);

	// Message Loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);

}

// Call back function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// code
	switch (iMsg)
	{
	case WM_CREATE:
		/*MessageBox(hwnd, TEXT("WM_CREATE Arrived"), TEXT("Message"), MB_OK);*/
		break;

	case WM_SIZE:
		/*MessageBox(hwnd, TEXT("WM_SIZE Arrived"), TEXT("Message"), MB_OK);*/
		break;

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			//MessageBox(hwnd, TEXT("WM_KEYDOWN-VK_ESCAPE Arrived"), TEXT("Message"), MB_OK);
			break;
		}
		break;
		
	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'F':
			//MessageBox(hwnd, TEXT("WM_CHAR-F Key Is Pressed"), TEXT("Message"), MB_OK);
			break;

		/*case 'X':
			buttonX++;
			break;

		case 'x':
			buttonX--;
			break;

		case 'Y':
			buttonY++;
			break;
		case 'y':
			buttonY--;
			break;

		case 'W':
			buttonWidth++;
			break;

		case 'w':
			buttonWidth--;
			break;

		case 'H':
			buttonHeight++;
			break;
		case 'h':
			buttonHeight--;
			break;

		case 'S':
			spacing++;
			break;
		case 's':
			spacing--;
			break;*/
		}
		break;

	case WM_COMMAND:
		if (lParam == (LPARAM)buttonHandle1) 
		{
			MessageBox(hwnd, TEXT("Button1 Is Pressed"), TEXT("Message"), MB_OK);
		}
		else if (lParam == (LPARAM)buttonHandle2)
		{
			MessageBox(hwnd, TEXT("Button2 Is Pressed"), TEXT("Message"), MB_OK);
		}
		else if (lParam == (LPARAM)checkBox1Handle)
		{
			//
			int checkBoxState = SendMessage(checkBox1Handle, BM_GETCHECK, 0, 0);
			switch (checkBoxState)
			{
			case BST_UNCHECKED:
				MessageBox(hwnd, TEXT("Checkbox1 Is Unchecked"), TEXT("Message"), MB_OK);
				break;

			case BST_CHECKED:
				MessageBox(hwnd, TEXT("Checkbox1 Is Checked"), TEXT("Message"), MB_OK);
				break;

			}
		}
		else if (lParam == (LPARAM)checkBox2Handle)
		{
			//MessageBox(hwnd, TEXT("Checkbox2 Is Pressed"), TEXT("Message"), MB_OK);
			int checkBoxState = SendMessage(checkBox2Handle, BM_GETCHECK, 0, 0);
			switch (checkBoxState)
			{
			case BST_UNCHECKED:
				MessageBox(hwnd, TEXT("Checkbox2 Is Unchecked"), TEXT("Message"), MB_OK);
				break;

			case BST_CHECKED:
				MessageBox(hwnd, TEXT("Checkbox2 Is Checked"), TEXT("Message"), MB_OK);
				break;

			case BST_INDETERMINATE:
				MessageBox(hwnd, TEXT("Checkbox2 Is Indeterminated"), TEXT("Message"), MB_OK);
				break;
			}
		}
		else if (lParam == (LPARAM)checkBox3Handle)
		{
			MessageBox(hwnd, TEXT("Checkbox3 Is Pressed"), TEXT("Message"), MB_OK);
		}
		else if (lParam == (LPARAM)checkBox4Handle)
		{
			int checkBoxState = SendMessage(checkBox4Handle, BM_GETCHECK, 0, 0);
			switch (checkBoxState)
			{
			case BST_UNCHECKED:
				MessageBox(hwnd, TEXT("Checkbox4 Is Unchecked"), TEXT("Message"), MB_OK);
				break;

			case BST_CHECKED:
				MessageBox(hwnd, TEXT("Checkbox4 Is Checked"), TEXT("Message"), MB_OK);
				break;
			}
		}
		else if (lParam == (LPARAM)checkBox5Handle)
		{
			int checkBoxState = SendMessage(checkBox5Handle, BM_GETCHECK, 0, 0);
			switch (checkBoxState)
			{
			case BST_UNCHECKED:
				MessageBox(hwnd, TEXT("Checkbox5 Is Unchecked"), TEXT("Message"), MB_OK);
				break;

			case BST_CHECKED:
				MessageBox(hwnd, TEXT("Checkbox5 Is Checked"), TEXT("Message"), MB_OK);
				break;

			case BST_INDETERMINATE:
				MessageBox(hwnd, TEXT("Checkbox5 Is Indeterminated"), TEXT("Message"), MB_OK);
				break;
			}
		}
	
		break;

	//case WM_LBUTTONDOWN:
	//case WM_RBUTTONDOWN:


	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));

}
