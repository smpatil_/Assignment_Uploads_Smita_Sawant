#pragma once

#include <winnt.h>

typedef struct Timer
{
	LARGE_INTEGER firstTick; 
	LARGE_INTEGER lastTick; 
	LARGE_INTEGER frequency;

	double elapsedTime;
	int frameCount;
	double fps;
	float splitTime;
	char titleBuffer[256];

	Timer() : elapsedTime(0.0), frameCount(0), fps(0.0), splitTime(1.0) {}

	void initializeTimer()
	{
		QueryPerformanceFrequency(&frequency);
		QueryPerformanceCounter(&firstTick);
	}

	void startTimer()
	{
		QueryPerformanceCounter(&lastTick);
	}

	void getElapsedTime()
	{

		elapsedTime = (double)((lastTick.QuadPart - firstTick.QuadPart) / (double)(frequency.QuadPart));
	}


	void evaluateTimer()
	{
		if (elapsedTime >= splitTime)
		{
			fps = frameCount / elapsedTime;
			frameCount = 0.0;
			QueryPerformanceCounter(&firstTick);
		}
	}

	void displayTime(HWND targetWindowHandle)
	{
		sprintf(titleBuffer, "FPS: %u || Split Time: %.1f", (unsigned int)fps, splitTime);
		SetWindowText(targetWindowHandle, titleBuffer);

	}

} Timer;

