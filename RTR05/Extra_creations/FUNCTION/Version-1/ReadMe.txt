CREATED draw():


1. Created draw() to replace the steps of adding hard-coded value everytime for vertices to draw premitive shapes. At some point, we would need to add hundreds and thousands of vertices. Hence the arrangement.


2. The draw function is defined, which takes several parameters:

	GLenum primitiveType: The type of primitive shape to draw (e.g., GL_POINTS, GL_TRIANGLES, etc.).
	float x, y, z: The coordinates of the vertex.
	float r, g, b: The color of the primitive.


3. Inside the draw function, a switch-case construct is used to handle different primitive types:

	For GL_POINTS, a single vertex is drawn at the specified coordinates, and its color is set using glColor3f.
	For GL_TRIANGLES, three vertices are drawn to form a triangle, and the color is set for the entire triangle.
	The code blocks for other primitive types like GL_LINES, GL_LINE_LOOP, etc., are currently empty and need to be coded with right vertex and color info.

4. Inside the display function, the draw function is called twice, each time with different values of vertex and color.

	i. draw(GL_TRIANGLES, 1.0f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f);
	ii. draw(GL_POINTS, -0.5f, -0.75f, 0.0f, 0.0f, 1.0f, 0.0f);