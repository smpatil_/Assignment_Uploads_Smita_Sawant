CREATED a loop:


1. The draw function remains the same as before, handling different primitive types with appropriate vertex and color information.

2. A loop is started using for (int i = 0; i < 10; i++) to draw a series of triangles. This loop will execute 10 times.

Inside the loop, the draw function is called with GL_TRIANGLES as the primitive type, and the current values of x and y are passed as vertex coordinates. The color of the triangles is set to red (1.0f, 0.0f, 0.0f).

After drawing a triangle, the values of x and y are updated by adding 0.05 and 0.04 respectively.

float x = 0.02;
	float y = 0.03;
	
	for (int i = 0; i < 10; i++)
	{
		draw(GL_TRIANGLES, x, y, 0.0f, 1.0f, 0.0f, 0.0f);
		x = x + 0.05;
		y = y + 0.04;
	}
