// Windows header files
#include <windows.h> //Win32 SDK API
#include <stdio.h> //for FileIO
#include <stdlib.h> //for Exit

//OpenGL Header Files
#include<gl/GL.h> // GL.h hi file Smita chya laptop chya C:\Program Files (x86)\Windows Kits\10\Include\10.0.22000.0\um\gl ya path la ahe

#include<GL/glu.h>

#include "OGL.h"


//Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//Link with openGL library
#pragma comment(lib, "OpenGL32.lib") //Hi lib sangte ki me OpenGL program karnar asun..........

#pragma comment(lib, "glu32.lib") //Mala opengl utility (GLU) function vapraych ahe

// Global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


//Global variable declarations
DWORD dwStyle = 0;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
BOOL gbFullscreen = FALSE;
FILE* gpFile = NULL;

HWND ghwnd = NULL;
BOOL gbActive = FALSE;

//OpenGL related variables
HDC ghdc = NULL;
HGLRC ghrc = NULL;
GLfloat rot = 90.0f;

GLUquadric* quadric = NULL;


// Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hprevInstance, LPSTR lpszCmdLine, int iCmdShow) //windows.h madhe '#pragma argused' ha macro ahe tyamule hprevInstance ani lpszCmdLine he vaparla nahi tari error ala nahi. Pan Microsoft backward compatibility sathi ajunhi te code madhe hprevInstance ani lpszCmdLine vapartat.
{

	//Window centering
	int screen_width = GetSystemMetrics(SM_CXSCREEN);
	int screen_height = GetSystemMetrics(SM_CYSCREEN);

	int window_width = 800;
	int window_height = 600;

	int window_x = (screen_width - window_width) / 2;
	int window_y = (screen_height - window_height) / 2;

	//Functions declarations >>>> Hi apli functions ahet
	int initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	// local variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Smita_Patil_chi_Window!"); //He process ch naav ahe
	int iResult = 0;
	BOOL bDone = FALSE;


	// code
	gpFile = fopen("Log.txt", "w");
	if (gpFile == NULL)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Opened"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	fprintf(gpFile, "Program Started Successfully!\n");

	// WNDCLASSEX declarations
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// Register WNDCLASSEX
	RegisterClassEx(&wndclass);

	// Create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("SMITA_PATIL"), //He window ch naav ahe
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		window_x,
		window_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//Initialization
	iResult = initialize();

	if (iResult != 0)
	{
		MessageBox(hwnd, TEXT("initialize Function Failed"), TEXT("Error"), MB_OK | MB_ICONERROR);
		DestroyWindow(hwnd);
	}

	// Show the window
	ShowWindow(hwnd, iCmdShow);

	// Paint/redraw the window
	//UpdateWindow(hwnd); >>> Removed because it was used for WM_PAINT and WM_PAINT is not used for real time rendering

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);


	// Game Loop >>>>>> Hi loop aplyala ashichi ashi DirectX madhe vapraychi ahe
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActive == TRUE)
			{
				//Render
				display();

				// Update
				update();
			}
		}
	}

	//Uninitialize
	uninitialize();

	return((int)msg.wParam);

}

// Call back function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Local function declarations
	void ToggleFullscreen(void);
	void resize(int, int);

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActive = TRUE;
		break;

	case WM_KILLFOCUS:
		gbActive = FALSE;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam)); //He height ani width sangta
		break;

	case WM_ERASEBKGND:
		return(0);
	
	case WM_KEYDOWN:
		switch (LOWORD(wParam))
			{
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			}
		break;
	
	case WM_CHAR:
		switch (LOWORD(wParam))
			{
			case 'F':
			case 'f':
				if (gbFullscreen == FALSE)
				{
					ToggleFullscreen();
					gbFullscreen = TRUE;
				}
				else
				{
					ToggleFullscreen();
					gbFullscreen = FALSE;
				}
				break;

			case 'r':
				rot = rot + 0.5f;
				break;

			case 'R':
				rot = rot - 0.5f;
				break;
			}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//Local variable declarations 
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

int initialize(void)
{
	//function declarations
	// Code
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	void resize(int, int);

	//Initialization of PIXELFORMATDESCRIPTOR
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;//1st change of depth - 3D

	//Get the DC
	ghdc = GetDC(ghwnd);
	if (ghdc == NULL)
	{
		fprintf(gpFile, "GetDC() Failed/n");
		return(-1);
	}

	//Tell OS 
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed\n");
		return(-2);
	}

	//Set the obtained pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() Failed\n");
		return(-3);
	}

	//Tell windows graphics (bridging) library to give me openGL compatible context from this device context
	//Create openGL context from device context
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() Failed\n");
		return(-4);
	}

	//Now ghdc will end its role and will give control to ghrc to do further drawing
	//Make rendering context current
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() Failed\n");
		return(-5);
	}

	//2nd step of Depth - 3D (3 compulsory - 2 beautification)
	//Enabling Depth
	glShadeModel(GL_SMOOTH); // Jevha pan apan color karu or shade karu, teva to shade smooth kar - Beautification

	glClearDepth(1.0f); //Compulsory step : Depth buffer la clear karayla glClearDepth() vapar ani depth buffer madhlya saglya buffers na 1 bit kar.

	glEnable(GL_DEPTH_TEST); //Compulsory: 8 test paiki hi ek test - GL_DEPTH_TEST enable kar. Ya tests by default disabled astat.

	glDepthFunc(GL_LEQUAL); //Compulsory: glDepthFunc -> Depth sathi konta function vaparu? - LEQUAL (L less than Equal to) with 1.0f (from glClearDepth()).

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); //Optional: Beautification - Jevha Depth enable karta, tevha corner chya goshti circular na dista elliptical distat. This is not good. Mhanun correction kartana te nicest kar.

	// Set the clear color of window to blue.
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//Initialize quadric

	quadric = gluNewQuadric();

	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

void resize(int width, int height)
{
	// Code
	if (height <= 0)
		height = 1;

	glMatrixMode(GL_PROJECTION); //Current Transformation Matrix
	glLoadIdentity();

	gluPerspective(45.0f, 
		(GLfloat)width / (GLfloat)height, 
		0.1f, 
		100.0f); //45.0f is fovy *** (GLfloat)width / (GLfloat)height is ratio **** 0.1f is near, 100.0f is far.. near and far are not hard coded, they are standard.

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void) //He call kela jaen Game Loop madhe
{
	// Code

	void turret1(void);
	void turret2(void);
	void turret3(void);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //3rd test : Compulsory for Depth 3D.

	glMatrixMode(GL_MODELVIEW); //Current Transformation Matrix
	glLoadIdentity();
	glTranslatef(0.0f, -0.7f, -5.0f);
	//glRotatef(-rot, 1.0f, 0.0f, 0.0f);*/
	//glRotatef(rot, 0.0f, 0.0f, 1.0f);

	turret1();
	turret2();
	turret3();
	


	SwapBuffers(ghdc);
}
void turret1(void)
{
	glLoadIdentity();
	glTranslatef(-2.0f, -2.0f, -8.0f);
	glRotatef(-rot, 1.0f, 0.0f, 0.0f);

	glPushMatrix();
	glTranslatef(0.0f, 0.0f, -0.2f);
	glScalef(2.0f, 1.0f, 0.5f);
	glColor3f(0.149, 0.122, 0.11);
	gluSphere(quadric, 0.5f, 50, 50);
	glPopMatrix();
	//1st Cylinder
	glColor3f(0.298, 0.259, 0.243);
	gluCylinder(quadric, 0.5f, 0.5f, 0.15, 50, 50);
	//Disk 
	glColor3f(0.337, 0.314, 0.318);
	gluDisk(quadric, 0.0, 0.5, 50, 50);

	//2nd Cylinder
	glTranslatef(0.0f, 0.0f, 0.15f);
	glColor3f(0.306, 0.275, 0.271);
	gluCylinder(quadric, 0.54f, 0.54f, 0.15, 50, 50);
	//Disk
	glColor3f(0.322, 0.29, 0.29);
	gluDisk(quadric, 0.0, 0.54, 50, 50);

	//3rd Cylinder
	glTranslatef(0.0f, 0.0f, 0.15f);
	glColor3f(0.427, 0.365, 0.353);
	gluCylinder(quadric, 0.5f, 0.5f, 0.15, 50, 50);
	//Disk
	glColor3f(0.4, 0.408, 0.439);
	gluDisk(quadric, 0.0, 0.5, 50, 50);


	//Disc
	glTranslatef(0.0f, 0.0f, 0.15f);
	glColor3f(0.4, 0.408, 0.439);
	gluDisk(quadric, 0.0f, 0.5f, 50, 50);


	glPushMatrix();
	//1st Plate
	glTranslatef(0.0f, -0.35f, 0.3f);
	glBegin(GL_QUADS);
	//Front face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, 0.4f);

	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	//Right face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);

	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, -0.4f);
	//Back face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	//Left Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	//Top Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);
	//Bottom Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glEnd();

	//2st Plate
	glTranslatef(0.0f, 0.75f, 0.0f);
	glBegin(GL_QUADS);
	//Front face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	//Right face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, -0.4f);
	//Back face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	//Left Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	//Top Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);
	//Bottom Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glEnd();
	glPopMatrix();



	//_________________________________________________________//
	//Up 1st Plate
	glTranslatef(-0.18f, -0.35f, 0.945f);
	glRotatef(-30, 0, 1, 0);
	glBegin(GL_QUADS);
	//Front face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	//Right face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, -0.4f);
	//Back face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	//Left Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	//Top Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);
	//Bottom Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glEnd();


	//Down 2st Plate
	glTranslatef(0.0f, 0.75f, 0.0f);
	glBegin(GL_QUADS);
	//Front face
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	//Right face
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, -0.4f);
	//Back face
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	//Left Face
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	//Top Face
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);
	//Bottom Face
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glEnd();
	glPushMatrix();



	//Mid Cylinder
	glTranslatef(0.0f, 0.13f, 0.25f);
	glRotatef(90, 1, 0, 0);
	glColor3f(0.404, 0.4, 0.42);
	gluCylinder(quadric, 0.05f, 0.05f, 1.0f, 50, 50);
	glPopMatrix();
	//Barrel 1
	glTranslatef(0.55f, -0.38f, 0.15f);
	glRotatef(-70, 0, 1, 0);
	glColor3f(0.38, 0.376, 0.392);
	gluCylinder(quadric, 0.28f, 0.28f, 1.0f, 50, 50);
	//Barrel Disk 1
	glTranslatef(0.0f, 0.0f, 0.0f);
	glColor3f(0.38, 0.376, 0.392);
	gluDisk(quadric, 0.0, 0.28, 50, 50);
	//Barrel Disk 1
	glTranslatef(0.0f, 0.0f, 0.0f);
	glColor3f(0.38, 0.376, 0.392);
	gluDisk(quadric, 0.2, 0.28, 50, 50);


	//Barrel 2
	glTranslatef(0.0f, 0.0f, -1.1f);
	glPushMatrix();
	glColor3f(0.561, 0.537, 0.541);
	gluCylinder(quadric, 0.2f, 0.2f, 2.2f, 50, 50);
	glColor3f(0.682, 0.808, 0.824);
	//Ring 1
	glTranslatef(0.0f, 0.0f, 0.5f);
	gluCylinder(quadric, 0.21f, 0.21f, 0.01f, 50, 50);

	//Ring 2
	glTranslatef(0.0f, 0.0f, 0.1f);
	gluCylinder(quadric, 0.21f, 0.21f, 0.01f, 50, 50);

	//Ring 3
	glTranslatef(0.0f, 0.0f, 0.1f);
	gluCylinder(quadric, 0.21f, 0.21f, 0.01f, 50, 50);

	//Ring 4
	glTranslatef(0.0f, 0.0f, -0.3f);
	gluCylinder(quadric, 0.21f, 0.21f, 0.01f, 50, 50);
	glPopMatrix();
	//Barrel Disk 1
	glTranslatef(0.0f, 0.0f, 1.8f);
	glColor3f(0.733, 0.737, 0.769);
	gluDisk(quadric, 0.2, 0.28, 50, 50);
	//Barrel Disk 1
	glTranslatef(0.0f, 0.0f, 0.0f);
	glColor3f(0.733, 0.737, 0.769);
	gluDisk(quadric, 0.15, 0.2, 50, 50);


	//Barrel 2
	glTranslatef(0.0f, 0.0f, -2.5f);
	glColor3f(0.443, 0.439, 0.431);
	gluCylinder(quadric, 0.15f, 0.15f, 2.2f, 50, 50);

}

void turret2(void)
{
	glLoadIdentity();
	glTranslatef(-6.5f, -1.5f, -15.0f);
	glRotatef(-rot, 1.0f, 0.0f, 0.0f);

	glPushMatrix();
	glTranslatef(0.0f, 0.0f, -0.2f);
	glScalef(2.0f, 1.0f, 0.5f);
	glColor3f(0.149, 0.122, 0.11);
	gluSphere(quadric, 0.5f, 50, 50);
	glPopMatrix();
	//1st Cylinder
	glColor3f(0.298, 0.259, 0.243);
	gluCylinder(quadric, 0.5f, 0.5f, 0.15, 50, 50);
	//Disk 
	glColor3f(0.337, 0.314, 0.318);
	gluDisk(quadric, 0.0, 0.5, 50, 50);

	//2nd Cylinder
	glTranslatef(0.0f, 0.0f, 0.15f);
	glColor3f(0.306, 0.275, 0.271);
	gluCylinder(quadric, 0.54f, 0.54f, 0.15, 50, 50);
	//Disk
	glColor3f(0.322, 0.29, 0.29);
	gluDisk(quadric, 0.0, 0.54, 50, 50);

	//3rd Cylinder
	glTranslatef(0.0f, 0.0f, 0.15f);
	glColor3f(0.427, 0.365, 0.353);
	gluCylinder(quadric, 0.5f, 0.5f, 0.15, 50, 50);
	//Disk
	glColor3f(0.4, 0.408, 0.439);
	gluDisk(quadric, 0.0, 0.5, 50, 50);


	//Disc
	glTranslatef(0.0f, 0.0f, 0.15f);
	glColor3f(0.4, 0.408, 0.439);
	gluDisk(quadric, 0.0f, 0.5f, 50, 50);


	glPushMatrix();
	//1st Plate
	glTranslatef(0.0f, -0.35f, 0.3f);
	glBegin(GL_QUADS);
	//Front face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, 0.4f);

	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	//Right face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);

	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, -0.4f);
	//Back face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	//Left Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	//Top Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);
	//Bottom Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glEnd();

	//2st Plate
	glTranslatef(0.0f, 0.75f, 0.0f);
	glBegin(GL_QUADS);
	//Front face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	//Right face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, -0.4f);
	//Back face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	//Left Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	//Top Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);
	//Bottom Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glEnd();
	glPopMatrix();



	//_________________________________________________________//
	//Up 1st Plate
	glTranslatef(-0.18f, -0.35f, 0.945f);
	glRotatef(-30, 0, 1, 0);
	glBegin(GL_QUADS);
	//Front face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	//Right face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, -0.4f);
	//Back face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	//Left Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	//Top Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);
	//Bottom Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glEnd();


	//Down 2st Plate
	glTranslatef(0.0f, 0.75f, 0.0f);
	glBegin(GL_QUADS);
	//Front face
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	//Right face
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, -0.4f);
	//Back face
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	//Left Face
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	//Top Face
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);
	//Bottom Face
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glEnd();
	glPushMatrix();



	//Mid Cylinder
	glTranslatef(0.0f, 0.13f, 0.25f);
	glRotatef(90, 1, 0, 0);
	glColor3f(0.404, 0.4, 0.42);
	gluCylinder(quadric, 0.05f, 0.05f, 1.0f, 50, 50);
	glPopMatrix();
	//Barrel 1
	glTranslatef(0.55f, -0.38f, 0.15f);
	glRotatef(-70, 0, 1, 0);
	glColor3f(0.38, 0.376, 0.392);
	gluCylinder(quadric, 0.28f, 0.28f, 1.0f, 50, 50);
	//Barrel Disk 1
	glTranslatef(0.0f, 0.0f, 0.0f);
	glColor3f(0.38, 0.376, 0.392);
	gluDisk(quadric, 0.0, 0.28, 50, 50);
	//Barrel Disk 1
	glTranslatef(0.0f, 0.0f, 0.0f);
	glColor3f(0.38, 0.376, 0.392);
	gluDisk(quadric, 0.2, 0.28, 50, 50);


	//Barrel 2
	glTranslatef(0.0f, 0.0f, -1.1f);
	glPushMatrix();
	glColor3f(0.561, 0.537, 0.541);
	gluCylinder(quadric, 0.2f, 0.2f, 2.2f, 50, 50);
	glColor3f(0.682, 0.808, 0.824);
	//Ring 1
	glTranslatef(0.0f, 0.0f, 0.5f);
	gluCylinder(quadric, 0.21f, 0.21f, 0.01f, 50, 50);

	//Ring 2
	glTranslatef(0.0f, 0.0f, 0.1f);
	gluCylinder(quadric, 0.21f, 0.21f, 0.01f, 50, 50);

	//Ring 3
	glTranslatef(0.0f, 0.0f, 0.1f);
	gluCylinder(quadric, 0.21f, 0.21f, 0.01f, 50, 50);

	//Ring 4
	glTranslatef(0.0f, 0.0f, -0.3f);
	gluCylinder(quadric, 0.21f, 0.21f, 0.01f, 50, 50);
	glPopMatrix();
	//Barrel Disk 1
	glTranslatef(0.0f, 0.0f, 1.8f);
	glColor3f(0.733, 0.737, 0.769);
	gluDisk(quadric, 0.2, 0.28, 50, 50);
	//Barrel Disk 1
	glTranslatef(0.0f, 0.0f, 0.0f);
	glColor3f(0.733, 0.737, 0.769);
	gluDisk(quadric, 0.15, 0.2, 50, 50);


	//Barrel 2
	glTranslatef(0.0f, 0.0f, -2.5f);
	glColor3f(0.443, 0.439, 0.431);
	gluCylinder(quadric, 0.15f, 0.15f, 2.2f, 50, 50);

}

void turret3(void)
{

	glLoadIdentity();
	glTranslatef(-12.5f, -1.0f, -21.0f);
	glRotatef(-rot, 1.0f, 0.0f, 0.0f);

	glPushMatrix();
	glTranslatef(0.0f, 0.0f, -0.2f);
	glScalef(2.0f, 1.0f, 0.5f);
	glColor3f(0.149, 0.122, 0.11);
	gluSphere(quadric, 0.5f, 50, 50);
	glPopMatrix();
	//1st Cylinder
	glColor3f(0.298, 0.259, 0.243);
	gluCylinder(quadric, 0.5f, 0.5f, 0.15, 50, 50);
	//Disk 
	glColor3f(0.337, 0.314, 0.318);
	gluDisk(quadric, 0.0, 0.5, 50, 50);

	//2nd Cylinder
	glTranslatef(0.0f, 0.0f, 0.15f);
	glColor3f(0.306, 0.275, 0.271);
	gluCylinder(quadric, 0.54f, 0.54f, 0.15, 50, 50);
	//Disk
	glColor3f(0.322, 0.29, 0.29);
	gluDisk(quadric, 0.0, 0.54, 50, 50);

	//3rd Cylinder
	glTranslatef(0.0f, 0.0f, 0.15f);
	glColor3f(0.427, 0.365, 0.353);
	gluCylinder(quadric, 0.5f, 0.5f, 0.15, 50, 50);
	//Disk
	glColor3f(0.4, 0.408, 0.439);
	gluDisk(quadric, 0.0, 0.5, 50, 50);


	//Disc
	glTranslatef(0.0f, 0.0f, 0.15f);
	glColor3f(0.4, 0.408, 0.439);
	gluDisk(quadric, 0.0f, 0.5f, 50, 50);


	glPushMatrix();
	//1st Plate
	glTranslatef(0.0f, -0.35f, 0.3f);
	glBegin(GL_QUADS);
	//Front face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, 0.4f);

	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	//Right face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);

	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, -0.4f);
	//Back face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	//Left Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	//Top Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);
	//Bottom Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glEnd();

	//2st Plate
	glTranslatef(0.0f, 0.75f, 0.0f);
	glBegin(GL_QUADS);
	//Front face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	//Right face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, -0.4f);
	//Back face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	//Left Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	//Top Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);
	//Bottom Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glEnd();
	glPopMatrix();



	//_________________________________________________________//
	//Up 1st Plate
	glTranslatef(-0.18f, -0.35f, 0.945f);
	glRotatef(-30, 0, 1, 0);
	glBegin(GL_QUADS);
	//Front face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	//Right face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, -0.4f);
	//Back face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	//Left Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	//Top Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);
	//Bottom Face
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glEnd();


	//Down 2st Plate
	glTranslatef(0.0f, 0.75f, 0.0f);
	glBegin(GL_QUADS);
	//Front face
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	//Right face
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, -0.4f);
	//Back face
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	//Left Face
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	//Top Face
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, 0.03f, -0.4f);
	glVertex3f(-0.2f, 0.03f, -0.4f);
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, 0.03f, 0.4f);
	glVertex3f(0.2f, 0.03f, 0.4f);
	//Bottom Face
	glColor3f(0.584, 0.561, 0.545);
	glVertex3f(0.2f, -0.03f, -0.4f);
	glVertex3f(-0.2f, -0.03f, -0.4f);
	glColor3f(0.404, 0.4, 0.42);
	glVertex3f(-0.2f, -0.03f, 0.4f);
	glVertex3f(0.2f, -0.03f, 0.4f);
	glEnd();
	glPushMatrix();



	//Mid Cylinder
	glTranslatef(0.0f, 0.13f, 0.25f);
	glRotatef(90, 1, 0, 0);
	glColor3f(0.404, 0.4, 0.42);
	gluCylinder(quadric, 0.05f, 0.05f, 1.0f, 50, 50);
	glPopMatrix();
	//Barrel 1
	glTranslatef(0.55f, -0.38f, 0.15f);
	glRotatef(-70, 0, 1, 0);
	glColor3f(0.38, 0.376, 0.392);
	gluCylinder(quadric, 0.28f, 0.28f, 1.0f, 50, 50);
	//Barrel Disk 1
	glTranslatef(0.0f, 0.0f, 0.0f);
	glColor3f(0.38, 0.376, 0.392);
	gluDisk(quadric, 0.0, 0.28, 50, 50);
	//Barrel Disk 1
	glTranslatef(0.0f, 0.0f, 0.0f);
	glColor3f(0.38, 0.376, 0.392);
	gluDisk(quadric, 0.2, 0.28, 50, 50);


	//Barrel 2
	glTranslatef(0.0f, 0.0f, -1.1f);
	glPushMatrix();
	glColor3f(0.561, 0.537, 0.541);
	gluCylinder(quadric, 0.2f, 0.2f, 2.2f, 50, 50);
	glColor3f(0.682, 0.808, 0.824);
	//Ring 1
	glTranslatef(0.0f, 0.0f, 0.5f);
	gluCylinder(quadric, 0.21f, 0.21f, 0.01f, 50, 50);

	//Ring 2
	glTranslatef(0.0f, 0.0f, 0.1f);
	gluCylinder(quadric, 0.21f, 0.21f, 0.01f, 50, 50);

	//Ring 3
	glTranslatef(0.0f, 0.0f, 0.1f);
	gluCylinder(quadric, 0.21f, 0.21f, 0.01f, 50, 50);

	//Ring 4
	glTranslatef(0.0f, 0.0f, -0.3f);
	gluCylinder(quadric, 0.21f, 0.21f, 0.01f, 50, 50);
	glPopMatrix();
	//Barrel Disk 1
	glTranslatef(0.0f, 0.0f, 1.8f);
	glColor3f(0.733, 0.737, 0.769);
	gluDisk(quadric, 0.2, 0.28, 50, 50);
	//Barrel Disk 1
	glTranslatef(0.0f, 0.0f, 0.0f);
	glColor3f(0.733, 0.737, 0.769);
	gluDisk(quadric, 0.15, 0.2, 50, 50);


	//Barrel 2
	glTranslatef(0.0f, 0.0f, -2.5f);
	glColor3f(0.443, 0.439, 0.431);
	gluCylinder(quadric, 0.15f, 0.15f, 2.2f, 50, 50);

}

void update(void) //ithe update cha code tevach yeto jeva animation karaych asta
{
	// Code
	//rot = rot + 0.01f;

}

void uninitialize(void)
{
	//Function declarations
	void ToggleFullscreen(void);

	// Code
	// If application is exitting in fullscreen
	if (gbFullscreen == TRUE)
	{
		ToggleFullscreen();
		gbFullscreen = FALSE;
	}

	//Make the HDC as current DC
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	// Destroy or delete Rendering context
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	// Release the HDC
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
	}

	// destroy window
	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	// Close the log file
	if (gpFile)
	{
		fprintf(gpFile, "Program Ended Successfully!\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

