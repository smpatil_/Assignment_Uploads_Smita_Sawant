// Windows header files
#include <windows.h> //Win32 SDK API
#include <stdio.h> //for FileIO
#include <stdlib.h> //for Exit

//OpenGL Header Files
#include<gl/GL.h> // GL.h hi file Smita chya laptop chya C:\Program Files (x86)\Windows Kits\10\Include\10.0.22000.0\um\gl ya path la ahe

#include "OGL.h"


//Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//Link with openGL library
#pragma comment(lib, "OpenGL32.lib")

// Global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


//Global variable declarations
DWORD dwStyle = 0;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
BOOL gbFullscreen = FALSE;
FILE* gpFile = NULL;

HWND ghwnd = NULL;
BOOL gbActive = FALSE;

//OpenGL related variables
HDC ghdc = NULL;
HGLRC ghrc = NULL;




// Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hprevInstance, LPSTR lpszCmdLine, int iCmdShow) //windows.h madhe '#pragma argused' ha macro ahe tyamule hprevInstance ani lpszCmdLine he vaparla nahi tari error ala nahi. Pan Microsoft backward compatibility sathi ajunhi te code madhe hprevInstance ani lpszCmdLine vapartat.
{

	//Window centering
	int screen_width = GetSystemMetrics(SM_CXSCREEN);
	int screen_height = GetSystemMetrics(SM_CYSCREEN);

	int window_width = 800;
	int window_height = 600;

	int window_x = (screen_width - window_width) / 2;
	int window_y = (screen_height - window_height) / 2;

	//Functions declarations >>>> Hi apli functions ahet
	int initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	// local variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Smita_Patil_chi_Window!"); //He process ch naav ahe
	int iResult = 0;
	BOOL bDone = FALSE;


	// code
	gpFile = fopen("Log.txt", "w");
	if (gpFile == NULL)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Opened"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	fprintf(gpFile, "Program Started Successfully!\n");

	// WNDCLASSEX declarations
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// Register WNDCLASSEX
	RegisterClassEx(&wndclass);

	// Create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("SMITA_PATIL"), //He window ch naav ahe
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		window_x,
		window_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//Initialization
	iResult = initialize();

	if (iResult != 0)
	{
		MessageBox(hwnd, TEXT("initialize Function Failed"), TEXT("Error"), MB_OK | MB_ICONERROR);
		DestroyWindow(hwnd);
	}

	// Show the window
	ShowWindow(hwnd, iCmdShow);

	// Paint/redraw the window
	//UpdateWindow(hwnd); >>> Removed because it was used for WM_PAINT and WM_PAINT is not used for real time rendering

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);


	// Game Loop >>>>>> Hi loop aplyala ashichi ashi DirectX madhe vapraychi ahe
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActive == TRUE)
			{
				//Render
				display();

				// Update
				update();
			}
		}
	}

	//Uninitialize
	uninitialize();

	return((int)msg.wParam);

}

// Call back function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Local function declarations
	void ToggleFullscreen(void);
	void resize(int, int);

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActive = TRUE;
		break;

	case WM_KILLFOCUS:
		gbActive = FALSE;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam)); //He height ani width sangta
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'F':
		case 'f':
			if (gbFullscreen == FALSE)
			{
				ToggleFullscreen();
				gbFullscreen = TRUE;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = FALSE;
			}
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//Local variable declarations 
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

int initialize(void)
{
	//function declarations
	// Code
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of PIXELFORMATDESCRIPTOR
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	//Get the DC
	ghdc = GetDC(ghwnd);
	if (ghdc == NULL)
	{
		fprintf(gpFile, "GetDC() Failed/n");
		return(-1);
	}

	//Tell OS 
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed\n");
		return(-2);
	}

	//Set the obtained pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() Failed\n");
		return(-3);
	}

	//Tell windows graphics (bridging) library to give me openGL compatible context from this device context
	//Create openGL context from device context
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() Failed\n");
		return(-4);
	}

	//Now ghdc will end its role and will give control to ghrc to do further drawing
	//Make rendering context current
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() Failed\n");
		return(-5);
	}

	// Set the clear color of window to blue.
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	return(0);
}

void resize(int width, int height)
{
	// Code
	if (height <= 0)
		height = 1;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void) //He call kela jaen Game Loop madhe
{
	// Code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	float d = 100.0f;

	//Tree top
	glBegin(GL_POLYGON);
	glColor3f(0.97, 0.42, 0.74);
	glVertex3f(-93.2706f/d, 3.39394f/d, 0.0f);	
	glVertex3f(-66.6771f/d, 16.2357f/d, 0.0f);
	glVertex3f(-2.46836f/d, -7.5266f/d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.929, 0.298, 0.565);
	glVertex3f(-93.2706f/d, 3.39394f/d, 0.0f);
	glVertex3f(-74.0586f/d, -17.436f/d, 0.0f);
	glVertex3f(-2.46836f/d, -7.5266f/d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.694, 0.243, 0.6);
	glVertex3f(-74.0586f/d, -17.436f/d, 0.0f);
	glVertex3f(-59.195f/d, -20.3696f/d, 0.0f);
	glVertex3f(-2.46836f / d, -7.5266f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.773, 0.298, 0.663);
	glVertex3f(-66.6771f/d, 16.2357f/d, 0.0f);
	glVertex3f(-2.46836f/d, -7.5266f/d, 0.0f);
	glVertex3f(-49.8578f/d, 23.4719f/d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.894, 0.369, 0.718);
	glVertex3f(-87.4079f / d, 33.0941f / d, 0.0f);
	glVertex3f(-66.6771f / d, 16.2357f / d, 0.0f);
	glVertex3f(-49.8578f / d, 23.4719f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.996, 0.435, 0.749);
	glVertex3f(-87.4079f / d, 33.0941f / d, 0.0f);
	glVertex3f(-49.8578f / d, 23.4719f / d, 0.0f);
	glVertex3f(-57.3678f / d, 52.1038f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.996, 0.435, 0.749);
	glVertex3f(-57.3678f / d, 52.1038f / d, 0.0f);
	glVertex3f(-49.8578f / d, 23.4719f / d, 0.0f);
	glVertex3f(-8.08333f / d, 40.1348f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.843, 0.267, 0.616);
	glVertex3f(-57.3678f / d, 52.1038f / d, 0.0f);
	glVertex3f(-49.8578f / d, 23.4719f / d, 0.0f);
	glVertex3f(-8.08333f / d, 40.1348f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.984, 0.31, 0.506);
	glVertex3f(-31.0827f / d, 45.2978f / d, 0.0f);
	glVertex3f(-8.08333f / d, 40.1348f / d, 0.0f);
	glVertex3f(23.7404f / d, 50.555f / d, 0.0f);
	glVertex3f(51.6213f / d, 36.7553f / d, 0.0f);
	glVertex3f(2.90006f / d, 74.4931f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.851, 0.204, 0.506);
	glVertex3f(2.90006f / d, 74.4931f / d, 0.0f);
	glVertex3f(51.6213f / d, 36.7553f / d, 0.0f);
	glVertex3f(42.046f / d, 69.1423f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.851, 0.318, 0.71);
	glVertex3f(23.7404f / d, 50.555f / d, 0.0f);
	glVertex3f(35.0054f / d, 22.674f / d, 0.0f);
	glVertex3f(91.3305f / d, 13.3804f / d, 0.0f);
	glVertex3f(51.6213f / d, 36.7553f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(1.0, 0.435, 0.776);
	glVertex3f(23.7404f / d, 50.555f / d, 0.0f);
	glVertex3f(35.0054f / d, 22.674f / d, 0.0f);
	glVertex3f(-49.8578f / d, 23.4719f / d, 0.0f);
	glVertex3f(-8.08333f / d, 40.1348f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.914, 0.333, 0.663);
	glVertex3f(35.0054f / d, 22.674f / d, 0.0f);
	glVertex3f(-49.8578f / d, 23.4719f / d, 0.0f);
	glVertex3f(-2.46836f / d, -7.5266f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.757, 0.188, 0.553);
	glVertex3f(35.0054f / d, 22.674f / d, 0.0f);
	glVertex3f(-2.46836f / d, -7.5266f / d, 0.0f);
	glVertex3f(39.2122f / d, 3.73843f / d, 0.0f);
	glEnd();
	
	glBegin(GL_POLYGON);
	glColor3f(0.855, 0.231, 0.482);
	glVertex3f(35.0054f / d, 22.674f / d, 0.0f);
	glVertex3f(91.3305f / d, 13.3804f / d, 0.0f);
	glVertex3f(39.2122f / d, 3.73843f / d, 0.0f);
	glEnd();
	
	glBegin(GL_POLYGON);
	glColor3f(0.969, 0.239, 0.431);
	glVertex3f(91.3305f / d, 13.3804f / d, 0.0f);
	glVertex3f(39.2122f / d, 3.73843f / d, 0.0f);
	glVertex3f(82.3185f / d, -9.14965f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.851, 0.204, 0.506);
	glVertex3f(63.7312f / d, -12.8108f / d, 0.0f);
	glVertex3f(39.2122f / d, 3.73843f / d, 0.0f);
	glVertex3f(82.3185f / d, -9.14965f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.671, 0.0, 0.275);
	glVertex3f(63.7312f / d, -12.8108f / d, 0.0f);
	glVertex3f(39.2122f / d, 3.73843f / d, 0.0f);
	glVertex3f(59.5068f / d, -28.0186f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.851, 0.204, 0.506);
	glVertex3f(13.6018f / d, -11.6843f / d, 0.0f);
	glVertex3f(39.2122f / d, 3.73843f / d, 0.0f);
	glVertex3f(59.5068f / d, -28.0186f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.655, 0.0, 0.486);
	glVertex3f(13.6018f / d, -11.6843f / d, 0.0f);
	glVertex3f(39.2122f / d, 3.73843f / d, 0.0f);
	glVertex3f(-2.46836f / d, -7.5266f / d, 0.0f);
	glEnd();

	//Tree trunk

	glBegin(GL_POLYGON);
	glColor3f(0.498, 0.514, 0.522);
	glVertex3f(-2.10449f / d, -7.61757f / d, 0.0f);
	glVertex3f(-3.43808f / d, -8.097880f / d, 0.0f);
	glVertex3f(10.2874f / d, -28.2465f / d, 0.0f);
	glEnd();
	
	glBegin(GL_POLYGON);
	glColor3f(0.553, 0.557, 0.576);
	glVertex3f(26.1288f / d, -16.121f / d, 0.0f);
	glVertex3f(28.6713f / d, -16.3165f / d, 0.0f);
	glVertex3f(8.17077f / d, -37.5258f / d, 0.0f);
	glVertex3f(10.2874f / d, -28.2465f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.306, 0.306, 0.318);
	glVertex3f(28.6713f / d, -16.3165f / d, 0.0f);
	glVertex3f(9.11393f / d, -37.2429f / d, 0.0f);
	glVertex3f(30.0403f / d, -17.6855f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.341, 0.357, 0.361);
	glVertex3f(-3.24159f / d, -8.16338f / d, 0.0f);
	glVertex3f(-5.55499f / d, -8.49268f / d, 0.0f);
	glVertex3f(8.13606f / d, -31.7668f / d, 0.0f);
	glVertex3f(10.2874f / d, -28.2465f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.471, 0.486, 0.498);
	glVertex3f(-7.90095f / d, -8.88475f / d, 0.0f);
	glVertex3f(-5.3585f / d, -8.68917f / d, 0.0f);
	glVertex3f(8.13606f / d, -31.7668f / d, 0.0f);
	glVertex3f(3.4423f / d, -30.789f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.482, 0.475, 0.498);
	glVertex3f(-18.0708f / d, -11.0361f / d, 0.0f);
	glVertex3f(-4.57621f / d, -14.7519f / d, 0.0f);
	glVertex3f(-20.6132f / d, -11.8183f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.286, 0.29, 0.298);
	glVertex3f(-20.6132f / d, -11.8183f / d, 0.0f);
	glVertex3f(-23.8098f / d, -12.6624f / d, 0.0f);
	glVertex3f(-3.20719f / d, -18.2723f / d, 0.0f);
	glVertex3f(-4.57621f / d, -14.7519f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.596, 0.6, 0.616);
	glVertex3f(8.13606f / d, -31.7668f / d, 0.0f);
	glVertex3f(3.4423f / d, -30.789f / d, 0.0f);
	glVertex3f(2.23627f / d, -41.9758f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.42, 0.424, 0.439);
	glVertex3f(10.2874f / d, -28.2465f / d, 0.0f);
	glVertex3f(8.17077f / d, -37.5258f / d, 0.0f);
	glVertex3f(2.23627f / d, -41.9758f / d, 0.0f);
	glVertex3f(1.74733f / d, -59.5774f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.604, 0.6, 0.62);
	glVertex3f(2.23627f / d, -41.9758f / d, 0.0f);
	glVertex3f(1.74733f / d, -59.5774f / d, 0.0f);
	glVertex3f(-27.1193f / d, -90.5562f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.42, 0.424, 0.443);
	glVertex3f(1.74733f / d, -59.5774f / d, 0.0f);
	glVertex3f(-27.1193f / d, -90.5562f / d, 0.0f);
	glVertex3f(-7.88488f / d, -91.4949f / d, 0.0f);
	glVertex3f(-3.44426f / d, -90.5562f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.227, 0.231, 0.251);
	glVertex3f(-3.44426f / d, -90.5562f / d, 0.0f);
	glVertex3f(-7.88488f / d, -91.4949f / d, 0.0f);
	glVertex3f(-0.364861f / d, -91.7296f / d, 0.0f);
	glEnd();


	glBegin(GL_POLYGON);
	glColor3f(0.337, 0.329, 0.38);
	glVertex3f(-0.364861f / d, -91.7296f / d, 0.0f);
	glVertex3f(-7.88488f / d, -91.4949f / d, 0.0f);
	glVertex3f(-27.1193f / d, -90.5562f / d, 0.0f);
	glVertex3f(-53.3261f / d, -91.9252f / d, 0.0f);
	glVertex3f(-74.0569f / d, -93.2786f / d, 0.0f);
	glVertex3f(32.9608f / d, -93.8418f / d, 0.0f);
	glEnd();

	SwapBuffers(ghdc);
}

void update(void) //ithe update cha code tevach yeto jeva animation karaych asta
{
	// Code

}

void uninitialize(void)
{
	//Function declarations
	void ToggleFullscreen(void);

	// Code
	// If application is exitting in fullscreen
	if (gbFullscreen == TRUE)
	{
		ToggleFullscreen();
		gbFullscreen = FALSE;
	}

	//Make the HDC as current DC
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	// Destroy or delete Rendering context
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	// Release the HDC
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
	}

	// destroy window
	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	// Close the log file
	if (gpFile)
	{
		fprintf(gpFile, "Program Ended Successfully!\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

