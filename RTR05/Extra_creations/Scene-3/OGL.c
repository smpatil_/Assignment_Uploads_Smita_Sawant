// Windows header files
#include <windows.h> //Win32 SDK API
#include <stdio.h> //for FileIO
#include <stdlib.h> //for Exit

//OpenGL Header Files
#include<gl/GL.h> // GL.h hi file Smita chya laptop chya C:\Program Files (x86)\Windows Kits\10\Include\10.0.22000.0\um\gl ya path la ahe

#include<GL/glu.h>

#include "OGL.h"


//Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//Link with openGL library
#pragma comment(lib, "OpenGL32.lib") //Hi lib sangte ki me OpenGL program karnar asun..........

#pragma comment(lib, "glu32.lib") //Mala opengl utility (GLU) function vapraych ahe

// Global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


//Global variable declarations
DWORD dwStyle = 0;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
BOOL gbFullscreen = FALSE;
FILE* gpFile = NULL;

HWND ghwnd = NULL;
BOOL gbActive = FALSE;

//OpenGL related variables
HDC ghdc = NULL;
HGLRC ghrc = NULL;

//GLfloat scale_x = 1.0f;
//GLfloat scale_y = 1.0f;

GLfloat tr_x = 1.0f;

// Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hprevInstance, LPSTR lpszCmdLine, int iCmdShow) //windows.h madhe '#pragma argused' ha macro ahe tyamule hprevInstance ani lpszCmdLine he vaparla nahi tari error ala nahi. Pan Microsoft backward compatibility sathi ajunhi te code madhe hprevInstance ani lpszCmdLine vapartat.
{

	//Window centering
	int screen_width = GetSystemMetrics(SM_CXSCREEN);
	int screen_height = GetSystemMetrics(SM_CYSCREEN);

	int window_width = 800;
	int window_height = 600;

	int window_x = (screen_width - window_width) / 2;
	int window_y = (screen_height - window_height) / 2;

	//Functions declarations >>>> Hi apli functions ahet
	int initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	// local variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Smita_Patil_chi_Window!"); //He process ch naav ahe
	int iResult = 0;
	BOOL bDone = FALSE;


	// code
	gpFile = fopen("Log.txt", "w");
	if (gpFile == NULL)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Opened"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	fprintf(gpFile, "Program Started Successfully!\n");

	// WNDCLASSEX declarations
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// Register WNDCLASSEX
	RegisterClassEx(&wndclass);

	// Create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("SMITA_PATIL"), //He window ch naav ahe
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		window_x,
		window_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//Initialization
	iResult = initialize();

	if (iResult != 0)
	{
		MessageBox(hwnd, TEXT("initialize Function Failed"), TEXT("Error"), MB_OK | MB_ICONERROR);
		DestroyWindow(hwnd);
	}

	// Show the window
	ShowWindow(hwnd, iCmdShow);

	// Paint/redraw the window
	//UpdateWindow(hwnd); >>> Removed because it was used for WM_PAINT and WM_PAINT is not used for real time rendering

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);


	// Game Loop >>>>>> Hi loop aplyala ashichi ashi DirectX madhe vapraychi ahe
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActive == TRUE)
			{
				//Render
				display();

				// Update
				update();
			}
		}
	}

	//Uninitialize
	uninitialize();

	return((int)msg.wParam);

}

// Call back function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Local function declarations
	void ToggleFullscreen(void);
	void resize(int, int);

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActive = TRUE;
		break;

	case WM_KILLFOCUS:
		gbActive = FALSE;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam)); //He height ani width sangta
		break;

	case WM_ERASEBKGND:
		return(0);
	
	case WM_KEYDOWN:
		switch (LOWORD(wParam))
			{
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			}
		break;
	
	case WM_CHAR:
		switch (LOWORD(wParam))
			{
			case 'F':
			case 'f':
				if (gbFullscreen == FALSE)
				{
					ToggleFullscreen();
					gbFullscreen = TRUE;
				}
				else
				{
					ToggleFullscreen();
					gbFullscreen = FALSE;
				}
				break;
			}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//Local variable declarations 
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

int initialize(void)
{
	//function declarations
	// Code
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	void resize(int, int);

	//Initialization of PIXELFORMATDESCRIPTOR
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	//Get the DC
	ghdc = GetDC(ghwnd);
	if (ghdc == NULL)
	{
		fprintf(gpFile, "GetDC() Failed/n");
		return(-1);
	}

	//Tell OS 
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed\n");
		return(-2);
	}

	//Set the obtained pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() Failed\n");
		return(-3);
	}

	//Tell windows graphics (bridging) library to give me openGL compatible context from this device context
	//Create openGL context from device context
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() Failed\n");
		return(-4);
	}

	//Now ghdc will end its role and will give control to ghrc to do further drawing
	//Make rendering context current
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() Failed\n");
		return(-5);
	}

	// Set the clear color of window to blue.
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

void resize(int width, int height)
{
	// Code
	if (height <= 0)
		height = 1;

	glMatrixMode(GL_PROJECTION); //Current Transformation Matrix
	glLoadIdentity();

	gluPerspective(45.0f, 
		(GLfloat)width / (GLfloat)height, 
		0.1f, 
		100.0f); //45.0f is fovy *** (GLfloat)width / (GLfloat)height is ratio **** 0.1f is near, 100.0f is far.. near and far are not hard coded, they are standard.

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void) //He call kela jaen Game Loop madhe
{
	// Code
	void drawSkySP(void);
	void drawSeaSP(void);
	void drawSeaWavesSP(void);
	void drawSeaMountain(void);
	void drawSandSP(void);
	void drawRockSP(void);
	void drawCloudSP(void);
	void drawCloudSP2(void);
	void drawTreeSP(void);
	void drawTreeSP2(void);
	
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();



	drawSkySP();
	drawSeaSP();
	drawSeaWavesSP();
	drawSeaMountain();
	drawSandSP();
	drawRockSP();
	drawCloudSP();
	drawCloudSP2();
	drawTreeSP();
	drawTreeSP2();

	SwapBuffers(ghdc);
}

void drawSkySP(void)
{
	

	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -1.1f);
	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	
	GLfloat d = 150.0f;
	
	glBegin(GL_POLYGON);
	glColor3f(0.357, 0.584, 0.796);
	glVertex3f(150.0f/d, 100.0f/d, 0.0f);

	glColor3f(0.357, 0.584, 0.796);
	glVertex3f(-150.0f/d, 100.0f/d, 0.0f);

	glColor3f(0.678, 0.812, 0.953);
	glVertex3f(-150.0f / d, 33.0f / d, 0.0f);

	glColor3f(0.773, 0.875, 0.949);
	glVertex3f(-150.0f/d, 14.0f/d, 0.0f);

	glColor3f(0.773, 0.875, 0.949);
	glVertex3f(150.0f/d, 14.0f/d, 0.0f);

	glColor3f(0.678, 0.812, 0.953);
	glVertex3f(150.0f / d, 33.0f / d, 0.0f);

	glEnd();

}

void drawSeaSP(void)
{
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -1.1f);
	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	GLfloat d = 150.0f;

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.0, 0.718, 0.984);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(150.0f / d, 14.0f / d, 0.0f);
	glVertex3f(-150.0f / d, 14.0f / d, 0.0f);
	glVertex3f(-150.0f / d, -10.0f / d, 0.0f);
	glVertex3f(-130.0f / d, -20.0f / d, 0.0f);
	glVertex3f(-100.0f / d, -30.0f / d, 0.0f);
	glVertex3f(-80.0f / d, -32.0f / d, 0.0f);
	glVertex3f(-60.0f / d, -33.0f / d, 0.0f);
	glVertex3f(-40.0f / d, -34.6f / d, 0.0f);
	glVertex3f(-10.0f / d, -35.0f / d, 0.0f);
	glVertex3f(10.0f / d, -35.0f / d, 0.0f);
	glVertex3f(30.0f / d, -34.0f / d, 0.0f);
	glVertex3f(50.0f / d, -33.5f / d, 0.0f);
	glVertex3f(70.0f / d, -32.0f / d, 0.0f);
	glVertex3f(90.0f / d, -30.0f / d, 0.0f);
	glVertex3f(100.0f / d, -28.0f / d, 0.0f);
	glVertex3f(130.0f / d, -20.0f / d, 0.0f);
	glVertex3f(150.0f / d, -10.0f / d, 0.0f);
	glVertex3f(150.0f / d, 14.0f / d, 0.0f);
	glEnd();

}

void drawSeaWavesSP(void)
{
	GLfloat d = 150.0f;
	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	//Sea waves
	glBegin(GL_POLYGON);
	glColor3f(0.376, 0.878, 1.0);
	glVertex3f(26.05f / d, -8.88f / d, 0.0f);
	glVertex3f(24.658f / d, -10.148f / d, 0.0f);
	glVertex3f(21.657f / d, -10.8745f / d, 0.0f);
	glVertex3f(26.3636f / d, -11.0324f / d, 0.0f);
	glVertex3f(30.564f / d, -11.0324f / d, 0.0f);
	glVertex3f(27.722f / d, -9.93f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.376, 0.878, 1.0);
	glVertex3f(61.13f / d, 0.66f / d, 0.0f);
	glVertex3f(60.26f / d, -0.16f / d, 0.0f);
	glVertex3f(58.54f / d, -0.85f / d, 0.0f);
	glVertex3f(61.35f / d, -0.94f / d, 0.0f);
	glVertex3f(64.0f / d, -0.62f / d, 0.0f);
	glVertex3f(62.17f / d, -0.16f / d, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.376, 0.878, 1.0);
	glVertex3f(65.67f / d, -15.63f / d, 0.0f);
	glVertex3f(63.0f / d, -17.0f / d, 0.0f);
	glVertex3f(68.0f / d, -17.0f / d, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.376, 0.878, 1.0);
	glVertex3f(68.0f / d, -5.2f / d, 0.0f);
	glVertex3f(66.0f / d, -6.0f / d, 0.0f);
	glVertex3f(70.0f / d, -6.0f / d, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.376, 0.878, 1.0);
	glVertex3f(79.0f / d, -12.8f / d, 0.0f);
	glVertex3f(77.0f / d, -14.5f / d, 0.0f);
	glVertex3f(81.0f / d, -14.5f / d, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.376, 0.878, 1.0);
	glVertex3f(26.0f / d, 0.56f / d, 0.0f);
	glVertex3f(24.0f / d, -0.68f / d, 0.0f);
	glVertex3f(28.0f / d, -0.46f / d, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.376, 0.878, 1.0);
	glVertex3f(26.0f / d, -8.9f / d, 0.0f);
	glVertex3f(21.0f / d, -10.8f / d, 0.0f);
	glVertex3f(30.0f / d, -10.62f / d, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.376, 0.878, 1.0);
	glVertex3f(12.0f / d, -18.9f / d, 0.0f);
	glVertex3f(10.0f / d, -20.8f / d, 0.0f);
	glVertex3f(15.0f / d, -19.9f / d, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.376, 0.878, 1.0);
	glVertex3f(-17.5f / d, -11.0f / d, 0.0f);
	glVertex3f(-21.5f / d, -13.4f / d, 0.0f);
	glVertex3f(-13.22f / d, -13.09f / d, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.376, 0.878, 1.0);
	glVertex3f(-34.4f / d, -11.12f / d, 0.0f);
	glVertex3f(-38.16f / d, -13.08f / d, 0.0f);
	glVertex3f(-29.5f / d, -13.4f / d, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.376, 0.878, 1.0);
	glVertex3f(-67.11f / d, -11.39f / d, 0.0f);
	glVertex3f(-70.88f / d, -13.37f / d, 0.0f);
	glVertex3f(-62.68f / d, -13.08f / d, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.376, 0.878, 1.0);
	glVertex3f(-56.0f / d, -8.0f / d, 0.0f);
	glVertex3f(-61.0f / d, -10.2f / d, 0.0f);
	glVertex3f(-54.8f / d, -10.3f / d, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.376, 0.878, 1.0);
	glVertex3f(-71.0f / d, 4.5f / d, 0.0f);
	glVertex3f(-74.0f / d, 3.5f / d, 0.0f);
	glVertex3f(-69.0f / d, 3.5f / d, 0.0f);
	glEnd();


	glBegin(GL_TRIANGLES);
	glColor3f(0.376, 0.878, 1.0);
	glVertex3f(-43.25f / d, -2.33f / d, 0.0f);
	glVertex3f(-45.4f / d, -3.7f / d, 0.0f);
	glVertex3f(-41.8f / d, -3.7f / d, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.376, 0.878, 1.0);
	glVertex3f(-58.3f / d, 2.08f / d, 0.0f);
	glVertex3f(-61.5f / d, 0.73f / d, 0.0f);
	glVertex3f(-56.4f / d, 0.5f / d, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.376, 0.878, 1.0);
	glVertex3f(-95.6f / d, -10.8f / d, 0.0f);
	glVertex3f(-98.0f / d, -12.12f / d, 0.0f);
	glVertex3f(-94.0f / d, -12.35f / d, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.376, 0.878, 1.0);
	glVertex3f(-98.0f / d, 9.16f / d, 0.0f);
	glVertex3f(-98.7f / d, 7.9f / d, 0.0f);
	glVertex3f(-96.0f / d, 8.2f / d, 0.0f);
	glEnd();
}

void drawSeaMountain(void)
{
	glTranslatef(0.2f, 0.0f, -0.1f);
	glScalef(1.1f, 1.1f, 0.0f);
	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	GLfloat d = 150.0f;

	//dark
	glBegin(GL_POLYGON);
	glColor3f(0.0, 0.18, 0.388);
	glVertex3f(-44.3f / d, 13.65f / d, 0.0f);
	glVertex3f(-53.26f / d, 17.27f / d, 0.0f);
	glVertex3f(-62.31f / d, 22.48f / d, 0.0f);
	glVertex3f(-72.46f / d, 17.65f / d, 0.0f);
	glVertex3f(-67.4f / d, 13.08f / d, 0.0f);
	glEnd();

	//light
	glBegin(GL_POLYGON);
	glColor3f(0.357, 0.584, 0.8);
	glVertex3f(-67.4f / d, 13.08f / d, 0.0f);
	glVertex3f(-72.46f / d, 17.65f / d, 0.0f);
	glVertex3f(-78.84f / d, 21.68f / d, 0.0f);
	glVertex3f(-82.12f / d, 20.44f / d, 0.0f);
	glVertex3f(-86.87f / d, 17.84f / d, 0.0f);
	glVertex3f(-97.14f / d, 22.564f / d, 0.0f);
	glVertex3f(-106.9f / d, 13.08f / d, 0.0f);
	glEnd();
}

void drawSandSP(void)
{

	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -1.1f);
	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);

	GLfloat d = 150.0f;
	
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.984, 0.855, 0.714);

	glVertex3f(0.0f / d, -46.0f / d, 0.0f);
	glVertex3f(150.0f / d, -10.0f / d, 0.0f);
	glVertex3f(130.0f / d, -20.0f / d, 0.0f);
	glVertex3f(100.0f / d, -28.0f / d, 0.0f);
	glVertex3f(90.0f / d, -30.0f / d, 0.0f);
	glVertex3f(70.0f / d, -32.0f / d, 0.0f);
	glVertex3f(50.0f / d, -33.5f / d, 0.0f);
	glVertex3f(30.0f / d, -34.0f / d, 0.0f);
	glVertex3f(10.0f / d, -35.0f / d, 0.0f);
	glVertex3f(-10.0f / d, -35.0f / d, 0.0f);
	glVertex3f(-40.0f / d, -34.6f / d, 0.0f);
	glColor3f(0.851, 0.467, 0.141);
	glVertex3f(-60.0f / d, -33.0f / d, 0.0f);
	glColor3f(0.984, 0.855, 0.714);
	glVertex3f(-80.0f / d, -32.0f / d, 0.0f);
	glVertex3f(-100.0f / d, -30.0f / d, 0.0f);
	glVertex3f(-130.0f / d, -20.0f / d, 0.0f);
	glVertex3f(-150.0f / d, -10.0f / d, 0.0f);
	glColor3f(1.0, 0.788, 0.502);
	glVertex3f(-150.0f / d, -100.0f / d, 0.0f);
	glColor3f(0.851, 0.467, 0.141);
	glVertex3f(150.0f / d, -100.0f / d, 0.0f);
	glColor3f(1.0, 0.788, 0.502);
	glVertex3f(150.0f / d, -10.0f / d, 0.0f);
	glEnd();


	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -0.9f);
	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.984, 0.788, 0.584);
	glVertex3f(0.0f / d, -46.0f / d, 0.0f);
	glVertex3f(150.0f / d, -10.0f / d, 0.0f);
	glVertex3f(130.0f / d, -20.0f / d, 0.0f);
	glVertex3f(100.0f / d, -28.0f / d, 0.0f);
	glVertex3f(90.0f / d, -30.0f / d, 0.0f);
	glVertex3f(70.0f / d, -32.0f / d, 0.0f);
	glVertex3f(50.0f / d, -33.5f / d, 0.0f);
	glVertex3f(30.0f / d, -34.0f / d, 0.0f);
	glVertex3f(10.0f / d, -35.0f / d, 0.0f);
	glVertex3f(-10.0f / d, -35.0f / d, 0.0f);
	glVertex3f(-40.0f / d, -34.6f / d, 0.0f);
	glColor3f(0.851, 0.467, 0.141);
	glVertex3f(-60.0f / d, -33.0f / d, 0.0f);
	glColor3f(0.984, 0.855, 0.714);
	glVertex3f(-80.0f / d, -32.0f / d, 0.0f);
	glVertex3f(-100.0f / d, -30.0f / d, 0.0f);
	glVertex3f(-130.0f / d, -20.0f / d, 0.0f);
	glVertex3f(-150.0f / d, -10.0f / d, 0.0f);
	glColor3f(1.0, 0.788, 0.502);
	glVertex3f(-150.0f / d, -100.0f / d, 0.0f);
	glColor3f(0.851, 0.467, 0.141);
	glVertex3f(150.0f / d, -100.0f / d, 0.0f);
	glColor3f(1.0, 0.788, 0.502);
	glVertex3f(150.0f / d, -10.0f / d, 0.0f);
	glEnd();


	////startfish
	//glLoadIdentity();
	//glScalef(0.5f, 0.5f, 0.5f);
	//glTranslatef(0.0f, 0.0f, -1.2f);
	//glBegin(GL_POLYGON);
	//glColor3f(0.773, 0.545, 0.62);
	//glVertex3f(-31.4301f / d, -42.2346f / d, 0.0f);
	//glVertex3f(-34.6897f / d, -38.1602f / d, 0.0f);
	//glVertex3f(-36.1565f / d, -42.3976f / d, 0.0f);
	//glVertex3f(-41.5347f / d, -43.0495f / d, 0.0f);
	//glVertex3f(-38.6011f / d, -47.287f / d, 0.0f);
	//glVertex3f(-40.5569f / d, -52.6653f / d, 0.0f);
	//glVertex3f(-34.0377f / d, -48.1019f / d, 0.0f);
	//glVertex3f(-28.4965f / d, -50.8725f / d, 0.0f);
	//glVertex3f(-29.8003f / d, -45.8202f / d, 0.0f);
	//glVertex3f(-26.2148f / d, -42.0717f / d, 0.0f);
	//glEnd();


	//glPointSize(6.0f);
	//glBegin(GL_POINTS);
	//glColor3f(0.741, 0.451, 0.71);
	//glVertex3f(-33.6846f / d, -43.294f / d, 0.0f);
	//glVertex3f(-36.0756f / d, -44.5705f / d, 0.0f);
	//glVertex3f(-34.6897f / d, -38.1602f / d, 0.0f);
	//glVertex3f(-35.4882f / d, -46.8884f / d, 0.0f);
	//glVertex3f(-31.7611f / d, -47.299f / d, 0.0f);
	//glVertex3f(-31.458f / d, -44.478f / d, 0.0f);
	//glEnd();


	//glPointSize(9.0f);
	//glEnable(GL_POINT_SMOOTH);
	//glBegin(GL_POINTS);
	//glColor3f(0.043, 0.471, 0.624);
	//glVertex3f(-82.4453f / d, -42.1433f / d, 0.0f);
	//glEnd();

	//glPointSize(9.0f);
	//glBegin(GL_POINTS);
	//glColor3f(0.043, 0.471, 0.624);
	//glVertex3f(-82.7282f / d, -44.0296f / d, 0.0f);
	//glEnd();
}

void drawRockSP(void) 
{
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -1.3f);
	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);

	GLfloat d = 150.0f;
	//Rock 1
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.4, 0.4, 0.4);
	glVertex3f(130.0f / d, -20.0f / d, 0.0f);
	glVertex3f(170.0f / d, -30.0f / d, 0.0f);
	glVertex3f(175.0f / d, -15.0f / d, 0.0f);
	glVertex3f(170.0f / d, 0.0f / d, 0.0f);
	glVertex3f(150.0f / d, 14.0f / d, 0.0f);
	glVertex3f(130.0f / d, 10.0f / d, 0.0f);
	glVertex3f(116.0f / d, 3.35f / d, 0.0f);
	glVertex3f(110.0f / d, -10.0f / d, 0.0f);
	glVertex3f(100.0f / d, -29.0f / d, 0.0f);
	glVertex3f(90.0f / d, -44.565f / d, 0.0f);
	glVertex3f(105.0f / d, -44.565f / d, 0.0f);
	glVertex3f(119.0f / d, -54.0f / d, 0.0f);
	glVertex3f(128.0f / d, -60.0f / d, 0.0f);
	glVertex3f(138.0f / d, -53.565f / d, 0.0f);
	glVertex3f(140.0f / d, -53.5f / d, 0.0f);
	glVertex3f(173.0f / d, -53.7f / d, 0.0f);
	glVertex3f(170.0f / d, -30.0f / d, 0.0f);
	glEnd();

	//Rock 2
	glLoadIdentity();
	
	glTranslatef(0.0f, 0.0f, -1.7f);
	
	glScalef(1.1f, 1.1f, 0.0f);
	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.502, 0.502, 0.502);
	glVertex3f(173.0f / d, -54.0f / d, 0.0f);
	glVertex3f(184.0f / d, -63.0f / d, 0.0f);
	glVertex3f(186.0f / d, -41.0f / d, 0.0f);
	glVertex3f(170.0f / d, -30.0f / d, 0.0f);
	glVertex3f(150.0f / d, -44.56f / d, 0.0f);
	glVertex3f(147.0f / d, -61.18f / d, 0.0f);
	glVertex3f(142.0f / d, -71.18f / d, 0.0f);
	glVertex3f(160.0f / d, -76.18f / d, 0.0f);
	glVertex3f(175.0f / d, -73.18f / d, 0.0f);
	glVertex3f(184.0f / d, -63.0f / d, 0.0f);
	glEnd();
}

void drawCloudSP(void)
{
	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	GLfloat d = 150.0f;


	glBegin(GL_TRIANGLE_FAN);
	glColor3f(1.0, 1.0, 1.0);
	glVertex3f(63.0f / d, 36.0f / d, 0.0f);
	glVertex3f(73.24f / d, 35.6627f / d, 0.0f);
	glVertex3f(69.379f / d, 36.1867f / d, 0.0f);
	glVertex3f(67.4796f / d, 37.4966f / d, 0.0f);
	glVertex3f(65.9076f / d, 37.1036f / d, 0.0f);
	glVertex3f(65.2527f / d, 38.3481f / d, 0.0f);
	glVertex3f(63.9427f / d, 38.9375f / d, 0.0f);
	glVertex3f(62.5018f / d, 38.5446f / d, 0.0f);
	glVertex3f(61.7713f / d, 37.6931f / d, 0.0f);
	glVertex3f(60.6024f / d, 37.5621f / d, 0.0f);
	glVertex3f(59.5544f / d, 35.5972f / d, 0.0f);
	glVertex3f(57.131f / d, 35.3352f / d, 0.0f);
	glVertex3f(56.869f / d, 34.5492f / d, 0.0f);
	glVertex3f(60.6678f / d, 33.9597f / d, 0.0f);
	glVertex3f(62.2398f / d, 33.8288f / d, 0.0f);
	glVertex3f(63.0912f / d, 33.1083f / d, 0.0f);
	glVertex3f(63.9427f / d, 32.9773f / d, 0.0f);
	glVertex3f(64.6632f / d, 33.6978f / d, 0.0f);
	glVertex3f(65.6456f / d, 33.3703f / d, 0.0f);
	glVertex3f(67.3486f / d, 33.5668f / d, 0.0f);
	glVertex3f(68.6585f / d, 33.8288f / d, 0.0f);
	glVertex3f(69.4445f / d, 34.6802f / d, 0.0f);
	glVertex3f(73.24f / d, 35.6627f / d, 0.0f);
	glEnd();
}

void drawCloudSP2(void)
{
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -1.1f);
	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	drawCloudSP();

	glLoadIdentity();
	glTranslatef(-1.0f, 0.0f, -1.1f);
	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	drawCloudSP();
}

void drawTreeSP(void)
{
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -0.88f);
	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);

	GLfloat d = 150.0f;
	//Trunk
	glBegin(GL_POLYGON);
	glColor3f(0.635, 0.212, 0.0);
	glVertex3f(-94.0f / d, -35.596f / d, 0.0f);
	glVertex3f(-92.5356f / d, -31.644f / d, 0.0f);
	glVertex3f(-90.7754f / d, -26.7547f / d, 0.0f);
	glVertex3f(-89.0153f / d, -20.8875f / d, 0.0f);
	glVertex3f(-86.864f / d, -13.6512f / d, 0.0f);
	glVertex3f(-84.7126f / d, -2.30796f / d, 0.0f);
	glVertex3f(-85.1038f / d, 5.71057f / d, 0.0f);
	glVertex3f(-87.4507f / d, -1.52566f / d, 0.0f);
	glVertex3f(-89.602f / d, -12.2822f / d, 0.0f);
	glVertex3f(-92.1445f / d, -20.1052f / d, 0.0f);
	glVertex3f(-93.709f / d, -25.5813f / d, 0.0f);
	glVertex3f(-95.8465f / d, -30.6413f / d, 0.0f);
	glVertex3f(-97.4738f / d, -35.2309f / d, 0.0f);
	glEnd();

	//Leaves
	//1
	glBegin(GL_POLYGON);
	glColor3f(0.176, 0.337, 0.0);
	glVertex3f(-85.1038f / d, 5.71057f / d, 0.0f);
	glVertex3f(-92.1445f / d, 3.36368f / d, 0.0f);
	glVertex3f(-95.8604f / d, -4.06812f / d, 0.0f);
	glVertex3f(-90.1887f / d, 0.625649f / d, 0.0f);
	glEnd();

	//2
	glBegin(GL_POLYGON);
	glColor3f(0.176, 0.337, 0.0);
	glVertex3f(-85.1038f / d, 5.71057f / d, 0.0f);
	glVertex3f(-88.8197f / d, 9.81762f / d, 0.0f);
	glVertex3f(-97.8161f / d, 11.5778f / d, 0.0f);
	glVertex3f(-92.34f / d, 7.66631f / d, 0.0f);
	glEnd();

	//3
	glBegin(GL_POLYGON);
	glColor3f(0.176, 0.337, 0.0);
	glVertex3f(-85.1038f / d, 5.71057f / d, 0.0f);
	glVertex3f(-83.0288f / d, 10.9923f / d, 0.0f);
	glVertex3f(-85.0095f / d, 14.6706f / d, 0.0f);
	glVertex3f(-86.1413f / d, 11.6525f / d, 0.0f);
	glEnd();

	//4
	glBegin(GL_POLYGON);
	glColor3f(0.176, 0.337, 0.0);
	glVertex3f(-85.1038f / d, 5.71057f / d, 0.0f);
	glVertex3f(-78.7846f / d, 9.01163f / d, 0.0f);
	glVertex3f(-77.6528f / d, 12.0297f / d, 0.0f);
	glVertex3f(-80.4823f / d, 10.2377f / d, 0.0f);
	glEnd();

	//5
	glBegin(GL_POLYGON);
	glColor3f(0.176, 0.337, 0.0);
	glVertex3f(-85.1038f / d, 5.71057f / d, 0.0f);
	glVertex3f(-77.9358f / d, 4.20151f / d, 0.0f);
	glVertex3f(-71.0507f / d, 4.01288f / d, 0.0f);
	glVertex3f(-77.6528f / d, 7.50258f / d, 0.0f);
	glEnd();

	//5
	glBegin(GL_POLYGON);
	glColor3f(0.176, 0.337, 0.0);
	glVertex3f(-85.1038f / d, 5.71057f / d, 0.0f);
	glVertex3f(-79.6277f / d, -2.11239f / d, 0.0f);
	glVertex3f(-74.5428f / d, -5.43714f / d, 0.0f);
	glVertex3f(-78.2587f / d, 0.821223f / d, 0.0f);
	glEnd();

}
void drawTreeSP2(void)
{
	glLoadIdentity();
	glTranslatef(-0.5f, 0.0f, -0.78f);
	//glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	drawTreeSP();
}

void update(void) //ithe update cha code tevach yeto jeva animation karaych asta
{
	// Code

}

void uninitialize(void)
{
	//Function declarations
	void ToggleFullscreen(void);

	// Code
	// If application is exitting in fullscreen
	if (gbFullscreen == TRUE)
	{
		ToggleFullscreen();
		gbFullscreen = FALSE;
	}

	//Make the HDC as current DC
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	// Destroy or delete Rendering context
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	// Release the HDC
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
	}

	// destroy window
	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	// Close the log file
	if (gpFile)
	{
		fprintf(gpFile, "Program Ended Successfully!\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

