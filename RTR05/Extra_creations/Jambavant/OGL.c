// Windows header files
#include <windows.h> //Win32 SDK API
#include <stdio.h> //for FileIO
#include <stdlib.h> //for Exit

//OpenGL Header Files
#include<gl/GL.h> // GL.h hi file Smita chya laptop chya C:\Program Files (x86)\Windows Kits\10\Include\10.0.22000.0\um\gl ya path la ahe

#include "OGL.h"


//Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//Link with openGL library
#pragma comment(lib, "OpenGL32.lib")

// Global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


//Global variable declarations
DWORD dwStyle = 0;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
BOOL gbFullscreen = FALSE;
FILE* gpFile = NULL;

HWND ghwnd = NULL;
BOOL gbActive = FALSE;

//OpenGL related variables
HDC ghdc = NULL;
HGLRC ghrc = NULL;




// Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hprevInstance, LPSTR lpszCmdLine, int iCmdShow) //windows.h madhe '#pragma argused' ha macro ahe tyamule hprevInstance ani lpszCmdLine he vaparla nahi tari error ala nahi. Pan Microsoft backward compatibility sathi ajunhi te code madhe hprevInstance ani lpszCmdLine vapartat.
{

	//Window centering
	int screen_width = GetSystemMetrics(SM_CXSCREEN);
	int screen_height = GetSystemMetrics(SM_CYSCREEN);

	int window_width = 800;
	int window_height = 600;

	int window_x = (screen_width - window_width) / 2;
	int window_y = (screen_height - window_height) / 2;

	//Functions declarations >>>> Hi apli functions ahet
	int initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	// local variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Smita_Patil_chi_Window!"); //He process ch naav ahe
	int iResult = 0;
	BOOL bDone = FALSE;


	// code
	gpFile = fopen("Log.txt", "w");
	if (gpFile == NULL)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Opened"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	fprintf(gpFile, "Program Started Successfully!\n");

	// WNDCLASSEX declarations
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// Register WNDCLASSEX
	RegisterClassEx(&wndclass);

	// Create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("SMITA_PATIL"), //He window ch naav ahe
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		window_x,
		window_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//Initialization
	iResult = initialize();

	if (iResult != 0)
	{
		MessageBox(hwnd, TEXT("initialize Function Failed"), TEXT("Error"), MB_OK | MB_ICONERROR);
		DestroyWindow(hwnd);
	}

	// Show the window
	ShowWindow(hwnd, iCmdShow);

	// Paint/redraw the window
	//UpdateWindow(hwnd); >>> Removed because it was used for WM_PAINT and WM_PAINT is not used for real time rendering

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);


	// Game Loop >>>>>> Hi loop aplyala ashichi ashi DirectX madhe vapraychi ahe
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActive == TRUE)
			{
				//Render
				display();

				// Update
				update();
			}
		}
	}

	//Uninitialize
	uninitialize();

	return((int)msg.wParam);

}

// Call back function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Local function declarations
	void ToggleFullscreen(void);
	void resize(int, int);

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActive = TRUE;
		break;

	case WM_KILLFOCUS:
		gbActive = FALSE;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam)); //He height ani width sangta
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'F':
		case 'f':
			if (gbFullscreen == FALSE)
			{
				ToggleFullscreen();
				gbFullscreen = TRUE;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = FALSE;
			}
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//Local variable declarations 
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

int initialize(void)
{
	//function declarations
	// Code
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of PIXELFORMATDESCRIPTOR
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	//Get the DC
	ghdc = GetDC(ghwnd);
	if (ghdc == NULL)
	{
		fprintf(gpFile, "GetDC() Failed/n");
		return(-1);
	}

	//Tell OS 
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed\n");
		return(-2);
	}

	//Set the obtained pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() Failed\n");
		return(-3);
	}

	//Tell windows graphics (bridging) library to give me openGL compatible context from this device context
	//Create openGL context from device context
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() Failed\n");
		return(-4);
	}

	//Now ghdc will end its role and will give control to ghrc to do further drawing
	//Make rendering context current
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() Failed\n");
		return(-5);
	}

	// Set the clear color of window to blue.
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	return(0);
}

void resize(int width, int height)
{
	// Code
	if (height <= 0)
		height = 1;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void) //He call kela jaen Game Loop madhe
{
	// Code
	void drawJHead(void);
	void drawJTorso(void);



	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	drawJHead();
	drawJTorso();

	SwapBuffers(ghdc);
}

void drawJHead(void)
{
	float d = 50.0f;

	//Crown

	glBegin(GL_TRIANGLES);
	glColor3f(0.965, 0.843, 0.0);
	glVertex3f(37.5933f / d, -10.2427f / d, 0.0f);
	glVertex3f(36.4554f / d, -10.8929f / d, 0.0f);
	glVertex3f(38.1215f / d, -11.218f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.984, 0.918, 0.0);
	glVertex3f(36.4554f / d, -10.8929f / d, 0.0f);
	glVertex3f(38.1215f / d, -11.218f / d, 0.0f);
	glVertex3f(38.3653f / d, -11.7056f / d, 0.0f);
	glVertex3f(38.8936f / d, -11.9901f / d, 0.0f);
	glVertex3f(38.1215f / d, -12.3964f / d, 0.0f);
	glVertex3f(36.7949f / d, -12.4596f / d, 0.0f);
	glVertex3f(35.4802f / d, -12.112f / d, 0.0f);
	glVertex3f(35.4395f / d, -11.3805f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.98, 0.945, 0.0);
	glVertex3f(38.1215f / d, -12.3964f / d, 0.0f);
	glVertex3f(36.7949f / d, -12.4596f / d, 0.0f);
	glVertex3f(35.4802f / d, -12.112f / d, 0.0f);
	glVertex3f(34.7893f / d, -12.0713f / d, 0.0f);
	glVertex3f(34.0985f / d, -12.6809f / d, 0.0f);
	glVertex3f(34.1392f / d, -13.4936f / d, 0.0f);
	glVertex3f(36.2523f / d, -13.8593f / d, 0.0f);
	glVertex3f(38.6498f / d, -14.3063f / d, 0.0f);
	glVertex3f(39.0562f / d, -13.2904f / d, 0.0f);
	glEnd();

	//forehead and mouth
	glBegin(GL_POLYGON);
	glColor3f(0.576, 0.584, 0.58);
	glVertex3f(34.9257f / d, -13.7645f / d, 0.0f);
	glVertex3f(34.1392f / d, -13.4936f / d, 0.0f);
	glVertex3f(33.5706f / d, -13.9042f / d, 0.0f);
	glVertex3f(33.318f / d, -14.4412f / d, 0.0f);
	glVertex3f(34.9331f / d, -15.4386f / d, 0.0f);
	glEnd();

	//mouth
	glBegin(GL_POLYGON);
	glColor3f(0.576, 0.584, 0.58);
	glVertex3f(33.318f / d, -14.4412f / d, 0.0f);
	glVertex3f(32.4967f / d, -15.0729f / d, 0.0f);
	glVertex3f(31.8018f / d, -15.4204f / d, 0.0f);
	glVertex3f(31.9282f / d, -16.0521f / d, 0.0f);
	glVertex3f(32.4967f / d, -16.6838f / d, 0.0f);
	glVertex3f(33.7286f / d, -16.5575f / d, 0.0f);
	glVertex3f(34.9331f / d, -15.4386f / d, 0.0f);
	glEnd();

	//Nose
	glBegin(GL_POLYGON);
	glColor3f(0.004, 0.0, 0.0);
	glVertex3f(32.4967f / d, -15.0729f / d, 0.0f);
	glVertex3f(31.8018f / d, -15.4204f / d, 0.0f);
	glVertex3f(31.9282f / d, -16.0521f / d, 0.0f);
	glVertex3f(32.7494f / d, -15.5467f / d, 0.0f);
	glEnd();

	//Eye
	glBegin(GL_POLYGON);
	glColor3f(0.0, 0.012, 0.012);
	glVertex3f(34.2655f / d, -14.0306f / d, 0.0f);
	glVertex3f(33.9813f / d, -14.4096f / d, 0.0f);
	glVertex3f(34.4235f / d, -14.4728f / d, 0.0f);
	glVertex3f(34.7709f / d, -14.1885f / d, 0.0f);
	glEnd();

	//Face side-up
	glBegin(GL_POLYGON);
	glColor3f(0.388, 0.392, 0.388);
	glVertex3f(36.8375f / d, -14.0432f / d, 0.0f);
	glVertex3f(36.2523f / d, -13.8593f / d, 0.0f);
	glVertex3f(34.9257f / d, -13.7645f / d, 0.0f);
	glVertex3f(34.9331f / d, -15.4386f / d, 0.0f);
	glVertex3f(36.8577f / d, -16.4862f / d, 0.0f);
	glEnd();

	//Face side-down
	glBegin(GL_POLYGON);
	glColor3f(0.388, 0.392, 0.388);
	glVertex3f(36.8577f / d, -16.4862f / d, 0.0f);
	glVertex3f(34.9331f / d, -15.4386f / d, 0.0f);
	glVertex3f(33.7286f / d, -16.5575f / d, 0.0f);
	glVertex3f(34.7046f / d, -17.3022f / d, 0.0f);
	glVertex3f(35.1152f / d, -17.9023f / d, 0.0f);
	glEnd();

	//Neck
	glBegin(GL_POLYGON);
	glColor3f(0.184, 0.188, 0.173);
	glVertex3f(38.5708f / d, -14.2537f / d, 0.0f);
	glVertex3f(36.8375f / d, -14.0432f / d, 0.0f);
	glVertex3f(36.8577f / d, -16.4862f / d, 0.0f);
	glVertex3f(39.253f / d, -17.3338f / d, 0.0f);
	glVertex3f(39.0319f / d, -15.944f / d, 0.0f);


	glBegin(GL_POLYGON);
	glColor3f(0.184, 0.188, 0.173);
	glVertex3f(39.253f / d, -17.3338f / d, 0.0f);
	glVertex3f(36.8577f / d, -16.4862f / d, 0.0f);
	glVertex3f(35.1152f / d, -17.9023f / d, 0.0f);
	glVertex3f(35.2415f / d, -19.2921f / d, 0.0f);
	glVertex3f(36.6629f / d, -19.0079f / d, 0.0f);
	glVertex3f(39.253f / d, -17.3338f / d, 0.0f);
	glEnd();

}

void drawJTorso(void)
{
	float d = 50.0f;

	//Left hand

	glBegin(GL_POLYGON);
	glColor3f(0.184, 0.188, 0.173);
	glVertex3f(39.4035f / d, -17.6013f / d, 0.0f);
	glVertex3f(36.6449f / d, -19.34f / d, 0.0f);
	glVertex3f(36.6208f / d, -20.7364f / d, 0.0f);
	glVertex3f(36.6449f / d, -22.2491f / d, 0.0f);
	glVertex3f(35.1235f / d, -23.8876f / d, 0.0f);
	glVertex3f(35.7173f / d, -24.5055f / d, 0.0f);
	glVertex3f(36.3072f / d, -25.0111f / d, 0.0f);
	glVertex3f(37.1899f / d, -24.7904f / d, 0.0f);
	glVertex3f(38.173f / d, -24.2286f / d, 0.0f);
	glVertex3f(38.494f / d, -23.7471f / d, 0.0f);
	glVertex3f(39.1159f / d, -22.8042f / d, 0.0f);
	glVertex3f(39.6175f / d, -21.8011f / d, 0.0f);
	glVertex3f(40.2394f / d, -20.818f / d, 0.0f);
	glVertex3f(40.2996f / d, -19.7948f / d, 0.0f);
	glVertex3f(40.0187f / d, -18.571f / d, 0.0f);
	glVertex3f(39.4035f / d, -17.6013f / d, 0.0f);
	glEnd();

	//Back
	glBegin(GL_POLYGON);
	glColor3f(0.154, 0.158, 0.143);
	glVertex3f(39.6175f / d, -21.8011f / d, 0.0f);
	glVertex3f(39.1159f / d, -22.8042f / d, 0.0f);
	glVertex3f(38.494f / d, -23.7471f / d, 0.0f);
	glVertex3f(38.173f / d, -24.2286f / d, 0.0f);
	glVertex3f(37.1899f / d, -24.7904f / d, 0.0f);
	glVertex3f(36.9291f / d, -26.275f / d, 0.0f);
	glVertex3f(38.5542f / d, -25.954f / d, 0.0f);
	glVertex3f(39.467f / d, -25.5126f / d, 0.0f);
	glVertex3f(39.5372f / d, -24.1082f / d, 0.0f);
	glVertex3f(39.5774f / d, -22.6838f / d, 0.0f);
	glVertex3f(39.6175f / d, -21.8011f / d, 0.0f);
	glEnd();

	//tummy below left hand
	glBegin(GL_POLYGON);
	glColor3f(0.388, 0.392, 0.388);
	glVertex3f(37.1899f / d, -24.7904f / d, 0.0f);
	glVertex3f(36.3072f / d, -25.0111f / d, 0.0f);
	glVertex3f(36.3272f / d, -25.3922f / d, 0.0f);
	glVertex3f(36.3272f / d, -25.7734f / d, 0.0f);
	glVertex3f(36.0263f / d, -26.1747f / d, 0.0f);
	glVertex3f(36.9291f / d, -26.275f / d, 0.0f);
	glEnd();

	//Left Palm
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.154, 0.158, 0.143);
	glVertex3f(34.008f / d, -25.9379f / d, 0.0f);
	glVertex3f(34.0f / d, -24.9308f / d, 0.0f);
	glVertex3f(33.4181f / d, -25.1114f / d, 0.0f);
	glVertex3f(32.9968f / d, -25.5929f / d, 0.0f);
	glVertex3f(32.6357f / d, -26.3753f / d, 0.0f);
	glVertex3f(32.2144f / d, -26.8568f / d, 0.0f);
	glVertex3f(32.736f / d, -27.2179f / d, 0.0f);
	glVertex3f(33.1975f / d, -27.3584f / d, 0.0f);
	glVertex3f(33.8395f / d, -27.5389f / d, 0.0f);
	glVertex3f(34.2247f / d, -27.2019f / d, 0.0f);
	glVertex3f(34.6821f / d, -26.913f / d, 0.0f);
	glVertex3f(35.0191f / d, -26.4796f / d, 0.0f);
	glVertex3f(35.284f / d, -25.8537f / d, 0.0f);
	glVertex3f(35.6932f / d, -25.637f / d, 0.0f);
	glVertex3f(35.4043f / d, -24.9148f / d, 0.0f);
	glVertex3f(34.8506f / d, -24.5536f / d, 0.0f);
	glVertex3f(34.0401f / d, -24.349f / d, 0.0f);
	glVertex3f(34.0f / d, -24.9308f / d, 0.0f);
	glEnd();

	//fingers
	glBegin(GL_POLYGON);
	glColor3f(0.031, 0.031, 0.035);
	glVertex3f(32.2144f / d, -26.8568f / d, 0.0f);
	glVertex3f(32.094f / d, -27.4787f / d, 0.0f);
	glVertex3f(32.5153f / d, -27.8278f / d, 0.0f);
	glVertex3f(33.045f / d, -27.8519f / d, 0.0f);
	glVertex3f(33.4783f / d, -27.8038f / d, 0.0f);
	glVertex3f(33.8395f / d, -27.5389f / d, 0.0f);
	glVertex3f(33.1975f / d, -27.3584f / d, 0.0f);
	glVertex3f(32.736f / d, -27.2179f / d, 0.0f);
	glEnd();

	//left hand gold kada
	glBegin(GL_POLYGON);
	glColor3f(1., 0.765, 0.);
	glVertex3f(35.1235f / d, -23.8876f / d, 0.0f);
	glVertex3f(34.5617f / d, -23.8876f / d, 0.0f);
	glVertex3f(34.6991f / d, -24.2002f / d, 0.0f);
	glVertex3f(34.8506f / d, -24.5536f / d, 0.0f);
	glVertex3f(35.4043f / d, -24.9148f / d, 0.0f);
	glVertex3f(35.6932f / d, -25.637f / d, 0.0f);
	glVertex3f(36.0263f / d, -26.1747f / d, 0.0f);
	glVertex3f(36.3272f / d, -25.7734f / d, 0.0f);
	glVertex3f(36.3272f / d, -25.3922f / d, 0.0f);
	glVertex3f(35.7173f / d, -24.5055f / d, 0.0f);
	glEnd();

}

void update(void) //ithe update cha code tevach yeto jeva animation karaych asta
{
	// Code

}

void uninitialize(void)
{
	//Function declarations
	void ToggleFullscreen(void);

	// Code
	// If application is exitting in fullscreen
	if (gbFullscreen == TRUE)
	{
		ToggleFullscreen();
		gbFullscreen = FALSE;
	}

	//Make the HDC as current DC
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	// Destroy or delete Rendering context
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	// Release the HDC
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
	}

	// destroy window
	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	// Close the log file
	if (gpFile)
	{
		fprintf(gpFile, "Program Ended Successfully!\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

