// Windows header files
#include <windows.h> //Win32 SDK API
#include <stdio.h> //for FileIO
#include <stdlib.h> //for Exit

//OpenGL Header Files
#include<gl/GL.h> // GL.h hi file Smita chya laptop chya C:\Program Files (x86)\Windows Kits\10\Include\10.0.22000.0\um\gl ya path la ahe

#include "OGL.h"


//Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//Link with openGL library
#pragma comment(lib, "OpenGL32.lib")

// Global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


//Global variable declarations
DWORD dwStyle = 0;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
BOOL gbFullscreen = FALSE;
FILE* gpFile = NULL;

HWND ghwnd = NULL;
BOOL gbActive = FALSE;

//OpenGL related variables
HDC ghdc = NULL;
HGLRC ghrc = NULL;




// Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hprevInstance, LPSTR lpszCmdLine, int iCmdShow) //windows.h madhe '#pragma argused' ha macro ahe tyamule hprevInstance ani lpszCmdLine he vaparla nahi tari error ala nahi. Pan Microsoft backward compatibility sathi ajunhi te code madhe hprevInstance ani lpszCmdLine vapartat.
{

	//Window centering
	int screen_width = GetSystemMetrics(SM_CXSCREEN);
	int screen_height = GetSystemMetrics(SM_CYSCREEN);

	int window_width = 800;
	int window_height = 600;

	int window_x = (screen_width - window_width) / 2;
	int window_y = (screen_height - window_height) / 2;

	//Functions declarations >>>> Hi apli functions ahet
	int initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	// local variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Smita_Patil_chi_Window!"); //He process ch naav ahe
	int iResult = 0;
	BOOL bDone = FALSE;


	// code
	gpFile = fopen("Log.txt", "w");
	if (gpFile == NULL)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Opened"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	fprintf(gpFile, "Program Started Successfully!\n");

	// WNDCLASSEX declarations
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// Register WNDCLASSEX
	RegisterClassEx(&wndclass);

	// Create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("SMITA_PATIL"), //He window ch naav ahe
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		window_x,
		window_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//Initialization
	iResult = initialize();

	if (iResult != 0)
	{
		MessageBox(hwnd, TEXT("initialize Function Failed"), TEXT("Error"), MB_OK | MB_ICONERROR);
		DestroyWindow(hwnd);
	}

	// Show the window
	ShowWindow(hwnd, iCmdShow);

	// Paint/redraw the window
	//UpdateWindow(hwnd); >>> Removed because it was used for WM_PAINT and WM_PAINT is not used for real time rendering

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);


	// Game Loop >>>>>> Hi loop aplyala ashichi ashi DirectX madhe vapraychi ahe
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActive == TRUE)
			{
				//Render
				display();

				// Update
				update();
			}
		}
	}

	//Uninitialize
	uninitialize();

	return((int)msg.wParam);

}

// Call back function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Local function declarations
	void ToggleFullscreen(void);
	void resize(int, int);

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActive = TRUE;
		break;

	case WM_KILLFOCUS:
		gbActive = FALSE;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam)); //He height ani width sangta
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'F':
		case 'f':
			if (gbFullscreen == FALSE)
			{
				ToggleFullscreen();
				gbFullscreen = TRUE;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = FALSE;
			}
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//Local variable declarations 
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

int initialize(void)
{
	//function declarations
	// Code
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of PIXELFORMATDESCRIPTOR
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	//Get the DC
	ghdc = GetDC(ghwnd);
	if (ghdc == NULL)
	{
		fprintf(gpFile, "GetDC() Failed/n");
		return(-1);
	}

	//Tell OS 
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed\n");
		return(-2);
	}

	//Set the obtained pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() Failed\n");
		return(-3);
	}

	//Tell windows graphics (bridging) library to give me openGL compatible context from this device context
	//Create openGL context from device context
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() Failed\n");
		return(-4);
	}

	//Now ghdc will end its role and will give control to ghrc to do further drawing
	//Make rendering context current
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() Failed\n");
		return(-5);
	}

	// Set the clear color of window to blue.
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	return(0);
}

void resize(int width, int height)
{
	// Code
	if (height <= 0)
		height = 1;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void) //He call kela jaen Game Loop madhe
{
	// Code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	float d = 100.0f;

	//head
	glBegin(GL_POLYGON);
	glColor3f(0.584, 0.584, 0.576);
	glVertex3f(-8.48299f/d, 26.6202f/d, 0.0f);
	glVertex3f(-7.0786f/d, 24.104f/d, 0.0f);
	glVertex3f(-2.22175f/d, 24.104f/d, 0.0f);
	glVertex3f(-5.08904f/d, 26.7373f/d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.722, 0.733, 0.71);
	glVertex3f(-5.08904f / d, 26.7373f / d, 0.0f);
	glVertex3f(-2.22175f / d, 24.104f / d, 0.0f);
	glVertex3f(1.93291f / d, 31.5941f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.545, 0.549, 0.529);
	glVertex3f(1.52329f / d, 24.6892f / d, 0.0f);
	glVertex3f(-2.22175f / d, 24.104f / d, 0.0f);
	glVertex3f(1.93291f / d, 31.5941f / d, 0.0f);
	glEnd();
	
	glBegin(GL_POLYGON);
	glColor3f(0.545, 0.553, 0.553);
	glVertex3f(-3.56762f / d, 30.1897f / d, 0.0f);
	glVertex3f(-5.08904f / d, 26.7373f / d, 0.0f);
	glVertex3f(-1.8339f / d, 28.8329f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.945, 0.949, 0.957);
	glVertex3f(-6.73354f / d, 32.3757f / d, 0.0f);
	glVertex3f(-5.08904f / d, 26.7373f / d, 0.0f);
	glVertex3f(-3.56762f / d, 30.1897f / d, 0.0f);
	glEnd();


	glBegin(GL_POLYGON);
	glColor3f(0.992, 0.996, 0.973);
	glVertex3f(-6.73354f / d, 32.3757f / d, 0.0f);
	glVertex3f(-3.56762f / d, 30.1897f / d, 0.0f);
	glVertex3f(-5.08904f / d, 26.7373f / d, 0.0f);
	glVertex3f(-8.48299f / d, 26.6202f / d, 0.0f);
	glVertex3f(-4.99982f / d, 45.2655f / d, 0.0f);
	glVertex3f(-2.36155f / d, 47.6777f / d, 0.0f);
	glVertex3f(-0.10018f / d, 53.708f / d, 0.0f);
	glVertex3f(-0.552455f / d, 43.4564f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.878, 0.882, 0.89);
	glVertex3f(-0.552455f / d, 43.4564f / d, 0.0f);
	glVertex3f(-6.73354f / d, 32.3757f / d, 0.0f);
	glVertex3f(-3.56762f / d, 30.1897f / d, 0.0f);
	glVertex3f(-1.8339f / d, 28.8329f / d, 0.0f);
	glVertex3f(1.93291f / d, 31.5941f / d, 0.0f);	
	glEnd();


	glBegin(GL_POLYGON);
	glColor3f(0.945, 0.949, 0.957);
	glVertex3f(-0.552455f / d, 43.4564f / d, 0.0f);
	glVertex3f(6.09384f / d, 46.4287f / d, 0.0f);
	glVertex3f(-0.10018f / d, 53.708f / d, 0.0f);
	glVertex3f(-2.13542f / d, 50.5421f / d, 0.0f);
	glEnd();


	glBegin(GL_POLYGON);
	glColor3f(0.031, 0.031, 0.027);
	glVertex3f(-0.552455f / d, 43.4564f / d, 0.0f);
	glVertex3f(6.09384f / d, 46.4287f / d, 0.0f);
	glVertex3f(3.06574f / d, 43.4564f / d, 0.0f);
	glEnd();


	glBegin(GL_POLYGON);
	glColor3f(0.718, 0.725, 0.714);
	glVertex3f(3.06574f / d, 43.4564f / d, 0.0f);
	glVertex3f(-0.552455f / d, 43.4564f / d, 0.0f);
	glVertex3f(1.93291f / d, 31.5941f / d, 0.0f);
	glVertex3f(6.09384f / d, 46.4287f / d, 0.0f);
	glEnd();


	glBegin(GL_POLYGON);
	glColor3f(0.831, 0.835, 0.843);
	glVertex3f(8.89794f / d, 35.5741f / d, 0.0f);
	glVertex3f(1.93291f / d, 31.5941f / d, 0.0f);
	glVertex3f(6.09384f / d, 46.4287f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.216, 0.22, 0.2);
	glVertex3f(14.7194f / d, 38.7527f / d, 0.0f);
	glVertex3f(1.93291f / d, 31.5941f / d, 0.0f);
	glVertex3f(2.14612f / d, 30.1595f / d, 0.0f);
	glEnd();


	glBegin(GL_POLYGON);
	glColor3f(0.91, 0.925, 0.922);
	glVertex3f(14.7194f / d, 38.7527f / d, 0.0f);
	glVertex3f(11.702f / d, 55.2028f / d, 0.0f);
	glVertex3f(6.09384f / d, 46.4287f / d, 0.0f);
	glVertex3f(8.89794f / d, 35.5741f / d, 0.0f);
	glEnd();


	glBegin(GL_POLYGON);
	glColor3f(1.0, 1.0, 1.0);
	glVertex3f(7.26975f / d, 57.0119f / d, 0.0f);
	glVertex3f(11.702f / d, 55.2028f / d, 0.0f);
	glVertex3f(6.09384f / d, 46.4287f / d, 0.0f);
	glVertex3f(-0.10018f / d, 53.708f / d, 0.0f);
	glVertex3f(3.01837f / d, 55.2028f / d, 0.0f);
	glVertex3f(3.74201f / d, 58.5497f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.561, 0.545, 0.525);
	glVertex3f(3.74201f / d, 58.5497f / d, 0.0f);
	glVertex3f(7.25975f / d, 57.0119f / d, 0.0f);
	glVertex3f(5.00838f / d, 65.6956f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.933, 0.937, 0.941);
	glVertex3f(8.44567f / d, 59.1829f / d, 0.0f);
	glVertex3f(7.25975f / d, 57.0119f / d, 0.0f);
	glVertex3f(5.00838f / d, 65.6956f / d, 0.0f);
	glEnd();


	glBegin(GL_POLYGON);
	glColor3f(0.914, 0.929, 0.941);
	glVertex3f(8.44567f / d, 59.1829f / d, 0.0f);
	glVertex3f(7.25975f / d, 57.0119f / d, 0.0f);
	glVertex3f(11.702f / d, 55.2028f / d, 0.0f);
	glVertex3f(15.7725f / d, 56.3788f / d, 0.0f);
	glEnd();


	glBegin(GL_POLYGON);
	glColor3f(0.208, 0.216, 0.2);
	glVertex3f(14.7194f / d, 38.7527f / d, 0.0f);
	glVertex3f(16.3153f / d, 37.9259f / d, 0.0f);
	glVertex3f(15.5916f / d, 40.4587f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.443, 0.451, 0.431);
	glVertex3f(14.7194f / d, 38.7527f / d, 0.0f);
	glVertex3f(11.702f / d, 55.2028f / d, 0.0f);
	glVertex3f(15.5916f / d, 40.4587f / d, 0.0f);
	glEnd();

	//Neck
	glBegin(GL_POLYGON);
	glColor3f(0.831, 0.835, 0.843);
	glVertex3f(15.7725f / d, 56.3788f / d, 0.0f);
	glVertex3f(11.702f / d, 55.2028f / d, 0.0f);
	glVertex3f(31.0594f / d, 24.3577f / d, 0.0f);
	glVertex3f(26.2652f / d, 41.725f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.996, 0.996, 0.996);
	glVertex3f(15.7725f / d, 56.3788f / d, 0.0f);
	glVertex3f(26.2652f / d, 41.725f / d, 0.0f);
	glVertex3f(38.8385f / d, 26.5286f / d, 0.0f);
	glVertex3f(24.6371f / d, 51.0419f / d, 0.0f);
	glEnd();


	glBegin(GL_POLYGON);
	glColor3f(0.824, 0.839, 0.851);
	glVertex3f(24.6371f / d, 51.0419f / d, 0.0f);
	glVertex3f(38.8385f / d, 26.5286f / d, 0.0f);
	glVertex3f(33.8635f / d, 41.1823f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.996, 0.996, 0.996);
	glVertex3f(11.702f / d, 55.2028f / d, 0.0f);
	glVertex3f(15.5916f / d, 40.4587f / d, 0.0f);
	glVertex3f(16.3153f / d, 37.9259f / d, 0.0f);
	glVertex3f(31.0594f / d, 24.3577f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.945, 0.949, 0.957);
	glVertex3f(26.2652f / d, 41.725f / d, 0.0f);
	glVertex3f(31.0594f / d, 24.3577f / d, 0.0f);
	glVertex3f(38.8385f / d, 26.5286f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.918, 0.922, 0.929);
	glVertex3f(38.8385f / d, 26.5286f / d, 0.0f);
	glVertex3f(31.0594f / d, 24.3577f / d, 0.0f);
	glVertex3f(34.5763f / d, 15.6306f / d, 0.0f);
	glVertex3f(40.6621f / d, 14.4148f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.839, 0.843, 0.851);
	glVertex3f(16.3153f / d, 37.9259f / d, 0.0f);
	glVertex3f(31.0594f / d, 24.3577f / d, 0.0f);
	glVertex3f(28.7799f / d, 21.2098f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.878, 0.882, 0.89);
	glVertex3f(16.3153f / d, 37.9259f / d, 0.0f);
	glVertex3f(25.6321f / d, 19.4731f / d, 0.0f);
	glVertex3f(28.7799f / d, 21.2098f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.549, 0.553, 0.529);
	glVertex3f(16.3153f / d, 37.9259f / d, 0.0f);
	glVertex3f(25.6321f / d, 19.4731f / d, 0.0f);
	glVertex3f(21.9415f / d, 19.6902f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.812, 0.82, 0.808);
	glVertex3f(21.9415f / d, 19.6902f / d, 0.0f);
	glVertex3f(16.3153f / d, 37.9259f / d, 0.0f);
	glVertex3f(17.9253f / d, 30.979f / d, 0.0f);
	glVertex3f(17.9253f / d, 27.2884f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.769, 0.769, 0.773);
	glVertex3f(17.9253f / d, 27.2884f / d, 0.0f);
	glVertex3f(16.7313f / d, 22.1867f / d, 0.0f);
	glVertex3f(21.9415f / d, 19.6902f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.839, 0.839, 0.859);
	glVertex3f(14.6168f / d, 19.408f / d, 0.0f);
	glVertex3f(16.7313f / d, 22.1867f / d, 0.0f);
	glVertex3f(21.9415f / d, 19.6902f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.914, 0.918, 0.925);
	glVertex3f(14.6168f / d, 19.408f / d, 0.0f);
	glVertex3f(21.9415f / d, 19.6902f / d, 0.0f);
	glVertex3f(9.92763f / d, 14.8751f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.945, 0.949, 0.949);
	glVertex3f(12.5848f / d, 9.24805f / d, 0.0f);
	glVertex3f(21.9415f / d, 19.6902f / d, 0.0f);
	glVertex3f(9.92763f / d, 14.8751f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(1.0, 1.0, 1.0);
	glVertex3f(12.5848f / d, 9.24805f / d, 0.0f);
	glVertex3f(21.9415f / d, 19.6902f / d, 0.0f);
	glVertex3f(27.3037f / d, 5.99167f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.945, 0.949, 0.957);
	glVertex3f(31.3416f / d, 9.1178f / d, 0.0f);
	glVertex3f(21.9415f / d, 19.6902f / d, 0.0f);
	glVertex3f(27.3037f / d, 5.99167f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.729, 0.737, 0.733);
	glVertex3f(25.6321f / d, 19.4731f / d, 0.0f);
	glVertex3f(21.9415f / d, 19.6902f / d, 0.0f);
	glVertex3f(31.3416f / d, 9.1178f / d, 0.0f);
	glVertex3f(30.9508f / d, 14.5885f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.91, 0.914, 0.922);
	glVertex3f(28.7799f / d, 21.2098f / d, 0.0f);
	glVertex3f(25.6321f / d, 19.4731f / d, 0.0f);
	glVertex3f(30.9508f / d, 14.5885f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.871, 0.875, 0.882);
	glVertex3f(31.0594f / d, 24.3577f / d, 0.0f);
	glVertex3f(28.7799f / d, 21.2098f / d, 0.0f);
	glVertex3f(30.9508f / d, 14.5885f / d, 0.0f);
	glVertex3f(34.5763f / d, 15.6306f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.737, 0.737, 0.737);
	glVertex3f(36.5518f / d, 3.51683f / d, 0.0f);
	glVertex3f(43.3251f / d, 3.12606f / d, 0.0f);
	glVertex3f(40.6621f / d, 14.4148f / d, 0.0f);
	glVertex3f(34.5763f / d, 15.6306f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.914, 0.914, 0.922);
	glVertex3f(42.0225f / d, 7.81525f / d, 0.0f);
	glVertex3f(43.3251f / d, 3.12606f / d, 0.0f);
	glVertex3f(46.3209f / d, 3.25632f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.929, 0.929, 0.937);
	glVertex3f(36.5518f / d, 3.51683f / d, 0.0f);
	glVertex3f(31.3416f / d, 9.1178f / d, 0.0f);
	glVertex3f(30.9508f / d, 14.5885f / d, 0.0f);
	glVertex3f(34.5763f / d, 15.6306f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.918, 0.922, 0.929);
	glVertex3f(42.5956f / d, -4.81951f / d, 0.0f);
	glVertex3f(43.3251f / d, 3.12606f / d, 0.0f);
	glVertex3f(46.3209f / d, 3.25632f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.925, 0.929, 0.937);
	glVertex3f(42.5956f / d, -4.81951f / d, 0.0f);
	glVertex3f(43.3251f / d, 3.12606f / d, 0.0f);
	glVertex3f(46.3209f / d, 3.25632f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.863, 0.871, 0.871);
	glVertex3f(42.5956f / d, -4.81951f / d, 0.0f);
	glVertex3f(45.8781f / d, -7.0078f / d, 0.0f);
	glVertex3f(46.3209f / d, 3.25632f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.773, 0.773, 0.78);
	glVertex3f(43.3251f / d, 3.12606f / d, 0.0f);
	glVertex3f(36.5518f / d, 3.51683f / d, 0.0f);
	glVertex3f(33.5299f / d, -4.03798f / d, 0.0f);
	glVertex3f(42.5956f / d, -4.81951f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(1.0, 1.0, 1.0);
	glVertex3f(36.5518f / d, 3.51683f / d, 0.0f);
	glVertex3f(34.0753f / d, 6.24771f / d, 0.0f);
	glVertex3f(31.3416f / d, 9.1178f / d, 0.0f);
	glVertex3f(27.3037f / d, 5.99167f / d, 0.0f);
	glVertex3f(33.5299f / d, -4.03798f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.871, 0.875, 0.882);
	glVertex3f(27.3037f / d, 5.99167f / d, 0.0f);
	glVertex3f(28.8407f / d, -5.91365f / d, 0.0f);
	glVertex3f(33.5299f / d, -4.03798f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.929, 0.929, 0.937);
	glVertex3f(12.5848f / d, 9.24805f / d, 0.0f);
	glVertex3f(28.8407f / d, -5.91365f / d, 0.0f);
	glVertex3f(27.3037f / d, 5.99167f / d, 0.0f);
	glEnd();

	/*glBegin(GL_POLYGON);
	glColor3f(0.435, 0.431, 0.424);
	glVertex3f(27.7702f / d, -15.4487f / d, 0.0f);
	glVertex3f(28.8407f / d, -5.91365f / d, 0.0f);
	glVertex3f(33.5299f / d, -4.03798f / d, 0.0f);
	glEnd();*/

	//Hairs

	glBegin(GL_POLYGON);
	glColor3f(0.706, 0.188, 0.557);
	glVertex3f(-4.99982f / d, 45.2655f / d, 0.0f);
	glVertex3f(-2.37155f / d, 47.6777f / d, 0.0f);
	glVertex3f(-22.5481f / d, 59.9192f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.871, 0.514, 0.686);
	glVertex3f(-1.86252f / d, 50.4056f / d, 0.0f);
	glVertex3f(-2.37155f / d, 47.6777f / d, 0.0f);
	glVertex3f(-22.5481f / d, 59.9192f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.58, 0.227, 0.498);
	glVertex3f(-0.10018f / d, 53.708f / d, 0.0f);
	glVertex3f(-5.1762f / d, 56.9283f / d, 0.0f);
	glVertex3f(3.01837f / d, 55.2028f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.58, 0.227, 0.498);
	glVertex3f(-0.10018f / d, 53.708f / d, 0.0f);
	glVertex3f(-5.1762f / d, 56.9283f / d, 0.0f);
	glVertex3f(3.01837f / d, 55.2028f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.8, 0.325, 0.627);
	glVertex3f(3.01837f / d, 55.2028f / d, 0.0f);
	glVertex3f(-1.51929f / d, 55.9458f / d, 0.0f);
	glVertex3f(-2.55632f / d, 61.6768f / d, 0.0f);

	glColor3f(0.706, 0.188, 0.573);
	glVertex3f(3.01837f / d, 55.2028f / d, 0.0f);
	glVertex3f(1.0435f / d, 57.6598f / d, 0.0f);
	glVertex3f(1.26434f / d, 63.9146f / d, 0.0f);
	glVertex3f(3.1201f / d, 61.8406f / d, 0.0f);
	glVertex3f(4.75752f / d, 63.0959f / d, 0.0f);
	glVertex3f(3.74201f / d, 58.5497f / d, 0.0f);
	glVertex3f(3.74201f / d, 58.5497f / d, 0.0f);
	glEnd();

	//Hairs neck
	glBegin(GL_POLYGON);
	glColor3f(0.667, 0.2, 0.549);
	glVertex3f(40.6621f / d, 14.4148f / d, 0.0f);
	glVertex3f(42.0225f / d, 7.81525f / d, 0.0f);
	glVertex3f(47.2005f / d, 6.98147f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.773, 0.369, 0.627);
	glVertex3f(38.9885f / d, 25.6279f / d, 0.0f);
	glVertex3f(40.6621f / d, 14.4148f / d, 0.0f);
	glVertex3f(48.9933f / d, 12.6857f / d, 0.0f);
	glVertex3f(44.8277f / d, 22.8246f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.871, 0.522, 0.725);
	glVertex3f(38.9885f / d, 25.6279f / d, 0.0f);
	glVertex3f(45.4406f / d, 25.2711f / d, 0.0f);
	glVertex3f(51.8069f / d, 19.3763f / d, 0.0f);
	glVertex3f(44.8277f / d, 22.8246f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.651, 0.196, 0.529);
	glVertex3f(45.4406f / d, 25.2711f / d, 0.0f);
	glVertex3f(52.7501f / d, 27.7861f / d, 0.0f);
	glVertex3f(49.9892f / d, 27.1574f / d, 0.0f);
	glVertex3f(38.9885f / d, 25.6279f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.808, 0.353, 0.639);
	glVertex3f(45.5978f / d, 31.716f / d, 0.0f);
	glVertex3f(35.9204f / d, 35.2528f / d, 0.0f);
	glVertex3f(38.9885f / d, 25.6279f / d, 0.0f);
	glVertex3f(49.9892f / d, 27.1574f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.686, 0.192, 0.569);
	glVertex3f(35.9204f / d, 35.2528f / d, 0.0f);
	glVertex3f(45.5978f / d, 31.716f / d, 0.0f);
	glVertex3f(49.9992f / d, 30.0654f / d, 0.0f);
	glVertex3f(56.2669f / d, 30.6942f / d, 0.0f);
	glVertex3f(49.842f / d, 31.716f / d, 0.0f);
	glVertex3f(48.5845f / d, 32.6591f / d, 0.0f);
	glVertex3f(43.0827f / d, 35.6458f / d, 0.0f);
	glVertex3f(33.8635f / d, 41.1823f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.878, 0.522, 0.682);
	glVertex3f(48.5845f / d, 32.6591f / d, 0.0f);
	glVertex3f(43.0827f / d, 35.6458f / d, 0.0f);
	glVertex3f(53.0645f / d, 35.017f / d, 0.0f);
	glEnd();
	
	glBegin(GL_POLYGON);
	glColor3f(0.808, 0.353, 0.639);
	glVertex3f(43.0827f / d, 35.6458f / d, 0.0f);
	glVertex3f(33.8635f / d, 41.1823f / d, 0.0f);
	glVertex3f(51.2468f / d, 39.5756f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.69, 0.184, 0.549);
	glVertex3f(33.8635f / d, 41.1823f / d, 0.0f);
	glVertex3f(24.6371f / d, 51.0419f / d, 0.0f);
	glVertex3f(28.6237f / d, 50.5484f / d, 0.0f);
	glVertex3f(38.3173f / d, 42.8197f / d, 0.0f);
	glVertex3f(41.3957f / d, 40.4618f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.89, 0.529, 0.722);
	glVertex3f(28.6237f / d, 50.5484f / d, 0.0f);
	glVertex3f(38.3173f / d, 42.8197f / d, 0.0f);

	glColor3f(0.769, 0.373, 0.627);
	glVertex3f(49.5073f / d, 46.7496f / d, 0.0f);
	glVertex3f(41.3957f / d, 46.7496f / d, 0.0f);
	glVertex3f(41.3957f / d, 40.4618f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.804, 0.353, 0.631);
	glVertex3f(24.6371f / d, 51.0419f / d, 0.0f);
	glVertex3f(34.8883f / d, 48.7931f / d, 0.0f);
	glVertex3f(44.7915f / d, 53.5875f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.698, 0.188, 0.569);
	glVertex3f(24.6371f / d, 51.0419f / d, 0.0f);
	glVertex3f(33.788f / d, 52.2514f / d, 0.0f);
	glVertex3f(40.0757f / d, 56.024f / d, 0.0f);
	glVertex3f(29.9368f / d, 55.9454f / d, 0.0f);
	glVertex3f(15.7725f / d, 56.3788f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.808, 0.361, 0.655);
	glVertex3f(15.7725f / d, 56.3788f / d, 0.0f);
	glVertex3f(8.44567f / d, 59.1829f / d, 0.0f);
	glVertex3f(25.2996f / d, 56.7314f / d, 0.0f);
	glVertex3f(29.9368f / d, 55.9454f / d, 0.0f);	
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.686, 0.192, 0.553);
	glVertex3f(8.44567f / d, 59.1829f / d, 0.0f);
	glVertex3f(25.2996f / d, 56.7314f / d, 0.0f);
	glVertex3f(31.1157f / d, 58.6963f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.792, 0.349, 0.639);
	glVertex3f(8.44567f / d, 59.1829f / d, 0.0f);
	glVertex3f(17.7543f / d, 60.897f / d, 0.0f);
	glVertex3f(21.5269f / d, 59.4822f / d, 0.0f);
	glVertex3f(22.7059f / d, 58.9321f / d, 0.0f);
	glEnd();


	glBegin(GL_POLYGON);
	glColor3f(0.69, 0.196, 0.569);
	glVertex3f(17.7543f / d, 60.897f / d, 0.0f);
	glVertex3f(21.5269f / d, 59.4822f / d, 0.0f);
	glVertex3f(24.9066f / d, 63.6479f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.678, 0.192, 0.545);
	glVertex3f(8.44567f / d, 59.1829f / d, 0.0f);
	glVertex3f(17.7543f / d, 60.897f / d, 0.0f);
	glVertex3f(7.28523f / d, 61.6515f / d, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.808, 0.353, 0.624);
	glVertex3f(13.2271f / d, 60.9913f / d, 0.0f);
	glVertex3f(19.169f / d, 63.6322f / d, 0.0f);
	glVertex3f(11.5294f / d, 63.7265f / d, 0.0f);
	glVertex3f(7.28523f / d, 61.6515f / d, 0.0f);
	glEnd();

	SwapBuffers(ghdc);
}

void update(void) //ithe update cha code tevach yeto jeva animation karaych asta
{
	// Code

}

void uninitialize(void)
{
	//Function declarations
	void ToggleFullscreen(void);

	// Code
	// If application is exitting in fullscreen
	if (gbFullscreen == TRUE)
	{
		ToggleFullscreen();
		gbFullscreen = FALSE;
	}

	//Make the HDC as current DC
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	// Destroy or delete Rendering context
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	// Release the HDC
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
	}

	// destroy window
	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	// Close the log file
	if (gpFile)
	{
		fprintf(gpFile, "Program Ended Successfully!\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

