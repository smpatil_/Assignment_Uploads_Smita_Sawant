#include <stdio.h>

int main(void)
{
	//declaring variables
	int SPS_i, SPS_j;
	char ch_1, ch_2;

	int SPS_a, result_int;
	float SPS_f, result_float;

	int SPS_i_explicit;
	float SPS_f_explicit;

	//code
	printf("\n\n");

	//Interconversion and implicit type conversion between the char and int types.
	SPS_i = 70;
	ch_1 = SPS_i;
	printf("I = %d\n", SPS_i);
	printf("Character 1 (after ch_1 = SPS_i) = %c\n\n", ch_1);

	ch_2 = 'Q';
	SPS_j = ch_2;
	printf("Character 2 = %c\n", ch_2);
	printf("J (after SPS_j = ch_2) = %d\n\n", SPS_j);

	//Implicit conversion of int to float type
	SPS_a = 5;
	SPS_f = 7.8f;
	result_float = SPS_a + SPS_f;
	printf("Interger a = %d And Floating-Point Number %f Added Gives Floating-Point Sum = %f\n\n", SPS_a, SPS_f, result_float);

	result_int = SPS_a + SPS_f;
	printf("Integer a = %d And Floating-Point Number %f Added Gives Integer Sum = %d\n\n", SPS_a, SPS_f, result_int);

	//Explicit type conversion using cast operator
	SPS_f_explicit = 30.121995f;
	SPS_i_explicit = (int)SPS_f_explicit;
	printf("Floating Point Number Which Will Be Type Casted Explicitly = %f\n", SPS_f_explicit, SPS_i_explicit);
	printf("Resultant Integer After Explicit Type Casting Of %f = %d\n\n", SPS_f_explicit, SPS_i_explicit);

	return(0);
}


