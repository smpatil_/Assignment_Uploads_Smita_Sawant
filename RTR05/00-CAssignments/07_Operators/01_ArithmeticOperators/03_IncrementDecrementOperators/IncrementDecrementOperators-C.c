#include <stdio.h>

int main(void)
{
	//variable declarations
	int sps_a = 5;
	int sps_b = 10;

	//code
	printf("\n\n");
	printf("SPS_A = %d\n", sps_a);
	printf("SPS_A = %d\n", sps_a++); //increament
	printf("SPS_A = %d\n", sps_a);
	printf("SPS_A = %d\n\n", ++sps_a);

	printf("SPS_B = %d\n", sps_b);
	printf("SPS_B = %d\n", sps_b--); //decreament
	printf("SPS_B = %d\n", sps_b);
	printf("SPS_B = %d\n\n", --sps_b);

	return(0);
}


