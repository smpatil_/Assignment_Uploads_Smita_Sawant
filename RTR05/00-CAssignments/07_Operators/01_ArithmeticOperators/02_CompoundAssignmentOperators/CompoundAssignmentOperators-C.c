#include <stdio.h>

int main(void)
{
	//variable declarations
	int sps_a;
	int sps_b;
	int sps_x;

	//code
	printf("\n\n");
	printf("Enter A Number : ");
	scanf("%d", &sps_a);

	printf("\n\n");
	printf("Enter Another Number : ");
	scanf("%d", &sps_b);

	printf("\n\n");

	//In all the following five scenarios, the operand on the LHS 'sps_a' is getting repeated immediately on the RHS (For example; sps_a = sps_a + sps_b or sps_a = sps_a - sps_b)
	//Compound Assignment Operators used : +=, -=, *=, /= and %=

	//Since 'sps_a' will get assigned the value of (sps_a + sps_b) in (sps_a += sps_b), we must save the original value of sps_a to another variable 'sps_x'
	sps_x = sps_a;
	sps_a += sps_b; //this means sps_a = sps_a + sps_b
	printf("Addition Of SPS_A = %d And SPS_B = %d Gives %d.\n", sps_x, sps_b, sps_a);

	// The changed value of sps_a is used here
	//As 'sps_a' will be assigned the value of (sps_a - sps_b) in (sps_a -= sps_b), we must save the original value of sps_a to another variable 'sps_x'
	sps_x = sps_a;
	sps_a -= sps_b; //this means sps_a = sps_a - sps_b
	printf("Subraction Of SPS_A = %d And SPS_B = %d Gives %d.\n", sps_x, sps_b, sps_a);

	// The changed value of sps_a is used here
	//As 'sps_a' will be assigned the value of (sps_a * sps_b) in (sps_a *= sps_b), we must save the original value of sps_a to another variable 'sps_x'
	sps_x = sps_a;
	sps_a *= sps_b; //this means sps_a = sps_a * sps_b
	printf("Multiplication Of SPS_A = %d And SPS_B = %d Gives %d.\n", sps_x, sps_b, sps_a);

	// The changed value of sps_a is used here
	//As 'sps_a' will be assigned the value of (sps_a / sps_b) in (sps_a /= sps_b), we must save the original value of sps_a to another variable 'sps_x'
	sps_x = sps_a;
	sps_a /= sps_b; //this means sps_a = sps_a / sps_b
	printf("Division Of SPS_A = %d And SPS_B = %d Gives Quotient %d.\n", sps_x, sps_b, sps_a);

	// The changed value of sps_a is used here
	//As 'sps_a' will be assigned the value of (sps_a % sps_b) in (sps_a %= sps_b), we must save the original value of sps_a to another variable 'sps_x'
	sps_x = sps_a;
	sps_a %= sps_b; //this means sps_a = sps_a % sps_b
	printf("Division Of SPS_A = %d And SPS_B = %d Gives Remainder %d.\n", sps_x, sps_b, sps_a);

	printf("\n\n");

	return(0);
}


