#include <stdio.h>

int main(void)
{
	//variable declarations
	int sps_a;
	int sps_b;
	int sps_result;

	//code
	printf("\n\n");
	printf("Enter A Number : ");
	scanf("%d", &sps_a);

	printf("\n\n");
	printf("Enter Another Number : ");
	scanf("%d", &sps_b);

	printf("\n\n");

	// +, -, *, /, and % are the arithmetic operators.
	// The result of the arithmetic operations in all the following five scenarios have been assigned to the variable 'sps_result' using the Assignment Operator (=) 
	sps_result = sps_a + sps_b;
	printf("Addition Of A = %d And B = %d Gives %d.\n", sps_a, sps_b, sps_result);

	sps_result = sps_a - sps_b;
	printf("Subtraction Of A = %d And B = %d Gives %d.\n", sps_a, sps_b, sps_result);

	sps_result = sps_a * sps_b;
	printf("Multiplication Of A = %d And B = %d Gives %d.\n", sps_a, sps_b, sps_result);

	sps_result = sps_a / sps_b;
	printf("Division Of A = %d And B = %d Gives Quotient %d.\n", sps_a, sps_b, sps_result);

	sps_result = sps_a % sps_b;
	printf("Division Of A = %d And B = %d Gives Remainder %d.\n", sps_a, sps_b, sps_result);

	printf("\n\n");

	return(0);
}



