#include <stdio.h>

int main(void)
{
	//Variable Declarations
	int SPS_a, SPS_b;
	int SPS_p, SPS_q;
	char ch_result_01, ch_result_02;
	int i_result_01, i_result_02;

	//code
	printf("\n\n");

	SPS_a = 7;
	SPS_b = 5;
	ch_result_01 = (SPS_a > SPS_b) ? 'A' : 'B';
	i_result_01 = (SPS_a > SPS_b) ? SPS_a : SPS_b;
	printf("Ternary Operator Answer 1 ----- %c and %d.\n\n", ch_result_01, i_result_01);

	SPS_p = 30;
	SPS_q = 30;
	ch_result_02 = (SPS_p != SPS_q) ? 'P' : 'Q';
	i_result_02 = (SPS_p != SPS_q) ? SPS_p : SPS_q;
	printf("Ternary Operator Answer 2 ----- %c and %d.\n\n", ch_result_02, i_result_02);

	printf("\n\n");
	return(0);
}

