#include <stdio.h>

int main(void)
{
	//variable declarations
	int sps_a;
	int sps_b;
	int sps_result;

	//code
	printf("\n\n");
	printf("Enter One Integer : ");
	scanf("%d", &sps_a);

	printf("\n\n");
	printf("Enter Another Integer : ");
	scanf("%d", &sps_b);

	printf("\n\n");
	printf("If Answer = 0, It Is 'FALSE'.\n");
	printf("If Answer = 1, It Is 'TRUE'.\n\n");

	sps_result = (sps_a < sps_b);
	printf("(sps_a < sps_b) SPS_A = %d Is Less Than SPS_B = %d					\t Answer = %d\n", sps_a, sps_b, sps_result);

	sps_result = (sps_a > sps_b);
	printf("(sps_a > sps_b) SPS_A = %d Is Greater Than SPS_B = %d					\t Answer = %d\n", sps_a, sps_b, sps_result);

	sps_result = (sps_a <= sps_b);
	printf("(sps_a <= sps_b) SPS_A = %d Is Less Than Or Equal To SPS_B = %d				\t Answer = %d\n", sps_a, sps_b, sps_result);

	sps_result = (sps_a >= sps_b);
	printf("(sps_a >= sps_b) SPS_A = %d Is Greate Than Or Equal To SPS_B = %d				\t Answer = %d\n", sps_a, sps_b, sps_result);

	sps_result = (sps_a == sps_b);
	printf("(sps_a == sps_b) SPS_A = %d Is Equal To SPS_B = %d					\t Answer = %d\n", sps_a, sps_b, sps_result);

	sps_result = (sps_a != sps_b);
	printf("(sps_a != sps_b) SPS_A = %d Is Not Equal To SPS_B = %d					\t Answer = %d\n", sps_a, sps_b, sps_result);

	return(0);
}


