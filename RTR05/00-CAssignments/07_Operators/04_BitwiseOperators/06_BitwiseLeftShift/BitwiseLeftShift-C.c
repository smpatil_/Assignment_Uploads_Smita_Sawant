#include <stdio.h>


int main(void)
{
	//function prototypes
	void PrintBinaryFormOfNumber(unsigned int);

	//Variable declaration
	unsigned int sps_a;
	unsigned int sps_num_bits;
	unsigned int sps_result;

	//Code for bitwise left shift
	printf("\n\n");
	printf("Enter An Integer = ");
	scanf("%u", &sps_a);

	printf("\n\n");
	printf("By How Many Bits Do You Want to Shift A = %d To The Left? ", sps_a);
	scanf("%u", &sps_num_bits);

	printf("\n\n\n\n");
	sps_result = sps_a << sps_num_bits;
	printf("Bitwise LEFT-SHIFT By %d Bits of A = %d \nGives The Result = %d (Decimal). \n\n", sps_num_bits, sps_a, sps_result);
	PrintBinaryFormOfNumber(sps_a);
	PrintBinaryFormOfNumber(sps_result);

	return(0);
}


// To obtain binary representation of decimal integers

void PrintBinaryFormOfNumber(unsigned int decimal_number)
{
	//variable declaration
	unsigned int ss_quotient, ss_remainder;
	unsigned int ss_num;
	unsigned int binary_array[8];
	int SPS_i;

	//code
	for (SPS_i = 0; SPS_i < 8; SPS_i++)
		binary_array[SPS_i] = 0;

	printf("The Binary Form of The Decimal Interger %d Is\t=\t", decimal_number);
	ss_num = decimal_number;
	SPS_i = 7;
	while (ss_num != 0)
	{
		ss_quotient = ss_num / 2;
		ss_remainder = ss_num % 2;
		binary_array[SPS_i] = ss_remainder;
		ss_num = ss_quotient;
		SPS_i--;
	}

	for (SPS_i = 0; SPS_i < 8; SPS_i++)
		printf("%u", binary_array[SPS_i]);

	printf("\n\n");
}


