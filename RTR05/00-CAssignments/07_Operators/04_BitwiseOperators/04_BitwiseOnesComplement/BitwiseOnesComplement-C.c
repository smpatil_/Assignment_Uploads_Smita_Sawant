#include <stdio.h>


int main(void)
{
	//function prototypes
	void PrintBinaryFormOfNumber(unsigned int);

	//declaring variables
	unsigned int smita_a;
	unsigned int smita_result;

	//code
	printf("\n\n");
	printf("Enter An Integer = ");
	scanf("%u", &smita_a);


	printf("\n\n\n\n");
	smita_result = ~smita_a;
	printf("Bitwise COMPLEMENTING Of \n\nA = %d (Decimal) gives result %d (Decimal). \n\n", smita_a, smita_result);

	PrintBinaryFormOfNumber(smita_a);
	PrintBinaryFormOfNumber(smita_result);

	return(0);
}


// The following part of code is understandeable after learning about Arrays, Loops, and Functions!

void PrintBinaryFormOfNumber(unsigned int decimal_number)
{
	//declaring variables
	unsigned int sps_quotient, sps_remainder;
	unsigned int sps_num;
	unsigned int binary_array[8];
	int smita_i;

	//code
	for (smita_i = 0; smita_i < 8; smita_i++)
		binary_array[smita_i] = 0;

	printf("The Binary Form of The Decimal Interger %d Is\t=\t", decimal_number);
	sps_num = decimal_number;
	smita_i = 7;
	while (sps_num != 0)
	{
		sps_quotient = sps_num / 2;
		sps_remainder = sps_num % 2;
		binary_array[smita_i] = sps_remainder;
		sps_num = sps_quotient;
		smita_i--;
	}

	for (smita_i = 0; smita_i < 8; smita_i++)
		printf("%u", binary_array[smita_i]);

	printf("\n\n");
}

