#include <stdio.h>


int main(void)
{
	//function prototypes
	void PrintBinaryFormOfNumber(unsigned int);

	//declaring variables
	unsigned int SPS_a;
	unsigned int SPS_num_bits;
	unsigned int SPS_result;

	//Code : bitwise right shift
	printf("\n\n");
	printf("Enter An Integer = ");
	scanf("%u", &SPS_a);

	printf("\n\n");
	printf("By How Many Bits Do You Want to Shift A = %d To The Right? ", SPS_a);
	scanf("%u", &SPS_num_bits);

	printf("\n\n\n\n");
	SPS_result = SPS_a >> SPS_num_bits;
	printf("Bitwise RIGHT-SHIFTing A = %d By %d Bits \nGives The Result = %d (Decimal). \n\n", SPS_a, SPS_num_bits, SPS_result);
	PrintBinaryFormOfNumber(SPS_a);
	PrintBinaryFormOfNumber(SPS_result);

	return(0);
}


// 2nd part of this code

void PrintBinaryFormOfNumber(unsigned int decimal_number)
{
	//Variable declarations
	unsigned int ss_quotient, ss_remainder;
	unsigned int ss_num;
	unsigned int binary_array[8];
	int SMITA_i;

	//code
	for (SMITA_i = 0; SMITA_i < 8; SMITA_i++)
		binary_array[SMITA_i] = 0;

	printf("The Binary Form of The Decimal Interger %d Is\t=\t", decimal_number);
	ss_num = decimal_number;
	SMITA_i = 7;
	while (ss_num != 0)
	{
		ss_quotient = ss_num / 2;
		ss_remainder = ss_num % 2;
		binary_array[SMITA_i] = ss_remainder;
		ss_num = ss_quotient;
		SMITA_i--;
	}

	for (SMITA_i = 0; SMITA_i < 8; SMITA_i++)
		printf("%u", binary_array[SMITA_i]);

	printf("\n\n");
}
