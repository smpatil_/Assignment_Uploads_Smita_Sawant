#include <stdio.h>

int main(void)
{
	//Function prototype - This one is used as a reference. Ma'am will teach later.
	void PrintBinaryFormOfNumber(unsigned int);

	//Variable Declarations
	unsigned int smita_a;
	unsigned int smita_b;
	unsigned int sawant_result;

	//Code
	printf("\n\n");
	printf("Enter An Integer = ");
	scanf("%u", &smita_a);

	printf("\n\n");
	printf("Enter Another Integer = ");
	scanf("%u", &smita_b);

	printf("\n\n\n\n");
	sawant_result = smita_a & smita_b;
	printf("Bitwise AND-ing Of \nA = %d (Decimal) and B = %d (Decimal) gives result %d (Decimal). \n\n", smita_a, smita_b, sawant_result);

	PrintBinaryFormOfNumber(smita_a);
	PrintBinaryFormOfNumber(smita_b);
	PrintBinaryFormOfNumber(sawant_result);

	return(0);
}


// 25th May 2023- I need to come back to see the following function snippet once I learn about Arrays, Loops, and Functions. 

void PrintBinaryFormOfNumber(unsigned int decimal_number)
{
	//Variable Declarations
	unsigned int quotient, remainder;
	unsigned int num;
	unsigned int binary_array[8];
	int i;

	//code
	for (i = 0; i < 8; i++)
		binary_array[i] = 0; //next time you come back to this part, check why this indent happen.

	printf("The Binary Form of The Decimal Interger %d Is\t=\t", decimal_number);
	num = decimal_number;
	i = 7;
	while (num != 0)
	{
		quotient = num / 2;
		remainder = num % 2;
		binary_array[i] = remainder;
		num = quotient;
		i--;
	}

	for (i = 0; i < 8; i++)
		printf("%u", binary_array[i]);

	printf("\n\n");
}

