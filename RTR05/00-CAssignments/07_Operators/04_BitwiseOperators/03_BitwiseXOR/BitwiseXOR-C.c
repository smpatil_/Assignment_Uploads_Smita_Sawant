#include <stdio.h>


int main(void)
{
	//function prototypes
	void PrintBinaryFormOfNumber(unsigned int);

	//Variable Declaration
	unsigned int ss_a;
	unsigned int ss_b;
	unsigned int ss_result;

	//Code
	printf("\n\n");
	printf("Enter An Integer = ");
	scanf("%u", &ss_a);

	printf("\n\n");
	printf("Enter Another Integer = ");
	scanf("%u", &ss_b);

	printf("\n\n\n\n");
	ss_result = ss_a ^ ss_b;
	printf("Bitwise XOR-ing Of \nA = %d (Decimal) and B = %d (Decimal) gives result %d (Decimal). \n\n", ss_a, ss_b, ss_result);

	PrintBinaryFormOfNumber(ss_a);
	PrintBinaryFormOfNumber(ss_b);
	PrintBinaryFormOfNumber(ss_result);

	return(0);
}


// After arrays, functions, and loops 

void PrintBinaryFormOfNumber(unsigned int decimal_number)
{
	//Variable declarations
	unsigned int quotient, remainder;
	unsigned int num;
	unsigned int binary_array[8];
	int i;

	//code
	for (i = 0; i < 8; i++)
		binary_array[i] = 0;

	printf("The Binary Form of The Decimal Interger %d Is\t=\t", decimal_number);
	num = decimal_number;
	i = 7;
	while (num != 0)
	{
		quotient = num / 2;
		remainder = num % 2;
		binary_array[i] = remainder;
		num = quotient;
		i--;
	}

	for (i = 0; i < 8; i++)
		printf("%u", binary_array[i]);

	printf("\n\n");
}
