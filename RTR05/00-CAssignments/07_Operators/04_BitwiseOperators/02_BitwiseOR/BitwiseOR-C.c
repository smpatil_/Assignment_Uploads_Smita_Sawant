#include <stdio.h>

int main(void)
{
	//Function prototypes: For reference
	void PrintBinaryFormOfNumber(unsigned int);

	//Variable Declarations
	unsigned int sps_a;
	unsigned int sps_b;
	unsigned int sps_result;

	//code
	printf("\n\n");
	printf("Enter An Integer = ");
	scanf("%u", &sps_a);

	printf("\n\n");
	printf("Enter Another Integer = ");
	scanf("%u", &sps_b);

	printf("\n\n\n\n");
	sps_result = sps_a | sps_b;
	printf("Bitwise OR-ing Of \nA = %d (Decimal) and B = %d (Decimal) gives result %d (Decimal). \n\n", sps_a, sps_b, sps_result);

	PrintBinaryFormOfNumber(sps_a);
	PrintBinaryFormOfNumber(sps_b);
	PrintBinaryFormOfNumber(sps_result);

	return(0);
}


// Coming! Coming!!  

void PrintBinaryFormOfNumber(unsigned int decimal_number)
{
	//Variable Declarations
	unsigned int quotient, remainder;
	unsigned int num;
	unsigned int binary_array[8];
	int i;

	//code
	for (i = 0; i < 8; i++)
		binary_array[i] = 0;

	printf("The Binary Form of The Decimal Interger %d Is\t=\t", decimal_number);
	num = decimal_number;
	i = 7;
	while (num != 0)
	{
		quotient = num / 2;
		remainder = num % 2;
		binary_array[i] = remainder;
		num = quotient;
		i--;
	}

	for (i = 0; i < 8; i++)
		printf("%u", binary_array[i]);

	printf("\n\n");
}
