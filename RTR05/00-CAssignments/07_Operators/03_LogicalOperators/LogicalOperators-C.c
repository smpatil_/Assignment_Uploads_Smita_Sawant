#include <stdio.h>

int main(void)
{
	//variable declarations
	int sps_a;
	int sps_b;
	int sps_c;
	int sps_result;

	//code
	printf("\n\n");
	printf("Enter First Integer : ");
	scanf("%d", &sps_a);

	printf("\n\n");
	printf("Enter Second Integer : ");
	scanf("%d", &sps_b);

	printf("\n\n");
	printf("Enter Third Integer : ");
	scanf("%d", &sps_c);

	printf("\n\n");
	printf("If Answer = 0, It Is 'FALSE'.\n");
	printf("If Answer = 1, It Is 'TRUE'.\n\n");

	sps_result = (sps_a <= sps_b) && (sps_b != sps_c);
	printf("LOGICAL AND (&&) : Answer is TRUE (1) If And Only If BOTH Conditions Are True. The Answer is FALSE (0), If Any One Or Both Conditions Are False. \n\n");
	printf("SPS_A = %d Is Less Than Or Equal To SPS_B = %d AND SPS_B = %d Is NOT Equal To SPS_C = %d			\t Answer = %d\n\n", sps_a, sps_b, sps_b, sps_c, sps_result);

	sps_result = (sps_b >= sps_a) || (sps_a == sps_c);
	printf("LOGICAL AND (||) : Answer is FALSE (0) If And Only If BOTH Conditions Are False. The Answer is TRUE (1), If Any One Or Both Conditions Are True. \n\n");
	printf("Either SPS_B = %d Is Greater Than Or Equal To SPS_A = %d OR SPS_A = %d Is Equal To SPS_C = %d			\t Answer = %d\n\n", sps_b, sps_a, sps_a, sps_c, sps_result);

	sps_result = !sps_a;
	printf("SPS_A = %d And Using Logical NOT (!) Operator on SPS_A Gives Result = %d\n\n", sps_a, sps_result);

	sps_result = !sps_b;
	printf("SPS_B = %d And Using Logical NOT (!) Operator on SPS_B Gives Result = %d\n\n", sps_b, sps_result);

	sps_result = !sps_c;
	printf("SPS_C = %d And Using Logical NOT (!) Operator on SPS_C Gives Result = %d\n\n", sps_c, sps_result);

	sps_result = (!(sps_a <= sps_b) && !(sps_b != sps_c));
	printf("Using Logical NOT (!) On (sps_a <= sps_b) And Also On (sps_b != sps_c) And then AND-ing Them Afterwards Gives Result = %d\n", sps_result);

	printf("\n\n");

	sps_result = !((sps_b >= sps_a) || (sps_a == sps_c));
	printf("Using Logical NOT (!) On Entire Logical Expression ((sps_b >= sps_a) || (sps_a == sps_c)) Gives Result = %d\n", sps_result);

	printf("\n\n");

	return(0);
}




