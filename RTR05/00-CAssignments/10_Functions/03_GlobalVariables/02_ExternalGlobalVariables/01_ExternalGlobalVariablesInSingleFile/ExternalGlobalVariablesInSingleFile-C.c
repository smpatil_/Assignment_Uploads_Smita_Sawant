#include <stdio.h>

//*** Global Scope ***

int main(void)
{
	//function prototypes
	void change_count(void);

	//variable declarations
	extern int sps_global_count;

	//code
	printf("\n");
	printf("Value of sps_global_count before change_count() = %d\n", sps_global_count);
	change_count();
	printf("Value of sps_global_count after change_count() = %d\n", sps_global_count);
	printf("\n");
	return(0);
}

//*** Global Scope ***
// sps_global_count is a global variable.
//Since, it is declared before change_count(), it can be accessed and used as any ordinary global variable in change_count()
//Since, it is declared after main(), it must be firest re-declared in main() as an external global variable by means of the 'extern' keyword and the type of the variable.
//once this is done, it can be used as an ordinary global variable in main as well.
int sps_global_count = 0;

void change_count(void)
{
	//code
	sps_global_count = 5;
	printf("Value Of sps_global_count in change_count() = %d\n", sps_global_count);
}
