#include <stdio.h> //'stdio.h contains declaration of 'printf()'
#include <ctype.h> //'ctype.h contains declaration of 'atoi()'
#include <stdlib.h> //'stdlib.h' contains declaration of 'exit()'

int main(int argc, char* argv[], char* envp[])
{
	//variable declarations
	int sps_i;
	int sps_num;
	int sps_sum = 0;

	//code
	if (argc == 1)
	{
		printf("\n\n");
		printf("No Numbers Given For Addition !!! Exitting now...\n\n");
		printf("Usage : CommandLineArgumentsApplication <first number> <second number>...\n\n");
		exit(0);
	}

	//This programs add all command line arguments given in integer form only and outputs the sum.
	//due to use of atoi(), all command line arguments of types other than 'int' are ignored.
	printf("\n\n");
	printf("Sum of All Integer Command Line Arguments Is : \n\n");
	for (sps_i = 1; sps_i < argc; sps_i++) //loop starts from sps_i = 1 because, sps_i = 0 will result in 'argv[i]' = 'argv[0]' which is the name of the program itself i.e. : 'CommandLineArgumentsApplication.exe'
	{
		sps_num = atoi(argv[sps_i]);
		sps_sum = sps_sum + sps_num;
	}

	printf("Sum = %d\n\n", sps_sum);

	return(0);
}

