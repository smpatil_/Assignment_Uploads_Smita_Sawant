#include <stdio.h> //'stdio.h contains declaration of 'printf()'
#include <stdlib.h> //'stdlib.h' contains declaration of 'exit()'

int main(int argc, char *argv[], char *envp[])
{
	//variable declarations
	int sps_i;

	//code
	if (argc != 4) //Program name + first name + middle name + surname = 4 command line arguments are required
	{
		printf("\n\n");
		printf("Invalid Usage !!! Exitting now...\n\n");
		printf("Usage : CommandLineArgumentsApplication <first name> <middle name> <surname>\n\n");
		exit(0);
	}

	//This programs prints your full name as entered in the command line arguments.
	printf("\n\n");
	printf("Your Full Name Is : \n\n");
	for (sps_i = 1; sps_i < argc; sps_i++) //loop starts from sps_i = 1 because, sps_i = 0 will result in 'argv[i]' = 'argv[0]' which is the name of the program itself i.e. : 'CommandLineArgumentsApplication.exe'.
	{
		printf("%s ", argv[sps_i]);
	}

	printf("\n\n");

	return(0);
}
