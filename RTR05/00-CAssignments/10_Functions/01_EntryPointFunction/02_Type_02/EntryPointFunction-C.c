#include <stdio.h> //'stdio.h' includes the declaration of 'printf()' function

//Entry-point function => main() => Valid Return Type (int) and No Parameters (void). Hence no command-line arguments
int main(void)
{
	//code
	printf("Hello World !!!\n"); //Library function printf()
	return(0);
}
