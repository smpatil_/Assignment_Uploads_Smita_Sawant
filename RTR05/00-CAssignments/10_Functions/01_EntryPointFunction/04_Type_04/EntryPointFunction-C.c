#include <stdio.h> //'stdio.h' contains declaration of 'printf()'

//Entry-point Function => main() => Valid Return Type (int) and 2 Parameters (int argc, char *argv[])
int main(int argc, char* argv[])
{
	//variable declarations
	int sps_i;

	//code
	printf("\n\n");
	printf("Hello World !!!\n\n"); //Library function
	printf("Number Of Command Line Arguments = %d\n\n", argc);

	printf("Command Line Arguments Passed To This Program Are : \n\n");
	for (sps_i = 0; sps_i < argc; sps_i++)
	{
		printf("Command Line Argument Number %d = %s\n", (sps_i + 1), argv[sps_i]);
	}
	printf("\n\n");
	return(0);
}

