#include <stdio.h>

#define MAX_NUMBER(a, b) ((a > b) ? a : b)

int main(int argc, char* argv[], char* envp[])
{
	//variable declarations
	int sps_iNum_01;
	int sps_iNum_02;
	int sps_iResult;

	float sps_fNum_01;
	float sps_fNum_02;
	float sps_fResult;

	//code
	//***** Comparing Integer Values *****
	printf("\n\n");
	printf("Enter An Integer Number : \n\n");
	scanf("%d", &sps_iNum_01);

	printf("\n\n");
	printf("Enter Another Integer Number : \n\n");
	scanf("%d", &sps_iNum_02);

	sps_iResult = MAX_NUMBER(sps_iNum_01, sps_iNum_02);
	printf("\n\n");
	printf("Result Of Macro Function MAX_NUMBER() : %d\n", sps_iResult);

	printf("\n\n");

	// ***** Comapring Floating Point Values *****
	printf("\n\n");
	printf("Enter A Floating Number : \n\n");
	scanf("%f", &sps_fNum_01);

	printf("\n\n");
	printf("Enter Another Floating Point Number : \n\n");
	scanf("%f", &sps_fNum_02);

	sps_fResult = MAX_NUMBER(sps_fNum_01, sps_fNum_02);
	printf("\n\n");
	printf("Result Of Macro Function MAX_NUMBER() : %f\n", sps_fResult);

	printf("\n\n");
	
	return(0);
}

