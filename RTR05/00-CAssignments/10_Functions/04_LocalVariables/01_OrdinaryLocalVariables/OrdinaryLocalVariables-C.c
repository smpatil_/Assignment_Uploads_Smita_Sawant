#include <stdio.h>

//*** Global Scope ***

int main(void)
{
	// *** Local Scope of main() begins ***

	//variable declarations
	//'a' is a Local Variable. It is local to main() only.
	int sps_a = 5;

	//function prototypes
	void change_count(void);

	//code
	printf("\n");
	printf("A = %d\n\n", sps_a);

	//local_count is initialized to 0.
	//local_count = local_count + 1 = 0 + 1 = 1

	change_count();

	//Since, 'local_count' is an ordinary local variable of change_count(), it will not retain its value from previous call to change_count().
	//So local_count is again Initialized to 0
	//local_count = local_count + 1 = 0 + 1 = 1

	change_count();

	//Since, 'local_count' is an ordinary local variable of change_count(), it will not retain its value from previous call to change_count().
	//So local_count is again initialized to 0
	//local_count = local_count + 1 = 0 + 1 = 1

	change_count();

	return(0);

	//*** Local Scope of main() ends ***
}

//*** Global Scope ***

void change_count(void)
{
	//*** Local Scope of change_count() begins ***

	//variable declarations
	//'local_count' is a local variable. It is local to change_count() only.

	int local_count = 0;

	//code
	local_count = local_count + 1;

	printf("Local Count = %d\n", local_count);

	//*** Local Scope of chnage_count() ends ***
}

// *** Global Scope ***

