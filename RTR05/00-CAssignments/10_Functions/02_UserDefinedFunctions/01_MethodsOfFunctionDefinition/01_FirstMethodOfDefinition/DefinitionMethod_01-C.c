#include <stdio.h> //'stdio.h' contains declaration of 'printf()'

//Entry-point Function => main() => Valid Return Type (int) and 3 parameters (int argc, char *argv[], char *envp[])
int main(int argc, char *argv[], char *envp[])
{
	//function prototype/declaration/ signature
	void MyAddition(void);

	//code
	MyAddition(); //function call
	return(0);
}

//User defined function : Method of Definition 1
//No Return Value, No Parameters

void MyAddition(void) //function definition
{
	//variable declarations : local variables to MyAddition()
	int sps_a, sps_b, sps_sum;

	//code
	printf("\n\n");
	printf("Enter Integer Value For 'A' : ");
	scanf("%d", &sps_a);

	printf("\n\n");
	printf("Enter Integer Value For 'B' : ");
	scanf("%d", &sps_b);

	sps_sum = sps_a + sps_b;

	printf("\n\n");
	printf("Sum Of %d And %d = %d\n\n", sps_a, sps_b, sps_sum);
}

