#include <stdio.h> //'stdio.h' contains declaration of 'printf()'

//Entry-point function => main() => Valid return Type (int) and 3 parameters (int argc, char *argv[], char *envp[])
int main(int argc, char* argv[], char* envp[])
{
	//function prototype/ declaration/ signature
	int MyAddition(void);

	//variable declarations : local variables to main()
	int ss_result;

	//code
	ss_result = MyAddition(); //function call

	printf("\n\n");
	printf("Sum = %d\n\n", ss_result);
	return(0);
}

//*****User Defined Function : Method of Definition 2*****
//***** Valid (int) Return Value, No Parameters ******

int MyAddition(void) //function definition
{
	//variable declarations : local variables to MyAddition()
	int sps_a, sps_b, sps_sum;

	//code
	printf("\n\n");
	printf("Enter Integer Value for 'A' : ");
	scanf("%d", &sps_a);

	printf("\n\n");
	printf("Enter Integer Value for 'B' : ");
	scanf("%d", &sps_b);

	sps_sum = sps_a + sps_b;

	return(sps_sum);
}
