#include <stdio.h> //'stdio.h' contains declaration of 'printf()'

//Entry-point function => main() => Valid return type (int) and 3 parameters (int argc, char *argv[], char *envp[])
int main(int argc, char* argv[], char* envp[])
{
	//function prototype/declaration/signature
	int MyAddition(int, int);

	//variable declarations : local variables to main()
	int sps_a, sps_b, sps_result;

	//code
	printf("\n\n");
	printf("Enter Integer Value for 'A' : ");
	scanf("%d", &sps_a);

	printf("\n\n");
	printf("Enter Integer Value for 'B' : ");
	scanf("%d", &sps_b);

	sps_result = MyAddition(sps_a, sps_b); //function call
	printf("\n\n");
	printf("Sum Of %d And %d = %d\n\n", sps_a, sps_b, sps_result);

	return(0);
}

//***** User Defined Function : Method of Definition 4 
//***** Valid (int) return value, valid parameters (int, int)

int MyAddition(int sps_a, int sps_b) //function definition
{
	//variable declarations : local variables to MyAddition()
	int sps_sum;

	//code
	sps_sum = sps_a + sps_b;
	return(sps_sum);
}
