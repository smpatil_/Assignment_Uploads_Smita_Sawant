#include <stdio.h> //'stdio.h' contains declaration of 'printf()'

// ***** User Defined Functions : Methods of Calling Function 2 *****
// ***** Calling Only Two Functions Directly In main(), Rest Of the Functions Trace Their Call Indirectly To main() *****

int main(int argc, char* argv[], char* envp[])
{
	//function prototypes
	void display_information(void);
	void Function_Country(void);

	//code
	display_information(); //function call
	Function_Country(); //function call

	return(0);
}

//***** User-Defined Functions' Definitions...*****
void display_information(void) //function definition
{
	//function prototypes
	void Function_My(void);
	void Function_Name(void);
	void Function_Is(void);
	void Function_FirstName(void);
	void Function_MiddleName(void);
	void Function_Surname(void);
	void Function_OfAMC(void);

	//code

	//*** Function Calls ***
	Function_My();
	Function_Name();
	Function_Is();
	Function_FirstName();
	Function_MiddleName();
	Function_Surname();
	Function_OfAMC();
}

void Function_My(void) //function definition
{
	//code
	printf("\n\n");

	printf("My");
}

void Function_Name(void) //function definition
{
	//code
	printf("\n\n");

	printf("Name");
}

void Function_Is(void) //function definition
{
	//code
	printf("\n\n");

	printf("Is");
}

void Function_FirstName(void) //function definition
{
	printf("\n\n");

	printf("Smita");
}

void Function_MiddleName(void) //function definition
{
	printf("\n\n");

	printf("Prasad");
}

void Function_Surname(void) //function definition
{
	printf("\n\n");

	printf("Sawant");
}

void Function_OfAMC(void) //function definition
{
	printf("\n\n");

	printf("Of ASTROMEDICOMP");
}

void Function_Country(void) //function definition
{
	printf("\n\n");

	printf("I am an Indian but live in the United Kingdom for work.");

	printf("\n\n");
}

