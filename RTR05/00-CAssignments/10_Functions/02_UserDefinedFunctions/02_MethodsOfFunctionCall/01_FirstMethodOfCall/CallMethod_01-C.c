#include <stdio.h> //'stdio.h' contains declaration of 'printf()'

//**** User defined functions: method of calling function 1
//**** Calling all function in main()directly

//entry-point function => main() => Valid Return Type (int) and 3 parameters (int argc, char *argv[], char *envp[])
int main(int argc, char* argv[], char* envp[])
{
	//function prototype OR declarations
	void MyAddition(void);
	int MySubstraction(void);
	void MyMultiplication(int, int);
	int MyDivision(int, int);

	//variable declarations
	int sps_result_subtraction;
	int sps_a_multiplication, sps_b_multiplication;
	int sps_a_division, sps_b_division, sps_result_division;

	//code

	//** Addition **
	MyAddition(); //function call

	//** Subtraction **
	sps_result_subtraction = MySubtraction(); //function call
	printf("\n\n");
	printf("Subtraction Yields Result = %d\n", sps_result_subtraction);

	//*** Multiplication ***
	printf("\n\n");
	printf("Enter Integer Value For 'A' For Multiplication : ");
		scanf("%d", &sps_a_multiplication);

	printf("\n\n");
	printf("Enter Integer Value For 'B' For Multiplication : ");
	scanf("%d", &sps_b_multiplication);

	MyMultiplication(sps_a_multiplication, sps_b_multiplication); //function call

	//*** Division ***
	printf("\n\n");
	printf("Enter Integer Value For 'A' For Division : ");
	scanf("%d", &sps_a_division);

	printf("\n\n");
	printf("Enter Integer Value For 'B' For Division : ");
	scanf("%d", &sps_b_division);

	sps_result_division = MyDivision(sps_a_division, sps_b_division); //function call
	printf("\n\n");
	printf("Division Of %d and %d Gives = %d (Quotient)\n", sps_a_division, sps_b_division, sps_result_division);

	printf("\n\n");

	return(0);
}

//*** Function Definition of MyAddition() ***
void MyAddition(void) //function definition
{
	//variable declarations : local variables to MyAddition()
	int sps_a, sps_b, sps_sum;

	//code
	printf("\n\n");
	printf("Enter Integer Values For 'A' For Addition : ");
	scanf("%d", &sps_a);

	printf("\n\n");
	printf("Enter Integer Values For 'B' For Addition : ");
	scanf("%d", &sps_b);

	sps_sum = sps_a + sps_b;

	printf("\n\n");
	printf("Sum Of %d And %d = %d\n\n", sps_a, sps_b, sps_sum);
}

//*** Function Definition Of MySubtraction() ***
int MySubtraction(void) //function definition
{
	//variable declarations : local variables to MySubtraction()
	int sps_a, sps_b, sps_subtraction;

	//code
	printf("\n\n");
	printf("Enter Integer Value For 'A' For Subtraction : ");
	scanf("%d", &sps_a);

	printf("\n\n");
	printf("Enter Integer Value For 'B' For Subtraction : ");
	scanf("%d", &sps_b);

	sps_subtraction = sps_a - sps_b;
	return(sps_subtraction);
}

//*** Function Definition Of MyMultiplication() ***
void MyMultiplication(int sps_a, int sps_b) //function definition
{
	//variable declarations : local variables to MyMultiplication()
	int sps_multiplication;

	//code
	sps_multiplication = sps_a * sps_b;

	printf("\n\n");
	printf("Multiplication Of %d And %d = %d\n\n", sps_a, sps_b, sps_multiplication);
}

//*** Function Definition Of MyDivision() ***
int MyDivision(int sps_a, int sps_b) //function definition
{
	//variable declarations: local variables to MyDivision()
	int sps_division_quotient;

	//code
	if (sps_a > sps_b)
		sps_division_quotient = sps_a / sps_b;
	else
		sps_division_quotient = sps_b / sps_a;

	return(sps_division_quotient);
}

