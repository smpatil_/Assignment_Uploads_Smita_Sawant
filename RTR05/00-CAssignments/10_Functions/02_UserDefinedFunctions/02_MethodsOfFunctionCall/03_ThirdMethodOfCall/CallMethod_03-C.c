#include <stdio.h> //'stdio.h' contains declaration of 'printf()'

// ***** User Defined Functions : Methods of Calling Function 3 *****
// ***** Calling Only Two Functions Directly In main(), Rest Of the Functions Trace Their Call Indirectly To main() *****

int main(int argc, char* argv[], char* envp[])
{
	//function prototypes
	void Function_Country(void);

	//code
	Function_Country(); 
	return(0);
}

void Function_Country(void) //function definition
{
	//function declarations
	void Function_OfAMC(void);

	//code
	Function_OfAMC();
	
	printf("\n\n");

	printf("I live in Scotland.");

	printf("\n\n");
}

void Function_OfAMC(void) //function definition
{
	//function declarations
	void Function_Surname(void);

	//code
	Function_Surname();

	printf("\n\n");

	printf("Of ASTROMEDICOMP");
}

void Function_Surname(void) //function definition
{
	//function declarations
	void Function_MiddleName(void);

	//code
	Function_MiddleName();

	printf("\n\n");

	printf("Sawant");
}

void Function_MiddleName(void) //function definition
{
	//function declarations
	void Function_FirstName(void);

	//code
	Function_FirstName();

	printf("\n\n");

	printf("Prasad");
}

void Function_FirstName(void) //function definition
{
	//function declarations
	void Function_Is(void);

	//code
	Function_Is();

	printf("\n\n");

	printf("Smita");
}

void Function_Is(void) //function definition
{
	//function declarations
	void Function_Name(void);

	//code
	Function_Name();

	printf("\n\n");

	printf("Is");
}

void Function_Name(void) //function definition
{
	//function declarations
	void Function_My(void);

	//code
	Function_My();

	printf("\n\n");

	printf("Name");
}

//*** User defined Functions' Definitions... ***
void Function_My(void) //function definition
{
	//code
	printf("\n\n");

	printf("My");
}

