#include <stdio.h>
int main(void)
{
	//variable declarations
	int sps_num;

	//code
	printf("\n\n");

	printf("Enter Value For 'sps_num' : ");
	scanf("%d", &sps_num);

	//If - Else - If ladder starts from here
	if (sps_num < 0)
		printf("SPS_Num = %d Is Less Than 0 (NEGATIVE) !!!\n\n", sps_num);

	else if ((sps_num > 0) && (sps_num <= 100))
		printf("SPS_Num = %d Is Between 0 And 100 !!!\n\n", sps_num);

	else if ((sps_num > 100) && (sps_num <= 200))
		printf("SPS_Num = %d Is Between 100 And 200 !!!\n\n", sps_num);

	else if ((sps_num > 200) && (sps_num <= 300))
		printf("SPS_Num = %d Is Between 200 And 300 !!!\n\n", sps_num);

	else if ((sps_num > 300) && (sps_num <= 400))
		printf("SPS_Num = %d Is Between 300 And 400 !!!\n\n", sps_num);

	else if ((sps_num > 400) && (sps_num <= 500))
		printf("SPS_Num = %d Is Between 400 And 500 !!!\n\n", sps_num);

	else if (sps_num > 500)
		printf("SPS_Num = %d Is Greater Than 500 !!!\n\n", sps_num);

	//***NO TERMINATING 'ELSE' IN THIS LADDER !!!***

	return(0);
}

