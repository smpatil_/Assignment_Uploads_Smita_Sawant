#include <stdio.h>
int main(void)
{
	//variable declarations
	float sps_f;
	float sps_f_num = 2.3f; //trying new value here

	//code
	printf("\n\n");

	printf("Printing Numbers %f to %f : \n\n", sps_f_num, (sps_f_num * 12.0f));

	for (sps_f = sps_f_num; sps_f <= (sps_f_num * 12.0f); sps_f = sps_f + sps_f_num)
	{
		printf("\t%f\n", sps_f);
	}

	printf("\n\n");

	return(0);
}

