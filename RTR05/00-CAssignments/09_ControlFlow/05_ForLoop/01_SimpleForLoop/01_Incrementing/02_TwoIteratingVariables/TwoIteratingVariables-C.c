#include <stdio.h>
int main(void)
{
	//variable declarations
	int sps_i, sps_j;

	//code
	printf("\n\n");

	printf("Printing Digits 1 to 10 and 10 to 100 : \n\n");

	for (sps_i = 1, sps_j = 10; sps_i <= 10, sps_j <= 100; sps_i++, sps_j = sps_j + 10)
	{
		printf("\t %d \t %d\n", sps_i, sps_j);
	}

	printf("\n\n");

	return(0);
}

