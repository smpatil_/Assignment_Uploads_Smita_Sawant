#include <stdio.h>
int main(void)
{
	//variable declarations
	int sps_i;

	//code
	printf("\n\n");

	printf("Printing Digits 1 to 10 : \n\n");

	for (sps_i = 1; sps_i <= 10; sps_i++)
	{
		printf("\t%d\n", sps_i);
	}

	printf("\n\n");

	return(0);
}

