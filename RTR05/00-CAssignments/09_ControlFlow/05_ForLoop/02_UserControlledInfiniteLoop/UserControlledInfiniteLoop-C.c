#include <stdio.h>
int main(void)
{
	//variable declarations
	char my_option, ch = '\0';

	//code
	printf("\n\n");
	printf("Once The Infinite Loop Begins, Enter 'Q' or 'q' To Quit The Infinite FOR Loop : \n\n");
	printf("Enter 'Y' or 'y' To Initiate User Controlled Infinite Loop : ");
	printf("\n\n");
	my_option = getch();
	if (my_option == 'Y' || my_option == 'y')
	{
		for (;;) //infinite loop
		{
			printf("In Loop...\n");
			ch = getch();
			if (ch == 'Q' || ch == 'q')
				break; //User controlled exitting from Infinite Loop
		}
	}

	printf("\n\n");
	printf("EXITTING USER CONTROLLED INFINITE LOOP...");
	printf("\n\n");

	return(0);
}

