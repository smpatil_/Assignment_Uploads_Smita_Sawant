#include <stdio.h>
int main(void)
{
	//variable declarations
	int ss_i, ss_j, ss_k;

	//code
	printf("\n\n");
	for (ss_i = 1; ss_i <= 10; ss_i++)
	{
		printf("ss_i = %d\n", ss_i);
		printf("----------------\n\n");
		for (ss_j = 1; ss_j <= 5; ss_j++)
		{
			printf("\tss_j = %d\n", ss_j);
			printf("\t-----------\n\n");
			for (ss_k = 1; ss_k <= 3; ss_k++)
			{
				printf("\t\tss_k = %d\n", ss_k);
			}
			printf("\n\n");
		}
		printf("\n\n");
	}
	return(0);
}

