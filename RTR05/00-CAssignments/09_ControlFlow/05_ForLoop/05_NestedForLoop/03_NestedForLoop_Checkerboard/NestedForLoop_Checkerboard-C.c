#include <stdio.h>
int main(void)
{
	//variable declarations
	int ss_i, ss_j, ss_c;

	//code
	printf("\n\n");
	for (ss_i = 0; ss_i < 64; ss_i++)
	{
		for (ss_j = 0; ss_j < 64; ss_j++)
		{
			ss_c = ((ss_i & 0x8) == 0) ^ ((ss_j & 0x8) == 0);

			if (ss_c == 0)
				printf(" ");

			if (ss_c == 1)
				printf("* ");

		}
		printf("\n\n");
	}
	return(0);
}

