#include <stdio.h>
int main(void)
{
	//variable declarations
	int SPS_a;

	//code
	printf("\n\n");

	SPS_a = 5;
	if (SPS_a) //Non-zero Positive Value
	{
		printf("if-block 1 : 'A' Exists and Has Value = %d !!!\n\n", SPS_a);
	}

	SPS_a = -5;
	if (SPS_a) //Non-zero Negative Value
	{
		printf("if-block 2 : 'A' Exists And Has Value = %d !!!\n\n", SPS_a);
	}

	SPS_a = 0;
	if (SPS_a) //Zero Value
	{
		printf("if-block 3 : 'A' Exists And Has Value = %d !!!\n\n", SPS_a);
	}

	printf("All Three if-statements Are Done !!!\n\n");

	return(0);
}

