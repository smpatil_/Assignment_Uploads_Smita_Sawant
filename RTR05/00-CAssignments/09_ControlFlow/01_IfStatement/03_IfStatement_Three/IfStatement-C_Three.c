#include <stdio.h>
int main(void)
{
	//variable declarations
	int SPS_num;

	//code
	printf("\n\n");
	printf("Enter Value For 'SPS_num' : ");

	scanf("%d", &SPS_num);

	if (SPS_num < 0)
	{
		printf("Num = %d Is Less Than 0 (Negative).\n\n", SPS_num);
	}

	if ((SPS_num > 0) && (SPS_num <= 100))
	{
		printf("Num = %d Is Between 0 And 100.\n\n", SPS_num);
	}

	if ((SPS_num > 100) && (SPS_num <= 200))
	{
		printf("Num = %d Is Between 100 And 200.\n\n", SPS_num);
	}

	if ((SPS_num > 200) && (SPS_num <= 300))
	{
		printf("Num = %d Is Between 200 And 300.\n\n", SPS_num);
	}

	if ((SPS_num > 300) && (SPS_num <= 400))
	{
		printf("Num = %d Is Between 300 And 400.\n\n", SPS_num);
	}

	if ((SPS_num > 400) && (SPS_num <= 500))
	{
		printf("Num = %d Is Between 400 And 500.\n\n", SPS_num);
	}

	if (SPS_num > 500)
	{
		printf("Num = %d Is Greater Than 500.\n\n", SPS_num);
	}

	return(0);
}

