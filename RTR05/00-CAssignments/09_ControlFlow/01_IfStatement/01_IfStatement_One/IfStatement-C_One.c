#include <stdio.h>
int main(void)
{
	//Variable declarations
	int SPS_a, SPS_b, SPS_p;

	//my first if block code
	SPS_a = 9;
	SPS_b = 30;
	SPS_p = 30;

	printf("\n\n");

	if (SPS_a < SPS_b)
	{
		printf("A Is Less Than B !!!\n\n");
	}

	if (SPS_b != SPS_p)
	{
		printf("B Is Not Equal To P !!!\n\n");
	}

	printf("Both Comparisons Have Been Done !!!\n\n");

	return(0);
}

