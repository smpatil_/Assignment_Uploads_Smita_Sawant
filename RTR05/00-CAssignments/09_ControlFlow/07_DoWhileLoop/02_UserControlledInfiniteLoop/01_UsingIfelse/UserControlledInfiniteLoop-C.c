#include <stdio.h>
int main(void)
{
	//variable declarations
	char ss_option, ch = '\0';

	//code
	printf("\n\n");
	printf("Once The Infinite Loop Begins, Enter 'Q' or 'q' To Quit The Infinite For Loop : \n\n");
	printf("Enter 'Y' or 'y' To Initiate User Controlled Infinite Loop : ");
	printf("\n\n");
	ss_option = getch();
	if (ss_option == 'Y' || ss_option == 'y')
	{
		do
		{
			printf("In Loop...\n");
			ch = getch(); //control flow wait for character input...
			if (ch == 'Q' || ch == 'q')
				break; //User Controlled Exitting From Infinite Loop
		} while (1); //infinite loop

		printf("\n\n");
		printf("EXITTING USER CONTROLLED INFINITE LOOP...");
		printf("\n\n");
	}

	else
		printf("You Must Press 'Y' or 'y' To Initiate The User Controlled Infinite Loop....Pease Try Again..\n\n");

	return(0);
}

