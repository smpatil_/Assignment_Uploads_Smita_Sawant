#include <stdio.h>
int main(void)
{
	//variable declarations
	float ss_f;
	float ss_f_num = 2.1f;

	//code
	printf("\n\n");

	printf("Printing Numbers %f to %f : \n\n", ss_f_num, (ss_f_num * 7.0f));

	ss_f = ss_f_num;
	do
	{
		printf("\t%f\n", ss_f);
		ss_f = ss_f + ss_f_num;
	} while (ss_f <= (ss_f * 7.0f));

	printf("\n\n");

	return(0);
}

