#include <stdio.h>
int main(void)
{
	//variable declarations
	int ss_i, ss_j;

	//code
	printf("\n\n");

	ss_i = 1;
	do 
	{
		printf("ss_i = %d\n", ss_i);
		printf("-------------\n\n");

		ss_j = 1;
		do 
		{
			printf("\tss_j = %d\n", ss_j);
			ss_j++;
		} while (ss_j <= 5);
		ss_i++;
		printf("\n\n");
	} while (ss_i <= 10);
	return(0);
}

