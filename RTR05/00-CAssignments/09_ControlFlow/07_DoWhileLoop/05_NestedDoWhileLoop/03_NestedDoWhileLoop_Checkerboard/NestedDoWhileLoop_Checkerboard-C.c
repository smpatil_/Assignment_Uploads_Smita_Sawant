#include <stdio.h>
int main(void)
{
	//variable declarations
	int ss_i, ss_j, ss_c;

	//code
	printf("\n\n");

	ss_i = 0;
	do
	{
		ss_j = 0;
		do
		{
			ss_c = ((ss_i & 0x8) == 0) ^ ((ss_j & 0x8) == 0);

			if (ss_c == 0)
				printf(" ");

			if (ss_c == 1)
				printf("* ");

			ss_j++;

		} while (ss_j < 64);
		printf("\n\n");
		ss_i++;
	} while (ss_i < 64);
	return(0);
}
