#include <stdio.h>
int main(void)
{
	//variable declarations
	int ss_i, ss_j, ss_k;

	//code
	printf("\n\n");

	ss_i = 1;
	do
	{
		printf("ss_i = %d\n", ss_i);
		printf("----------------\n\n");

		ss_j = 1;
		do
		{
			printf("\tss_j = %d\n", ss_j);
			printf("\t--------------\n\n");

			ss_k = 1;
			do
			{
				printf("\t\tss_k = %d\n", ss_k);
				ss_k++;
			} while (ss_k <= 3);
			printf("\n\n");
			ss_j++;
		} while (ss_j <= 5);
		printf("\n\n");
		ss_i++;
	} while (ss_i <= 10);
	return(0);
}
