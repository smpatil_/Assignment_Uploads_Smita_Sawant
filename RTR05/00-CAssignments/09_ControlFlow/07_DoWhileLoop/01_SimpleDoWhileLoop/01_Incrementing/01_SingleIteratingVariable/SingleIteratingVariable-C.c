#include <stdio.h>
int main(void)
{
	//variable declarations
	int sps_i;

	//code
	printf("\n\n");

	printf("Printing Digits 1 to 10 : \n\n");

	sps_i = 1;
	do
	{
		printf("\t%d\n", sps_i);
		sps_i++;
	}while (sps_i <= 10);

	printf("\n\n");

	return(0);
}

