#include <stdio.h>
#include <conio.h>
int main(void)
{
	//variable declarations
	int sps_i, sps_j;

	//code
	printf("\n\n");

	for (sps_i = 1; sps_i <= 20; sps_i++)
	{
		for (sps_j = 1; sps_j <= 20; sps_j++)
		{
			if (sps_j > sps_i)
			{
				break;
			}
			else
			{
				printf("* ");
			}
		}
		printf("\n");
	}
	printf("\n\n");
	return(0);
}

