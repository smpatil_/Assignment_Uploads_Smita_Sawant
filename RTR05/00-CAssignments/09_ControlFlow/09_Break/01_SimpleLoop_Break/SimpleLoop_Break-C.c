#include <stdio.h>
#include <conio.h>
int main(void)
{
	//variable declarations
	int sps_i;
	char sps_ch;

	//code
	printf("\n\n");

	printf("Printing Even Numbers From 0 to 100 For Every User Input. Exitting the Loop When User Enters Character 'Q' or 'q' : \n\n");
	printf("Enter Character 'Q' or 'q' To Exit Loop : \n\n");

	for (sps_i = 1; sps_i <= 100; sps_i++)
	{
		printf("\t%d\n", sps_i);
		sps_ch = getch();
		if (sps_ch == 'Q' || sps_ch == 'q')
		{
			break;
		}
	}

	printf("\n\n");
	printf("Exitting Loop...");
	printf("\n\n");

	return(0);
}

