#include <stdio.h>
int main(void)
{
	//variable declarations
	int sps_i;

	//code
	printf("\n\n");

	printf("Printing Even Numbers From 0 to 100: \n\n");

	for (sps_i = 0; sps_i <= 100; sps_i++)
	{
		//condition for a number to be even number => division of number by 2 leaves no remainder : remainder = 0
		//if remainder is not 0, the number is odd number.
		if (sps_i % 2 != 0)
		{
			continue;
		}
		else
		{
			printf("\t%d\n", sps_i);
		}
	}

	printf("\n\n");

	return(0);
}