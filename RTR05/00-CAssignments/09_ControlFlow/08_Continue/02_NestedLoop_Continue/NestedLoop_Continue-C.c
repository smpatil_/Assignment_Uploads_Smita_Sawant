#include <stdio.h>
int main(void)
{
	//variable declarations
	int sps_i, sps_j;

	//code
	printf("\n\n");

	printf("Outer Loop Prints Odd Numbers Between 1 and 10. \n\n");
	printf("Inner Loop Prints Even Numbers Between 1 and 10 For Every Odd Number Printed By Outer Loop. \n\n");

	//condition for a number to be even number is - division of number by 2 leaves remainder = 0
	//condition for a number to be odd number is - division of number by 2 leaves remainder = 1 usually

	for (sps_i = 1; sps_i <= 10; sps_i++)
	{
		if (sps_i % 2 != 0) //if sps_i is an odd number
		{
			printf("sps_i = %d\n", sps_i);
			printf("--------------------\n");
			for (sps_j = 1; sps_j <= 10; sps_j++)
			{
				if (sps_j % 2 == 0) //if sps_j is an even number
				{
					printf("\tsps_j = %d\n", sps_j);
				}
				else //if sps_j is an odd number
				{
					continue;
				}
			}
			printf("\n\n");
		}
		else //if sps_i is an even number
		{
			continue;
		}
	}

	printf("\n\n");

	return(0);
}

