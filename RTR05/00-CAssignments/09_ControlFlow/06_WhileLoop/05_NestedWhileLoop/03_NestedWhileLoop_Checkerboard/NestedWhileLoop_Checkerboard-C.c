#include <stdio.h>
int main(void)
{
	//variable declarations
	int sps_i, sps_j, sps_c;

	//code
	printf("\n\n");

	sps_i = 0;
	while (sps_i < 64)
	{
		sps_j = 0;
		while (sps_j < 64)
		{
			sps_c = ((sps_i & 0x8) == 0) ^ ((sps_j & 0x8) == 0);

			if (sps_c == 0)
				printf(" ");

			if (sps_c == 1)
				printf("* ");

			sps_j++;

		}
		printf("\n\n");
		sps_i++;
	}
	return(0);
}

