#include <stdio.h>
int main(void)
{
	//variable declarations
	int sps_i, sps_j, sps_k;

	//code
	printf("\n\n");

	sps_i = 1;
	while (sps_i <= 10)
	{
		printf("sps_i = %d\n", sps_i);
		printf("----------------\n\n");

		sps_j = 1;
		while (sps_j <= 5)
		{
			printf("\tsps_j = %d\n", sps_j);
			printf("\t--------------\n\n");

			sps_k = 1;
			while (sps_k <= 3)
			{
				printf("\t\tsps_k = %d\n", sps_k);
				sps_k++;
			}
			printf("\n\n");
			sps_j++;
		}
		printf("\n\n");
		sps_i++;
	}
	return(0);
}

