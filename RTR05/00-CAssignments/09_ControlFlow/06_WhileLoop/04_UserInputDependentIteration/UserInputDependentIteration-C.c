#include <stdio.h>
int main(void)
{
	//variable declarations
	int sps_i_num, sps_num, sps_i;

	//code
	printf("\n\n");

	printf("Enter An Integer Value From Which Iteration Must Begin : ");
	scanf("%d", &sps_i_num);

	printf("How Many Digits Do You Want To Print From %d Onwards ? : ", sps_i_num);
	scanf("%d", &sps_num);

	printf("Printing Digits %d to %d : \n\n", sps_i_num, (sps_i_num + sps_num));

	sps_i = sps_i_num;
	while (sps_i <= (sps_i_num + sps_num))
	{
		printf("\t%d\n", sps_i);
		sps_i++;
	}

	printf("\n\n");

	return(0);
}

