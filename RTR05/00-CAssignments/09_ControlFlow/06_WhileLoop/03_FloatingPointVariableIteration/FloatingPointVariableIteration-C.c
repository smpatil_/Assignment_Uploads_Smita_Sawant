#include <stdio.h>
int main(void)
{
	//variable declarations
	float sps_f;
	float sps_f_num = 1.2f;

	//code
	printf("\n\n");

	printf("Printing Numbers %f to %f : \n\n", sps_f_num, (sps_f_num * 3.0f));

	sps_f = sps_f_num;
	while(sps_f <= (sps_f * 3.0f))
	{
		printf("\t%f\n", sps_f);
		sps_f = sps_f + sps_f_num;
	}

	printf("\n\n");

	return(0);
}
