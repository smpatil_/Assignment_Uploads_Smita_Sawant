#include <stdio.h>
int main(void)
{
	//variable declarations
	int sps_i;

	//code
	printf("\n\n");

	printf("Printing Digits 10 to 1 : \n\n");

	sps_i = 10;
	while (sps_i >= 1)
	{
		printf("\t%d\n", sps_i);
		sps_i--;
	}

	printf("\n\n");

	return(0);
}

