#include <stdio.h>
int main(void)
{
	//variable declarations
	int sps_i, sps_j;

	//code
	printf("\n\n");

	printf("Printing Digits 10 to 1 and 100 to 10 : \n\n");

	sps_i = 10;
	sps_j = 100;
	while (sps_i >= 1, sps_j >= 10)
	{
		printf("\t %d \t %d\n", sps_i, sps_j);
		sps_i--;
		sps_j = sps_j - 10;
	}

	printf("\n\n");

	return(0);
}
