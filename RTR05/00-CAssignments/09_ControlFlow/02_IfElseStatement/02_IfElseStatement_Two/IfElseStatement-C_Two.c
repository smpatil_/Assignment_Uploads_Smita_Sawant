#include <stdio.h>
int main(void)
{
	//variable declarations
	int smita_age;

	//code
	printf("\n\n");
	printf("Enter Smita's Age : ");
	scanf("%d", &smita_age);
	printf("\n\n");
	if (smita_age >= 18)
	{
		printf("Entering if-block...\n\n");
		printf("Smita Is Eligible For Voting !!!\n\n");
	}
	else
	{
		printf("Entering else-block...\n\n");
		printf("Smita Is NOT Eligible For Voting !!!\n\n");
	}
	printf("Bye !!!\n\n");
	return(0);
}