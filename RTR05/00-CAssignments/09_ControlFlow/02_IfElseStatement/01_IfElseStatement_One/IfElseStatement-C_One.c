#include <stdio.h>
int main(void)
{
	//variable declarations
	int sps_a, sps_b, sps_p;

	//code
	sps_a = 9;
	sps_b = 30;
	sps_p = 30;

	//First if-else pair
	printf("\n\n");
	if (sps_a < sps_b)
	{
		printf("Entering First if-block...\n\n");
		printf("A Is Less Than B !!!\n\n");
	}
	else
	{
		printf("Entering First else-bolck...\n\n");
		printf("A Is NOT Less Than B!!!\n\n");
	}
	printf("First if-else Pair Done !!!\n\n");

	//Second if-else pair
	printf("\n\n");
	if (sps_b != sps_p)
	{
		printf("Entering Second if-block...\n\n");
		printf("B Is NOT Equal To P !!!\n\n");
	}
	else
	{
		printf("Entering Second else-block...\n\n");
		printf("B Is Equal To P !!!\n\n");
	}
	printf("Second if-else Pair Done !!!\n\n");

	return(0);
}

