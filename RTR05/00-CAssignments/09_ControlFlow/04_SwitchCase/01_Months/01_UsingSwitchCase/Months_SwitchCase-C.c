#include <stdio.h>
int main(void)
{
	//variable declarations
	int sps_num_month;

	//code
	printf("\n\n");

	printf("Enter Number Of Month (1 to 12) : ");
	scanf("%d", &sps_num_month);

	printf("\n\n");

	switch (sps_num_month)
	{
	case 1: //used like 'if'
		printf("Month Number %d IS JANUARY !!!\n\n", sps_num_month);
		break;

	case 2: //used like 'else if'
		printf("Month Number %d Is FEBRUARY !!!\n\n", sps_num_month);
		break;

	case 3: //used like 'else if'
		printf("Month Number %d Is MARCH !!!\n\n", sps_num_month);
		break;

	case 4: //used like 'else if'
		printf("Month Number %d Is APRIL !!!\n\n", sps_num_month);
		break;

	case 5: //used like 'else if'
		printf("Month Number %d Is MAY !!!\n\n", sps_num_month);
		break;

	case 6: //used like 'else if'
		printf("Month Number %d Is JUNE !!!\n\n", sps_num_month);
		break;

	case 7: //used like 'else if'
		printf("Month Number %d Is JULY !!!\n\n", sps_num_month);
		break;

	case 8: //used like 'else if'
		printf("Month Number %d Is AUGUST !!!\n\n", sps_num_month);
		break;

	case 9: //used like 'else if'
		printf("Month Number %d Is SEPTEMBER !!!\n\n", sps_num_month);
		break;

	case 10: //used like 'else if'
		printf("Month Number %d Is OCTOBER !!!\n\n", sps_num_month);
		break;

	case 11: //used like 'else if'
		printf("Month Number %d Is NOVEMBER !!!\n\n", sps_num_month);
			break;

	case 12: //used like 'else if'
		printf("Month Number %d Is DECEMBER !!!\n\n", sps_num_month);
		break;

	default: //like ending OPTIONAL 'else' just ike terminating 'else' is optional in if-else if-else ladder, so is the 'default' case optional in switch-case
		printf("Invalid Month Number %d Entered !!! Please Try Again...\n\n", sps_num_month);
		break;
	}

	printf("Switch Case Block Complete !!!\n");

	return(0);
}

