#include <stdio.h> //for printf()
#include <conio.h> //for getch()

int main(void)
{
	//variable declarations
	int ss_a, ss_b;
	int sps_result;

	char ss_option, ss_option_division;

	//code
	printf("\n\n");

	printf("Enter Value For 'A' : ");
	scanf("%d", &ss_a);

	printf("Enter Value For 'B' : ");
	scanf("%d", &ss_b);

	printf("Enter Option In Character : \n\n");
	printf("'A' or 'a' For Addition : \n");
	printf("'S' or 's' For Subraction : \n");
	printf("'M' or 'm' For Multiplication : \n");
	printf("'D' or 'd' For Division : \n\n");

	printf("Enter Option : ");
	ss_option = getch();

	printf("\n\n");

	switch (ss_option)
	{
		// FALL THROUGH CONSITION FOR 'A' and 'a'
	case 'A': 
	case'a':
		sps_result = ss_a + ss_b;
		printf("Addition Of A = %d And B = %d Gives Result %d !!!\n\n", ss_a, ss_b, sps_result);
		break;

		// FALL THROUGH CONSITION FOR 'S' and 's'
	case 'S':
	case 's': 
		if (ss_a >= ss_b)
		{
			sps_result = ss_a - ss_b;
			printf("Subraction Of B = %d From A = %d Gives Result %d !!!\n\n", ss_b, ss_a, sps_result);
		}
		else
		{
			sps_result = ss_b - ss_a;
			printf("Subtraction Of A = %d From B = %d Gives Result %d !!!\n\n", ss_a, ss_b, sps_result);
		}
		break;

		//FALL THROUGH CONSITION FOR 'M' and 'm'
	case 'M':
	case 'm':
		sps_result = ss_a * ss_b;
		printf("Multiplication Of A = %d And B = %d Gives Result %d !!!\n\n", ss_a, ss_b, sps_result);
		break;

		// FALL THROUGH CONSITION FOR 'D' and 'd'
	case 'D':
	case 'd':
		printf("Enter Option In Character : \n\n");
		printf("'Q' or 'q' or '/' For Quotient Upon Division : \n");
		printf("'R' or 'r' or '%%' For Remainder Upon Division : \n");

		printf("Enter Option : ");
		ss_option_division = getch();

			printf("\n\n");

		switch (ss_option_division)
		{
			//FALL THROUGH CONSITION FOR 'Q' and 'q' and '/'
		case 'Q':
		case'q':
		case '/':
			if (ss_a >= ss_b)
			{
				sps_result = ss_a / ss_b;
				printf("Division Of A = %d By B = %d Gives Quotient = %d !!!\n\n", ss_a, ss_b, sps_result);
			}
			else
			{
				sps_result = ss_b / ss_a;
				printf("Division Of B = %d By A = %d Gives Quotient = %d !!!\n\n", ss_b, ss_a, sps_result);
			}
			break; //'break' of case 'Q' or case 'q' or case '/'

			// FALL THROUGH CONSITION FOR 'R' and 'r' and '%'
		case 'R':
		case 'r':
		case '%': 
			if (ss_a >= ss_b)
			{
				sps_result = ss_a % ss_b;
				printf("Division Of A = %d By B = %d Gives Remainder = %d !!!\n\n", ss_a, ss_b, sps_result);
			}
			else
			{
				sps_result = ss_b % ss_a;
				printf("Division Of B = %d By A = %d Gives Remainder = %d !!!\n\n", ss_b, ss_a, sps_result);
			}
			break; //'break' of case 'R' or case 'r' or case '%'

		default: //'default' case for switch(ss_option_division)
			printf("Invalid Character %c Entered For Division !!! Please Try Again...\n\n", ss_option_division);
			break; // 'break' of 'default' of switch(ss_option_division)

		} //ending curly brace of switch(ss_option_division)

		break; // 'break' of case 'D' or case 'd'

	default: //'default' case for switch (ss_option)
		printf("Invalid Character %c Entered !!! Please Try Again....!!!\n\n", ss_option);
		break;
	} //ending curly brace of switch (ss_option)

	printf("Switch Case Block Complete !!!\n");

	return(0);
}

