#include <stdio.h> //for printf()
#include <conio.h> //for getch()

int main(void)
{
	//variable declarations
	int sps_a, sps_b;
	int sps_result;

	char sps_option, sps_option_division;

	//code
	printf("\n\n");

	printf("Enter Value For 'A' : ");
	scanf("%d", &sps_a);

	printf("Enter Value For 'B' : ");
	scanf("%d", &sps_b);

	printf("Enter Option In Character : \n\n");
	printf("'A' or 'a' For Addition : \n");
	printf("'S' or 's' For Subraction : \n");
	printf("'M' or 'm' For Multiplication : \n");
	printf("'D' or 'd' For Division : \n\n");

	printf("Enter Option : ");
	sps_option = getch();

	printf("\n\n");

	if (sps_option == 'A' || sps_option == 'a')
	{
		sps_result = sps_a + sps_b;
		printf("Addition Of A = %d And B = %d Gives Result %d !!!\n\n", sps_a, sps_b, sps_result);
	}

	else if (sps_option == 'S' || sps_option == 's')
	{
		if (sps_a >= sps_b)
		{
			sps_result = sps_a - sps_b;
			printf("Subraction Of B = %d From A = %d Gives Result %d !!!\n\n", sps_b, sps_a, sps_result);
		}
		else
		{
			sps_result = sps_b - sps_a;
			printf("Subtraction Of A = %d From B = %d Gives Result %d !!!\n\n", sps_a, sps_b, sps_result);
		}
	}

	else if (sps_option == 'M' || sps_option == 'm')
	{
		sps_result = sps_a * sps_b;
		printf("Multiplication Of A = %d And B = %d Gives Result %d !!!\n\n", sps_a, sps_b, sps_result);
	}

	else if (sps_option == 'D' || sps_option == 'd')
	{
		printf("Enter Option In Character : \n\n");
		printf("'Q' or 'q' or '/' For Quotient Upon Division : \n");
		printf("'R' or 'r' or '%%' For Remainder Upon Division : \n");

		printf("Enter Option : ");
		sps_option_division = getch();

		printf("\n\n");

		if (sps_option_division == 'Q' || sps_option_division == 'q' || sps_option_division == '/')
		{
			if (sps_a >= sps_b)
			{
				sps_result = sps_a / sps_b;
				printf("Division Of A = %d By B = %d Gives Quotient = %d !!!\n\n", sps_a, sps_b, sps_result);
			}
			else
			{
				sps_result = sps_b / sps_a;
				printf("Division Of B = %d By A = %d Gives Quotient = %d !!!\n\n", sps_b, sps_a, sps_result);
			}
		}

		else if (sps_option_division == 'R' || sps_option_division == 'r' || sps_option_division == '%')
		{
			if (sps_a >= sps_b)
			{
				sps_result = sps_a % sps_b;
				printf("Division Of A = %d By B = %d Gives Remainder = %d !!!\n\n", sps_a, sps_b, sps_result);
			}
			else
			{
				sps_result = sps_b % sps_a;
				printf("Division Of B = %d By A = %d Gives Remainder = %d !!!\n\n", sps_b, sps_a, sps_result);
			}
		}

		else
			printf("Invalid Character %c Entered For Division !!! Please Try Again...\n\n", sps_option_division);
	}

	else
		printf("Invalid Character %c Entered !!! Please Try Again....!!!\n\n", sps_option);

	printf("If - Else If - Else Ladder Complete !!!\n");

	return(0);
}
