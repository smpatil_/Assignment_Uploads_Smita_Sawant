#include <stdio.h> //for print()
#include <conio.h> //for getch()

//ASCII Values For 'A' to 'Z' => 65 to 90
#define CHAR_ALPHABET_UPPER_CASE_BEGINNING 65
#define CHAR_ALPHABET_UPPER_CASE_ENDING 90

//ASCII Value For 'a' to 'z' => 97 to 122
#define CHAR_ALPHABET_LOWER_CASE_BEGINNING 97
#define CHAR_ALPHABET_LOWER_CASE_ENDING 122

//ASCII Value For '0' to '9' => 48 to 57
#define CHAR_DIGIT_BEGINNING 48
#define CHAR_DIGIT_ENDING 57

int main(void)
{
	//variable declarations
	char sps_ch;
	int sps_ch_value;

	//code
	printf("\n\n");

	printf("Enter Character : ");
	sps_ch = getch();

	printf("\n\n");

	switch (sps_ch)
	{
	//Fall Through Condition...
	case 'A':
	case 'a':

	case'E':
	case'e':

	case'I':
	case'i':

	case'O':
	case'o':

	case'U':
	case'u':
		printf("Character \'%c\' Entered By You, Is A VOWEL CHARACTER From The English Alphabet !!!\n\n", sps_ch);
		break;
	default:
		sps_ch_value = (int)sps_ch;

		//If The Character Has ASCII Value Between 65 AND 90 OR Between 97 AND 122, It Is still a letter of the alphabet, but it is a 'CONSONANT', and NOT a 'VOWEL'.
		if ((sps_ch_value >= CHAR_ALPHABET_UPPER_CASE_BEGINNING && sps_ch_value <= CHAR_ALPHABET_UPPER_CASE_ENDING) || (sps_ch_value >= CHAR_ALPHABET_LOWER_CASE_BEGINNING && sps_ch <= CHAR_ALPHABET_LOWER_CASE_ENDING))
		{
			printf("Character \'%c\' Entered By You, Is A CONSONANT CHARACTER From The English Alphabet !!!\n\n", sps_ch);
		}

		else if (sps_ch_value >= CHAR_DIGIT_BEGINNING && sps_ch_value <= CHAR_DIGIT_ENDING)
		{
			printf("Character \'%c\' Entered By You, Is A DIGIT CHARACTER !!!\n\n", sps_ch);
		}

		else
		{
			printf("Character \'%c\' Entered By You, Is A SPECIAL CHARACTER !!!\n\n", sps_ch);
		}
		break;
	}

	printf("Switch Case Block Complete !!!\n");

	return(0);
}

