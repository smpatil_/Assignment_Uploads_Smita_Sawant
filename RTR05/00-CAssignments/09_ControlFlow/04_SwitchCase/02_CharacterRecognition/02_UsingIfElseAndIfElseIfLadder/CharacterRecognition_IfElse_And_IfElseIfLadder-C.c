#include <stdio.h> //for printf()
#include <conio.h> //for getch()

// ASCII Values For 'A' to 'Z' => 65 to 90
#define CHAR_ALPHABET_UPPER_CASE_BEGINNING 65
#define CHAR_ALPHABET_UPPER_CASE_ENDING 90

// ASCII Values for 'a' to 'z' => 97 to 122
#define CHAR_ALPHABET_LOWER_CASE_BEGINNING 97
#define CHAR_ALPHABET_LOWER_CASE_ENDING 122

// ASCII Values For '0' to '9' => 48 to 57
#define CHAR_DIGIT_BEGINNING 48
#define CHAR_DIGIT_ENDING 57

int main(void)
{
	//variable declarations
	char SS_ch;
	int SPS_ch_value;

	//code
	printf("\n\n");

	printf("Enter Character : ");
	SS_ch = getch();

	printf("\n\n");

	if ((SS_ch == 'A' || SS_ch == 'a') || (SS_ch == 'E' || SS_ch == 'e') || (SS_ch == 'I' || SS_ch == 'i') || (SS_ch == 'O' || SS_ch == 'o') || (SS_ch == 'U' || SS_ch == 'u'))
	{
		printf("Character \'%c\' Entered By You, Is A VOWEL CHARACTER From The English Alphabet !!!\n\n", SS_ch);
	}

	else
	{
		SPS_ch_value = (int)SS_ch;

		//If The Character Has ASCII Value Between 65 AND 90 OR Between 97 AND 122, It is still a letter of the alphabet, but it is a 'CONSONANT', and not a Vowel.
		if ((SPS_ch_value >= CHAR_ALPHABET_UPPER_CASE_BEGINNING && SPS_ch_value <= CHAR_ALPHABET_UPPER_CASE_ENDING) || (SPS_ch_value >= CHAR_ALPHABET_LOWER_CASE_BEGINNING && SPS_ch_value >= CHAR_ALPHABET_LOWER_CASE_BEGINNING && SPS_ch_value <= CHAR_ALPHABET_LOWER_CASE_ENDING))
		{
			printf("Character \'%c\' Entered By You, Is a CONSONANT CHARACTER From The English Alphabet !!!\n\n", SS_ch);
		}
		
		else if (SPS_ch_value >= CHAR_DIGIT_BEGINNING && SPS_ch_value <= CHAR_DIGIT_ENDING)
		{
			printf("Character \'%c\' Entered By You, Is A DIGIT CHARACTER !!!\n\n", SS_ch);
		}
		
		else
		{
			printf("Character \'%c\' Entered by You, Is a SPECIAL CHARACTER !!!\n\n", SS_ch);
		}
	}
	
	return(0);
}

