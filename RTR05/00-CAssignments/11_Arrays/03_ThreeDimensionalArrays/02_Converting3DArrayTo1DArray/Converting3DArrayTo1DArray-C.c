#include <stdio.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3
#define DEPTH 2

int main(void)
{
	//variable declarations

	//In-line Initialization
	int sps_iArray[NUM_ROWS][NUM_COLUMNS][DEPTH] = { {{9, 8}, {27, 36}, {45, 54}},
		{{8, 16}, {24, 32}, {40, 48}},
		{{7, 14}, {21, 28}, {35, 42}},
		{{6, 12}, {18, 24}, {30, 36}},
		{{5, 10}, {15, 20}, {25, 30}} };
	int sps_i, sps_j, sps_k;

	int sps_iArray_1D[NUM_ROWS * NUM_COLUMNS * DEPTH]; //5 *3 * 2 Elements => 30 elements in 1D Array

	//code

	//**** Display 3D Array ****
	printf("\n\n");
	printf("Elements In The 3D Array : \n\n");
	for (sps_i = 0; sps_i < NUM_ROWS; sps_i++)
	{
		printf("****** ROW %d ******\n", (sps_i + 1));
		for (sps_j = 0; sps_j < NUM_COLUMNS; sps_j++)
		{
			printf("****** COLUMN %d ******\n", (sps_j + 1));
			for (sps_k = 0; sps_k < DEPTH; sps_k++)
			{
				printf("sps_iArray[%d][%d][%d] = %d\n", sps_i, sps_j, sps_k, sps_iArray[sps_i][sps_j][sps_k]);
			}
			printf("\n");
		}
		printf("\n");
	}

	//*** Converting 3D to 1D  ***
	for (sps_i = 0; sps_i < NUM_ROWS; sps_i++)
	{
		for (sps_j = 0; sps_j < NUM_COLUMNS; sps_j++)
		{
			for (sps_k = 0; sps_k < DEPTH; sps_k++)
			{
				sps_iArray_1D[(sps_i * NUM_COLUMNS * DEPTH) + (sps_j * DEPTH) + sps_k] = sps_iArray[sps_i][sps_j][sps_k];
			}
		}
	}

	//**** Display 1D Array ****
	printf("\n\n\n\n");
	printf("Elements In The 1D Array : \n\n");
	for (sps_i = 0; sps_i < (NUM_ROWS * NUM_COLUMNS * DEPTH); sps_i++)
	{
		printf("sps_iArray_1D[%d] = %d\n", sps_i, sps_iArray_1D[sps_i]);
	}

	return(0);
}

