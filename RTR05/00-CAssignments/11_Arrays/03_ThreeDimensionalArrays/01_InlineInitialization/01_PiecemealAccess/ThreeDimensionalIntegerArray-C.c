#include <stdio.h>
int main(void)
{
	//variable declarations

	//In-line Initialization
	int sps_iArray[5][3][2] = { {{9, 18}, {27, 36}, {45, 54}},
							   {{8, 16}, {24, 32}, {40, 48}},
							   {{7, 14}, {21, 28}, {35, 42}},
							   {{6, 12}, {18, 24}, {30, 36}},
							   {{5, 10}, {15, 20}, {25, 30}} };
	int sps_int_size;
	int sps_iArray_size;
	int sps_iArray_num_elements, sps_iArray_width, sps_iArray_height, sps_iArray_depth;

	//code
	printf("\n\n");

	sps_int_size = sizeof(int);

	sps_iArray_size = sizeof(sps_iArray);
	printf("Size of Three Dimensional (3D) Integer Array Is = %d\n\n", sps_iArray_size);

	sps_iArray_width = sps_iArray_size / sizeof(sps_iArray[0]);
	printf("Number of Rows (Width) In Three Dimensional (3D) Integer Array Is = %d\n\n", sps_iArray_width);
	
	sps_iArray_height = sizeof(sps_iArray[0]) / sizeof(sps_iArray[0][0]);
	printf("Number of Columns (Height) In Three Dimensional (3D) Integer Array Is = %d\n\n", sps_iArray_height);
	
	sps_iArray_depth = sizeof(sps_iArray[0][0]) / sps_int_size;
	printf("Depth of Three Dimensional (3D) Integer Array Is = %d\n\n", sps_iArray_depth);

	sps_iArray_num_elements = sps_iArray_width * sps_iArray_height * sps_iArray_depth;
	printf("Number of Elements In Three Dimensional (3D) Integer Array Is = %d\n\n", sps_iArray_num_elements);

	printf("\n\n");
	printf("Elements In Integer 3D Array : \n\n");

	//*** Piece-Meal Display ***
	// ***** ROW 1 *****
	printf("****** ROW 1 *****\n");
	printf("****** COLUMN 1 *****\n");
	printf("sps_iArray[0][0][0] = %d\n", sps_iArray[0][0][0]);
	printf("sps_iArray[0][0][1] = %d\n", sps_iArray[0][0][1]);
	printf("\n");
	
	printf("****** COLUMN 2 *****\n");
	printf("sps_iArray[0][1][0] = %d\n", sps_iArray[0][1][0]);
	printf("sps_iArray[0][1][1] = %d\n", sps_iArray[0][1][1]);
	printf("\n");	
	
	printf("****** COLUMN 3 *****\n");
	printf("sps_iArray[0][2][0] = %d\n", sps_iArray[0][2][0]);
	printf("sps_iArray[0][2][1] = %d\n", sps_iArray[0][2][1]);
	printf("\n\n");

	// ***** ROW 2 *****
	printf("****** ROW 2 *****\n");
	printf("****** COLUMN 1 *****\n");
	printf("sps_iArray[1][0][0] = %d\n", sps_iArray[1][0][0]);
	printf("sps_iArray[1][0][1] = %d\n", sps_iArray[1][0][1]);
	printf("\n");

	printf("****** COLUMN 2 *****\n");
	printf("sps_iArray[1][1][0] = %d\n", sps_iArray[1][1][0]);
	printf("sps_iArray[1][1][1] = %d\n", sps_iArray[1][1][1]);
	printf("\n");

	printf("****** COLUMN 3 *****\n");
	printf("sps_iArray[1][2][0] = %d\n", sps_iArray[1][2][0]);
	printf("sps_iArray[1][2][1] = %d\n", sps_iArray[1][2][1]);
	printf("\n\n");

	// ***** ROW 3 *****
	printf("****** ROW 3 *****\n");
	printf("****** COLUMN 1 *****\n");
	printf("sps_iArray[2][0][0] = %d\n", sps_iArray[2][0][0]);
	printf("sps_iArray[2][0][1] = %d\n", sps_iArray[2][0][1]);
	printf("\n");

	printf("****** COLUMN 2 *****\n");
	printf("sps_iArray[2][1][0] = %d\n", sps_iArray[2][1][0]);
	printf("sps_iArray[2][1][1] = %d\n", sps_iArray[2][1][1]);
	printf("\n");

	printf("****** COLUMN 3 *****\n");
	printf("sps_iArray[2][2][0] = %d\n", sps_iArray[2][2][0]);
	printf("sps_iArray[2][2][1] = %d\n", sps_iArray[2][2][1]);
	printf("\n\n");

	// ***** ROW 4 *****
	printf("****** ROW 4 *****\n");
	printf("****** COLUMN 1 *****\n");
	printf("sps_iArray[3][0][0] = %d\n", sps_iArray[3][0][0]);
	printf("sps_iArray[3][0][1] = %d\n", sps_iArray[3][0][1]);
	printf("\n");

	printf("****** COLUMN 2 *****\n");
	printf("sps_iArray[3][1][0] = %d\n", sps_iArray[3][1][0]);
	printf("sps_iArray[3][1][1] = %d\n", sps_iArray[3][1][1]);
	printf("\n");

	printf("****** COLUMN 3 *****\n");
	printf("sps_iArray[3][2][0] = %d\n", sps_iArray[3][2][0]);
	printf("sps_iArray[3][2][1] = %d\n", sps_iArray[3][2][1]);
	printf("\n\n");

	// ***** ROW 5 *****
	printf("****** ROW 5 *****\n");
	printf("****** COLUMN 1 *****\n");
	printf("sps_iArray[4][0][0] = %d\n", sps_iArray[4][0][0]);
	printf("sps_iArray[4][0][1] = %d\n", sps_iArray[4][0][1]);
	printf("\n");

	printf("****** COLUMN 2 *****\n");
	printf("sps_iArray[4][1][0] = %d\n", sps_iArray[4][1][0]);
	printf("sps_iArray[4][1][1] = %d\n", sps_iArray[4][1][1]);
	printf("\n");

	printf("****** COLUMN 3 *****\n");
	printf("sps_iArray[4][2][0] = %d\n", sps_iArray[4][2][0]);
	printf("sps_iArray[4][2][1] = %d\n", sps_iArray[4][2][1]);
	printf("\n\n");

	return(0);
}


