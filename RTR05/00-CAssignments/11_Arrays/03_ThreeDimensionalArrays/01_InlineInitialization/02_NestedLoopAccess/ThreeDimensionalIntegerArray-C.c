#include <stdio.h>
int main(void)
{
	//variable declarations

	//In-line Initialization
	int sps_iArray[5][3][2] = { {{9, 18}, {27, 36}, {45, 54}},
							   {{8, 16}, {24, 32}, {40, 48}},
							   {{7, 14}, {21, 28}, {35, 42}},
							   {{6, 12}, {18, 24}, {30, 36}},
							   {{5, 10}, {15, 20}, {25, 30}} };
	int sps_int_size;
	int sps_iArray_size;
	int sps_iArray_num_elements, sps_iArray_width, sps_iArray_height, sps_iArray_depth;
	int sps_i, sps_j, sps_k;

	//code
	printf("\n\n");

	sps_int_size = sizeof(int);

	sps_iArray_size = sizeof(sps_iArray);
	printf("Size of Three Dimensional (3D) Integer Array Is = %d\n\n", sps_iArray_size);

	sps_iArray_width = sps_iArray_size / sizeof(sps_iArray[0]);
	printf("Number of Rows (Width) In Three Dimensional (3D) Integer Array Is = %d\n\n", sps_iArray_width);

	sps_iArray_height = sizeof(sps_iArray[0]) / sizeof(sps_iArray[0][0]);
	printf("Number of Columns (Height) In Three Dimensional (3D) Integer Array Is = %d\n\n", sps_iArray_height);

	sps_iArray_depth = sizeof(sps_iArray[0][0]) / sps_int_size;
	printf("Depth of Three Dimensional (3D) Integer Array Is = %d\n\n", sps_iArray_depth);

	sps_iArray_num_elements = sps_iArray_width * sps_iArray_height * sps_iArray_depth;
	printf("Number of Elements In Three Dimensional (3D) Integer Array Is = %d\n\n", sps_iArray_num_elements);

	printf("\n\n");
	printf("Elements In Integer 3D Array : \n\n");

	for (sps_i = 0; sps_i < sps_iArray_width; sps_i++)
	{
		printf("****** ROW %d *****\n", (sps_i + 1));
		for (sps_j = 0; sps_j < sps_iArray_height; sps_j++)
		{
			printf("***** COLUMN %d *****\n", (sps_j + 1));
			for (sps_k = 0; sps_k < sps_iArray_depth; sps_k++)
			{
				printf("sps_iArray[%d][%d][%d] = %d\n", sps_i, sps_j, sps_k, sps_iArray[sps_i][sps_j][sps_k]);
			}
			printf("\n");
		}
		printf("\n\n");
	}

	return(0);
}

