#include <stdio.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3

int main(void)
{
	//Variable declarations
	int sps_iArray_2D[NUM_ROWS][NUM_COLUMNS]; //Total number 0f elements = NUM_ROWS * NUM_COLUMNS
	int sps_iArray_1D[NUM_ROWS * NUM_COLUMNS];

	int sps_i, sps_j;
	int sps_num;

	//code
	printf("Enter Elements Of Your Choice To Fill Up The Integer 2D Array : \n\n");
	for (sps_i = 0; sps_i < NUM_ROWS; sps_i++)
	{
		printf("For ROW NUMBER %d : \n", (sps_i + 1));
		for (sps_j = 0; sps_j < NUM_COLUMNS; sps_j++)
		{
			printf("Enter Element Number %d : \n", (sps_j + 1));
			scanf("%d", &sps_num);
			sps_iArray_2D[sps_i][sps_j] = sps_num;
		}
		printf("\n\n");
	}

	//*** Display of 2D Array ***
	printf("\n\n");
	printf("Two-Dimensional (2D) Array Of Integers : \n\n");
	for (sps_i = 0; sps_i < NUM_ROWS; sps_i++)
	{
		printf("****** ROW %d ******\n", (sps_i + 1));
		for (sps_j = 0; sps_j < NUM_COLUMNS; sps_j++)
		{
			printf("sps_iArray_2D[%d][%d] = %d\n", sps_i, sps_j, sps_iArray_2D[sps_i][sps_j]);
		}
		printf("\n\n");
	}

	//*** Converting 2D Integer Array to 1D Integer Array ***
	for (sps_i = 0; sps_i < NUM_ROWS; sps_i++)
	{
		for (sps_j = 0; sps_j < NUM_COLUMNS; sps_j++)
		{
			sps_iArray_1D[(sps_i * NUM_COLUMNS) + sps_j++] = sps_iArray_2D[sps_i][sps_j];
		}
	}

	//**** Printing 1D Array ****
	printf("\n\n");
	printf("One-Dimensional (1D) Array Of Integers : \n\n");
	for (sps_i = 0; sps_i < (NUM_ROWS * NUM_COLUMNS); sps_i++)
	{
		printf("sps_iArray_1D[%d] = %d\n", sps_i, sps_iArray_1D[sps_i]);
	}

	printf("\n\n");

	return(0);
}

