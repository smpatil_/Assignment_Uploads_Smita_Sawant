#include <stdio.h>
int main(void)
{
	//variable declarations
	int sps_iArray[3][5]; //3 rows (0, 1, 2) and 5 columns (0, 1, 2, 3, 4)
	int sps_int_size;
	int sps_iArray_size;
	int sps_iArray_num_elements, sps_iArray_num_rows, sps_iArray_num_columns;
	int sps_i, sps_j;

	//code
	printf("\n\n");

	sps_int_size = sizeof(int);

	sps_iArray_size = sizeof(sps_iArray);
	printf("Size Of Two Dimentional (2D) Integer Array Is = %d\n\n", sps_iArray_size);

	sps_iArray_num_rows = sps_iArray_size / sizeof(sps_iArray[0]);
	printf("Number of Rows In Two Dimentional (2D) Integer Array Is = %d\n\n", sps_iArray_num_rows);

	sps_iArray_num_columns = sizeof(sps_iArray[0]) / sps_int_size;
	printf("Number of Columns In Two Dimentional (2D) Integer Array Is = %d\n\n", sps_iArray_num_columns);

	sps_iArray_num_elements = sps_iArray_num_rows * sps_iArray_num_columns;
	printf("Number of Elements In Two Dimentional (2D) Integer Array Is = %d\n\n", sps_iArray_num_elements);

	printf("\n\n");
	printf("Elements In The 2D Array : \n\n");

	//**** Piece-Meal Assignment ****
	//****** ROW 1 *****
	sps_iArray[0][0] = 21;
	sps_iArray[0][1] = 42;
	sps_iArray[0][2] = 63;
	sps_iArray[0][3] = 84;
	sps_iArray[0][4] = 105;

	//***** ROW 2 *****
	sps_iArray[1][0] = 22;
	sps_iArray[1][1] = 44;
	sps_iArray[1][2] = 66;
	sps_iArray[1][3] = 88;
	sps_iArray[1][4] = 110;

	//***** ROW 3 *****
	sps_iArray[2][0] = 23;
	sps_iArray[2][1] = 46;
	sps_iArray[2][2] = 69;
	sps_iArray[2][3] = 92;
	sps_iArray[2][4] = 115;

	//*** Display ***
	for (sps_i = 0; sps_i < sps_iArray_num_rows; sps_i++)
	{
		printf("****** ROW %d ******\n", (sps_i + 1));
		for (sps_j = 0; sps_j < sps_iArray_num_columns; sps_j++)
		{
			printf("sps_iArray[%d][%d] = %d\n", sps_i, sps_j, sps_iArray[sps_i][sps_j]);
		}
		printf("\n\n");
	}

	return(0);
}

