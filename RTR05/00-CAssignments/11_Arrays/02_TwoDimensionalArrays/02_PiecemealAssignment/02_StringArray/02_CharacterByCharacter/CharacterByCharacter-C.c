#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//variable declarations

	//*** A 'STRING' IS AN ARRAY OF CHARACTERS.. SO char[] IS A char ARRAY AND HENCE, CHAR[] IS A 'STRING'***
	//*** AN ARRAY OF char ARRAYS IS AN ARRAY OF STRINGS!!! ***
	//*** HENCE , char[] IS ONE char ARRAY AND HENCE, IS ONE STRING ***
	//*** HENCE, char[][] IS AN ARRAY OF char ARRAYS AND HENCE, IS AN ARRAY OF STRINGS ***

	//HERE, THE STRING ARRAY CAN ALLOW A MAXIMUM NUMBER OF 10 STRINGS (10  ROWS) AND EACH OF THESE 10 STRINGS CAN HAVE ONLY UPTO 15 CHARACTERS MAXIMUM (15 COLUMNS)
	char strArray[5][10]; //5 rows (0, 1, 2, 3, 4) -> 5 strings (each string can have a maximum of 10 characters)
	int sps_char_size;
	int sps_strArray_size;
	int sps_strArray_num_elements, sps_strArray_num_rows, sps_strArray_num_columns;
	int sps_i;

	//code
	printf("\n\n");

	sps_char_size = sizeof(char);

	sps_strArray_size = sizeof(strArray);
	printf("Size Of Two Dimentional (2D) Character Array (String Array) Is = %d\n\n", sps_strArray_size);

	sps_strArray_num_rows = sps_strArray_size / sizeof(strArray[0]);
	printf("Number of Rows (Strings) In Two Dimentional (2D) Character Array (String Array) Is = %d\n\n", sps_strArray_num_rows);

	sps_strArray_num_columns = sizeof(strArray[0]) / sps_char_size;
	printf("Number of Columns In Two Dimentional (2D) Character Array (String Array) Is = %d\n\n", sps_strArray_num_columns);

	sps_strArray_num_elements = sps_strArray_num_rows * sps_strArray_num_columns;
	printf("Maximum Number of Elements (Characters) In Two Dimentional (2D) Character Array (String Array) Is = %d\n\n", sps_strArray_num_elements);

	//Piece-meal assignment
	//**** ROW 1 / STRING 1 *****
	strArray[0][0] = 'M';
	strArray[0][1] = 'y';
	strArray[0][2] = '\0'; //Null-terminating character

	//**** ROW 2 / STRING 2 *****
	strArray[1][0] = 'N';
	strArray[1][1] = 'a';
	strArray[1][2] = 'm';
	strArray[1][3] = 'e';
	strArray[1][4] = '\0'; //Null-terminating character

	//**** ROW 3 / STRING 3 *****
	strArray[2][0] = 'I';
	strArray[2][1] = 's';
	strArray[2][2] = '\0'; //Null-terminating character

	//**** ROW 4 / STRING 4 *****
	strArray[3][0] = 'S';
	strArray[3][1] = 'm';
	strArray[3][2] = 'i';
	strArray[3][3] = 't';
	strArray[3][4] = 'a';
	strArray[3][5] = '\0'; //Null-terminating character	
	
	//**** ROW 5 / STRING 5 *****
	strArray[4][0] = 'S';
	strArray[4][1] = 'a';
	strArray[4][2] = 'w';
	strArray[4][3] = 'a';
	strArray[4][4] = 'n';
	strArray[4][5] = 't';
	strArray[4][6] = '\0'; //Null-terminating character


	printf("\n\n");
	printf("The Strings In the 2D Character Array Are : \n\n");

	for (sps_i = 0; sps_i < sps_strArray_num_rows; sps_i++)
		printf("%s", strArray[sps_i]);

	printf("\n\n");

	return(0);
}

int MyStrlen(char str[])
{
	//variable declarations
	int sps_j;
	int sps_string_length = 0;

	//code
	//*** Determining Exact Length Of The String, By Detecting The First Occurence Of Null-Terminating Character (\0)***
	for (sps_j = 0; sps_j < MAX_STRING_LENGTH; sps_j++)
	{
		if (str[sps_j] == '\0')
			break;
		else
			sps_string_length++;
	}
	return(sps_string_length);
}

