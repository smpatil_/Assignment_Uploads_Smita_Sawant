#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	void MyStrcpy(char[], char []);

	//variable declarations

	//*** A 'STRING' IS AN ARRAY OF CHARACTERS.. SO char[] IS A char ARRAY AND HENCE, CHAR[] IS A 'STRING'***
	//*** AN ARRAY OF char ARRAYS IS AN ARRAY OF STRINGS!!! ***
	//*** HENCE , char[] IS ONE char ARRAY AND HENCE, IS ONE STRING ***
	//*** HENCE, char[][] IS AN ARRAY OF char ARRAYS AND HENCE, IS AN ARRAY OF STRINGS ***

	//HERE, THE STRING ARRAY CAN ALLOW A MAXIMUM NUMBER OF 10 STRINGS (10  ROWS) AND EACH OF THESE 10 STRINGS CAN HAVE ONLY UPTO 15 CHARACTERS MAXIMUM (15 COLUMNS)
	char strArray[5][10]; //5 rows (0, 1, 2, 3, 4) -> 5 strings (each string can have a maximum of 10 characters)
	int sps_char_size;
	int sps_strArray_size;
	int sps_strArray_num_elements, sps_strArray_num_rows, sps_strArray_num_columns;
	int sps_i;

	//code
	printf("\n\n");

	sps_char_size = sizeof(char);

	sps_strArray_size = sizeof(strArray);
	printf("Size Of Two Dimentional (2D) Character Array (String Array) Is = %d\n\n", sps_strArray_size);

	sps_strArray_num_rows = sps_strArray_size / sizeof(strArray[0]);
	printf("Number of Rows (Strings) In Two Dimentional (2D) Character Array (String Array) Is = %d\n\n", sps_strArray_num_rows);

	sps_strArray_num_columns = sizeof(strArray[0]) / sps_char_size;
	printf("Number of Columns In Two Dimentional (2D) Character Array (String Array) Is = %d\n\n", sps_strArray_num_columns);

	sps_strArray_num_elements = sps_strArray_num_rows * sps_strArray_num_columns;
	printf("Maximum Number of Elements (Characters) In Two Dimentional (2D) Character Array (String Array) Is = %d\n\n", sps_strArray_num_elements);

	//Piece-meal assignment
	MyStrcpy(strArray[0], "My");
	MyStrcpy(strArray[1], " Name");
	MyStrcpy(strArray[2], " Is");
	MyStrcpy(strArray[3], " Smita");
	MyStrcpy(strArray[4], " Sawant");

	printf("\n\n");
	printf("The Strings In the 2D Character Array Are : \n\n");

	for (sps_i = 0; sps_i < sps_strArray_num_rows; sps_i++)
		printf("%s", strArray[sps_i]);

	printf("\n\n");
	
	return(0);
}

void MyStrcpy(char str_destination[], char str_source[])
{
	//function prototype
	int MyStrlen(char[]);

	//variable declarations
	int i_StringLength = 0;
	int sps_j;

	//code
	i_StringLength = MyStrlen(str_source);
	for (sps_j = 0; sps_j < i_StringLength; sps_j++)
		str_destination[sps_j] = str_source[sps_j];

	str_destination[sps_j] = '\0';
}

int MyStrlen(char str[])
{
	//variable declarations
	int sps_j;
	int sps_string_length = 0;

	//code
	//*** Determining Exact Length Of The String, By Detecting The First Occurence Of Null-Terminating Character (\0)***
	for (sps_j = 0; sps_j < MAX_STRING_LENGTH; sps_j++)
	{
		if (str[sps_j] == '\0')
			break;
		else
			sps_string_length++;
	}
	return(sps_string_length);
}

