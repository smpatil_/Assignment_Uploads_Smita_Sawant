#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	int MyStrlen(char[]);

	//variable declarations

	//*** A 'STRING' IS AN ARRAY OF CHARACTERS.. SO char[] IS A char ARRAY AND HENCE, CHAR[] IS A 'STRING'***
	//*** AN ARRAY OF char ARRAYS IS AN ARRAY OF STRINGS!!! ***
	//*** HENCE , char[] IS ONE char ARRAY AND HENCE, IS ONE STRING ***
	//*** HENCE, char[][] IS AN ARRAY OF char ARRAYS AND HENCE, IS AN ARRAY OF STRINGS ***

	//HERE, THE STRING ARRAY CAN ALLOW A MAXIMUM NUMBER OF 10 STRINGS (10  ROWS) AND EACH OF THESE 10 STRINGS CAN HAVE ONLY UPTO 15 CHARACTERS MAXIMUM (15 COLUMNS)
	char strArray[10][15] = { "Hello!", "Welcome", "To", "Real", "Time", "Rendering", "Batch", "(2023-24)", "Of", "ASTROMEDICOMP." }; //In-line initialization
	int sps_iStrLengths[10]; //1D Integer Array - Stores lengths of those strings at corresponding indices in 'strArray[]' e.g.: sps_iStrLengths[0] will be the length of string at strArray[0], sps_iStrLengths[1] will be the length of string st strArray[1]... 10 strings, 10 lengths...
	int sps_strArray_size;
	int sps_strArray_num_rows, sps_strArray_num_columns;
	int sps_i, sps_j;

	//code
	sps_strArray_size = sizeof(strArray);
	sps_strArray_num_rows = sps_strArray_size / sizeof(strArray[0]);

	//storing in lengths of all the strings...
	for (sps_i = 0; sps_i < sps_strArray_num_rows; sps_i++)
		sps_iStrLengths[sps_i] = MyStrlen(strArray[sps_i]);

	printf("\n\n");
	printf("The Entire String Array : \n\n");
	for (sps_i = 0; sps_i < sps_strArray_num_rows; sps_i++)
		printf("%s", strArray[sps_i]);

	printf("\n\n");
	printf("Strings In The 2D Array : \n\n");

	//Since, char[][] is an array of strings, referencing only by the row number (first []) will give the row or the string
	//The Column Number (second []) is the particular character in that string/ row
	for (sps_i = 0; sps_i < sps_strArray_num_rows; sps_i++)
	{
		printf("String Number %d => %s\n\n", (sps_i + 1), strArray[sps_i]);
		for (sps_j = 0; sps_j < sps_iStrLengths[sps_i]; sps_j++)
		{
			printf("Character %d = %c\n", (sps_j + 1), strArray[sps_i][sps_j]);
		}
		printf("\n\n");
	}
	return(0);
}

int MyStrlen(char str[])
{
	//Variable Declarations
	int sps_j;
	int sps_string_length = 0;

	//code
	//*** Determining Exact Length Of the string by detecting the first occurence of Null-Terminator, character (\0) ***
	for (sps_j = 0; sps_j < MAX_STRING_LENGTH; sps_j++)
	{
		if (str[sps_j] == '\0')
			break;
		else
			sps_string_length++;
	}
	return(sps_string_length);
}

