#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	int MyStrlen(char[]);

	//variable declarations

	//*** A 'STRING' IS AN ARRAY OF CHARACTERS.. SO char[] IS A char ARRAY AND HENCE, CHAR[] IS A 'STRING'***
	//*** AN ARRAY OF char ARRAYS IS AN ARRAY OF STRINGS!!! ***
	//*** HENCE , char[] IS ONE char ARRAY AND HENCE, IS ONE STRING ***
	//*** HENCE, char[][] IS AN ARRAY OF char ARRAYS AND HENCE, IS AN ARRAY OF STRINGS ***

	//HERE, THE STRING ARRAY CAN ALLOW A MAXIMUM NUMBER OF 10 STRINGS (10  ROWS) AND EACH OF THESE 10 STRINGS CAN HAVE ONLY UPTO 15 CHARACTERS MAXIMUM (15 COLUMNS)
	char strArray[10][15] = { "Hello!", "Welcome", "To", "Real", "Time", "Rendering", "Batch", "(2023-24)", "Of", "ASTROMEDICOMP." }; //In-line initialization
	int sps_char_size;
	int sps_strArray_size;
	int sps_strArray_num_elements, sps_strArray_num_rows, sps_strArray_num_columns;
	int sps_strActual_num_chars = 0;
	int sps_i;

	//code
	printf("\n\n");

	sps_char_size = sizeof(char);

	sps_strArray_size = sizeof(strArray);
	printf("Size Of Two Dimentional (2D) Character Array (String Array) Is = %d\n\n", sps_strArray_size);

	sps_strArray_num_rows = sps_strArray_size / sizeof(strArray[0]);
	printf("Number of Rows (Strings) In Two Dimentional (2D) Character Array (String Array) Is = %d\n\n", sps_strArray_num_rows);

	sps_strArray_num_columns = sizeof(strArray[0]) / sps_char_size;
	printf("Number of Columns In Two Dimentional (2D) Character Array (String Array) Is = %d\n\n", sps_strArray_num_columns);

	sps_strArray_num_elements = sps_strArray_num_rows * sps_strArray_num_columns;
	printf("Maximum Number of Elements (Characters) In Two Dimentional (2D) Character Array (String Array) Is = %d\n\n", sps_strArray_num_elements);

	for (sps_i = 0; sps_i < sps_strArray_num_rows; sps_i++)
	{
		sps_strActual_num_chars = sps_strActual_num_chars + MyStrlen(strArray[sps_i]);
	}
	printf("Actual Number Of Elements (Characters) In Two Dimentional (2D) Character Array (String Array) Is = %d\n\n", sps_strActual_num_chars);

	printf("\n\n");
	printf("Strings In The 2D Array : \n\n");

	//since, char[][] is an array of strings, referencing only by the row number (first[]) will give the row or the string
	//The Column Number (second []) is the particular character in that string / row
	printf("%s", strArray[0]);
	printf("%s", strArray[1]);
	printf("%s", strArray[2]);
	printf("%s", strArray[3]);
	printf("%s", strArray[4]);
	printf("%s", strArray[5]);
	printf("%s", strArray[6]);
	printf("%s", strArray[7]);
	printf("%s", strArray[8]);
	printf("%s\n\n", strArray[9]);

	return(0);
}

int MyStrlen(char str[])
{
	//variable declarations
	int sps_j;
	int sps_string_length = 0;

	//code
	//*** Determining Exact Length Of The String, By Detecting The First Occurence Of Null-Terminating Character (\0)***
	for (sps_j = 0; sps_j < MAX_STRING_LENGTH; sps_j++)
	{
		if (str[sps_j] == '\0')
			break;
		else
			sps_string_length++;
	}
	return(sps_string_length);
}

