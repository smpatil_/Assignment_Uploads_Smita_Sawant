#include <stdio.h>
int main(void)
{
	//variable declarations
	int sps_iArray[5][3] = { {1, 2, 3}, {2, 4, 6}, {3, 6, 9}, {4, 8, 12}, {5, 10, 15} }; //In-line Initialization
	int sps_int_size;
	int sps_iArray_size;
	int sps_iArray_num_elements, sps_iArray_num_rows, sps_iArray_num_columns;

	//code
	printf("\n\n");

	sps_int_size = sizeof(int);

	sps_iArray_size = sizeof(sps_iArray);
	printf("Size Of Two Dimentional (2D) Integer Array Is = %d\n\n", sps_iArray_size);

	sps_iArray_num_rows = sps_iArray_size / sizeof(sps_iArray[0]);
	printf("Number of Rows In Two Dimentional (2D) Integer Array Is = %d\n\n", sps_iArray_num_rows);

	sps_iArray_num_columns = sizeof(sps_iArray[0]) / sps_int_size;
	printf("Number of Columns In Two Dimentional (2D) Integer Array Is = %d\n\n", sps_iArray_num_columns);

	sps_iArray_num_elements = sps_iArray_num_rows * sps_iArray_num_columns;
	printf("Number of Elements In Two Dimentional (2D) Integer Array Is = %d\n\n", sps_iArray_num_elements);

	printf("\n\n");
	printf("Elements In The 2D Array : \n\n");

	//*** Array Indices begin from 0, hence, 1st row is actually 0th row and 1st column is actually 0th column ***

	//*** Row 1 ***
	printf("****** ROW 1 ******\n");
	printf("iArray[0][0] = %d\n", sps_iArray[0][0]); //*** Column 1 *** (0th Element) => 1
	printf("iArray[0][1] = %d\n", sps_iArray[0][1]); //*** Column 2 *** (1st Element) => 2
	printf("iArray[0][2] = %d\n", sps_iArray[0][2]); //*** Column 3 *** (2nd Element) => 3

	printf("\n\n");

	//*** Row 2 ***
	printf("****** ROW 2 ******\n");
	printf("iArray[1][0] = %d\n", sps_iArray[1][0]); //*** Column 1 *** (0th Element) => 2
	printf("iArray[1][1] = %d\n", sps_iArray[1][1]); //*** Column 2 *** (1st Element) => 4
	printf("iArray[1][2] = %d\n", sps_iArray[1][0]); //*** Column 3 *** (2nd Element) => 6

	printf("\n\n");

	//*** Row 3 ***
	printf("****** ROW 3 ******\n");
	printf("iArray[2][0] = %d\n", sps_iArray[2][0]); //*** Column 1 *** (0th Element) => 3
	printf("iArray[2][1] = %d\n", sps_iArray[2][1]); //*** Column 2 *** (1st Element) => 6
	printf("iArray[2][2] = %d\n", sps_iArray[2][2]); //*** Column 3 *** (2nd Element) => 9printf("\n\n");

	printf("\n\n");

	//*** Row 4 ***
	printf("****** ROW 4 ******\n");
	printf("iArray[3][0] = %d\n", sps_iArray[3][0]); //*** Column 1 *** (0th Element) => 4
	printf("iArray[3][1] = %d\n", sps_iArray[3][1]); //*** Column 2 *** (1st Element) => 8
	printf("iArray[3][2] = %d\n", sps_iArray[3][2]); //*** Column 3 *** (2nd Element) => 12

	printf("\n\n");

	//*** Row 5 ***
	printf("****** ROW 5 ******\n");
	printf("iArray[4][0] = %d\n", sps_iArray[4][0]); //*** Column 1 *** (0th Element) => 5
	printf("iArray[4][1] = %d\n", sps_iArray[4][1]); //*** Column 2 *** (1st Element) => 10
	printf("iArray[4][2] = %d\n", sps_iArray[4][2]); //*** Column 3 *** (2nd Element) => 15

	printf("\n\n");

	return(0);
}
