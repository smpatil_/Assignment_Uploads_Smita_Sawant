#include <stdio.h>
int main(void)
{
	//variable declarations
	int sps_iArray[5][3] = { {1, 2, 3}, {2, 4, 6}, {3, 6, 9}, {4, 8, 12}, {5, 10, 15} }; //In-line Initialization
	int sps_int_size;
	int sps_iArray_size;
	int sps_iArray_num_elements, sps_iArray_num_rows, sps_iArray_num_columns;
	int sps_i, sps_j;

	//code
	printf("\n\n");

	sps_int_size = sizeof(int);

	sps_iArray_size = sizeof(sps_iArray);
	printf("Size Of Two Dimentional (2D) Integer Array Is = %d\n\n", sps_iArray_size);

	sps_iArray_num_rows = sps_iArray_size / sizeof(sps_iArray[0]);
	printf("Number of Rows In Two Dimentional (2D) Integer Array Is = %d\n\n", sps_iArray_num_rows);

	sps_iArray_num_columns = sizeof(sps_iArray[0]) / sps_int_size;
	printf("Number of Columns In Two Dimentional (2D) Integer Array Is = %d\n\n", sps_iArray_num_columns);

	sps_iArray_num_elements = sps_iArray_num_rows * sps_iArray_num_columns;
	printf("Number of Elements In Two Dimentional (2D) Integer Array Is = %d\n\n", sps_iArray_num_elements);

	printf("\n\n");
	printf("Elements In The 2D Array : \n\n");

	//*** Array Indices Begin From 0, Hence, 1st Row Is Actually 0th Row and 1st Column Is Actually 0th Column ***
	for (sps_i = 0; sps_i < sps_iArray_num_rows; sps_i++)
	{
		printf("***** ROW %d *****\n", (sps_i + 1));
		for (sps_j = 0; sps_j < sps_iArray_num_columns; sps_j++)
		{	
			printf("sps_iArray[%d][%d] = %d\n", sps_i, sps_j, sps_iArray[sps_i][sps_j]);
		}
		printf("\n\n");
	}

	return(0);
}

