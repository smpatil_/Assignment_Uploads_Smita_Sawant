#include <stdio.h>

#define NUM_ELEMENTS 10

int main(void)
{
	//variable declarations
	int sps_iArray[NUM_ELEMENTS];
	int sps_i, sps_num, sps_j, count = 0;

	//code
	printf("\n\n");

	// **** Array Elements Inputs ****
	printf("Enter Integer Elements For Array : \n\n");
	for (sps_i = 0; sps_i < NUM_ELEMENTS; sps_i++)
	{
		scanf("%d", &sps_num);

		//If 'num' is negative (< 0), then convert it to positive (multiply by -1)

		if (sps_num < 0)
			sps_num = -1 * sps_num;

		sps_iArray[sps_i] = sps_num;
	}

	// *** Printing Entire Array ***
	printf("\n\n");
	printf("Array Elements Are : \n\n");
	for (sps_i = 0; sps_i < NUM_ELEMENTS; sps_i++)
		printf("% d\n", sps_iArray[sps_i]);
	
	//**** Seperating Out Prime Numbers From Array Elements ****
	printf("\n\n");
	printf("Prime Numbers Amongst The Array Elements Are : \n\n");
	for (sps_i = 0; sps_i < NUM_ELEMENTS; sps_i++)
	{
		for (sps_j = 1; sps_j <= sps_iArray[sps_i]; sps_j++)
		{
			if ((sps_iArray[sps_i] % sps_j) == 0)
				count++;
		}

		//Number 1 is neither a prime number nor a constant
		//If a number is prime, it is only divisible by 1 and itself.
		//Hence, if a number is prime, the value of 'count' will be exactly 2.
		//If the value of 'count' is greater than 2, the number is divisible by numbers other than 1 and itself and hence, it is not prime.
		//The value of 'count' will be 1 only if sps_iArray[sps_i] is 1.
		if (count == 2)
			printf("%d\n", sps_iArray[sps_i]);

		count = 0; //Reset 'count' to 0 for checking the next number...
	}

	return(0);
}

