#include <stdio.h>

#define NUM_ELEMENTS 10

int main(void)
{
	//variables declarations
	int sps_iArray[NUM_ELEMENTS];
	int sps_i, sps_num, sum = 0;

	//code
	printf("\n\n");

	//*** Array Elements Input ***
	printf("Enter Integer Elements For Array : \n\n");
	for (sps_i = 0; sps_i < NUM_ELEMENTS; sps_i++)
	{
		scanf("%d", &sps_num);
		sps_iArray[sps_i] = sps_num;
	}

	//*** Separating Out Even Numbers From Array Elements ***
	printf("\n\n");
	printf("Even Numbers Amongst The Array Elements Are : \n\n");
	for (sps_i = 0; sps_i < NUM_ELEMENTS; sps_i++)
	{
		if ((sps_iArray[sps_i] % 2) == 0)
			printf("%d\n", sps_iArray[sps_i]);
	}

	//*** Separating Out Odd Numbers From Array Elements ***
	printf("\n\n");
	printf("Odd Numbers Amongst The Array Elements Are : \n\n");
	for (sps_i = 0; sps_i < NUM_ELEMENTS; sps_i++)
	{
		if ((sps_iArray[sps_i] % 2) != 0)
			printf("%d\n", sps_iArray[sps_i]);
	}

	return(0);
}

