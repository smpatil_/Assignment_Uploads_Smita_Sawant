#include <stdio.h>

#define NUM_ELEMENTS 10

int main(void)
{
	//variable declarations
	int sps_iArray[NUM_ELEMENTS];
	int sps_i, sps_num, sum = 0;

	//code
	printf("\n\n");
	printf("Enter Integer Elements For Array : \n\n");
	for (sps_i = 0; sps_i < NUM_ELEMENTS; sps_i++)
	{
		scanf("%d", &sps_num);
		sps_iArray[sps_i] = sps_num;
	}

	for (sps_i = 0; sps_i < NUM_ELEMENTS; sps_i++)
	{
		sum = sum + sps_iArray[sps_i];
	}

	printf("\n\n");
	printf("Sum Of All Elements Of Array = %d\n\n", sum);

	return(0);
}
