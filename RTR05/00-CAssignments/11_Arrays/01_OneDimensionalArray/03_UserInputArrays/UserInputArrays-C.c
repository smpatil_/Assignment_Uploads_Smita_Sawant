#include <stdio.h>

//MACRO CONSTANT USED AS ARRAY SIZE IN SUBCRIPTION AND AS ARRAY LENGTH.
//HENCE THIS PROGRAM'S ARRAYS' SIZES CAN BE CHANGED BY SIMPLY CHANGING THE FOLLOWING 3 GLOBAL MACRO CONSTANT VALUES, BEFORE COMPILING, LINKING AND EXECUTING THE PROGRAM !

#define INT_ARRAY_NUM_ELEMENTS 5
#define FLOAT_ARRAY_NUM_ELEMENTS 3
#define CHAR_ARRAY_NUM_ELEMENTS 15

int main(void)
{
	//variable declarations
	int sps_iArray[INT_ARRAY_NUM_ELEMENTS];
	float sps_fArray[FLOAT_ARRAY_NUM_ELEMENTS];
	char sps_cArray[CHAR_ARRAY_NUM_ELEMENTS];
	int sps_i, sps_num;

	//code

	//***** Array Elements Input ******
	printf("\n\n");
	printf("Enter Elements For 'Integer' Array : \n");
	for (sps_i = 0; sps_i < INT_ARRAY_NUM_ELEMENTS; sps_i++)
		scanf("%d", &sps_iArray[sps_i]);

	printf("\n\n");
	printf("Enter Elements For 'Floating-Point' Array : \n");
	for (sps_i = 0; sps_i < FLOAT_ARRAY_NUM_ELEMENTS; sps_i++)
		scanf("%f", &sps_fArray[sps_i]);

	printf("\n\n");
	printf("Enter Elements For 'Character' Array : \n");
	for (sps_i = 0; sps_i < CHAR_ARRAY_NUM_ELEMENTS; sps_i++)
	{
		sps_cArray[sps_i] = getch();
		printf("%c\n", sps_cArray[sps_i]);
	}

	//****** Array Elements Output ******
	printf("\n\n");
	printf("Integer Array Entered By You : \n\n");
	for (sps_i = 0; sps_i < INT_ARRAY_NUM_ELEMENTS; sps_i++)
		printf("%d\n", sps_iArray[sps_i]);

	printf("\n\n");
	printf("Floating-Point Array Entered By You : \n\n");
	for (sps_i = 0; sps_i < FLOAT_ARRAY_NUM_ELEMENTS; sps_i++)
		printf("%f\n", sps_fArray[sps_i]);

	printf("\n\n");
	printf("Character Array Entered By You : \n\n");
	for (sps_i = 0; sps_i < CHAR_ARRAY_NUM_ELEMENTS; sps_i++)
		printf("%c\n", sps_cArray[sps_i]);

	return(0);
}
