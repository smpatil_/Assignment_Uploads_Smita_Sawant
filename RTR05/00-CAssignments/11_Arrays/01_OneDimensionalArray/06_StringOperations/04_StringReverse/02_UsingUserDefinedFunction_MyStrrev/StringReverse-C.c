#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	void MyStrrev(char[], char[]);
	
	//variable declarations
	char chArray_Original[MAX_STRING_LENGTH], chArray_Reversed[MAX_STRING_LENGTH]; //A character array is a String

	//code

	//*** String Input ***
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(chArray_Original, MAX_STRING_LENGTH);

	//*** String Reverse ***
	MyStrrev(chArray_Reversed, chArray_Original);

	//*** String Output ***
	printf("\n\n");
	printf("The Original String Entered By You (i.e : 'chArray_Original[]') Is : \n\n");
	printf("%s\n", chArray_Original);

	printf("\n\n");
	printf("The Reversed String (i.e. : 'chArray_Reverse[]' Is : \n\n");
	printf("%s\n", chArray_Reversed);

	return(0);
}

void MyStrrev(char str_destination[], char str_source[])
{
	//function prototype
	int MyStrlen(char[]);

	//variable declarations
	int iStringLength = 0;
	int sps_i, sps_j, sps_len;

	//code
	iStringLength = MyStrlen(str_source);

	//Array Indices Begin From 0, Hence, Last Index Will Always Be (Length -1)
	sps_len = iStringLength - 1;

	//We need to put the character which is at last Index of 'str_source' to the first index of 'str_destination'
	//And second-last character of 'str_source' to the second character of 'str_destination' and so on...
	for (sps_i = 0, sps_j = sps_len; sps_i < iStringLength, sps_j >= 0; sps_i++, sps_j--)
	{
		str_destination[sps_i] = str_source[sps_j];
	}

	str_destination[sps_i] = '\0';
}

int MyStrlen(char str[])
{
	//variable declarations
	int sps_j;
	int string_length = 0;

	//code
	//**** Determining Exact Length Of the String, by Detecting The First Ocuurence of Null-Terminating character (\0) ****
	for (sps_j = 0; sps_j < MAX_STRING_LENGTH; sps_j++)
	{
		if (str[sps_j] == '\0')
			break;
		else
			string_length++;
	}
	return(string_length);
}
