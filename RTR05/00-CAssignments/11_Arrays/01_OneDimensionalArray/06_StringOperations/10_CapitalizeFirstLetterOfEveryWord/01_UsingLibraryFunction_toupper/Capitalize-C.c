#include <stdio.h>
#include <ctype.h> //for toupper()

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	int MyStrlen(char[]);
	

	//variable declarations
	char chArray[MAX_STRING_LENGTH], chArray_CapitalizedFirstLetterOfEveryWord[MAX_STRING_LENGTH]; //A character array is a string
	int iStringLength;
	int sps_i, sps_j;

	//code

	//*** String Input ***
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(chArray, MAX_STRING_LENGTH);

	iStringLength = MyStrlen(chArray);
	sps_j = 0;
	for (sps_i = 0; sps_i < iStringLength; sps_i++)
	{
		if (sps_i == 0)
			chArray_CapitalizedFirstLetterOfEveryWord[sps_j] = toupper(chArray[sps_i]);

		else if (chArray[sps_i] == ' ')
		{
			chArray_CapitalizedFirstLetterOfEveryWord[sps_j] = chArray[sps_i];
			chArray_CapitalizedFirstLetterOfEveryWord[sps_j + 1] = toupper(chArray[sps_i + 1]);

			//since already two characters (at indices 'sps_i' & 'sps_i + 1' have been considered in this else-if block... We are extra-incrementing 'sps_i' and 'sps_j' by 1
			sps_j++;
			sps_i++;
		}

		else
			chArray_CapitalizedFirstLetterOfEveryWord[sps_j] = chArray[sps_i];

		sps_j++;
	}

	chArray_CapitalizedFirstLetterOfEveryWord[sps_j] = '\0';

	//*** String Output ***
	printf("\n\n");
	printf("String Entered By You Is : \n\n");
	printf("%s\n", chArray);

	printf("\n\n");
	printf("String After Capitalizing First Letter Of Every Word : \n\n");
	printf("%s\n", chArray_CapitalizedFirstLetterOfEveryWord);

	return(0);
}

int MyStrlen(char str[])
{
	//variable declarations
	int sps_j;
	int string_length = 0;

	//code
	//*** Determining exact length of the string, by detecting the first occurence of null-terminating character (\0) ***
	for (sps_j = 0; sps_j < MAX_STRING_LENGTH; sps_j++)
	{
		if (str[sps_j] == '\0')
			break;
		else
			string_length++;
	}
	return(string_length);
}
