#include <stdio.h>

#define MAX_STRING_LENGTH 512

#define SPACE ' '

#define FULLSTOP '.'
#define COMMA ','
#define EXCLAMATION '!'
#define QUESTION_MARK '?'

int main(void)
{
	//function prototype
	int MyStrlen(char[]);
	char MyToUpper(char);

	//variable declarations
	char chArray[MAX_STRING_LENGTH], chArray_CapitalizedFirstLetterOfEveryWord[MAX_STRING_LENGTH]; //A character array is a string
	int iStringLength;
	int sps_i, sps_j;

	//code

	//*** String Input ***
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(chArray, MAX_STRING_LENGTH);

	iStringLength = MyStrlen(chArray);
	sps_j = 0;
	for (sps_i = 0; sps_i < iStringLength; sps_i++)
	{
		if (sps_i == 0) //First letter of any sentense must bea capital letter
			chArray_CapitalizedFirstLetterOfEveryWord[sps_j] = MyToUpper(chArray[sps_i]);

		else if (chArray[sps_i] == SPACE) //First letter of every word in the sentence must be a capital letter. Words are seperated by spaces.
		{
			chArray_CapitalizedFirstLetterOfEveryWord[sps_j] = chArray[sps_i];
			chArray_CapitalizedFirstLetterOfEveryWord[sps_j + 1] = toupper(chArray[sps_i + 1]);

			//since already two characters (at indices 'sps_i' & 'sps_i + 1' have been considered in this else-if block... We are extra-incrementing 'sps_i' and 'sps_j' by 1
			sps_j++;
			sps_i++;
		}

		else if ((chArray[sps_i] == FULLSTOP || chArray[sps_i] == COMMA || chArray[sps_i] == EXCLAMATION || chArray[sps_i] == QUESTION_MARK) && (chArray[sps_i] != SPACE)) //first letter of every word after punctuation mark, in the sentence must be a capital letter. Words are separated by punctuation.
		{
			chArray_CapitalizedFirstLetterOfEveryWord[sps_j] = chArray[sps_i];
			chArray_CapitalizedFirstLetterOfEveryWord[sps_j + 1] = SPACE;
			chArray_CapitalizedFirstLetterOfEveryWord[sps_j + 2] = MyToUpper(chArray[sps_i + 1]);

			//since already two characters (at indices 'sps_i' & 'sps_i + 1' have been considered in this else-if block... We are extra-incrementing 'sps_i' by 1.
			////since already three characters (at indices 'sps_j' & 'sps_j + 1' and 'sps_j + 2' have been considered in this else-if block... We are extra-incrementing 'sps_j' by 2
			sps_j = sps_j + 2;
			sps_i++;
		}

		else
			chArray_CapitalizedFirstLetterOfEveryWord[sps_j] = chArray[sps_i];

		sps_j++;
	}

	chArray_CapitalizedFirstLetterOfEveryWord[sps_j] = '\0';

	//*** String Output ***
	printf("\n\n");
	printf("String Entered By You Is : \n\n");
	printf("%s\n", chArray);

	printf("\n\n");
	printf("String After Capitalizing First Letter Of Every Word : \n\n");
	printf("%s\n", chArray_CapitalizedFirstLetterOfEveryWord);

	return(0);
}

int MyStrlen(char str[])
{
	//variable declarations
	int sps_j;
	int string_length = 0;

	//code
	//*** Determining exact length of the string, by detecting the first occurence of null-terminating character (\0) ***
	for (sps_j = 0; sps_j < MAX_STRING_LENGTH; sps_j++)
	{
		if (str[sps_j] == '\0')
			break;
		else
			string_length++;
	}
	return(string_length);
}

char MyToUpper(char ch)
{
	//variable declarations
	int sps_num;
	int sps_c;

	//code

	// ASCII value of 'a' (97) - ASCII value of 'A'(65) = 32
	//This subtraction will give the exact difference between the upper nad lower case counterparts of each letter of the alphabet
	//If this difference is subtracted from the ASCII value of a lower case letter, the resultant ASCII value will be that of its upper case counterpart, hence, helping us to find its upper case letter!!!
	//ASCII Values of 'a' to 'z' => 97 to 122
	//ASCII Values of 'A' to 'Z' => 65 to 90

	sps_num = 'a' - 'A';

	if ((int)ch >= 97 && (int)ch <= 122)
	{
		sps_c = (int)ch - sps_num;
		return((char)sps_c);
	}
	
	else
		return(ch);
}

