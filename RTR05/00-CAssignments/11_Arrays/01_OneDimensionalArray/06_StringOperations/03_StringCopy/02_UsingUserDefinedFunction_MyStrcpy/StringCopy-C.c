#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	void MyStrcpy(char[], char[]);

	//variable declarations
	char chArray_Original[MAX_STRING_LENGTH], chArray_Copy[MAX_STRING_LENGTH]; //A character array is a string

	//code

	//*** String Input ***
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(chArray_Original, MAX_STRING_LENGTH);

	//*** String Copy ***
	MyStrcpy(chArray_Copy, chArray_Original);

	//*** String Output ***
	printf("\n\n");
	printf("The Original String Entered By You (i.e : 'chArray_Original[]') Is : \n\n");
	printf("%s\n", chArray_Original);

	printf("\n\n");
	printf("The Copied String (i.e. : 'chArray_Copy[]' Is : \n\n");
	printf("%s\n", chArray_Copy);

	return(0);
}

void MyStrcpy(char str_destination[], char str_source[])
{
	//function prototype
	int MyStrlen(char[]);

	//variable declarations
	int iStringLength = 0;
	int sps_j;

	//code
	iStringLength = MyStrlen(str_source);
	for (sps_j = 0; sps_j < iStringLength; sps_j++)
		str_destination[sps_j] = str_source[sps_j];

	str_destination[sps_j] = '\0';
}

int MyStrlen(char str[])
{
	//variable declarations
	int sps_j;
	int string_length = 0;

	//code
	//*** Determining exact length of the string, by detecting the first occurence  of null-terminating character (\0) ***
	for (sps_j = 0; sps_j < MAX_STRING_LENGTH; sps_j++)
	{
		if (str[sps_j] == '\0')
			break;
		else
			string_length++;
	}
	return(string_length);
}
