#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	void MyStrcat(char[], char[]);

	//variable declarations
	char chArray_One[MAX_STRING_LENGTH], chArray_Two[MAX_STRING_LENGTH]; //A character Array i a string

	//code

	//*** String Input ***
	printf("\n\n");
	printf("Enter First String : \n\n");
	gets_s(chArray_One, MAX_STRING_LENGTH);

	printf("\n\n");
	printf("Enter Second String : \n\n");
	gets_s(chArray_Two, MAX_STRING_LENGTH);

	//*** String Concat ***
	printf("\n\n");
	printf("****** Before Concatenation ******");
	printf("\n\n");
	printf("The Original First String Entered By You (i.e. : 'chArray_One[]' Is :  \n\n");
	printf("%s\n", chArray_One);

	printf("\n\n");
	printf("The Original Second String Entered By You (i.e. : 'chArray_Two[]') Is  : \n\n");
	printf("%s\n", chArray_Two);

	MyStrcat(chArray_One, chArray_Two);

	printf("\n\n");
	printf("***** After Concatenation ******");
	printf("\n\n");
	printf("'chArray_One[]' Is : \n\n");
	printf("%s\n", chArray_One);

	printf("\n\n");
	printf("'chArray_Two[]' Is : \n\n");
	printf("%s\n", chArray_Two);

	return(0);
}

void MyStrcat(char str_destination[], char str_source[])
{
	//function prototype
	int MyStrlen(char[]);

	//variable declarations
	int iStringLength_Source = 0, iStringLength_Destination = 0;
	int sps_i, sps_j;

	//code
	iStringLength_Source = MyStrlen(str_source);
	iStringLength_Destination = MyStrlen(str_destination);

	//Array Indices Begin From 0, Hence, Last Valid Index Of Array Will Always Be (Length -1)
	//So, Concatenation Must Begin From Index Number Equal To Length Of the Array 'str_destination'
	//We need to put the character which is at first index of 'str_source' to the (last index = 1) Of 'str_destination'
	for (sps_i = iStringLength_Destination, sps_j = 0; sps_j < iStringLength_Source; sps_i++, sps_j++)
	{
		str_destination[sps_i] = str_source[sps_j];
	}

	str_destination[sps_i] = '\0';
}

int MyStrlen(char str[])
{
	//variable declarations
	int sps_j;
	int string_length = 0;

	//code
	//*** Determining Exact Length Of the String, By Detecting The First Occurence Of Null-Terminating Character (\0) ***
	for (sps_j = 0; sps_j < MAX_STRING_LENGTH; sps_j++)
	{
		if (str[sps_j] == '\0')
			break;
		else
			string_length++;
	}
	return(string_length);
}

