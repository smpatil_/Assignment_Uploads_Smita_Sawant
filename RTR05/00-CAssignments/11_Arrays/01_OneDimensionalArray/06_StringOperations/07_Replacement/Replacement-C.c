//*** This program replaces all vowels in the input string with the *(asterisk) symbol ***
//*** For Example; Original String 'Dr. Vijay Dattatray Gokhale ASTROMEDICOMP' will become 'Dr. V*j*y D*tt*tr*y* G*kh*le *STR*M*D*C*MP'


#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	int MyStrlen(char[]);
	void MyStrcpy(char[], char[]);

	//variable declarations
	char chArray_Original[MAX_STRING_LENGTH], chArray_VowelsReplaced[MAX_STRING_LENGTH]; //A character Array is a string
	int iStringLength;
	int sps_i;

	//code

	//*** String Input ***
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(chArray_Original, MAX_STRING_LENGTH);

	//*** String Output ***
	MyStrcpy(chArray_VowelsReplaced, chArray_Original);

	iStringLength = MyStrlen(chArray_VowelsReplaced);

	for (sps_i = 0; sps_i < iStringLength; sps_i++)
	{
		switch (chArray_VowelsReplaced[sps_i])
		{
		case 'A':
		case 'a':
		case 'E':
		case 'e':
		case 'I':
		case 'i':
		case 'O':
		case 'o':
		case 'U':
		case 'u':
			chArray_VowelsReplaced[sps_i] = '*';
			break;
		default:
			break;
		}
	}

	//*** String Output ***
	printf("\n\n");
	printf("String Entered By You Is : \n\n");
	printf("%s\n", chArray_Original);

	printf("\n\n");
	printf("String After Replacement Of Vowels By * Is : \n\n");
	printf("%s\n", chArray_VowelsReplaced);

	return(0);
}

int MyStrlen(char str[])
{
	//variable declarations
	int sps_j;
	int string_length = 0;

	//code
	//*** Determining exact length of the string, By detecting the first occurence of null-terminating character (\0) ***
	for (sps_j = 0; sps_j < MAX_STRING_LENGTH; sps_j++)
	{
		if (str[sps_j] == '\0')
			break;
		else
			string_length++;
	}
	return(string_length);
}

void MyStrcpy(char str_destination[], char str_source[])
{
	//function prototype
	int MyStrlen(char[]);

	//variable declarations
	int iStringLength = 0;
	int sps_j;

	//code
	iStringLength = MyStrlen(str_source);
	for (sps_j = 0; sps_j < iStringLength; sps_j++)
		str_destination[sps_j] = str_source[sps_j];

	str_destination[sps_j] = '\0';
}

