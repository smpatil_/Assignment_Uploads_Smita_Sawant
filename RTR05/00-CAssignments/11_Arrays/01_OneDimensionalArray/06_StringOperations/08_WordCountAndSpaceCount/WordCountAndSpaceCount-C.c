#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	int MyStrlen(char[]);
	void MyStrcpy(char[], char[]);

	//variable declarations
	char chArray[MAX_STRING_LENGTH]; //A character Array is a string
	int iStringLength;
	int sps_i;
	int word_count = 0, space_count = 0;

	//code

	//*** String Input ***
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(chArray, MAX_STRING_LENGTH);

	iStringLength = MyStrlen(chArray);

	for (sps_i = 0; sps_i < iStringLength; sps_i++)
	{
		switch (chArray[sps_i])
		{
		case 32: //32 Is the ASCII value for space (' ') character
			space_count++;
			break;
		default:
			break;
		}
	}

	word_count = space_count + 1;

	//*** String Output ***
	printf("\n\n");
	printf("String Entered By You Is : \n\n");
	printf("%s\n", chArray);

	printf("\n\n");
	printf("Number Of Spaces In The Input String = %d\n\n", space_count);
	printf("Number Of Words In The Input String = %d\n\n", word_count);

	return(0);
}

int MyStrlen(char str[])
{
	//variable declarations
	int sps_j;
	int string_length = 0;

	//code
	//*** Determining exact length of the string, by detecting the first occurence of null-terminating character (\0) ***
	for (sps_j = 0; sps_j < MAX_STRING_LENGTH; sps_j++)
	{
		if (str[sps_j] == '\0')
			break;
		else
			string_length++;
	}
	return(string_length);
}

