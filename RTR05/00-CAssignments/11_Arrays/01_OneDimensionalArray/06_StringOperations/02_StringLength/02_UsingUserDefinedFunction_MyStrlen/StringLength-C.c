#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//function prototype
	int MyStrlen(char[]);

	//variable declarations
	char chArray[MAX_STRING_LENGTH]; //A character array is a string
	int iStringLength = 0;

	//code

	//*** String Inputs ***
	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(chArray, MAX_STRING_LENGTH);

	//*** String Output ***
	printf("\n\n");
	printf("String Entered By You Is : \n\n");
	printf("%s\n", chArray);

	//*** String Length ***
	printf("\n\n");
	iStringLength = MyStrlen(chArray);
	printf("Length Of String Is = %d Characters !!!\n\n", iStringLength);

	return(0);
}

int MyStrlen(char str[])
{
	//variable declarations
	int sps_j;
	int sps_string_length = 0;

	//code
	//*** Determining Exact Length Of The String, By Detecting The First Occurence Of Null-Terminating Character ( \0 ) ***
	for (sps_j = 0; sps_j < MAX_STRING_LENGTH; sps_j++)
	{
		if (str[sps_j] == '\0')
			break;
		else
			sps_string_length++;
	}
	return(sps_string_length);
}

