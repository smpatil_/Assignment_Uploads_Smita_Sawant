#include <stdio.h>
int main(void)
{
	//variable declarations
	int sps_iArray[] = { 9, 30, 6, 12, 98, 95, 20, 23, 2, 45 };
	int sps_int_size;
	int sps_iArray_size;
	int sps_iArray_num_elements;

	float sps_fArray[] = { 1.2f, 2.3f, 3.4f, 4.5f, 5.6f, 6.7f, 7.8f, 8.9f };
	int sps_float_size;
	int sps_fArray_size;
	int sps_fArray_num_elements;

	char sps_cArray[] = { 'A', 'S', 'T', 'R', 'O', 'M', 'E', 'D', 'I', 'C', 'O', 'M', 'P' };
	int sps_char_size;
	int sps_cArray_size;
	int sps_cArray_num_elements;

	int sps_i;

	//code

	//**** sps_iArray ****
	printf("\n\n");
	printf("In-line Initialization And Loop (for) Display Of Elements of Array 'sps_iArray[]' : \n\n");

	sps_int_size = sizeof(int);
	sps_iArray_size = sizeof(sps_iArray);
	sps_iArray_num_elements = sps_iArray_size / sps_int_size;

	for (sps_i = 0; sps_i < sps_iArray_num_elements; sps_i++)
	{
		printf("sps_iArray[%d] (Element %d) = %d\n", sps_i, (sps_i + 1), sps_iArray[sps_i]);
	}

	printf("\n\n");
	printf("Size Of Data type 'int'								= %d bytes \n", sps_int_size);
	printf("Number of Elements In 'int' Array 'sps_iArray[]'				= %d Elements\n", sps_iArray_num_elements);
	printf("Size Of Array 'sps_iArray' (%d Elements * %d Bytes)				= %d Bytes\n\n", sps_iArray_num_elements, sps_int_size, sps_iArray_size);

	//**** sps_fArray ****
	printf("\n\n");
	printf("In-line Initialization And Loop (while) Display Of Elements Of Array 'sps_fArray[]': \n\n");

	sps_float_size = sizeof(float);
	sps_fArray_size = sizeof(sps_fArray);
	sps_fArray_num_elements = sps_fArray_size / sps_float_size;


	for (sps_i = 0; sps_i < sps_fArray_num_elements; sps_i++)
	{
		printf("sps_fArray[%d] (Element %d) = %f\n", sps_i, (sps_i + 1), sps_fArray[sps_i]);
	}

	printf("\n\n");
	printf("Size Of Data Type 'float'							= %d Bytes\n", sps_float_size);
	printf("Number Of Elements In 'float' Array 'sps_fArray[]'				= %d Elements\n", sps_fArray_num_elements);
	printf("Size Of Array 'fArray[]' (%d Elements * %d Bytes)					= %d Bytes\n\n", sps_fArray_num_elements, sps_float_size, sps_fArray_size);

	//**** sps_cArray ****
	printf("\n\n");
	printf("In-line Initialization And Loop (while) Display Of Elements Of Array 'sps_cArray[]': \n\n");

	sps_char_size = sizeof(char);
	sps_cArray_size = sizeof(sps_cArray);
	sps_cArray_num_elements = sps_cArray_size / sps_char_size;

	for (sps_i = 0; sps_i < sps_cArray_num_elements; sps_i++)
	{
		printf("sps_cArray[%d] (Element %d) = %c\n", sps_i, (sps_i + 1), sps_cArray[sps_i]);
	}

	printf("\n\n");
	printf("Size Of Data Type 'char'							= %d bytes\n", sps_char_size);
	printf("Number Of Elements In 'char' Array 'cArray'					= %d Elements\n", sps_cArray_num_elements);
	printf("Size Of Array 'cArray[]' (%d Elements * %d Bytes)				= %d Bytes\n\n", sps_cArray_num_elements, sps_char_size, sps_cArray_size);

	return(0);

}