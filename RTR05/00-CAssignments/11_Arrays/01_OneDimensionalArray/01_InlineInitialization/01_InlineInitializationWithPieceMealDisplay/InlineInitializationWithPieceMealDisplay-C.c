#include <stdio.h>
int main(void)
{
	//variable declarations
	int sps_iArray[] = { 9, 30, 6, 12, 98, 95, 20, 23, 2, 45 };
	int sps_int_size;
	int sps_iArray_size;
	int sps_iArray_num_elements;

	float sps_fArray[] = { 1.2f, 2.3f, 3.4f, 4.5f, 5.6f, 6.7f, 7.8f, 8.9f };
	int sps_float_size;
	int sps_fArray_size;
	int sps_fArray_num_elements;

	char sps_cArray[] = { 'A', 'S', 'T', 'R', 'O', 'M', 'E', 'D', 'I', 'C', 'O', 'M', 'P' };
	int sps_char_size;
	int sps_cArray_size;
	int sps_cArray_num_elements;

	//code

	//****** sps_iArray[] ******
	printf("\n\n");
	printf("In-line Initialization And Piece-meal Display Of Elements of Array 'sps_iArray[]': \n\n");
	printf("sps_iArray[0] (1st Element)  = %d\n", sps_iArray[0]);
	printf("sps_iArray[1] (2nd Element)  = %d\n", sps_iArray[1]);
	printf("sps_iArray[2] (3rd Element)  = %d\n", sps_iArray[2]);
	printf("sps_iArray[3] (4th Element)  = %d\n", sps_iArray[3]);
	printf("sps_iArray[4] (5th Element)  = %d\n", sps_iArray[4]);
	printf("sps_iArray[5] (6th Element)  = %d\n", sps_iArray[5]);
	printf("sps_iArray[6] (7th Element)  = %d\n", sps_iArray[6]);
	printf("sps_iArray[7] (8th Element)  = %d\n", sps_iArray[7]);
	printf("sps_iArray[8] (9th Element)  = %d\n", sps_iArray[8]);
	printf("sps_iArray[9] (10th Element) = %d\n", sps_iArray[9]);

	sps_int_size = sizeof(int);
	sps_iArray_size = sizeof(sps_iArray);
	sps_iArray_num_elements = sps_iArray_size / sps_int_size;
	printf("Size Of Data type 'int'								= %d bytes\n", sps_int_size);
	printf("Number Of Elements in 'int' Array 'sps_iArray[]'				= %d Elements\n", sps_iArray_num_elements);
	printf("Size Of Array 'sps_iArray[]' (%d Elements * %d Bytes)				= %d Bytes\n\n", sps_iArray_num_elements, sps_int_size, sps_iArray_size);

	// ***** sps_fArray[] *****
	printf("\n\n");
	printf("In-Line Initialization And Piece-meal Display Of Elements Of Array 'sps_fArray[]': \n\n");
	printf("sps_fArray[0] (1st Element)  = %f\n", sps_fArray[0]);
	printf("sps_fArray[1] (2nd Element)  = %f\n", sps_fArray[1]);
	printf("sps_fArray[2] (3rd Element)  = %f\n", sps_fArray[2]);
	printf("sps_fArray[3] (4th Element)  = %f\n", sps_fArray[3]);
	printf("sps_fArray[4] (5th Element)  = %f\n", sps_fArray[4]);
	printf("sps_fArray[5] (6th Element)  = %f\n", sps_fArray[5]);
	printf("sps_fArray[6] (7th Element)  = %f\n", sps_fArray[6]);
	printf("sps_fArray[7] (8th Element)  = %f\n", sps_fArray[7]);
	printf("sps_fArray[8] (9th Element)  = %f\n", sps_fArray[8]);
	printf("sps_fArray[9] (10th Element) = %f\n", sps_fArray[9]);

	sps_float_size = sizeof(float);
	sps_fArray_size = sizeof(sps_fArray);
	sps_fArray_num_elements = sps_fArray_size / sps_float_size;
	printf("Size Of Data Type 'float'						= %d Bytes\n", sps_float_size);
	printf("Number Of Elements In 'float' Array 'sps_fArray[]'			= %d Elements\n", sps_fArray_num_elements);
	printf("Size Of Array 'fArray[]' (%d Elements * %d Bytes)				= %d Bytes\n\n", sps_fArray_num_elements, sps_float_size, sps_fArray_size);

	// ***** sps_cArray[] *****
	printf("\n\n");
	printf("In-line Initialization And Piece-meal Display Of Elements of Array 'sps_cArray[]': \n\n");
	printf("sps_cArray[0] (1st Element)   = %c\n", sps_cArray[0]);
	printf("sps_cArray[1] (2nd Element)   = %c\n", sps_cArray[1]);
	printf("sps_cArray[2] (3rd Element)   = %c\n", sps_cArray[2]);
	printf("sps_cArray[3] (4th Element)   = %c\n", sps_cArray[3]);
	printf("sps_cArray[4] (5th Element)   = %c\n", sps_cArray[4]);
	printf("sps_cArray[5] (6th Element)   = %c\n", sps_cArray[5]);
	printf("sps_cArray[6] (7th Element)   = %c\n", sps_cArray[6]);
	printf("sps_cArray[7] (8th Element)   = %c\n", sps_cArray[7]);
	printf("sps_cArray[8] (9th Element)   = %c\n", sps_cArray[8]);
	printf("sps_cArray[9] (10th Element)  = %c\n", sps_cArray[9]);
	printf("sps_cArray[10] (11th Element) = %c\n", sps_cArray[10]);
	printf("sps_cArray[11] (12th Element) = %c\n", sps_cArray[11]);
	printf("sps_cArray[12] (13th Element) = %c\n", sps_cArray[12]);

	sps_char_size = sizeof(char);
	sps_cArray_size = sizeof(sps_cArray);
	sps_cArray_num_elements = sps_cArray_size / sps_char_size;
	printf("Size Of Data Type 'char'							= %d Bytes\n", sps_char_size);
	printf("Number Of Elements In 'char' Array 'cArray'					= %d Elements\n", sps_cArray_num_elements);
	printf("Size Of Array 'cArray[]' (%d Elements * %d Bytes)				= %d Bytes\n\n", sps_cArray_num_elements, sps_char_size, sps_cArray_size);

	return(0);
}

