#include <stdio.h>

int main(void)
{
	//variable declaration
	int sps_iArray_One[5];
	int sps_iArray_Two[5][3];
	int sps_iArray_Three[100][100][5];

	int sps_num_rows_2D;
	int sps_num_columns_2D;

	int sps_num_rows_3D;
	int sps_num_columns_3D;
	int sps_depth_3D;

	//code
	printf("\n\n");
	printf("Size of 1-D integer array sps_iArray_One = %zu\n", sizeof(sps_iArray_One));
	printf("Number of elements in 1-D integer array sps_iArray_One = %zu\n", (sizeof(sps_iArray_One) / sizeof(int)));

	printf("\n\n");
	printf("Size of 2-D integer array sps_iArray_Two = %zu\n", sizeof(sps_iArray_Two));

	printf("Number of rows in 2-D integer array sps_iArray_Two = %zu\n", (sizeof(sps_iArray_Two) / sizeof(sps_iArray_Two[0])));
	sps_num_rows_2D = (sizeof(sps_iArray_Two) / sizeof(sps_iArray_Two[0]));

	printf("Number of elements (columns) in each row in 2-D integer array sps_iArray_Two = %zu\n", (sizeof(sps_iArray_Two[0]) / sizeof(sps_iArray_Two[0][0])));
	sps_num_columns_2D = (sizeof(sps_iArray_Two[0]) / sizeof(sps_iArray_Two[0]));

	printf("Number of Elements in total in 2-D Array sps_iArray_Two = %d\n", (sps_num_rows_2D * sps_num_columns_2D));

	printf("\n\n");

	printf("\n\n");
	printf("Size of 3-D integer array sps_iArray_Three = %zu\n", sizeof(sps_iArray_Three));

	printf("Number rows in 3-D integer array sps_iArray_Three = %zu\n", sizeof(sps_iArray_Three) / sizeof(sps_iArray_Three[0]));
	sps_num_rows_3D = (sizeof(sps_iArray_Three) / sizeof(sps_iArray_Three[0]));

	printf("Number of elements (columns) in one row in 3-D integer array sps_iArray_Three = %zu\n", (sizeof(sps_iArray_Three[0]) / sizeof(sps_iArray_Three[0][0])));
	sps_num_columns_3D = (sizeof(sps_iArray_Three[0]) / sizeof(sps_iArray_Three[0][0]));

	printf("Number of elements (depth) in one column in one row in 3-D integer array sps_iArray_Three = %zu\n", (sizeof(sps_iArray_Three[0][0]) / sizeof(sps_iArray_Three[0][0][0])));
	sps_depth_3D = (sizeof(sps_iArray_Three[0][0]) / sizeof(sps_iArray_Three[0][0][0]));

	printf("Number of elements in total in 3-D Array sps_iArray_Three = %d\n", (sps_num_rows_3D * sps_num_columns_3D * sps_depth_3D));

	printf("\n\n");

	return(0);
}

