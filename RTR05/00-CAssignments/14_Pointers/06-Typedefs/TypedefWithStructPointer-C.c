#include <stdio.h>
#include <stdlib.h>


// DEFINING STRUCT
struct MyData
{
	int sps_i;
	float sps_f;
	double sps_d;
};

int main(void)
{
	// Variable declarations 
	int i_size;
	int f_size;
	int d_size;
	int struct_MyData_size;
	int pointer_to_struct_MyData_size;

	typedef struct MyData *MyDataPtr;

	MyDataPtr pData;

	// Code
	printf("\n\n");

	pData = (MyDataPtr)malloc(sizeof(struct MyData));
	if (pData == NULL)
	{
		printf("FAILED TO ALLOCATE MEMORY TO 'struct MyData' !!! EXITTING NOW ...\n\n");
		exit(0);
	}
	else
		printf("SUCCESSFULLY ALLOCATED MEMORY TO 'struct MyData' !!!\n\n");

	//Assigning data values to the Data Members of 'struct MyData'
	pData->sps_i = 30;
	pData->sps_f = 11.45f;
	pData->sps_d = 1.2995;

	//Displaying values Of The Data Members Of 'struct MyData' 
	printf("\n\n");
	printf("DATA MEMBERS OF 'struct MyData' ARE : \n\n");
	printf("sps_i = %d\n", pData->sps_i);
	printf("sps_f = %f\n", pData->sps_f);
	printf("sps_d = %lf\n", pData->sps_d);

	//Calculating sizes (In Bytes) of the Data Members of 'struct MyData' 
	i_size = sizeof(pData->sps_i);
	f_size = sizeof(pData->sps_f);
	d_size = sizeof(pData->sps_d);

	// Displaying sizes (In Bytes) of the Data Members of 'struct MyData'
	printf("\n\n");
	printf("SIZES (in bytes) OF DATA MEMBERS OF 'struct MyData' ARE : \n\n");
	printf("Size of 'sps_i'	%d bytes\n", i_size);
	printf("Size of 'sps_f'	%d bytes\n", f_size);
	printf("Size of 'sps_d' %d bytes\n", d_size);

	//Calculating size (In Bytes) of the entire 'struct Mydata' 
	struct_MyData_size = sizeof(struct MyData);
	pointer_to_struct_MyData_size = sizeof(MyDataPtr);

	//Displaying sizes (In Bytes) of the entire 'struct Mydata'
	printf("\n\n");
	printf("Size of 'struct MyData' : %d bytes\n\n", struct_MyData_size);
	printf("Size of pointer to 'struct MyData' : %d bytes\n\n", pointer_to_struct_MyData_size);

	if (pData)
	{
		free(pData);
		pData = NULL;
		printf("MEMORY ALLOCATED TO 'struct MyData' HAS BEEN SUCCESSFULLY FREED !!!\n\n");
	}

	return(0);
}
