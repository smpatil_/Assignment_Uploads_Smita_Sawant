#include <stdio.h>
#include <stdlib.h>

// DEFINING STRUCT
struct MyData
{
	int *ptr_i;
	int sps_i;

	float *ptr_f;
	float sps_f;

	double *ptr_d;
	double sps_d;
};

int main(void)
{
	// Variable declarations
	struct MyData *pData = NULL;

	// Code
	printf("\n\n");
	pData = (struct MyData *)malloc(sizeof(struct MyData));
	if (pData == NULL)
	{
		printf("FAILED TO ALLOCATE MEMORY TO 'struct MyData' !!! EXITTING NOW ...\n\n");
		exit(0);
	}
	else
		printf("SUCCESSFULLY ALLOCATED MEMORY TO 'struct MyData' !!!\n\n");

	(*pData).sps_i = 9;
	(*pData).ptr_i = &(*pData).sps_i;

	(*pData).sps_f = 11.45f;
	(*pData).ptr_f = &(*pData).sps_f;

	(*pData).sps_d = 30.121995;
	(*pData).ptr_d = &(*pData).sps_d;

	printf("\n\n");
	printf("i = %d\n", *((*pData).ptr_i));
	printf("Adress Of 'sps_i' = %p\n", (*pData).ptr_i);

	printf("\n\n");
	printf("sps_f = %f\n", *((*pData).ptr_f));
	printf("Adress Of 'sps_f' = %p\n", (*pData).ptr_f);

	printf("\n\n");
	printf("d = %lf\n", *((*pData).ptr_d));
	printf("Adress Of 'sps_d' = %p\n", (*pData).ptr_d);

	if (pData)
	{
		free(pData);
		pData = NULL;
		printf("MEMORY ALLOCATED TO 'struct MyData' HAS BEEN SUCCESSFULLY FREED !!!\n\n");
	}

	return(0);
}

