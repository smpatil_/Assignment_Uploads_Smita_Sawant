#include <stdio.h>

// DEFINING STRUCT
struct MyData
{
	int *ptr_i;
	int sps_i;
	
	float *ptr_f;
	float sps_f;
	
	double *ptr_d;
	double sps_d;
};

int main(void)
{
	// Variable declarations
	struct MyData data;
	
	// Code
	data.sps_i = 9;
	data.ptr_i = &data.sps_i;
	
	data.sps_f = 11.45f;
	data.ptr_f = &data.sps_f;
	
	data.sps_d = 30.121995;
	data.ptr_d = &data.sps_d;
	
	printf("\n\n");
	printf("i = %d\n", *(data.ptr_i));
	printf("Adress Of 'sps_i' = %p\n", data.ptr_i);

	printf("\n\n");
	printf("sps_f = %f\n", *(data.ptr_f));
	printf("Adress Of 'sps_f' = %p\n", data.ptr_f);

	printf("\n\n");
	printf("d = %lf\n", *(data.ptr_d));
	printf("Adress Of 'sps_d' = %p\n", data.ptr_d);

	return(0);
}

