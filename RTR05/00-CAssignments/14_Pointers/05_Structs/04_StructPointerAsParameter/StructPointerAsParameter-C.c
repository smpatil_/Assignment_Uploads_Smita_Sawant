#include <stdio.h>
#include <stdlib.h>

// DEFINING STRUCT
struct MyData
{
	int sps_i;
	float sps_f;
	double sps_d;
};

int main(void)
{
	// Function prototypes
	void ChangeValues(struct MyData *);
	

	//Variable declarations
	struct MyData *pData = NULL;
		
	// Code
	printf("\n\n");
	
	pData = (struct MyData *)malloc(sizeof(struct MyData));
	if (pData == NULL)
	{
		printf("FAILED TO ALLOCATE MEMORY TO 'struct MyData' !!! EXITTING NOW ...\n\n");
		exit(0);
	}
	else
		printf("SUCCESSFULLY ALLOCATED MEMORY TO 'struct MyData' !!!\n\n");


	//Assigning Data Values To The Data Members Of 'struct MyData'
	pData->sps_i = 30;
	pData->sps_f = 11.45f;
	pData->sps_d = 1.2995;

	//Displaying Values Of The Data Members Of 'struct MyData'
	printf("\n\n");
	printf("DATA MEMBERS OF 'structMyData' ARE: \n\n");
	printf("sps_i = %d\n", pData->sps_i);
	printf("sps_f = %f\n", pData->sps_f);
	printf("sps_d = %lf\n", pData->sps_d);
	
	ChangeValues(pData);

	//Displaying Values Of The Data Members Of 'struct MyData'
	printf("\n\n");
	printf("DATA MEMBERS OF 'struct MyData' ARE : \n\n");
	printf("sps_i = %d\n", pData->sps_i);
	printf("sps_f = %f\n", pData->sps_f);
	printf("sps_d = %lf\n\n", pData->sps_d);

	if (pData)
	{
		free(pData);
		pData = NULL;
		printf("MEMORY ALLOCATED TO 'struct MyData' HAS BEEN SUCCESSFULLY FREED !!!\n\n");
	}

	return(0);
}

void ChangeValues(struct MyData *pParam_Data)
{
	// Code
	
	pParam_Data->sps_i = 9;
	pParam_Data->sps_f = 8.2f;
	pParam_Data->sps_d = 6.1998;

	// Can also be done as...

/*
	(*pParam_Data).sps_i = 9;
	(*pParam_Data).sps_f = 8.2f;
	(*pParam_Data).sps_d = 6.1998;
*/
}

