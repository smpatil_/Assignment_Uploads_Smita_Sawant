#include <stdio.h>

int main(void)
{
	// Variable declarations
	int sps_num;
	int *ptr = NULL;
	int ans;

	// Code
	sps_num = 5;
	ptr = &sps_num;

	printf("\n\n");
	printf("sps_num			= %d\n", sps_num);
	printf("&sps_num		= %p\n", &sps_num);
	printf("*(&sps_num)		= %d\n", *(&sps_num));
	printf("ptr			= %p\n", ptr);
	printf("*ptr			= %d\n", *ptr);

	printf("\n\n");

	// Add 10 to 'ptr' which is the address of 'sps_num'...
	// Hence, 10 will be added to the address of 'sps_num' and the resultant address will be displayed
	printf("Answer Of (ptr + 10) = %p\n", (ptr + 10));


	// Add 10 to 'ptr' which is the address of 'sps_num' and give value at the new address...
	// Hence, 10 will be added to the address of 'sps_num' and the value at resultant address will be displayed...
	printf("Answer Of *(ptr + 10) = %d\n", *(ptr + 10));


	// Add 10 to 'ptr' which is the value at address of 'sps_num' (i.e. : 'sps_num' i.e.: 5) and give new value without any change in any address...
	// Hence, 10 will be added to the '*ptr' (sps_num = 5) and the resultant value will be given (*ptr + 10) = (sps_num + 10) = (5 + 10) = 15..
	printf("Answer Of (*ptr + 10) = %d\n\n", (*ptr + 10));


	// *** Associativity of *(Value at Address) and ++ AND -- Operators is From Right To Left ***


	// (Right to Left) Consider value *ptr... Pre-increament *ptr... That is, value at address 'ptr' i.e. *ptr is pre-incremented (++*ptr)
	++* ptr; // *ptr is pre-incremented... *ptr is 5.. after execution of this statement.. *ptr = 6
	printf("Answer Of ++*ptr : %d\n", *ptr); //Brackets are not necessary for pre-increment / pre-decrement


	// (Right to Left) Post-increment address ptr... That is, address 'ptr' i.e.: ptr is post-incremented (ptr++) and then the value at the new address is displayed (*ptr++)...
	*ptr++; // Incorrect method of post-incrementing a value using pointer
	printf("Answer Of *ptr++ : %d\n", *ptr); //Brackets are necessary for post-increment / post-decrement


	// (Right to Left) Post-increment address *ptr... That is, value at address 'ptr' i.e.: *ptr is post-incremented (*ptr)++ 
	ptr = &sps_num;
	(*ptr)++; // Correct method of post-incrementing a value using pointer.. *ptr is 6... at this statement *ptr remains 6 but at next statement *ptr = 7 (post-increment)
	printf("Answer Of (*ptr)++ : %d\n\n", *ptr); //Brackets are necessary for post-increment / post-decrement

	return(0);
}

