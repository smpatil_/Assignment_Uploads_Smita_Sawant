#include <stdio.h>

struct Employee
{
	char name_of_sps[100];
	int age_of_sps;
	float salary_of_sps;
	char sex_of_sps;
	char marital_status_sps;
};

int main(void)
{
	// Code
	printf("\n\n");
	printf("SIZES OF DATA TYPES AND POINTERS TO THOSE RESPECTIVE DATA TYPES ARE : \n\n");
	printf("Size of (int)			: %zd \t \t \t Size of pointer to int (int*)					: %zd\n\n", sizeof(int), sizeof(int*));
	printf("Size of (float)			: %zd \t \t \t Size of pointer to float (float*)				: %zd\n\n", sizeof(float), sizeof(float*));
	printf("Size of (double)		: %zd \t \t \t Size of pointer to double (double*)				: %zd\n\n", sizeof(double), sizeof(double*));
	printf("Size of (char)			: %zd \t \t \t Size of pointer to char (char*)				: %zd\n\n", sizeof(char), sizeof(char*));
	printf("Size of (struct Employee)	: %zd \t \t Size of pointer to struct Employee (struct Employee*)			: %zd\n\n", sizeof(struct Employee), sizeof(struct Employee*));

	return(0);
}

