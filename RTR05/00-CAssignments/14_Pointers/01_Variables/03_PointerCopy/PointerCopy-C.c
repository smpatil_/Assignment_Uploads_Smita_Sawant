#include <stdio.h>

int main(void)
{
	// Variable Declarations
	int sps_num;
	int *ptr = NULL;
	int *copy_ptr = NULL;

	// Code
	sps_num = 5;
	ptr = &sps_num;

	printf("\n\n");
	printf("***** BEFORE copy_ptr = ptr *****\n\n");
	printf("	sps_num			= %d\n", sps_num);
	printf("	&sps_num		= %p\n", &sps_num);
	printf("	*(&sps_num)		= %d\n", *(&sps_num));
	printf("	ptr			= %p\n", ptr);
	printf("	*ptr			= %d\n", *ptr);

	// 'ptr' is an integer pointer variable... that it can hold the address of any integer variable only
	// 'copy_ptr' is another integer pointer variable
	// If ptr = &sps_num.. 'ptr' will contain address of integer variable 'sps_num'
	//If 'ptr' is assigned to 'copy_ptr', copy_ptr' will also contain address of integer variable 'num'
	// Hence, now, both 'ptr' and 'copy_ptr' will point to 'num'...

	copy_ptr = ptr; // copy_ptr = ptr = &sps_num

	printf("\n\n");
	printf("***** AFTER copy_ptr = ptr *****\n\n");
	printf("	sps_num			= %d\n", sps_num);
	printf("	&sps_num		= %p\n", &sps_num);
	printf("	*(&sps_num)		= %d\n", *(&sps_num));
	printf("	ptr			= %p\n", ptr);
	printf("	*ptr			= %d\n", *ptr);
	printf("	copy_ptr		= %p\n", copy_ptr);
	printf("	*copy_ptr		= %d\n", *copy_ptr);
	return(0);
}
