#include <stdio.h>

int main(void)
{
	//variable declarations
	double sps_num;
	double* ptr = NULL; // Declaration method 2: 'ptr' is a variable of type 'double*'

	// Code
	sps_num = 2.34343434f;

	printf("\n\n");

	printf(" ***** BEFORE ptr = &sps_num *****\n\n");
	printf("Value Of 'sps_num'			= %lf\n\n", sps_num);
	printf("Address Of 'sps_num'			= %p\n\n", &sps_num);
	printf("Value At Address Of 'sps_num'		= %lf\n\n", *(&sps_num));

	//Assigning address of variable 'sps_num' to pointer variable 'ptr'
	//'ptr' now contains address of 'sps_num'.. hence, 'ptr' is same as '&sps_num'
	ptr = &sps_num;

	printf("\n\n");

	printf("***** AFTER ptr = &sps_num *****\n\n");
	printf("Value Of 'sps_num'			= %lf\n\n", sps_num);
	printf("Address Of 'sps_num'			= %p\n\n", ptr);
	printf("Value At Address Of 'sps_num'		= %lf\n\n", *ptr);

	return(0);
}
