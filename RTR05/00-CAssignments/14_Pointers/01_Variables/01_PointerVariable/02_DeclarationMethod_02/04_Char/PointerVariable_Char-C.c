#include <stdio.h>

int main(void)
{
	//variable declarations
	char sps_ch;
	char* ptr = NULL; // Declaration method 2: 'ptr' is a variable of type 'char*'

	// Code
	sps_ch = 'A';

	printf("\n\n");

	printf(" ***** BEFORE ptr = &sps_ch *****\n\n");
	printf("Value Of 'sps_ch'			= %c\n\n", sps_ch);
	printf("Address Of 'sps_ch'			= %p\n\n", &sps_ch);
	printf("Value At Address Of 'sps_ch'		= %c\n\n", *(&sps_ch));

	//Assigning address of variable 'sps_ch' to pointer variable 'ptr'
	//'ptr' now contains address of 'sps_ch'.. hence, 'ptr' is same as '&sps_ch'
	ptr = &sps_ch;

	printf("\n\n");

	printf("***** AFTER ptr = &sps_ch *****\n\n");
	printf("Value Of 'sps_ch'			= %c\n\n", sps_ch);
	printf("Address Of 'sps_ch'			= %p\n\n", ptr);
	printf("Value At Address Of 'sps_ch'		= %c\n\n", *ptr);

	return(0);
}
