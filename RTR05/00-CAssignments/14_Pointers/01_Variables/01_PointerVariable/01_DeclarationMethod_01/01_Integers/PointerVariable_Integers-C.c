#include <stdio.h>

int main(void)
{
	//variable declarations
	int sps_num;
	int *ptr = NULL; //Declaration Method 1:- '*ptr' is a variable of type 'int'

	// Code
	sps_num = 10;

	printf("\n\n");

	printf("***** BEFORE ptr = &sps_num *****\n\n");
	printf("Value Of 'sps_num'				= %d\n\n", sps_num);
	printf("Address Of 'sps_num'				= %p\n\n", &sps_num);
	printf("Value At Address Of 'sps_num'			= %d\n\n", *(&sps_num));

	// Assigning address of variable 'sps_num' to pointer variable 'ptr'
	// 'ptr' now contains address of 'sps_num'... hence, 'ptr' is Same as '&sps_num'
	ptr = &sps_num;

	printf("\n\n");

	printf("***** AFTER ptr = &sps_num *****\n\n");
	printf("Value Of 'sps_num'				= %d\n\n", sps_num);
	printf("Address Of 'sps_num'				= %p\n\n", ptr);
	printf("Value At Address Of 'sps_num'			= %d\n\n", *ptr);

	return(0);
}
