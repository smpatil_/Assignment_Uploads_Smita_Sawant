#include <stdio.h>

int main(void)
{
	// Variable declarations
	int sps_num;
	int *ptr = NULL; 
	int** pptr = NULL; //Declaration Method 1:- '**pptr' is a variable of type 'int'

	// Code
	sps_num = 10;

	printf("\n\n");

	printf("***** BEFORE ptr = &sps_num *****\n\n");
	printf("Value Of 'sps_num'				= %d\n\n", sps_num);
	printf("Address Of 'sps_num'				= %p\n\n", &sps_num);
	printf("Value At Address Of 'sps_num'			= %d\n\n", *(&sps_num));

	// Assigning address of variable 'sps_num' to pointer variable 'ptr'
	// 'ptr' now contains address of 'sps_num'... hence, 'ptr' is Same as '&sps_num'
	ptr = &sps_num;

	printf("\n\n");

	printf("***** AFTER ptr = &sps_num *****\n\n");
	printf("Value Of 'sps_num'				= %d\n\n", sps_num);
	printf("Address Of 'sps_num'				= %p\n\n", ptr);
	printf("Value At Address Of 'sps_num'			= %d\n\n", *ptr);

	// Assigning address of variable 'ptr' to pointer-to-pointer variable 'pptr'
	// 'pptr' now contains the address of 'ptr' which in turn contains the address of 'num'
	//Hence 'pptr' is Same as '&ptr'
	// 'ptr' is Same as '&num'
	// Hence, pptr = &ptr = &(&num)
	// If ptr = &num and *ptr = *(&num) = value at address of 'num'
	// Then, pptr = &ptr and *pptr = *(&ptr) = ptr = Value at address of 'ptr' i.e. 'ptr' i.e. address of 'num'
	//Then, **pptr = **(&ptr) = *(*(&ptr)) = *ptr = *(&num) = num = 1=
	// Hence, num = *(&num) = *ptr = *(*pptr) = **pptr

	pptr = &ptr;

	printf("\n\n");

	printf("***** AFTER pptr = &ptr *****\n\n");
	printf("Value Of 'sps_num'						= %d\n\n", sps_num);
	printf("Value Of 'sps_num'	(ptr)					= %p\n\n", ptr);
	printf("Address Of 'ptr' (pptr)						= %p\n\n", pptr);
	printf("Value At Address Of 'ptr' (*pptr)				= %p\n\n", *pptr);
	printf("Value At Address Of 'sps_num' (*ptr) (*pptr)			= %d\n\n", **pptr);

	return(0);
}
