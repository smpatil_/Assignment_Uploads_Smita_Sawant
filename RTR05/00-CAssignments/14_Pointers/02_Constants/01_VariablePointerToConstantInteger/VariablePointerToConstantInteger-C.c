#include <stdio.h>

int main(void)
{
	//Variable Declarations
	int sps_num = 5;
	const int* ptr = NULL; // Read this line from right to left => ptr is a point(*) to integer (int) constant (const).

	//Code
	ptr = &sps_num;
	printf("\n");
	printf("Current Value Of 'sps_num' = %d\n", sps_num);
	printf("Current 'ptr' (Address of 'sps_num') = %p\n", ptr);

	// The following line does not give error.. as we are modifying the value of the variable individually
	sps_num++;
	printf("\n\n");
	printf("After sps_num++, value of 'sps_num' = %d\n", sps_num);


	// The following line gives error and is hence commented out.
	// We cannot alter the value stored in "A pointer to constant integer"
	// With respect to the pointer, the value it points to should be constant.
	// Uncomment it and see the error.

	// (*ptr)++;

	// The following line does not give error
	// We do not get error because we are changing the pointer (address).
	// The pointer is not constant. The value to which the pointer points is constant.
	ptr++;

	printf("\n\n");
	printf("After ptr++, Value of 'ptr' = %p\n", ptr);
	printf("Value at this new 'ptr' = %d\n", *ptr);
	printf("\n");
	return(0);
}

// Conclusion:
// As 'ptr' is a "variable pointer to constant integer" - we can change the value of pointer  "ptr".
// We can change the value of the variable (sps_num) individually - whose address is contained in 'ptr'.
//So in short, we cannot change "the value at address of ptr" - we cannot change the value of "num" with respect to "ptr" => (*ptr)++ is not allowed.
//But, we can change the address 'ptr' itself => So, ptr++ is allowed.

