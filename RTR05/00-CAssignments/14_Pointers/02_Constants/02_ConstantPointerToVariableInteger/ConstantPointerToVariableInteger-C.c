#include <stdio.h>

int main(void)
{
	//Variable Declarations
	int sps_num = 5;
	int* const ptr = &sps_num; // Read this line from right to left => "ptr is a constant (const) pointer (*) to integer (int).

	//Code
	printf("\n");
	printf("Current Value Of 'sps_num' = %d\n", sps_num);
	printf("Current 'ptr' (Address of 'sps_num') = %p\n", ptr);

	// The following line does not give error.. as we are modifying the value of the variable individually
	sps_num++;
	printf("\n\n");
	printf("After sps_num++, value of 'sps_num' = %d\n", sps_num);


	// The following line gives error and is hence commented out.
	// We cannot alter the 'ptr' value as 'ptr' is "a constant pointer to integer".
	// With respect to the pointer, the value it points to is not constant but the pointer itself is constant.
	// Uncomment it and see the error.

	// ptr++;

	// The following line does not give error
	// We do not get error because we are changing the pointer (address).
	// The pointer is not constant. The value to which the pointer points is constant.
	(*ptr)++;

	printf("\n\n");
	printf("After (*ptr)++, Value of 'ptr' = %p\n", ptr);
	printf("Value at this 'ptr' = %d\n", *ptr);
	printf("\n");
	return(0);
}

// Conclusion:
// As "ptr" is a "constant pointer to a variable integer" - we can change the value stored at address "ptr" but we cannot change the 'ptr' (Address) itself.
// We can change the value of the variable (sps_num) individually - whose address is contained in 'ptr'.
//We can also change "the value at address of ptr" - we can change the value of "sps_num" with respect to "ptr" => (*ptr)++ is allowed.
//We cannot change the value of 'ptr' => That is we cannot store a new address inside 'ptr' => So, ptr++ is NOT allowed.

