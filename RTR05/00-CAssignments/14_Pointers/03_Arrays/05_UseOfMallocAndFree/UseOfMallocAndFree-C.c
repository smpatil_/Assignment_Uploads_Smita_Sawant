#include <stdio.h> 
#include <stdlib.h> // Contains prototypes of malloc() and free() 

int main(void)
{
	// Variable Declarations 
	int *ptr_iArray = NULL; // It is a good discipline to initialize any pointer with null address to prevent any garbage value getting into it, that way,it makes it easy to check for success or failure of memory allocation later on after malloc()...
	unsigned int intArrayLength = 0;
	int sps_i;
	
	// code 
	printf("\n\n");
	printf("Enter The Number Of Elements You Want In Your Integer Array	: ");
	scanf("%d", &intArrayLength);

	//**** Allocating as much memory required to the integer array **** 
	//**** Memory required for integer array = size in bytes of one integer * Number of integers to be stored in array **** 
	//**** T allocate said amount of memory, function malloc() will be used **** 
	// **** malloc() will allocate said amount of memory and will return the Initial/ Starting/ Base Address of the allocated memory, which must be captured in a pointer variable *** 
	//*** Using this Base Address, the integer array can be accessed and used *** 

	ptr_iArray = (int *)malloc(sizeof(int) * intArrayLength);
	if (ptr_iArray == NULL) // If ptr_iArray is still null, even after call to malloc(), it means malloc() has failed to allocate memory and no address has been returned by malloc() in ptr_iArray.
	{
		printf("\n\n");
		printf("MEMORY ALLOCATION FOR INTEGER ARRAY HAS FAILED !!! EXITTING NOW...\n\n"); 
		exit(0);
	}
	else // If ptr_iArray is not null, it means that it must contain a valid address which is returned by malloc(), hence malloc() has succeeded in memory allocation..
	{
		printf("\n\n");
		printf("MEMORY ALLOCATION FOR INTEGER ARRAY HAS SUCCEEDED !!!\n\n");
		printf("MEMORY ADDRESSES FROM %p TO %p HAVE BEEN ALLOCATED TO INTEGER ARRAY!!!\n\n", ptr_iArray, (ptr_iArray + (intArrayLength - 1)));
	}

	printf("\n\n");
	printf("Enter %d Elements For The Integer Array	\n\n", intArrayLength);
	for (sps_i = 0; sps_i < intArrayLength; sps_i++)
		scanf("%d", (ptr_iArray + sps_i));

	printf("\n\n");
	printf("The Integer Array Entered By You, Consisting Of %d Elements	: \n\n", intArrayLength);
	for (sps_i = 0; sps_i < intArrayLength; sps_i++)
	{
		printf("ptr_iArray[%d] = %d \t \t At Address &ptr_iArray[%d] %p\n", sps_i, ptr_iArray[sps_i], sps_i, &ptr_iArray[sps_i]);
	}

	printf("\n\n");
	for (sps_i = 0; sps_i < intArrayLength; sps_i++)
	{
		printf("*(ptr_iArray + %d) = %d \t \t At Address (ptr_iArray + %d)  %p\n", sps_i, *(ptr_iArray + sps_i), sps_i, (ptr_iArray + sps_i));
	}

	//*** Checking If Memory Is Still Allocated By Checking Validity Of Base Address 'ptr_iarray' ***
	//**** If the address is valid, that is if 'ptr_iArray' exists, that is, if  it is not null, memory is still allocated **** 
	//*** In That Case, The Allocated Memory Must Be Freed *** 
	//*** Memory is allocated using malloc() and freed using free() **** 
	//*** Once memory is freed using free(), the base address must be cleaned, that is, it must be re-initilaized to 'NULL' to keep away garbage values. This is not compulsory, but it is a GOOD CODING PRACTICE ***

	if (ptr_iArray)
	{
		free(ptr_iArray);
		ptr_iArray = NULL;

		printf("\n\n");
		printf("MEMORY ALLOCATED FOR INTEGER ARRAY HAS BEEN SUCCESSFULLY FREED !!!\n\n"); 
	}

	return(0);
}

