#include <stdio.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3

int main(void)
{
    // Variable declarations
    int iArray[NUM_ROWS][NUM_COLUMNS];
    int sps_i, sps_j;

    int *ptr_iArray_Row = NULL;

    //Code
    //**** Every Row Of A 2D Array Is An Integer Array Itself Comprising Of 'NUM_COLUMNS' Integer Elements ****
    
    //*** There are 5 Rows And 3 Columns In A 2D Integer Array. Each Of The 5 Rows Is A 1D Array Of 3 Integers ****
    //***Hence Each Of These 5 Rows Themselves Being Arrays, Will Be The Base Addresses Of Their Respective Rows***

    for (sps_i = 0; sps_i < NUM_ROWS; sps_i++)
    {
        ptr_iArray_Row = iArray[sps_i]; //'iArray[sps_i]' is the base address of ith row...
        for (sps_j = 0; sps_j < NUM_COLUMNS; sps_j++)
            *(ptr_iArray_Row + sps_j) = (sps_i + 1) * (sps_j + 1); //'ptr_iArray_Row' (i.e. 'iArray[sps_i]' Can Be Treated As 1D Array Using Pointers)...
    }

    printf("\n\n");
    printf("2D Integer Array Elements Along With Addresses : \n\n");
    for (sps_i = 0; sps_i < NUM_ROWS; sps_i++)
    {
        ptr_iArray_Row = iArray[sps_i];
        for (sps_j = 0; sps_j < NUM_COLUMNS; sps_j++)
        {
            printf("*(ptr_iArray_Row + %d) = %d \t \t At Address (ptr_iArray_Row + sps_j) : %p\n", sps_j, *(ptr_iArray_Row + sps_j), (ptr_iArray_Row + sps_j));
        }
        printf("\n\n");
    }

    return(0);
}
