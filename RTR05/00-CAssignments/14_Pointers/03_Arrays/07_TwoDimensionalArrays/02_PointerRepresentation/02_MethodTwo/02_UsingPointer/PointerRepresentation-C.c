#include <stdio.h>
#include <stdlib.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3

int main(void)
{
	// Variable declarations
	int sps_i, sps_j;
	int **ptr_iArray = NULL;

	// Code
	// *** Every Row Of A 2D Array Is An Integer Array Itself Comprising Of 'NUM_COLUMNS' Integer Elements ***
	// *** There Are 5 Rows and 3 Columns In a 2D Integer Array. Each of the 5 Rows is a 1D Array of 3 Integers.
	// *** Hence, Each of These 5 Rows Themselves Being Arrays, Will Be The Base Addresses of Their Respective Rows ***
	printf("\n\n");

	// *** Memory Allocation ***
	ptr_iArray = (int**)malloc(NUM_ROWS * sizeof(int*)); //ptr_iArray is the name and base address of 1D Array Containing 5 integer pointers to 5 Integer Arrays.. So it is an Array Containing Elements of Data Type (int *)
	if (ptr_iArray == NULL)
	{
		printf("MEMORY ALLOCATION TO THE 1D ARRAY OF BASE ADDRESSES OF %d ROWS FAILED !!! EXCITING NOW...\n\n", NUM_ROWS);
		exit(0);
	}
	else
		printf("MEMORY ALLOCATION TO THE 1D ARRAY OF BASE ADDRESSES OF %d ROWS HAS SUCCEEDED !!!\n\n", NUM_ROWS);

	// *** Allocating Memory To Each Row ***
	for (sps_i = 0; sps_i < NUM_ROWS; sps_i++)
	{
		ptr_iArray[sps_i] = (int*)malloc(NUM_COLUMNS * sizeof(int)); //ptr_iArray[sps_i] is the base address of ith row..
		if (ptr_iArray == NULL)
		{
			printf("MEMORY ALLOCATION TO THE COLUMNS OF ROW %d FAILED !!! EXCITING NOW...\n\n", sps_i);
			exit(0);
		}
		else
			printf("MEMORY ALLOCATION TO THE COLUMNS OF ROW %d HAS SUCCEEDED !!! \n\n", sps_i);
	}

	// *** Assigning Values ***
	for (sps_i = 0; sps_i < NUM_ROWS; sps_i++)
	{
		for (sps_j = 0; sps_j < NUM_COLUMNS; sps_j++)
		{
			*(*(ptr_iArray + sps_i) + sps_j) = (sps_i + 1) * (sps_j + 1); //ptr_iArray[sps_i][sps_j] = (sps_i + 1) * (sps_j + 1);
		}
	}

	// *** Displaying Values ***
	printf("\n\n");
	printf("2D Integer Array Elements Along With Addresses : \n\n");
	for (sps_i = 0; sps_i < NUM_ROWS; sps_i++)
	{
		for (sps_j = 0; sps_j < NUM_COLUMNS; sps_j++)
		{
			printf("ptr_iArray_Row[%d][%d] = %d \t \t At Address &ptr_iArray_Row[%d][%d] : %p\n", sps_i, sps_j, ptr_iArray[sps_i][sps_j], sps_i, sps_j, &ptr_iArray[sps_i][sps_j]);
		}
		printf("\n\n");
	}

	// *** FREEING ALLOCATED MEMORY ***
	// *** FREEING MEMORY OF EACH ROW ***
	for (sps_i = (NUM_ROWS - 1); sps_i >= 0; sps_i--)
	{
		if (*(ptr_iArray + sps_i)) //if (ptr_iArray[sps_i])
		{
			free(*(ptr_iArray + sps_i)); //free(ptr_iArray[sps_i])
			*(ptr_iArray + sps_i) = NULL; //ptr_iArray[sps_i] = NULL;
			printf("MEMORY ALLOCATED TO ROW %d HAS BEEN SUCCESSFULLY FREED !!!\n\n", sps_i);
		}
	}

	// *** FREEING MEMORY OF ptr_iArray WHICH IS THE ARRAY  OF 5 INTEGER POINTERS ... THAT IT, IT IS AN ARRAY HAVING 5 INTEGER ADDRESSES (TYPE int *) ***
	if (ptr_iArray)
	{
		free(ptr_iArray);
		ptr_iArray = NULL;
		printf("MEMORY ALLOCATED TO ptr_iArray HAS BEEN SUCCESSFULLY FREED !!!\n\n");
	}

	return(0);
}

