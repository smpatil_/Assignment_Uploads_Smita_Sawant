#include <stdio.h> 

#define NUM_ROWS 5 
#define NUM_COLUMNS 3 

int main(void)
{
    //variable declarations 
    int iArray[NUM_ROWS][NUM_COLUMNS];
    int sps_i, sps_j;

    // Code 
    // *** NAME OF AN ARRAY ITSELF IS ITS BASE ADDRESS *** 
    //*** Hence, 'iArray' Is Base Address Of 2d Integer Array iArray[][] 
 
    // iArray[5][3] = > iArray Is A 2d Array Having 5 Rows And 3 Columns Each Of These 5 Rows Is A 1d Intger Array Of 3 Integers * **
    // iArray[0] => IS THE 0th ROW	HENCE, IS THE BASE ADDRESS OF ROW 0
    // iArray[1] => IS THE 1st ROW	HENCE, IS THE BASE ADDRESS OF ROW 1 
    // iArray[4] => IS THE 4th ROW	HENCE, IS THE BASE ADDRESS OF ROW 4 

    // (iArray[0] + 0) => ADDRESS OF 0th INTEGER FROM BASE ADDRESS OF 0th ROW (iArray[0][0])
    // (iArray[0] + 1) => ADDRESS OF 1st INTEGER FROM BASE ADDRESS OF 0th ROW (iArray[0][1])
    // (iArray[0] + 2) => ADDRESS OF 2nd INTEGER FROM BASE ADDRESS OF 0th ROW (iArray[0][2])

    // (iArray[1] + 0) => ADDRESS OF 0th INTEGER FROM BASE ADDRESS OF 1st ROW (iArray[1][0])
    // (iArray[1] + 1) => ADDRESS OF 1st INTEGER FROM BASE ADDRESS OF 1st ROW (iArray[1][1])
    // (iArray[1] + 2) => ADDRESS OF 2nd INTEGER FROM BASE ADDRESS OF 1st ROW (iArray[1][2])

    // iArray[0], iArray[1]... ARE 1D INTEGR ARRAYS AND HENCE CAN BE TREATED AS 1D INTEGER ARRAYS USING POINTERS ... 
    // 'iArray' IS THE NAME AND BASE ADDRESS OF 2D INTEGER ARRAY *** 
    // (*(iArray + 0) + 0) = (iArray[0] + 0) = ADDRESS OF 0th ELEMENT FROM BASE ADDRESS OF 0th ROW= (iArray[0] + 0) = (iArray[0][0]) 
    // (*(iArray + 0) + 1) = (iArray[0] + 1) = ADDRESS OF 1st ELEMENT FROM BASE ADDRESS OF 0th ROW= (iArray[0] + 1) = (iArray[0][1]) 
    // (*(iArray + 0) + 2) = (iArray[0] + 2) = ADDRESS OF 2nd ELEMENT FROM BASE ADDRESS OF 0th ROW= (iArray[0] + 2) = (iArray[0][2]) 

    // (*(iArray + 1) + 0) = (iArray[1] + 0) = ADDRESS OF 0th ELEMENT FROM BASE ADDRESS OF 1st ROW = (iArray[1] + 0) = (iArray[1][0]) 
    // (*(iArray + 1) + 1) = (iArray[1] + 1) = Address Of 1st Element From Base Address Of 1st Row= (iArray[1] + 1) = (iArray[1][1]) 
    // (*(iArray + 1) + 2) = (iArray[1] + 2) = Address Of 2nd Element From Base Address Of 1st Row= (iArray[1] + 2) = (iArray[1][2]) 


    for (sps_i = 0; sps_i < NUM_ROWS; sps_i++)
    {
        for (sps_j = 0; sps_j < NUM_COLUMNS; sps_j++)
            *(*(iArray + sps_i) + sps_j) = (sps_i + 1) * (sps_j + 1); // 'iArray[sps_i]' Can Be Treated As 1D Array Using Pointers..
    }
    
    printf("\n\n");
    printf("2D Integer Array Elements Along With Addresses : \n\n");
    for (sps_i = 0; sps_i < NUM_ROWS; sps_i++)
    {
        for (sps_j = 0; sps_j < NUM_COLUMNS; sps_j++)
        {
            printf("*(*(iArray + %d) + %d)= %d \t \t At Address (*(iArray + %d) + % d) : %p\n", sps_i, sps_j, *(*(iArray + sps_i) + sps_j), sps_i, sps_j, (*(iArray + sps_i) + sps_j));
        }
        printf("\n\n");
    }
    
    return(0);
}

