#include <stdio.h>
#include <stdlib.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3

int main(void)
{
	// Variable declarations
	int *iArray[NUM_ROWS]; //A 2D Array which will have 5 rows and number of columns can be decided later on.....
	int sps_i, sps_j;

	// Code
	printf("\n\n");
	for (sps_i = 0; sps_i < NUM_ROWS; sps_i++)
	{
		iArray[sps_i] = (int*)malloc(NUM_COLUMNS * sizeof(int));
		if (iArray[sps_i] == NULL)
		{
			printf("FAILED TO ALLOCATE MEMORY TO ROW %d OF 2D INTEGER ARRAY !!! EXITING NOW...\n\n", sps_i);
			exit(0);
		}
		else
			printf("MEMORY ALLOCATION TO ROW %d OF 2D INTEGER ARRAY SUCCEEDED !!!\n\n", sps_i);
	}

	// Assigning Values To 2D Array...
	for (sps_i = 0; sps_i < NUM_ROWS; sps_i++)
	{
		for (sps_j = 0; sps_j < NUM_COLUMNS; sps_j++)
		{
			iArray[sps_i][sps_j] = (sps_i + 1) * (sps_j + 1);
		}
	}

	// Displaying 2D Array
	printf("\n\n");
	printf("DISPLAYING 2D ARRAY : \n\n");
	for (sps_i = 0; sps_i < NUM_ROWS; sps_i++)
	{
		for (sps_j = 0; sps_j < NUM_COLUMNS; sps_j++)
		{
			printf("iArray[%d][%d] = %d\n", sps_i, sps_j, iArray[sps_i][sps_j]);
		}
		printf("\n\n");
	}
	printf("\n\n");

	// FREEING MEMORY ASSIGNED TO 2D ARRAY (MUST BE DONE IN REVERSE ORDER)
	for (sps_i = (NUM_ROWS - 1); sps_i >= 0; sps_i--)
	{
		free(iArray[sps_i]);
		iArray[sps_i] = NULL;
		printf("MEMORY ALLOCATED TO ROW %d OF 2D INTEGER ARRAY HAS BEEN SUCCESSFULLY FREED !!!\n\n", sps_i);
	}

	return(0);
}

