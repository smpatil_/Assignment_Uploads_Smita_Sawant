#include <stdio.h>
#include <stdlib.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3

int main(void)
{
    // Variable Declarations
    int iArray[NUM_ROWS][NUM_COLUMNS];
    int sps_i, sps_j;

    //Code
    for (sps_i = 0; sps_i < NUM_ROWS; sps_i++)
    {
        for (sps_j = 0; sps_j < NUM_COLUMNS; sps_j++)
            iArray[sps_i][sps_j] = (sps_i + 1) * (sps_j + 1);
    }

    printf("\n\n");
    printf("2D Integer Array Elements Along With Address : \n\n");
    for (sps_i = 0; sps_i < NUM_ROWS; sps_i++)
    {
        for (sps_j = 0; sps_j < NUM_COLUMNS; sps_j++)
        {
            printf("iArray[%d][%d] = %d \t \t At Address : %p\n", sps_i, sps_j, iArray[sps_i][sps_j], &iArray[sps_i][sps_j]);
        }
        printf("\n\n");
    }

    return(0);
}

