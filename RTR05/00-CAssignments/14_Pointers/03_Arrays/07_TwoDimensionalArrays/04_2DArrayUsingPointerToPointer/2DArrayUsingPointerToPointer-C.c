#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	// Variable Declarations
	int **ptr_iArray = NULL; //A pointer-to-pointer to integer, but can also hold base address of a 2D Array which can have any number of rows and any number of columns.
	int sps_i, sps_j;
	int num_rows, num_columns;
	
	// Code
	
	// **** ACCEPT NUMBER OF ROWS 'num_rows' FROM USER ****
	printf("\n\n");
	printf("Enter Number Of Rows : ");
	scanf("%d", &num_rows);

	// **** ACCEPT NUMBER OF COLUMNS 'num_columns' FROM USER ****
	printf("\n\n");
	printf("Enter Number Of Columns : ");
	scanf("%d", &num_columns);

	// **** ALLOCATING MEMORY TO 1D ARRAY CONSISTING OF BASE ADDRESS OF ROWS ****
	printf("\n\n");
	printf("****** MEMORY ALLOCATION TO 2D INTEGER ARRAY ******\n\n");
	ptr_iArray = (int **)malloc(num_rows * sizeof(int *));
	if (ptr_iArray == NULL)
	{
		printf("FAILED TO ALLOCATE MEMORY TO %d ROWS OF 2D INTEGER ARRAY !!! EXITTING NOW...\n\n", num_rows);
		exit(0);
	}
	else
		printf("MEMORY ALLOCATION TO %d ROWS OF 2D INTEGER ARRAY SUCCEEDED !!!\n\n", num_rows);

	 // **** ALLOCATING MEMORY TO EACH ROW WHICH IS A lD ARRAY CONTAINING CONSISTING OF COLUMNS WHICH CONTAIN THE ACTUAL INTEGERS **** 
	for (sps_i = 0; sps_i < num_rows; sps_i++)
	{
		ptr_iArray[sps_i] = (int *)malloc(num_columns * sizeof(int)); //ALLOCATING MEMORY (Number Of Columns * size of 'int') TO ROW 'sps_i' 
		if (ptr_iArray[sps_i] == NULL) //ROW 'sps_i' MEMORY ALLOCATED?
		{
			printf("FAILED TO ALLOCATE MEMORY TO COLUMNS OF ROW %d OF 2D INTEGER ARRAY!!! EXITTING NOW...\n\n", sps_i);
			exit(0);
		}
		else
			printf("MEMORY ALLOCATION TO COLUMNS OF ROW %d OF 2D INTEGER ARRAY SUCCEEDED !!!\n\n", sps_i);
	}

	// **** FILLING UP VALUES  ****
	for (sps_i = 0; sps_i < num_rows; sps_i++)
	{
		for (sps_j = 0; sps_j < num_columns; sps_j++)
		{
			ptr_iArray[sps_i][sps_j] = (sps_i * 1) + (sps_j * 1); // Can also use *(*(ptr_iArray + sps_i]) + sps_j) = (sps_i] * 1) + (sps_j * 1)
		}
	}
	
	// **** DISPLAYING VALUES ****
	for (sps_i = 0; sps_i < num_rows; sps_i++)
	{
		printf("Base Address Of Row %d : ptr_iArray[%d] %p \t At Address : %p\n", sps_i, sps_i, ptr_iArray[sps_i], &ptr_iArray[sps_i]);
	}

	printf("\n\n");
	
	for (sps_i = 0; sps_i < num_rows; sps_i++)
	{
		for (sps_j = 0; sps_j < num_columns; sps_j++)
		{
			printf("ptr_iArray[%d][%d] = %d \t At Address : %p\n", sps_i, sps_j, ptr_iArray[sps_i][sps_j], &ptr_iArray[sps_i][sps_j]); // Can also use *(*(ptr_iArray + sps_i) + sps_j) for value and *(ptr_iArray + sps_i) + sps_j for address.
		}
		printf("\n");
	}

	// **** FREEING MEMORY ALLOCATED TO EACH ROW ****
	for (sps_i = (num_rows - 1); sps_i >= 0; sps_i--)
	{
		if (ptr_iArray[sps_i])
		{
			free(ptr_iArray[sps_i]);
			ptr_iArray[sps_i] = NULL;
			printf("MEMORY ALLOCATED TO ROW %d HAS BEEN SUCCESSFULLY FREED !!!\n\n", sps_i);
		}
	}

	// **** FREEING MEMORY ALLOCATED TO 1D ARRAY CONSISTING OF BASE ADDRESSES OF ROWS ***
	if (ptr_iArray)
	{
		free(ptr_iArray);
		ptr_iArray = NULL;
		printf("MEMORY ALLOCATED TO ptr_iArray HAS BEEN SUCCESSFULLY FREED !!!\n\n");
	}
	
	return(0);
}
