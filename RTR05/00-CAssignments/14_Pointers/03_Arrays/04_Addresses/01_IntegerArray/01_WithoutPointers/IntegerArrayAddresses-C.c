#include <stdio.h>

int main(void)
{
	//Variable Declarations
	int sps_iArray[10];
	int sps_i;

	//Code
	for (sps_i = 0; sps_i < 10; sps_i++)
		sps_iArray[sps_i] = (sps_i + 1) * 3;

	printf("\n\n");
	printf("Elements Of The Integer Array : \n\n");
	for (sps_i = 0; sps_i < 10; sps_i++)
		printf("sps_iArray[%d] = %d\n", sps_i, sps_iArray[sps_i]);

	printf("\n\n");
	printf("Elements Of The Integer Array : \n\n");
	for (sps_i = 0; sps_i < 10; sps_i++)
		printf("sps_iArray[%d] = %d \t \t Address = %p\n", sps_i, sps_iArray[sps_i], &sps_iArray[sps_i]);

	printf("\n\n");

	return(0);
}

