#include <stdio.h>

int main(void)
{
	//Variable Declarations
	int sps_iArray[10];
	int *ptr_iArray = NULL;
	int sps_i;

	//Code
	for (sps_i = 0; sps_i < 10; sps_i++)
		sps_iArray[sps_i] = (sps_i + 1) * 3;

	// *** Name Of Any Array Is Its Base Address ***
	// *** Hence, 'sps_iArray' Is The Base Address Of Array sps_iArray[] OR 'sps_iArray' Is The Address Of Element sps_iArray[0] ***
	// *** Assigning Base Address Of Array 'sps_iArray[]' To Integer Pointer 'ptr_iArray'

	ptr_iArray = sps_iArray; // ptr_iArray = &sps_iArray[0]

	printf("\n\n");
	printf("Elements Of The Integer Array : \n\n");
	for (sps_i = 0; sps_i < 10; sps_i++)
		printf("sps_iArray[%d] = %d\n", sps_i, *(ptr_iArray + sps_i));

	printf("\n\n");
	printf("Elements Of The Integer Array : \n\n");
	for (sps_i = 0; sps_i < 10; sps_i++)
		printf("sps_iArray[%d] = %d \t \t Address = %p\n", sps_i, *(ptr_iArray + sps_i), (ptr_iArray + sps_i));

	printf("\n\n");

	return(0);
}

