#include <stdio.h>

int main(void)
{
	// variable declarations
	char sps_cArray[10];
	int sps_i;

	//Code
	for (sps_i = 0; sps_i < 10; sps_i++)
		sps_cArray[sps_i] = (char)(sps_i + 65);

	printf("\n\n");
	printf("Elements Of The Character Array : \n\n");
	for (sps_i = 0; sps_i < 10; sps_i++)
		printf("sps_cArray[%d] = %c\n", sps_i, sps_cArray[sps_i]);

	printf("\n\n");
	printf("Elements Of The Character Array : \n\n");
	for (sps_i = 0; sps_i < 10; sps_i++)
		printf("sps_cArray[%d] = %c \t \t Address = %p\n", sps_i, sps_cArray[sps_i], &sps_cArray[sps_i]);

	printf("\n\n");

	return(0);
}

