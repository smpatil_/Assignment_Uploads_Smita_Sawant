#include <stdio.h>

int main(void)
{
	//Variable Declarations
	char sps_cArray[10];
	char *ptr_cArray = NULL;
	int sps_i;

	//Code
	for (sps_i = 0; sps_i < 10; sps_i++)
		sps_cArray[sps_i] = (char)(sps_i + 65);

	// *** Name Of Any Array Is Its Base Address ***
	// *** Hence, 'sps_cArray' Is The Base Address Of Array sps_cArray[] OR 'sps_cArray' Is The Address Of Element sps_cArray[0] ***
	// *** Assigning Base Address Of Array 'sps_cArray[]' To Character Pointer 'ptr_cArray'

	ptr_cArray = sps_cArray; // ptr_cArray = &sps_cArray[0]

	printf("\n\n");
	printf("Elements Of The Character Array : \n\n");
	for (sps_i = 0; sps_i < 10; sps_i++)
		printf("sps_cArray[%d] = %c\n", sps_i, *(ptr_cArray + sps_i));

	printf("\n\n");
	printf("Elements Of The Character Array : \n\n");
	for (sps_i = 0; sps_i < 10; sps_i++)
		printf("sps_cArray[%d] = %c \t \t Address = %p\n", sps_i, *(ptr_cArray + sps_i), (ptr_cArray + sps_i));

	printf("\n\n");

	return(0);
}

