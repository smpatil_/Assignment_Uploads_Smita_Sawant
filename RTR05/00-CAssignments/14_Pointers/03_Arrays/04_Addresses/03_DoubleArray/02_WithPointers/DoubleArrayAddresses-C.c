#include <stdio.h>

int main(void)
{
	//Variable Declarations
	float sps_dArray[10];
	float *ptr_dArray = NULL;
	int sps_i;

	//Code
	for (sps_i = 0; sps_i < 10; sps_i++)
		sps_dArray[sps_i] = (float)(sps_i + 1) * 1.333333f;

	// *** Name Of Any Array Is Its Base Address ***
	// *** Hence, 'sps_dArray' Is The Base Address Of Array sps_dArray[] OR 'sps_dArray' Is The Address Of Element sps_dArray[0] ***
	// *** Assigning Base Address Of Array 'sps_dArray[]' To Double Pointer 'ptr_dArray'

	ptr_dArray = sps_dArray; // ptr_dArray = &sps_dArray[0]

	printf("\n\n");
	printf("Elements Of The Double Array : \n\n");
	for (sps_i = 0; sps_i < 10; sps_i++)
		printf("sps_dArray[%d] = %lf\n", sps_i, *(ptr_dArray + sps_i));

	printf("\n\n");
	printf("Elements Of The Double Array : \n\n");
	for (sps_i = 0; sps_i < 10; sps_i++)
		printf("sps_dArray[%d] = %lf \t \t Address = %p\n", sps_i, *(ptr_dArray + sps_i), (ptr_dArray + sps_i));

	printf("\n\n");

	return(0);
}

