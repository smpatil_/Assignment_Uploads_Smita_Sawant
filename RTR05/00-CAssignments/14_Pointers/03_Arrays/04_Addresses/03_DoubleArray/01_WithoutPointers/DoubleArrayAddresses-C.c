#include <stdio.h>

int main(void)
{
	//Variable Declarations
	double sps_dArray[10];
	int sps_i;

	//Code
	for (sps_i = 0; sps_i < 10; sps_i++)
		sps_dArray[sps_i] = (float)(sps_i + 1) * 1.333333f;

	printf("\n\n");
	printf("Elements Of The Double Array : \n\n");
	for (sps_i = 0; sps_i < 10; sps_i++)
		printf("sps_dArray[%d] = %lf\n", sps_i, sps_dArray[sps_i]);

	printf("\n\n");
	printf("Elements Of The Double Array : \n\n");
	for (sps_i = 0; sps_i < 10; sps_i++)
		printf("sps_dArray[%d] = %lf \t \t Address = %p\n", sps_i, sps_dArray[sps_i], &sps_dArray[sps_i]);

	printf("\n\n");

	return(0);
}

