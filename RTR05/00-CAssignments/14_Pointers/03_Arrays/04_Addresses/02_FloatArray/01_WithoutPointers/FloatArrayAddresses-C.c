#include <stdio.h>

int main(void)
{
	//Variable Declarations
	float sps_fArray[10];
	int sps_i;

	//Code
	for (sps_i = 0; sps_i < 10; sps_i++)
		sps_fArray[sps_i] = (float)(sps_i + 1) * 1.5f;

	printf("\n\n");
	printf("Elements Of The Float Array : \n\n");
	for (sps_i = 0; sps_i < 10; sps_i++)
		printf("sps_fArray[%d] = %f\n", sps_i, sps_fArray[sps_i]);

	printf("\n\n");
	printf("Elements Of The Float Array : \n\n");
	for (sps_i = 0; sps_i < 10; sps_i++)
		printf("sps_fArray[%d] = %f \t \t Address = %p\n", sps_i, sps_fArray[sps_i], &sps_fArray[sps_i]);

	printf("\n\n");

	return(0);
}

