#include <stdio.h>

int main(void)
{
	//Variable Declarations
	float sps_fArray[10];
	float *ptr_fArray = NULL;
	int sps_i;

	//Code
	for (sps_i = 0; sps_i < 10; sps_i++)
		sps_fArray[sps_i] = (float)(sps_i + 1) * 1.5f;

	// *** Name Of Any Array Is Its Base Address ***
	// *** Hence, 'sps_fArray' Is The Base Address Of Array sps_fArray[] OR 'sps_fArray' Is The Address Of Element sps_fArray[0] ***
	// *** Assigning Base Address Of Array 'sps_fArray[]' To Float Pointer 'ptr_fArray'

	ptr_fArray = sps_fArray; // ptr_fArray = &sps_fArray[0]

	printf("\n\n");
	printf("Elements Of The Float Array : \n\n");
	for (sps_i = 0; sps_i < 10; sps_i++)
		printf("sps_fArray[%d] = %f\n", sps_i, *(ptr_fArray + sps_i));

	printf("\n\n");
	printf("Elements Of The Float Array : \n\n");
	for (sps_i = 0; sps_i < 10; sps_i++)
		printf("sps_fArray[%d] = %f \t \t Address = %p\n", sps_i, *(ptr_fArray + sps_i), (ptr_fArray + sps_i));

	printf("\n\n");

	return(0);
}

