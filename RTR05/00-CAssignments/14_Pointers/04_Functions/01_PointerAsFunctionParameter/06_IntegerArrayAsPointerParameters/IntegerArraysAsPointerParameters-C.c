#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	// Function declarations
	void MultiplyArrayElementsByNumber(int*, int, int);

	// Variable declaration
	int *iArray = NULL;
	int num_elements;
	int sps_i, sps_num;

	// Code
	printf("\n\n");
	printf("Enter How Many Elements You Want In The Integer Array : ");
	scanf("%d", &num_elements);

	iArray = (int *)malloc(num_elements * sizeof(int));
	if (iArray == NULL)
	{
		printf("MEMORY ALLOCATION TO 'iArray' HAS FAILED!!! EXITTING NOW...\n\n");
		exit(0);
	}

	printf("\n\n");
	printf("Enter %d Elements For The Integer Array :\n\n", num_elements);
	for (sps_i = 0; sps_i < num_elements; sps_i++)
		scanf("%d", &iArray[sps_i]);

	//**** ONE ****
	printf("\n\n");
	printf("Array Before Passing To Function MultiplyArrayElementsByNumber() :\n\n");
	for (sps_i = 0; sps_i < num_elements; sps_i++)
		printf("iArray[%d] = %d\n", sps_i, iArray[sps_i]);
		printf("\n\n");
	printf("Enter The Number By Which You Want To Multiply Each Array Element :");
	scanf("%d", &sps_num);

	MultiplyArrayElementsByNumber(iArray, num_elements, sps_num);
	printf("\n\n");
	printf("Array Returned By Function MultiplyArrayElementsByNumber() :\n\n");
	for (sps_i = 0; sps_i < num_elements; sps_i++)
		printf("iArray[%d] = %d\n", sps_i, iArray[sps_i]);
	if (iArray)
	{
		free(iArray);
		iArray = NULL;
		printf("\n\n");
		printf("MEMORY ALLOCATED TO 'iArray' HAS BEEN SUCCESSFULLY FREED !!!\n\n");
	}

	return(0);
}

void MultiplyArrayElementsByNumber(int *arr, int iNumElements, int n)
{
	//variable declarations
	int sps_i;

	//code
	for (sps_i = 0; sps_i < iNumElements; sps_i++)
		arr[sps_i] = arr[sps_i] * n;
}

