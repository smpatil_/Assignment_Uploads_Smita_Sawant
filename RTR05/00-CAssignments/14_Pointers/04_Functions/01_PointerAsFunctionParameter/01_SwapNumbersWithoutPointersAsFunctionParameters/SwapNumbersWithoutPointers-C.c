#include <stdio.h>

int main(void)
{
	// Function Declarations
	void SwapNumbers(int, int);

	// Variable Declarations
	int sps_a;
	int sps_b;

	// Code
	printf("\n\n");
	printf("Enter Value For 'A' : ");
	scanf("%d", &sps_a);

	printf("\n\n");
	printf("Enter Value For 'B' : ");
	scanf("%d", &sps_b);

	printf("\n\n");
	printf("***** Before Swapping *****\n\n");
	printf("Value Of 'A' = %d\n\n", sps_a);
	printf("Value Of 'B' = %d\n\n", sps_b);

	SwapNumbers(sps_a, sps_b); // ***** Arguments Passed 'By Value'... *****

	printf("\n\n");
	printf("***** After Swapping *****\n\n");
	printf("Value Of 'A' = %d\n\n", sps_a);
	printf("Value Of 'B' = %d\n\n", sps_b);

	return(0);
}

void SwapNumbers(int sps_x, int sps_y) // Value of 'sps_a' is copied into 'sps_x' and value of 'sps_b' is copied into 'sps_y'... swapping takes place between 'sps_x' and 'sps_y', not between 'sps_a' and 'sps_b'...
{
	// Variable Declarrations
	int temp;

	// Code
	printf("\n\n");
	printf("***** Before Swapping *****\n\n");
	printf("Value Of 'X' = %d\n\n", sps_x);
	printf("Value Of 'Y' = %d\n\n", sps_y);

	temp = sps_x;
	sps_x = sps_y;
	sps_y = temp;

	printf("\n\n");
	printf("***** After Swapping *****\n\n");
	printf("Value Of 'X' = %d\n\n", sps_x);
	printf("Value Of 'Y' = %d\n\n", sps_y);

}

