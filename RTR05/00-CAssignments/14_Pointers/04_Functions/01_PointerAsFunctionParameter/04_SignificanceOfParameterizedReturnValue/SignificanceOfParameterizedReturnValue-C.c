#include <stdio.h> 

enum
{
	NEGATIVE = -1,
	ZERO,
	POSITIVE
};

int main(void)
{
	// Function declarations 
	int Difference(int, int, int *);

	// Variable declaration 
	int a;
	int b;
	int answer, ret;

	// Code 
	printf("\n\n");
	printf("Enter Value Of 'A' : ");
	scanf("%d", &a);

	printf("\n\n");
	printf("Enter Value Of 'B' : ");
	scanf("%d", &b);

	ret = Difference(a, b, &answer);

	printf("\n\n");
	printf("Difference Of %d and %d	= %d\n\n", a, b, answer);

	if (ret == POSITIVE)
		printf("The Difference Of %d And %d Is Positive !!!\n\n", a, b);

	else if (ret == NEGATIVE)
		printf("The Difference Of %d And %d Is Negative !!!\n\n", a, b);

	else
		printf("The Difference Of %d And %d Is Zero !!!\n\n", a, b);

	return(0);
}

//we want our function difference() to perform 2 jobs ... 
// One, is to subtract the input numbers ('y' from 'x') and the second, is to tell whether the difference of 'x' and 'y'	is positive or negative or zero ... 
// But any function has only one valid return value, then how can we manage to return two values to the calling function? 
// This is where parameterized return value comes into the picture ...  
// we can return the actual difference of 'x' and 'y', that is, the actual answer value, via out-parameter/ parameterized return value 
// And we can return the status of the answer (positive/ negative/ zero) via the actual return value of the function ...   

int Difference(int x, int y, int *diff)
{
	//code 
	*diff = x - y;

	if (*diff > 0)
		return(POSITIVE);

	else if (*diff < 0)
		return(NEGATIVE);
	
	else
		return(ZERO);
}

