#include <stdio.h>

int main(void)
{
	// Function Declarations
	void SwapNumbers(int *, int *);

	// Variable Declarations
	int sps_a;
	int sps_b;

	// Code
	printf("\n\n");
	printf("Enter Value For 'A' : ");
	scanf("%d", &sps_a);

	printf("\n\n");
	printf("Enter Value For 'B' : ");
	scanf("%d", &sps_b);

	printf("\n\n");
	printf("***** Before Swapping *****\n\n");
	printf("Value Of 'A' = %d\n\n", sps_a);
	printf("Value Of 'B' = %d\n\n", sps_b);

	SwapNumbers(&sps_a, &sps_b); // ***** Arguments Passed 'By Reference / Address'... *****

	printf("\n\n");
	printf("***** After Swapping *****\n\n");
	printf("Value Of 'A' = %d\n\n", sps_a);
	printf("Value Of 'B' = %d\n\n", sps_b);

	return(0);
}

// Address of 'sps_a' is copied into 'sps_x' and address of 'sps_b' is copied into 'sps_y'.. So, '&sps_a' and 'sps_x' are pointing to ONE and the SAME address and '&sps_b' and 'sps_y' are pointing to ONE and the SAME address...
// Swapping takes place between 'value at address of 'sps_x' (value at &sps_a i.e. : 'sps_a') and 'value at address of 'sps_y' (value at &sps_b i.e. : 'sps_b')...
//Hence, swapping in this case takes place between '*sps_x' and '*sps_y' as well as between 'sps_a' and 'sps_b'...

void SwapNumbers(int *sps_x, int *sps_y)
{
	// Variable Declarations
	int temp;

	// Code
	printf("\n\n");
	printf("***** Before Swapping *****\n\n");
	printf("Value Of 'X' = %d\n\n", *sps_x);
	printf("Value Of 'Y' = %d\n\n", *sps_y);

	temp = *sps_x;
	*sps_x = *sps_y;
	*sps_y = temp;

	printf("\n\n");
	printf("***** After Swapping *****\n\n");
	printf("Value Of 'X' = %d\n\n", *sps_x);
	printf("Value Of 'Y' = %d\n\n", *sps_y);
}

