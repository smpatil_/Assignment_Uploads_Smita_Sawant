#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	// Function Declarations
	void MathematicalOperations(int, int, int *, int *, int *, int *, int *);
	
	//variable declaration
	int sps_a;
	int sps_b;
	int *answer_sum = NULL;
	int *answer_difference = NULL;
	int *answer_product = NULL;
	int *answer_quotient = NULL;
	int *answer_remainder = NULL;

	// Code
	printf("\n\n");
	printf("Enter Value Of 'A' : ");
	scanf("%d", &sps_a);

	printf("\n\n");
	printf("Enter Value Of 'B' : ");
	scanf("%d", &sps_b);

	// Passing Addresses To Function ... Function Will Fill Them Up With Values ... Hence, They Go Into The Function As Address Parameters And Come Out Of The Function Filled With Valid Values 

	// Thus, (&answer_sum, &answer_difference, &answer_product, &answer_quotient, &answer_remainder) are Called "Out Parameters" or "Parameterized Return Values"... Return Values Of Functions Coming Via Parameters
	
	// Hence, Although Each Function Has Only One Return Value, Using The Concept Of "Parameterized Return Values", Our Function "MathematicalOperations()" Has Given Us 5 Return Values!!!

	answer_sum = (int *)malloc(1 * sizeof(int));
	if (answer_sum == NULL)
	{
		printf("Could Not Allocate Memory For 'answer sum'. Exitting Now...\n\n");
		exit(0);
	}

	answer_difference = (int *)malloc(1 * sizeof(int));
	if (answer_difference  == NULL)
	{
		printf("Could Not Allocate Memory For 'answer difference'. Exitting Now...\n\n"); 
		exit(0);
	}

	answer_product = (int *)malloc(1 * sizeof(int));
	if (answer_product == NULL)
	{
		printf("Could Not Allocate Memory For 'answer_product'. Exitting Now...\n\n");
		exit(0);
	}
	
	answer_quotient = (int *)malloc(1 * sizeof(int));
	if (answer_quotient == NULL)
	{
		printf("Could Not Allocate Memory For 'answer_quotient'. Exitting Now...\n\n");
		exit(0);
	}
	
	answer_remainder = (int *)malloc(1 * sizeof(int));
	if (answer_remainder == NULL)
	{
		printf("Could Not Allocate Memory For 'answer remainder'. Exitting Now...\n\n");
		exit(0);
	}
	
	MathematicalOperations(sps_a, sps_b, answer_sum, answer_difference, answer_product, answer_quotient, answer_remainder);
	
	printf("\n\n");
	printf("**** RESULTS **** \n\n");
	printf("Sum = %d\n\n", *answer_sum);
	printf("Difference = %d\n\n", *answer_difference);
	printf("Product = %d\n\n", *answer_product);
	printf("Quotient = %d\n\n", *answer_quotient);
	printf("Remainder = %d\n\n", *answer_remainder);
	
	if (answer_remainder)
	{
		free(answer_remainder);
		answer_remainder = NULL;
		printf("Memory Allocated For 'answer_remainder' Successfully Freed !!!\n\n");
	}
	
	if (answer_quotient)
	{
		free(answer_quotient); 
		answer_quotient = NULL;
		printf("Memory Allocated For 'answer_quotient' Successfully Freed !!!\n\n");
	}
	
	if (answer_product)
	{
		free(answer_product);
		answer_product = NULL;
		printf("Memory Allocated For 'answer_product' Successfully Freed !!!\n\n");
	}
	
	if (answer_difference)
	{
		free(answer_difference); 
		answer_difference = NULL;
		printf("Memory Allocated For 'answer_difference' Successfully Freed !!!\n\n");
	}

	if (answer_sum)
	{
		free(answer_sum);
		answer_sum = NULL;
		printf("Memory Allocated For 'answer_sum' Successfully Freed !!!\n\n");
	}

	return(0);
}

void MathematicalOperations(int sps_x, int sps_y, int *sum, int *difference, int *product, int *quotient, int *remainder)
{
	//Code
	*sum = sps_x + sps_y;				// Value at address 'sum' = (sps_x + sps_y)
	*difference = sps_x - sps_y;		// Value at address 'difference' = (sps_x - sps_y)
	*product = sps_x * sps_y;			// Value at address 'product' = (sps_x * sps_y)
	*quotient = sps_x / sps_y;			// Value at address 'quotient' = (sps_x / sps_y)
	*remainder = sps_x % sps_y;			// Value at address 'remainder' = (sps_x % sps_y)
}

