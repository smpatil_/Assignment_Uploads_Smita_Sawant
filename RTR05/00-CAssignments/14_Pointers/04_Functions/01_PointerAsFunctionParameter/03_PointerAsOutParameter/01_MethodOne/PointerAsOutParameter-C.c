#include <stdio.h>

int main(void)
{
	//function declarations 
	void MathematicalOperations(int, int, int *, int *, int *, int *, int *);

	// Variable Declarations
	int a;
	int b;
	int answer_sum;
	int answer_difference;
	int answer_product;
	int answer_quotient;
	int answer_remainder;

	//code 
	printf("\n\n");
	printf("Enter Value of 'A' : ");
	scanf("%d", &a);

	printf("\n\n");
	printf("Enter Value of 'B' : ");
	scanf("%d", &b);

	// Passing Addresses To Function ... Function will fill them Up with values ... Hence, they go into the function as address parameters and come out of the function filled with valid values.
	// Thus, (&answer_sum, &answer_difference, &answer_product, &answer_quotient, &answer_remainder) are called "out parameters" or "parameterized return values" ... return values of functions coming via parameters 
	// Hence, although each function has only one return value, using the concept of "parameterized return values", our function "MathematicalOperations()" has given us 5 return values..!!

	MathematicalOperations(a, b, &answer_sum, &answer_difference, &answer_product, &answer_quotient, &answer_remainder);

	printf("\n\n");
	printf("****** Results ****** : \n\n");
	printf("Sum = %d\n\n", answer_sum);
	printf("Difference = %d\n\n", answer_difference);
	printf("Product = %d\n\n", answer_product);
	printf("Quotient = %d\n\n", answer_quotient);
	printf("Remainder = %d\n\n", answer_remainder);
	return(0);
}

void MathematicalOperations(int x, int y, int *sum, int *difference, int *product, int *quotient, int *remainder)
{
	// Code
	*sum = x + y; // Value at address 'sum' = (x + y)
	*difference = x - y; // Value at address 'difference' = (x - y)
	*product = x * y; // Value at address 'product' = (x * y)
	*quotient = x / y; //Value at address 'quotient' = (x / y)
	*remainder = x % y; // Value at address 'remainder' = (x % y)
}

