#include <stdio.h>

int main(void)
{
	// Function declarations
	int Addintegers(int, int);
	int Subtractintegers(int, int);
	float AddFloats(float, float);
	
	// Variable declaration
	typedef int (*AddintsFnPtr)(int, int);
	AddintsFnPtr ptrAddTwointegers = NULL;
	AddintsFnPtr ptrFunc = NULL;
	
	typedef float (*AddFloatsFnPtr)(float, float);
	AddFloatsFnPtr ptrAddTwoFloats = NULL;

	int iAnswer = 0;
	float fAnswer = 0.0f;
	
	// Code
	ptrAddTwointegers = Addintegers;
	iAnswer = ptrAddTwointegers(9, 30);
	printf("\n\n");
	printf("Sum Of Integers= %d\n\n", iAnswer);

	ptrFunc = Subtractintegers;
	iAnswer = ptrFunc(9, 30);
	printf("\n\n");
	printf("Subtraction Of Integers %d\n\n", iAnswer);
	
	ptrAddTwoFloats = AddFloats;
	fAnswer = ptrAddTwoFloats(11.45f, 8.2f);
	printf("\n\n");
	printf("Sum Of Floating-Point Numbers= %f\n\n", fAnswer);
	
	return(0);
}

int Addintegers(int sps_a, int sps_b)
{
	// Varibale declarations
	int sps_c;
	
	// Code
	sps_c = sps_a + sps_b;
	return(sps_c);
}

int Subtractintegers(int sps_a, int sps_b)
{
	// Varibale declarations
	int sps_c;

	// Code
	if (sps_a > sps_b)
		sps_c = sps_a - sps_b;
	else
		sps_c = sps_b - sps_a;
	
	return(sps_c);
}

float AddFloats(float f_num1, float f_num2)
{
	// Varibale declarations
	float ans;
	
	//code
	ans = f_num1 + f_num2;
	return(ans);
}
