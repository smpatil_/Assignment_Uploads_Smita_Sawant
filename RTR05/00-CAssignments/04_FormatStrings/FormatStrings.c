#include <stdio.h>
int main(void)
{
	//code
	printf("\n\n");
	printf("*********************************************************************");
	printf("\n\n");

	printf("Hello World !!!\n\n");

	int sps_a = 13;
	printf("Integer Decimal Value Of 'sps_a' = %d\n", sps_a);
	printf("Integer Octal Value Of 'sps_a' = %o\n", sps_a);
	printf("Integer Hexadecimal Value Of 'sps_a' (Hexadecimal Letters In Lower Case) = %x\n", sps_a);
	printf("Integer Hexadecimal Value Of 'sps_a' (Hexadecimal Letters In Upper Case) = %X\n\n", sps_a);

	char sps_ch = 'A';
	printf("Character sps_ch = %c\n", sps_ch);
	char sps_str[] = "AstroMediComp's Real Time Rendering Batch";
	printf("String sps_str = %s\n\n", sps_str);

	long sps_num = 30121995L;
	printf("Long Integer = %ld\n\n", sps_num);

	unsigned int sps_b = 7;
	printf("Unsigned Integer 'sps_b' = %u\n\n", sps_b);

	float sps_f_num = 3012.1995f;
	printf("Floating Point Number With Just %%f 'sps_f_num' = %f\n", sps_f_num);
	printf("Floating Point Number With %%4.2f 'sps_f_num' = %4.2f\n", sps_f_num);
	printf("Floating Point Number With %%6.5f 'sps_f_num' = %6.5f\n\n", sps_f_num);

	double sps_d_pi = 3.14159265358979323846;
	printf("Double Precision Floating Point Number Without Exponential = %g\n", sps_d_pi);
	printf("Double Precision Floating Point Number With Exponential (Lower Case) = %e\n", sps_d_pi);
	printf("Double Precision Floating Point Number With Exponential (Upper Case) = %E\n\n", sps_d_pi);
	printf("Double Hexadecimal Value Of 'sps_d_pi' (Hexadecimal Letters In Lower Case) = %a\n", sps_d_pi);
	printf("Double Hexadecimal Value Of 'sps_d_pi' (Hexadecimal Letters In Upper Case) = %A\n\n", sps_d_pi);

	printf("***************************************************************************************\n");
	printf("\n\n");
	return(0);
}