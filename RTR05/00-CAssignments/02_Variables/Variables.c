#include <stdio.h>

int main(void)
{

	//variable declarations
	int sps_i = 5;
	float sps_f = 3.9f;
	double sps_d = 8.041997;
	char sps_c = 'A';

	//code
	printf("\n\n");

	printf("sps_i = %d\n", sps_i);
	printf("sps_f = %f\n", sps_f);
	printf("sps_d = %lf\n", sps_d);
	printf("sps_c = %c\n", sps_c);

	printf("\n\n");

	sps_i = 43;
	sps_f = 6.54f;
	sps_d = 26.1294;
	sps_c = 'P';

	printf("sps_i = %d\n", sps_i);
	printf("sps_f = %f\n", sps_f);
	printf("sps_d = %lf\n", sps_d);
	printf("sps_c = %c\n", sps_c);

	printf("\n\n");

	return(0);
}