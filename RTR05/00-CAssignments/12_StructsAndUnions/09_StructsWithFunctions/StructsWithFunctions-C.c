#include <stdio.h>

// Defining Struct
struct MyData
{
    int sps_i;
    float sps_f;
    double sps_d;
    char sps_c;
};

int main(void)
{
    // Function Prototype
    struct MyData AddStructMembers(struct MyData, struct MyData, struct MyData);

    // Variable Declarations
    struct MyData data1, data2, data3, answer_data;

    // Code
    // *** data1 ***
    printf("\n\n\n\n");
    printf("******* DATA 1 *******\n\n");
    printf("Enter Integer Value For 'sps_i' Of 'struct MyData data1' : ");
    scanf("%d", &data1.sps_i);

    printf("\n\n");
    printf("Enter Floating-Point Value For 'sps_f' Of 'struct MyData data1' : ");
    scanf("%f", &data1.sps_f);

    printf("\n\n");
    printf("Enter 'Double' Value For 'sps_d' Of 'struct MyData data1' : ");
    scanf("%lf", &data1.sps_d);

    printf("\n\n");
    printf("Enter Character Value For 'sps_c' Of 'struct MyData data1' : ");
    data1.sps_c = getch();
    printf("%c", data1.sps_c);

    // *** data2 ***
    printf("\n\n\n\n");
    printf("******* DATA 2 *******\n\n");
    printf("Enter Integer Value For 'sps_i' Of 'struct MyData data2' : ");
    scanf("%d", &data2.sps_i);

    printf("\n\n");
    printf("Enter Floating-Point Value For 'sps_f' Of 'struct MyData data2' : ");
    scanf("%f", &data2.sps_f);

    printf("\n\n");
    printf("Enter 'Double' Value For 'sps_d' Of 'struct MyData data2' : ");
    scanf("%lf", &data2.sps_d);

    printf("\n\n");
    printf("Enter Character Value For 'sps_c' Of 'struct MyData data2' : ");
    data2.sps_c = getch();
    printf("%c", data2.sps_c);

    // *** data3 ***
    printf("\n\n\n\n");
    printf("******* DATA 3 *******\n\n");
    printf("Enter Integer Value For 'sps_i' Of 'struct MyData data3' : ");
    scanf("%d", &data3.sps_i);

    printf("\n\n");
    printf("Enter Floating-Point Value For 'sps_f' Of 'struct MyData data3' : ");
    scanf("%f", &data1.sps_f);

    printf("\n\n");
    printf("Enter 'Double' Value For 'sps_d' Of 'struct MyData data3' : ");
    scanf("%lf", &data3.sps_d);

    printf("\n\n");
    printf("Enter Character Value For 'sps_c' Of 'struct MyData data3' : ");
    data3.sps_c = getch();
    printf("%c", data3.sps_c);

    // **** Calling function AddStructMembers() which accepts three variables of type 'struct MyData' as parameters and adds up the respective members and returns the answer in another struct of same type ****
    answer_data = AddStructMembers(data1, data2, data3);

    printf("\n\n\n\n");
    printf("******* ANSWER *******\n\n");
    printf("answer_data.sps_i = %d\n", answer_data.sps_i);
    printf("answer_data.sps_f = %f\n", answer_data.sps_f);
    printf("answer_data.sps_d = %lf\n", answer_data.sps_d);

    answer_data.sps_c = data1.sps_c;
    printf("answer_data.sps_c (from data1) = %c\n\n", answer_data.sps_c);

    answer_data.sps_c = data2.sps_c;
    printf("answer_data.sps_c (from data2) = %c\n\n", answer_data.sps_c);

    answer_data.sps_c = data3.sps_c;
    printf("answer_data.sps_c (from data3) = %c\n\n", answer_data.sps_c);

    return(0);
}

struct MyData AddStructMembers(struct MyData md_one, struct MyData md_two, struct MyData md_three)
{
    // Variable Declarations
    struct MyData answer;

    // Code
    answer.sps_i = md_one.sps_i + md_two.sps_i + md_three.sps_i;
    answer.sps_f = md_one.sps_f + md_two.sps_f + md_three.sps_f;
    answer.sps_d = md_one.sps_d + md_two.sps_d + md_three.sps_d;
    return(answer);
}
