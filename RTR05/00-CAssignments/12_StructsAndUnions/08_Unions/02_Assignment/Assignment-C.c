#include <stdio.h>

union MyUnion
{
    int sps_i;
    float sps_f;
    double sps_d;
    char sps_c;
};

int main(void)
{
    // Variable Declarations
    union MyUnion u1, u2;

    // Code
    // *** MyUnion u1 ***
    printf("\n\n");
    printf("Members Of Union u1 Are : \n\n");

    u1.sps_i = 6;
    u1.sps_f = 1.2f;
    u1.sps_d = 8.333333;
    u1.sps_c = 'S';

    printf("u1.sps_i = %d\n\n", u1.sps_i);
    printf("u1.sps_f = %f\n\n", u1.sps_f);
    printf("u1.sps_d = %lf\n\n", u1.sps_d);
    printf("u1.sps_c = %c\n\n", u1.sps_c);

    printf("Addresses Of Members Of Union u1 Are : \n\n");
    printf("u1.sps_i = %p\n\n", &u1.sps_i);
    printf("u1.sps_f = %p\n\n", &u1.sps_f);
    printf("u1.sps_d = %p\n\n", &u1.sps_d);
    printf("u1.sps_c = %p\n\n", &u1.sps_c);

    printf("MyUnion u1 = %p\n\n", &u1);

    // *** MyUnion u2 ***
    printf("\n\n");
    printf("Members Of Union u2 Are : \n\n");

    u2.sps_i = 3;
    printf("u2.sps_i = %d\n\n", u2.sps_i);

    u2.sps_f = 4.5f;
    printf("u2.sps_f = %f\n\n", u2.sps_f);

    u2.sps_d = 5.12764;
    printf("u2.sps_d = %lf\n\n", u2.sps_d);

    u2.sps_c = 'A';
    printf("u2.sps_c = %c\n\n", u2.sps_c);


    printf("Addresses Of Members Of Union u2 Are : \n\n");
    printf("u2.sps_i = %p\n\n", &u2.sps_i);
    printf("u2.sps_f = %p\n\n", &u2.sps_f);
    printf("u2.sps_d = %p\n\n", &u2.sps_d);
    printf("u2.sps_c = %p\n\n", &u2.sps_c);

    printf("MyUnion u2 = %p\n\n", &u2);

    return(0);
}

