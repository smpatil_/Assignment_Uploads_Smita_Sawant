#include <stdio.h>

struct MyStruct
{
    int sps_i;
    float sps_f;
    double sps_d;
    char sps_c;
};

union MyUnion
{
    int sps_i;
    float sps_f;
    double sps_d;
    char sps_c;
};

int main(void)
{
    // Variable Declarations
    struct MyStruct sps_s;
    union MyUnion sps_u;

    // Code
    printf("\n\n");
    printf("Members Of Struct Are : \n\n");

    sps_s.sps_i = 9;
    sps_s.sps_f = 8.7f;
    sps_s.sps_d = 1.233422;
    sps_s.sps_c = 'P';

    printf("sps_s.sps_i = %d\n\n", sps_s.sps_i);
    printf("sps_s.sps_f = %f\n\n", sps_s.sps_f);
    printf("sps_s.sps_d = %lf\n\n", sps_s.sps_d);
    printf("sps_s.sps_c = %c\n\n", sps_s.sps_c);


    printf("Addresses Of Members Of Struct Are : \n\n");
    printf("sps_s.sps_i = %p\n\n", &sps_s.sps_i);
    printf("sps_s.sps_f = %p\n\n", &sps_s.sps_f);
    printf("sps_s.sps_d = %p\n\n", &sps_s.sps_d);
    printf("sps_s.sps_c = %p\n\n", &sps_s.sps_c);

    printf("MyStruct sps_s = %p\n\n", &sps_s);

    printf("\n\n");
    printf("Members Of Union Are : \n\n");

    sps_u.sps_i = 3;
    printf("sps_u.sps_i = %d\n\n", sps_u.sps_i);

    sps_u.sps_f = 4.5f;
    printf("sps_u.sps_f = %f\n\n", sps_u.sps_f);

    sps_u.sps_d = 5.12764;
    printf("sps_u.sps_d = %lf\n\n", sps_u.sps_d);

    sps_u.sps_c = 'A';
    printf("sps_u.sps_c = %c\n\n", sps_u.sps_c);


    printf("Addresses Of Members Of Union Are : \n\n");
    printf("sps_u.sps_i = %p\n\n", &sps_u.sps_i);
    printf("sps_u.sps_f = %p\n\n", &sps_u.sps_f);
    printf("sps_u.sps_d = %p\n\n", &sps_u.sps_d);
    printf("sps_u.sps_c = %p\n\n", &sps_u.sps_c);

    printf("MyUnion sps_u = %p\n\n", &sps_u);

    return(0);
}

