#include <stdio.h>

struct MyStruct
{
    int sps_i;
    float sps_f;
    double sps_d;
    char sps_c;
};

union MyUnion
{
    int sps_i;
    float sps_f;
    double sps_d;
    char sps_c;
};

int main(void)
{
    // Variable Declarations
    struct MyStruct sps_s;
    union MyUnion sps_u;

    // Code
    printf("\n\n");
    printf("Size Of MyStruct = %zu\n", sizeof(sps_s));
    printf("\n\n");
    printf("Size Of MyUnion = %zu\n", sizeof(sps_u));
    return(0);
}
