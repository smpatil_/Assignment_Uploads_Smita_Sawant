#include <stdio.h>
#include <conio.h>

struct MyData
{
	int sps_i;
	float sps_f;
	double sps_d;
	char sps_ch;
};

int main(void)
{
	// Variable declarations
	struct MyData data; // Declaring a single struct variable
	
	// code
	// User inputs for the values of data members of 'struct MyData'
	printf("\n\n");

	printf("Enter Integer Value For Data Member 'i' Of 'struct MyData' : \n");
	scanf("%d", &data.sps_i);
	
	printf("Enter Floating-Point Value For Data Member 'f' Of 'struct MyData' : \n");
	scanf("%f", &data.sps_f);
	
	printf("Enter Double Value For Data Member 'd' Of 'struct MyData' : \n");
	scanf("%lf", &data.sps_d);
	
	printf("Enter Character Value For Data Member 'c' Of 'struct MyData' : \n");
	data.sps_ch = getch();
	
	// Displaying Values Of the Data Members Of 'struct MyData'
	printf("\n\n");
	printf("DATA MEMBERS OF 'struct MyData' ARE : \n\n");
	printf("i = %d\n", data.sps_i);
	printf("f = %f\n", data.sps_f);
	printf("d = %lf\n", data.sps_d);
	printf("c = %c\n\n", data.sps_ch);
	
	return(0);
}

