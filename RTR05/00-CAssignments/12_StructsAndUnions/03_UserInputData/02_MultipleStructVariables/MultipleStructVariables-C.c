#include <stdio.h>

struct MyPoint
{
	int sps_x;
	int sps_y;
};

int main(void)
{
	// variable declarations
	struct MyPoint point_A, point_B, point_C, point_D, point_E; // declaring 5 sruct variables of type 'struct MyPoint'

	//code
	//User inputs for the data members of 'struct MyPoint' variable 'point_A'
	printf("\n\n");
	printf("Enter X-Coordinate For Point 'A' : ");
	scanf("%d", &point_A.sps_x);
	printf("Enter Y-Coordinate For Point 'A' : ");
	scanf("%d", &point_A.sps_y);

	//User inputs for the data members of 'struct MyPoint' variable 'point_B'
	printf("\n\n");
	printf("Enter X-Coordinate For Point 'B' : ");
	scanf("%d", &point_B.sps_x);
	printf("Enter Y-Coordinate For Point 'B' : ");
	scanf("%d", &point_B.sps_y);

	//User inputs for the data members of 'struct MyPoint' variable 'point_C'
	printf("\n\n");
	printf("Enter X-Coordinate For Point 'C' : ");
	scanf("%d", &point_C.sps_x);
	printf("Enter Y-Coordinate For Point 'C' : ");
	scanf("%d", &point_C.sps_y);

	//User inputs for the data members of 'struct MyPoint' variable 'point_D'
	printf("\n\n");
	printf("Enter X-Coordinate For Point 'D' : ");
	scanf("%d", &point_D.sps_x);
	printf("Enter Y-Coordinate For Point 'D' : ");
	scanf("%d", &point_D.sps_y);

	//User inputs for the data members of 'struct MyPoint' variable 'point_E'
	printf("\n\n");
	printf("Enter X-Coordinate For Point 'E' : ");
	scanf("%d", &point_E.sps_x);
	printf("Enter Y-Coordinate For Point 'E' : ");
	scanf("%d", &point_E.sps_y);

	//Displaying values of the data members of 'struct MyPoint' (all variables)
	printf("\n\n");
	printf("Co-ordinates (sps_x,sps_y) Of Point 'A' Are : (%d, %d)\n\n", point_A.sps_x, point_A.sps_y);
	printf("Co-ordinates (sps_x,sps_y) Of Point 'B' Are : (%d, %d)\n\n", point_B.sps_x, point_B.sps_y);
	printf("Co-ordinates (sps_x,sps_y) Of Point 'C' Are : (%d, %d)\n\n", point_C.sps_x, point_C.sps_y);
	printf("Co-ordinates (sps_x,sps_y) Of Point 'D' Are : (%d, %d)\n\n", point_D.sps_x, point_D.sps_y);
	printf("Co-ordinates (sps_x,sps_y) Of Point 'E' Are : (%d, %d)\n\n", point_E.sps_x, point_E.sps_y);

	return(0);
}
