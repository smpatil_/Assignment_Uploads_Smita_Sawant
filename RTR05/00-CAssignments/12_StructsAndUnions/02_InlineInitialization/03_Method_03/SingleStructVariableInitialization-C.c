#include <stdio.h>

int main(void)
{
	//DEFINING STRUCT
	struct MyData
	{
		int sps_i;
		float sps_f;
		double sps_d;
		char sps_c;
	} data = { 5, 9.1f, 3.78623, 'N' };

	// Displaying Values Of the Data Members Of 'struct MyData'
	printf("\n\n");
	printf("DATA MEMBERS OF 'struct MyData' ARE : \n\n");
	printf("sps_i = %d\n", data.sps_i);
	printf("sps_f = %f\n", data.sps_f);
	printf("sps_d = %lf\n", data.sps_d);
	printf("sps_c = %c\n\n", data.sps_c);
	return(0);
}

