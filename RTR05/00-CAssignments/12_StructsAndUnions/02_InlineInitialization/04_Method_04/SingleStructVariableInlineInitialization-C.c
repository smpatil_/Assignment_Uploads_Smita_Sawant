#include <stdio.h>

//DEFINING STRUCT
struct MyData
{
	int sps_i;
	float sps_f;
	double sps_d;
	char sps_c;
};

int main(void)
{
	// Variable Declarations

	// 35 will be assigned to 'sps_i' of 'data_one'
	// 3.9 will be assigned to 'sps_f' of 'data_one'
	// 1.23765 will be assigned to 'sps_d' of 'data_one'
	// 'A' will be assigned to 'sps_c' of 'data_one'
	struct MyData data_one = { 35, 3.9f, 1.23765, 'A' }; // Inline initialiazation

	// 'P' will be assigned to 'sps_i' of 'data_two'..But 'P' is a character (char) and 'sps_i' is an integer.. In this case, 'P' gets converted into its decimal integer ASCII value (80) and 80 is assigned to 'sps_i' of 'data_two'.
	// 6.2 will be assigned to 'sps_f' of 'data_two'
	// 12.199523 will be assigned to 'sps_d' of 'data_two'
	// 68 will be assigned to 'sps_c' of 'data_two'.. BUt 68 is an integer (int) and 'sps_c' is a 'char'.. In this case, 68 is considered as a decimal ASCII value and its corresponding character (D) is assigned to 'sps_c' of 'data_two'.
	struct MyData data_two = { 'P', 6.2f, 12.199523, 68 }; // Inline initialiazation

	// 36 will be assigned to 'sps_i' of 'data_three'
	// 'G' is 'char, but 'f' of 'data_three' is 'float'.. Hence, 'G' is converted to its decimal integer ASCII value (71) and this in turn is converted to 'float' (71.000000) and then it will be assigned to 'sps_f' of 'data_three'
	// 0.0000000 will be assigned to 'sps_d' of 'data_three'
	// No character will be assigned to 'sps_c' of 'data_three'
	struct MyData data_three = { 36, 'G' }; // Inline initialiazation

	// 79 will be assigned to 'sps_i' of 'data_four'
	// 0.000000 will be assigned to 'sps_f' of 'data_four'
	// 0.000000 will be assigned to 'sps_d' of 'data_four'
	// No character will be assigned to 'sps_c' of 'data_four'
	struct MyData data_four = { 79 }; // Inline initialiazation

	// code
	// Displaying values of the data members of 'struct MyData'
	printf("\n\n");
	printf("DATA MEMBERS OF 'sturct MyData data_one' ARE : \n\n");
	printf("sps_i = %d\n", data_one.sps_i);
	printf("sps_f = %f\n", data_one.sps_f);
	printf("sps_d = %lf\n", data_one.sps_d);
	printf("sps_c = %c\n", data_one.sps_c);

	printf("\n\n");
	printf("DATA MEMBERS OF 'sturct MyData data_two' ARE : \n\n");
	printf("sps_i = %d\n", data_two.sps_i);
	printf("sps_f = %f\n", data_two.sps_f);
	printf("sps_d = %lf\n", data_two.sps_d);
	printf("sps_c = %c\n", data_two.sps_c);

	printf("\n\n");
	printf("DATA MEMBERS OF 'sturct MyData data_three' ARE : \n\n");
	printf("sps_i = %d\n", data_three.sps_i);
	printf("sps_f = %f\n", data_three.sps_f);
	printf("sps_d = %lf\n", data_three.sps_d);
	printf("sps_c = %c\n", data_three.sps_c);

	printf("\n\n");
	printf("DATA MEMBERS OF 'sturct MyData data_four' ARE : \n\n");
	printf("sps_i = %d\n", data_four.sps_i);
	printf("sps_f = %f\n", data_four.sps_f);
	printf("sps_d = %lf\n", data_four.sps_d);
	printf("sps_c = %c\n", data_four.sps_c);

	return(0);
}

