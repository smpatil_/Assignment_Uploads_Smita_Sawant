#include <stdio.h>

struct MyData
{
    int sps_i;
    float sps_f;
    double sps_d;
    char sps_c;
};

int main(void)
{
    // Variable Declarations
    struct MyData data;

    //Code
    //Assigning Data Values To The Data Members of 'struct MyData'
    data.sps_i = 30;
    data.sps_f = 11.45f;
    data.sps_d = 1.2995;
    data.sps_c = 'A';

    //Displaying Values Of The Data Members Of 'struct MyData'
    printf("\n\n");
    printf("DATA MEMBERS OF 'struct MyData' ARE : \n\n");
    printf("sps_i = %d\n", data.sps_i);
    printf("sps_f = %f\n", data.sps_f);
    printf("sps_d = %lf\n", data.sps_d);
    printf("sps_c = %c\n", data.sps_c);

    printf("\n\n");
    printf("ADDRESSES OF DATA MEMBERS OF 'struct MyData' ARE : \n\n");
    printf("'sps_i' Occupies Addresses From %p\n", &data.sps_i);
    printf("'sps_f' Occupies Addresses From %p\n", &data.sps_f);
    printf("'sps_d' Occupies Addresses From %p\n", &data.sps_d);
    printf("'sps_c' Occupies Addresses From %p\n", &data.sps_c);

    printf("Starting Address Of 'struct MyData' variable 'data' = %p\n\n", &data);

    return(0);
}
