#include <stdio.h> 

#define NAME_LENGTH 100 
#define MARITAL_STATUS 10 

struct Employee
{
	char name[NAME_LENGTH];
	int age;
	float salary;
	char sex;
	char marital_status[MARITAL_STATUS];
};

int main(void)
{
	// Variable Declarations 
	struct Employee EmployeeRecord[5]; //An array of 5 structs - Each being type 'struct Employee'

	char employee_rajesh[] = "Rajesh";
	char employee_sameer[] = "Sameer";
	char employee_kalyani[] = "Kalyani";
	char employee_sonali[] = "Sonali";
	char employee_shantanu[] = "Shantanu";

	int sps_i;

	// Code
	// **** Hard- Coded Initialization of Array of 'struct Employee' ****

	//**** Employee 1 ****
	strcpy(EmployeeRecord[0].name, employee_rajesh);
	EmployeeRecord[0].age = 30;
	EmployeeRecord[0].sex = 'M';
	EmployeeRecord[0].salary = 50000.0f;
	strcpy(EmployeeRecord[0].marital_status, "Unmarried");

	//**** Employee 2 ****
	strcpy(EmployeeRecord[1].name, employee_sameer);
	EmployeeRecord[1].age = 32;
	EmployeeRecord[1].sex = 'M';
	EmployeeRecord[1].salary = 60000.0f;
	strcpy(EmployeeRecord[1].marital_status, "Married");

	//**** Employee 3 ****
	strcpy(EmployeeRecord[2].name, employee_kalyani);
	EmployeeRecord[2].age = 29;
	EmployeeRecord[2].sex = 'F';
	EmployeeRecord[2].salary = 62000.0f;
	strcpy(EmployeeRecord[2].marital_status, "Unmarried");

	//**** Employee 4 ****
	strcpy(EmployeeRecord[3].name, employee_sonali);
	EmployeeRecord[3].age = 33;
	EmployeeRecord[3].sex = 'F';
	EmployeeRecord[3].salary = 50000.0f;
	strcpy(EmployeeRecord[3].marital_status, "Married");

	//**** Employee 5 ****
	strcpy(EmployeeRecord[4].name, employee_shantanu);
	EmployeeRecord[4].age = 35;
	EmployeeRecord[4].sex = 'M';
	EmployeeRecord[4].salary = 55000.0f;
	strcpy(EmployeeRecord[4].marital_status, "Married");

	// *** Display ***
	printf("\n\n");
	printf("***** DISPLAYING EMPLOYEE RECORDS*****\n\n");
	for (sps_i = 0; sps_i < 5; sps_i++)
	{
		printf("***** EMPLOYEE NUMBER %d *****\n\n", (sps_i + 1));
		printf("Name			: %s\n", EmployeeRecord[sps_i].name);
		printf("Age			: %d years\n", EmployeeRecord[sps_i].age);

		if (EmployeeRecord[sps_i].sex == 'M' || EmployeeRecord[sps_i].sex == 'm')
			printf("Sex			: Male\n");
		else
			printf("Sex			: Female\n");

		printf("Salary			: Rs. %f\n", EmployeeRecord[sps_i].salary);
		printf("Marriatal Status	: %s\n", EmployeeRecord[sps_i].marital_status);

		printf("\n\n");
	}

	return(0);
}
