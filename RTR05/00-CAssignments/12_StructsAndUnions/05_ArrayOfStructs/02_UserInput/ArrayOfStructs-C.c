#include <stdio.h>
#include <ctype.h>

#define NUM_EMPLOYEES 6

#define NAME_LENGTH 100
#define MARITAL_STATUS 10

struct Employee
{
	char name[NAME_LENGTH];
	int age;
	char sex;
	float salary;
	char marital_status;
};

int main(void)
{
	// Function Prototype
	void MyGetString(char[], int);

	// Variable Delarations
	struct Employee EmployeeRecord[NUM_EMPLOYEES]; //An array of <NUM EMPLOYEES> structs - Each being type 'struct Employee'
	int sps_i;

	//code
	//******User Input Initialization ARRAY of 'struct Employee' ******
	for (sps_i = 0; sps_i < NUM_EMPLOYEES; sps_i++)
	{
		printf("\n\n\n\n");
		printf("********** DATA ENTRY FOR EMPLOYEE NUMBER %d **********\n", (sps_i  + 1));
		
		printf("\n\n");
		printf("Enter Employee Name : ");
		MyGetString(EmployeeRecord[sps_i].name, NAME_LENGTH);

		printf("\n\n\n"); 
		printf("Enter Employee's Age (in years) : ");
		scanf("%d", &EmployeeRecord[sps_i].age);

		printf("\n\n");
		printf("Enter Employee's Sex (M/m For Male, F/f For Female) : ");
		EmployeeRecord[sps_i].sex = getch();
		printf("%c", EmployeeRecord[sps_i].sex);
		EmployeeRecord[sps_i].sex = toupper(EmployeeRecord[sps_i].sex);

		printf("\n\n\n");
		printf("Enter Employee's Salary (in Indian Rupees) : ");
		scanf("%f", &EmployeeRecord[sps_i].salary);

		printf("\n\n");
		printf("Is The Employee Married? (Y/y For Yes, N/n For No) : ");
		EmployeeRecord[sps_i].marital_status = getch();
		printf("%c", EmployeeRecord[sps_i].marital_status);
		EmployeeRecord[sps_i].marital_status = toupper(EmployeeRecord[sps_i].marital_status);
	}


	// *** DISPLAY ***
	printf("\n\n\n\n");
	printf("********** DISPLAYING EMPLOYEE RECORDS **********\n\n");
	for (sps_i = 0; sps_i < NUM_EMPLOYEES; sps_i++)
	{
		printf("*********** EMPLOYEE NUMBER %d **********\n\n", (sps_i + 1));
		printf("Name	: %s\n", EmployeeRecord[sps_i].name);
		printf("Age	: %d years\n", EmployeeRecord[sps_i].age);

		if (EmployeeRecord[sps_i].sex == 'M')
			printf("Sex : Male\n");
		else if (EmployeeRecord[sps_i].sex == 'F')
			printf("Sex : Female\n");
		else
			printf("Sex	: Invalid Data Entered\n");


		printf("Salary	: Rs. %f\n", EmployeeRecord[sps_i].salary);

		if (EmployeeRecord[sps_i].marital_status == 'Y')
			printf("Marital Status : Married\n");

		else if (EmployeeRecord[sps_i].marital_status == 'N')
			printf("Marital Status : Unmarried\n");
		else
			printf("Marital Status : Invalid Data Entered\n");

		printf("\n\n");
	}

	return(0);
}

// *** Simple Rudimentary Implementation of gets_s() ***
// *** Implemented Due to Different Behaviour of gets_s() / fgets() / fscanf() on Different Platforms ***
// ***Backspace / Character Deletion And Arrow Key Cursor Movement Not Implemented ***

void MyGetString(char str[], int str_size)
{
	// Variable Declarations
	
	int sps_i;
	char ch = '\0';

	// Code 
	sps_i = 0;

	do
	{
		ch = getch(); str[sps_i] = ch;
		printf("%c", str[sps_i]);
		sps_i++;
	}while ((ch != '\r') && (sps_i < str_size));

	if (sps_i == str_size)
		str[sps_i - 1] = '\0';
	else
		str[sps_i] = '\0';
}

