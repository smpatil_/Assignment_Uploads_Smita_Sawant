#include <stdio.h>

// Defining struct
struct MyPoint
{
	int sps_x;
	int sps_y;
} point; // declaring a single variable of type 'struct MyPoint' globally...

//Defining Struct
struct MyPointProperties
{
	int quadrant;
	char axis_location[10];
} point_properties; // declaring a single variable of type 'struct MyPointProperties' globally...

int main(void)
{
	// code
	// User Input For The Data Members Of 'Struct MyPoint' variable 'point_A'
	printf("\n\n");
	printf("Enter X-Coordinate For A Point : ");
	scanf("%d", &point.sps_x);
	printf("Enter Y-Coordinate For A Point : ");
	scanf("%d", &point.sps_y);

	printf("\n\n");
	printf("Point Co-ordinates (sps_x, sps_y) Are : (%d, %d) !!!\n\n", point.sps_x, point.sps_y);

	if (point.sps_x == 0 && point.sps_y == 0)
		printf("The Point Is The Origin (%d %d) !!!\n", point.sps_x, point.sps_y);
	else // Atleast one of the two values (either X or Y or both) is a non-zero value...
	{
		if (point.sps_x == 0) // If X is zero, obviously Y is the non-zero value
		{
			if (point.sps_y < 0) // If Y is -ve
				strcpy(point_properties.axis_location, "Negative Y");

			if (point.sps_y > 0) // If Y is +ve
				strcpy(point_properties.axis_location, "Positive Y");

			point_properties.quadrant = 0; //A point lying on any of the co-ordinate axes is not a part of any quadrant...
			printf("The Point Lies On The %s Axis !!!\n\n", point_properties.axis_location);
		
		}
		else if (point.sps_y == 0) //If Y is zero, obviously X is the non-zero value
		{
			if (point.sps_x < 0) // If X is -ve
				strcpy(point_properties.axis_location, "Negative X");

			if (point.sps_x > 0) // If X is +ve
				strcpy(point_properties.axis_location, "Positive X");

			point_properties.quadrant = 0; //A point lying on any of the co-ordinate axes is not a part of any quadrant...
			printf("The Point Lies On The %s Axis !!!\n\n", point_properties.axis_location);
		}
		else //Both X and Y are non-zero
		{
			point_properties.axis_location[0] = '\0'; //A point lying in any of the 4 quadrants cannot be lying on any of the co-ordinate axes..

				if (point.sps_x > 0 && point.sps_y > 0) // X is +ve and Y is +ve
					point_properties.quadrant = 1;

				else if (point.sps_x < 0 && point.sps_y > 0) // X is -ve and Y is +ve
					point_properties.quadrant = 2;

				else if (point.sps_x < 0 && point.sps_y < 0) // X is -ve and Y is -ve
					point_properties.quadrant = 3;
				
				else	// X is +ve and Y is -ve
					point_properties.quadrant = 4;

			printf("The Point Lies In Quadrant Number %d !!!\n\n", point_properties.quadrant);
		}
	}

	return(0);
}

