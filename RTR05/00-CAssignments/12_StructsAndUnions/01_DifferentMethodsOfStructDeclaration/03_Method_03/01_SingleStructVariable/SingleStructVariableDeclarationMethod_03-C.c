#include <stdio.h>

int main(void)
{
	//DEFINING STRUCT
	struct MyData
	{
		int sps_i;
		float sps_f;
		double sps_d;
	} data; //Declaring a single struct variable of type 'struct MyData' locally..

	//variable declarations
	int sps_i_size;
	int sps_f_size;
	int sps_d_size;
	int struct_MyData_size;

	//code
	//Assigning Data Values To The Data Members Of 'struct MyData'
	data.sps_i = 30;
	data.sps_f = 11.45f;
	data.sps_d = 1.2995;

	//Displaying Values Of The Data Members Of 'struct MyData'
	printf("\n\n");
	printf("DATA MEMBERS OF 'struct MyData' ARE : \n\n");
	printf("sps_i = %d\n", data.sps_i);
	printf("sps_f = %f\n", data.sps_f);
	printf("sps_d = %lf\n", data.sps_d);

	//Calculating sizes (In Bytes) Of the Data Members Of 'struct MyData'
	sps_i_size = sizeof(data.sps_i);
	sps_f_size = sizeof(data.sps_f);
	sps_d_size = sizeof(data.sps_d);

	//Displaying sizes (In Bytes) Of The Data Members of 'struct MyData'
	printf("\n\n");
	printf("SIZES (in bytes) OF DATA MEMBERS OF 'struct MyData' ARE : \n\n");
	printf("Size of 'sps_i' = %d bytes\n", sps_i_size);
	printf("Size of 'sps_f' = %d bytes\n", sps_f_size);
	printf("Size of 'sps_d' = %d bytes\n", sps_d_size);

	//Calculating Size (In Bytes) Of The Entire 'struct MyData'
	struct_MyData_size = sizeof(struct MyData); // can also give struct name -> sizeof(MyData)

	//Displaying Sizes (In Bytes) Of The Entire 'struct MyData'
	printf("\n\n");
	printf("Size of 'struct MyData' : %d bytes \n\n", struct_MyData_size);

	return(0);
}
