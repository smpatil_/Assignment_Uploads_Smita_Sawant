#include <stdio.h>

int main(void)
{
	//DEFINING STRUCT..
	struct MyPoint
	{
		int sps_x;
		int sps_y;
	} point_A, point_B, point_C, point_D, point_E; //Declaring 5 sruct variables of type 'struct MyPoint' locally...


	//code
	//Assigning data values to the data members of 'struct MyPoint' variable 'point_A'
	point_A.sps_x = 3;
	point_A.sps_y = 0;

	//Assigning data values to the data members of'struct MyPoint' variable 'point_B'
	point_B.sps_x = 1;
	point_B.sps_y = 2;

	//Assigning data values to the data members of 'struct MyPoint' variable 'point_C'
	point_C.sps_x = 9;
	point_C.sps_y = 6;

	//Assigning data values to the data members of 'struct MyPoint' variable 'point_D'
	point_D.sps_x = 8;
	point_D.sps_y = 2;

	//Assigning data values to the Data Members of 'struct MyPoint' variable 'point_E'
	point_E.sps_x = 11;
	point_E.sps_y = 8;

	//Displaying values of the data members of 'struct MyPoint' (all variables)
	printf("\n\n");
	printf("Co-ordinates (sps_x,sps_y) Of Point 'A' Are : (%d, %d)\n\n", point_A.sps_x, point_A.sps_y);
	printf("Co-ordinates (sps_x,sps_y) Of Point 'B' Are : (%d, %d)\n\n", point_B.sps_x, point_B.sps_y);
	printf("Co-ordinates (sps_x,sps_y) Of Point 'C' Are : (%d, %d)\n\n", point_C.sps_x, point_C.sps_y);
	printf("Co-ordinates (sps_x,sps_y) Of Point 'D' Are : (%d, %d)\n\n", point_D.sps_x, point_D.sps_y);
	printf("Co-ordinates (sps_x,sps_y) Of Point 'E' Are : (%d, %d)\n\n", point_E.sps_x, point_E.sps_y);

	return(0);
}
