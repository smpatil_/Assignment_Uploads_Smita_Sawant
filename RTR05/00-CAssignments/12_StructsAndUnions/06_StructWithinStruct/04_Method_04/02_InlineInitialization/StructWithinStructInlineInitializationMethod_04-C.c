#include <stdio.h>

int main(void)
{
	//Variable declarartions
	int length, breadth, area;

	struct MyPoint
	{
		int sps_x;
		int sps_y;
	};

	struct Rectangle
	{
		struct MyPoint point_01;
		struct MyPoint point_02;
	};

	struct Rectangle rect = { {2, 3}, {5, 6} };

	// Code

	length = rect.point_02.sps_y - rect.point_01.sps_y;
	if (length < 0)
		length = length * -1;

	breadth = rect.point_02.sps_x - rect.point_01.sps_x;
	if (breadth < 0)
		breadth = breadth * -1;

	area = length * breadth;

	printf("\n\n");
	printf("Length Of Rectangle = %d\n\n", length);
	printf("Breadth Of Rectangle = %d\n\n", breadth);
	printf("Breadth Of Rectangle = %d\n\n", area);

	return(0);
}

