#include <stdio.h>

struct MyNumber
{
	int sps_num;
	int num_table[10];
};

struct NumTables
{
	struct MyNumber n;
};

int main(void)
{
	// Variable declarations
	struct NumTables tables[10]; //An Array of 10 'struct NumTables'
	int sps_i, sps_j;

	//Code
	for (sps_i = 0; sps_i < 10; sps_i++)
	{
		tables[sps_i].n.sps_num = (sps_i + 1);
	}

	for (sps_i = 0; sps_i < 10; sps_i++)
	{
		printf("\n\n");
		printf("Table Of %d : \n\n", tables[sps_i].n.sps_num);
		for (sps_j = 0; sps_j < 10; sps_j++)
		{
			tables[sps_i].n.num_table[sps_j] = tables[sps_i].n.sps_num * (sps_j + 1);
			printf("%d * %d = %d\n", tables[sps_i].n.sps_num, (sps_j + 1), tables[sps_i].n.num_table[sps_j]);
		}
	}
	return(0);
}

