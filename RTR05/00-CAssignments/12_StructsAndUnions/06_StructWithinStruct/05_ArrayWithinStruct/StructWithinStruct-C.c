#include <stdio.h>

struct MyNumber
{
	int sps_num;
	int num_table[10];
};

struct NumTables
{
	struct MyNumber sps_a;
	struct MyNumber sps_b;
	struct MyNumber sps_c;
};

int main(void)
{
	// Variable declarations
	struct NumTables tables;
	int sps_i;

	// Code
	tables.sps_a.sps_num = 2;
	for (sps_i = 0; sps_i < 10; sps_i++)
		tables.sps_a.num_table[sps_i] = tables.sps_a.sps_num * (sps_i + 1);
	printf("\n\n");
	printf("Table Of %d : \n\n", tables.sps_a.sps_num);
	for (sps_i = 0; sps_i < 10; sps_i++)
		printf("%d * %d = %d\n", tables.sps_a.sps_num, (sps_i + 1), tables.sps_a.num_table[sps_i]);

	tables.sps_b.sps_num = 3;
	for (sps_i = 0; sps_i < 10; sps_i++)
		tables.sps_b.num_table[sps_i] = tables.sps_b.sps_num * (sps_i + 1);
	printf("\n\n");
	printf("Table Of %d : \n\n", tables.sps_b.sps_num);
	for (sps_i = 0; sps_i < 10; sps_i++)
		printf("%d * %d = %d\n", tables.sps_b.sps_num, (sps_i + 1), tables.sps_b.num_table[sps_i]);

	tables.sps_c.sps_num = 4;
	for (sps_i = 0; sps_i < 10; sps_i++)
		tables.sps_c.num_table[sps_i] = tables.sps_c.sps_num * (sps_i + 1);
	printf("\n\n");
	printf("Table Of %d : \n\n", tables.sps_c.sps_num);
	for (sps_i = 0; sps_i < 10; sps_i++)
		printf("%d * %d = %d\n", tables.sps_c.sps_num, (sps_i + 1), tables.sps_c.num_table[sps_i]);

	return(0);
}

