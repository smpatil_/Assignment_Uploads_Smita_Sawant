#include <stdio.h>

struct Rectangle
{
	struct MyPoint
	{
		int sps_x;
		int sps_y;
	} point_01, point_02;

} rect = { {2, 3}, {5, 6} };

int main(void)
{
	// Variable Declarartions
	int length, breadth, area;

	// Code
	length = rect.point_02.sps_y - rect.point_01.sps_y;
	if (length < 0)
		length = length * -1;
	
	breadth = rect.point_02.sps_x - rect.point_01.sps_x;
	if (breadth < 0)
		breadth = breadth * -1;

	area = length * breadth;

	printf("\n\n");
	printf("Length Of Rectangle = %d\n\n", length);
	printf("Breadth Of Rectangle = %d\n\n", breadth);
	printf("Breadth Of Rectangle = %d\n\n", area);

	return(0);
}

