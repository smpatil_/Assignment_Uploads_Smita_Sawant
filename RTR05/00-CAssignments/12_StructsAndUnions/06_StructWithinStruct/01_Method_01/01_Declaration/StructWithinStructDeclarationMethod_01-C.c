#include <stdio.h>

struct Rectangle
{
	struct MyPoint
	{
		int sps_x;
		int sps_y;
	} point_01, point_02;

} rect;

int main(void)
{
	// Variable Declarations
	int length, breadth, area;

	//Code
	printf("\n\n");
	printf("Enter Leftmost X-Coordinate Of Rectangle : ");
	scanf("%d", &rect.point_01.sps_x);

	printf("\n\n");
	printf("Enter Bottommost Y-Coordinate Of Rectangle : ");
	scanf("%d", &rect.point_01.sps_y);

	printf("\n\n");
	printf("Enter Rightmost X-Coordinate Of Rectangle : ");
	scanf("%d", &rect.point_02.sps_x);

	printf("\n\n");
	printf("Enter Topmost Y-Coordinate Of Rectangle : ");
	scanf("%d", &rect.point_02.sps_y);

	length = rect.point_02.sps_y - rect.point_01.sps_y;
	if (length < 0)
		length = length * -1;
	
	breadth = rect.point_02.sps_x - rect.point_01.sps_x;
	if (breadth < 0)
		breadth = breadth * -1;

	area = length * breadth;

	printf("\n\n");
	printf("Length Of Rectangle  = %d\n\n", length);
	printf("Breadth Of Rectangle  = %d\n\n", breadth);
	printf("Area Of Rectangle  = %d\n\n", area);

	return(0);
}

