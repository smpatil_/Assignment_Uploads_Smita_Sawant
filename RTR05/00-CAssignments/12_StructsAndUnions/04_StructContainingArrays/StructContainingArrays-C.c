#include <stdio.h>

#define INT_ARRAY_SIZE 10
#define FLOAT_ARRAY_SIZE 5
#define CHAR_ARRAY_SIZE 26

#define NUM_STRINGS 10
#define MAX_CHARACTERS_PER_STRING 20

#define ALPHABET_BEGINNING 65 //'A'

struct MyDataOne
{
	int sps_iArray[INT_ARRAY_SIZE];
	float sps_fArray[FLOAT_ARRAY_SIZE];
};

struct MyDataTwo
{
	char sps_cArray[CHAR_ARRAY_SIZE];
	char sps_strArray[NUM_STRINGS][MAX_CHARACTERS_PER_STRING];
};

int main(void)
{
	// Variable declarations
	struct MyDataOne data_one;
	struct MyDataTwo data_two;
	int sps_i;

	// Code
	// *** Piece-Meal Assignment (Hard-Coded) ***
	data_one.sps_fArray[0] = 0.1f;
	data_one.sps_fArray[1] = 1.2f;
	data_one.sps_fArray[2] = 2.3f;
	data_one.sps_fArray[3] = 3.4f;
	data_one.sps_fArray[4] = 4.5f;

	// *** Loop Assignemnt (User Input) ***
	printf("\n\n");
	printf("Enter %d Integers : \n\n", INT_ARRAY_SIZE);
	for (sps_i = 0; sps_i < INT_ARRAY_SIZE; sps_i++)
		scanf("%d", &data_one.sps_iArray[sps_i]);

	//*** Loop Assignment (Hard-Coded) ***
	for (sps_i = 0; sps_i < CHAR_ARRAY_SIZE; sps_i++)
		data_two.sps_cArray[sps_i] = (char)(sps_i + ALPHABET_BEGINNING);

	// *** Piece-Meal Assignment (Hard-Coded) ***
	strcpy(data_two.sps_strArray[0], "Welcome !!!");
	strcpy(data_two.sps_strArray[1], " This");
	strcpy(data_two.sps_strArray[2], " Is");
	strcpy(data_two.sps_strArray[3], " ASTROMEDICOMP'S");
	strcpy(data_two.sps_strArray[4], " Real");
	strcpy(data_two.sps_strArray[5], " Time");
	strcpy(data_two.sps_strArray[6], " Rendering");
	strcpy(data_two.sps_strArray[7], " Batch");
	strcpy(data_two.sps_strArray[8], " Of");
	strcpy(data_two.sps_strArray[9], " 2023-2024 !!!");

	// *** Displaying Data Members Of 'struct DataOne' And their Values ***
	printf("\n\n");
	printf("Members Of 'struct DataOne' AlongWith Their Assigned Values Are: \n\n");

	printf("\n\n");
	printf("Integer Array (data_one.sps_iArray[]) : \n\n");
	for (sps_i = 0; sps_i < INT_ARRAY_SIZE; sps_i++)
		printf("data_one.sps_iArray[%d] = %d\n", sps_i, data_one.sps_iArray[sps_i]);

	printf("\n\n");
	printf("Floating-Point Array (data_one.sps_fArray[]) : \n\n");
	for (sps_i = 0; sps_i < FLOAT_ARRAY_SIZE; sps_i++)
		printf("data_one.sps_fArray[%d] = %f\n", sps_i, data_one.sps_fArray[sps_i]);

	// *** Displaying Data Members Of 'struct DataTwo' And their Values ***
	printf("\n\n");
	printf("Members Of 'struct DataTwo' AlongWith Their Assigned Values Are: \n\n");

	printf("\n\n");
	printf("Character Array (data_two.sps_cArray[]) : \n\n");
	for (sps_i = 0; sps_i < CHAR_ARRAY_SIZE; sps_i++)
		printf("data_two.sps_cArray[%d] = %c\n", sps_i, data_two.sps_cArray[sps_i]);

	printf("\n\n");
	printf("String Array (data_two.sps_strArray[]) : \n\n");
	for (sps_i = 0; sps_i < NUM_STRINGS; sps_i++)
		printf("%s", data_two.sps_strArray[sps_i]);

	printf("\n\n");

	return(0);
}

