//Standard header file
#include <stdio.h> //for printf
#include <stdlib.h> //for exit
#include <memory.h> //for memsate
//#include <SOIL/SOIL.h>

//X11 header files
#include <X11/Xlib.h>
#include <X11/Xutil.h> //for XVisualInfo and related APIs
#include <X11/XKBlib.h> //for all XWindow APIs

//OpenGL header files
#include <GL/gl.h>
#include <GL/glx.h> //glx : graphic library for x window
#include <GL/glu.h> 

//Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//Global Variable Declarations
Display *display = NULL;
Colormap colormap;
Window window;
XVisualInfo *visualInfo; 

//OpenGL global variable
GLXContext glxContext = NULL; 

Bool bFullscreen = False;
Bool bActiveWindow = False;

FILE* gpFile = NULL;

//Variables for Light
Bool bLight = False;

GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
//GLfloat lightSpecularZero[] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightPosition[] = { 0.0f, 0.0f, 0.0f, 1.0f };

GLUquadric* quadric = NULL;


GLfloat angleForXRotation = 0.0f;
GLfloat angleForYRotation = 0.0f;
GLfloat angleForZRotation = 0.0f;

GLuint keyPressed;



int main(void)
{
    //Local function declarartions
    void toggleFullscreen(void);
    int initialize(void);
    void resize(int, int);
    void draw(void);
    void update(void);
    void uninitialize(void);

    //Local variable declarartions
    int defaultScreen;
   
    XSetWindowAttributes windowAttributes;
    int styleMask;
    Atom windowManagerDelete;
   
    XEvent event;
    KeySym keySym;


    int screenWidth;
    int screenHeight;

    char keys[26];
    
    //display = XOpenDisplay(NULL);

    int frameBufferAttributes[] = 
    {
        GLX_DOUBLEBUFFER, True,
        GLX_RGBA,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24
    };

    Bool bDone = False;

    int result = 0;


    gpFile = fopen("Log.txt", "w");
    if (gpFile == NULL)
    {
        fprintf(gpFile, "Program Failed!\n");
        exit(0);
    }
    else
    {
        fprintf(gpFile, "Program Started Successfully!\n");
    }
    
    //code
    // 1) Open connection with x server and get the display interface
    display = XOpenDisplay(NULL);
    if (display == NULL)
    {
        printf("XOpenDisplay() Failed/n");
        uninitialize();
        exit(1);
    }

    //2) get default screen from above display
    defaultScreen = XDefaultScreen(display);


    //Get visual info from above three
    //memset((void *)&visualInfo, 0, sizeof(XVisualInfo));
    visualInfo = glXChooseVisual(display, defaultScreen, frameBufferAttributes);

    if (visualInfo == NULL)
    {
        printf("XglXChooseVisual() Failed/n");
        uninitialize();
        exit(1);
    }

    //Set window attributes/properties
    memset((void *)&windowAttributes, 0, sizeof(XSetWindowAttributes));
    windowAttributes.border_pixel = 0;
    windowAttributes.background_pixel = XBlackPixel(display, visualInfo->screen);
    windowAttributes.background_pixmap = 0;
    windowAttributes.colormap = XCreateColormap(display,
                                                XRootWindow(display, visualInfo->screen),
                                                visualInfo->visual,
                                                AllocNone);
    
    //Assign this color map to global color map
    colormap = windowAttributes.colormap;


    //Set the style mask
    styleMask = CWBorderPixel | CWBackPixel | CWColormap | CWEventMask;

    //Now finally create the window
    window = XCreateWindow(display,
                           XRootWindow(display, visualInfo->screen),
                           0, 
                           0,
                           WIN_WIDTH, 
                           WIN_HEIGHT,
                           0,
                           visualInfo->depth,
                           InputOutput,
                           visualInfo->visual,
                           styleMask,
                           &windowAttributes);

    if(!window)
    {
        printf("XCreateWindow() Failed /n");
        uninitialize();
        exit(1);
    }

    //Specify to which events this window should respond
    XSelectInput(display, window, ExposureMask | VisibilityChangeMask | StructureNotifyMask | KeyPressMask | ButtonPressMask | PointerMotionMask | FocusChangeMask);
    

    // Specify window manager delete atom
    windowManagerDelete = XInternAtom(display, "WM_DELETE_WINDOW", True);

    //Add/set above atom as protocol for window manager
    XSetWMProtocols(display, window, &windowManagerDelete, 1);

    //Give caption to the window
    XStoreName(display, window, "Smita_Patil");

    //Show/map the window
    XMapWindow(display, window);

    //Centre the window
    screenWidth = XWidthOfScreen((XScreenOfDisplay(display, visualInfo->screen)));
    screenHeight = XHeightOfScreen((XScreenOfDisplay(display, visualInfo->screen)));

    XMoveWindow(display, window, (screenWidth - WIN_WIDTH)/2, (screenHeight - WIN_HEIGHT)/2);

    //OpenGL initialization
    //Initialization
    result = initialize();
   
    if (result == -1)
    {
        fprintf(gpFile, "Initialize Failed \n");
        uninitialize();
        exit(1);
    }
    else if (result == -2)
    {
        fprintf(gpFile, "Initialize Failed /n");
        uninitialize();
        exit(1);
    }

    //Event loop (mhanje windows ch message loop)
    while(bDone == False)
    {
        while(XPending(display))
        {
            XNextEvent(display, &event);
            
            switch (event.type)
            {
                case MapNotify:
                break;

                case FocusIn:
                bActiveWindow = True;
                break;

                case FocusOut:
                bActiveWindow = False;
                break;

                case ConfigureNotify:
                    resize(event.xconfigure.width, event.xconfigure.height);
                    break;

                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                        break;

                        case 2:
                        break;

                        case 3:
                        break;

                        default:
                        break;
                    }
                break;


                case KeyPress:
                keySym = XkbKeycodeToKeysym(display, event.xkey.keycode, 0, 0);

                switch (keySym)
                {
                    case XK_Escape:
                    bDone = True;
                    break;

                    default:
                    break;
                }

                XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                
                switch (keys[0])
                {
                    case 'F':
                    case 'f':
                    if (bFullscreen == False)
                    {
                        toggleFullscreen();
                        bFullscreen = True;
                    }
                    else
                    {
                        toggleFullscreen();
                        bFullscreen = False;
                    }
                    break;

                    case 'L':
                    case 'l':
                    if (bLight == False)
                    {
                        glEnable(GL_LIGHTING);
                        bLight = True;
                    }
                    else
                    {
                        glDisable(GL_LIGHTING);
                        bLight = False;
                    }
                    break;

                    case 'X':
                    case 'x':
                        keyPressed = 1;
                        angleForXRotation = 0.0f; //reset
                        break;

                    case 'Y':
                    case 'y':
                        keyPressed = 2;
                        angleForYRotation = 0.0f; //reset
                        break;

                    case 'Z':
                    case 'z':
                        keyPressed = 3;
                        angleForZRotation = 0.0f; //reset
                        break;

                    default:
                    break;
                }
                break;

                case 33:
                {
                    bDone = True;
                }
                break;

                default:
                break;

            }
        }
        if (bActiveWindow == True)
        {
            draw();

            update();
        }
    }
    

    uninitialize();

    


    return(0);
}

void toggleFullscreen(void)
{
    //Local Variable declarations
    Atom windowManagerStateNormal;
    Atom windowManagerStateFullscreen;
    XEvent event;

    //Code
    windowManagerStateNormal = XInternAtom(display, "_NET_WM_STATE", False);

    windowManagerStateFullscreen = XInternAtom(display, "_NET_WM_STATE_FULLSCREEN", False);

    //memset the event structure and fill it with above two atoms
    memset((void *)&event, 0, sizeof(XEvent));

    event.type = ClientMessage;
    event.xclient.window = window;
    event.xclient.message_type = windowManagerStateNormal;
    event.xclient.format = 32;
    event.xclient.data.l[0] = bFullscreen?0:1; 
    event.xclient.data.l[1] = windowManagerStateFullscreen;

    //send the event
    XSendEvent(display,
               XRootWindow(display, visualInfo->screen),
               False,
               SubstructureNotifyMask,
               &event);

}



int initialize(void)
{
    //local function declarartion
    void resize(int, int);
    //void uninitialize(void);

    
    //code
    //create openGL context
    glxContext = glXCreateContext(display, visualInfo, NULL, True);

    if(glxContext == NULL)
    {
        printf("In initialize glxCreateContext() failed\n");
        return(-1);
    }

    //make this context as current context
    if (glXMakeCurrent(display, window, glxContext) == False)
    {
        printf("In initialize glXMakeCurrent() failed\n");
        return(-2);
    }

    //Enabling Depth
	glShadeModel(GL_SMOOTH); // Jevha pan apan color karu or shade karu, teva to shade smooth kar - Beautification

	glClearDepth(1.0f); //Compulsory step : Depth buffer la clear karayla glClearDepth() vapar ani depth buffer madhlya saglya buffers na 1 bit kar.

	glEnable(GL_DEPTH_TEST); //Compulsory: 8 test paiki hi ek test - GL_DEPTH_TEST enable kar. Ya tests by default disabled astat.

	glDepthFunc(GL_LEQUAL); //Compulsory: glDepthFunc -> Depth sathi konta function vaparu? - LEQUAL (L less than Equal to) with 1.0f (from glClearDepth()).

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); //Optional: Beautification - Jevha Depth enable karta, tevha corner chya goshti circular na dista elliptical distat. This is not good. Mhanun correction kartana te nicest kar.


    //Usual openGL code
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    //Light related initialization code
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	//glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
	//glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	glEnable(GL_LIGHT0); //----->>>>>>>> Hi line asli or nasli ithe tari farak padnar nahi..because GL_LIGHT0 ha by default enabled asto

	//material properties
	/*glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);*/
	//material la kadhi enable karaychi garaj naste

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	//Initialize quadric
	quadric = gluNewQuadric();


    resize(WIN_WIDTH, WIN_HEIGHT);

    return(0);
}

void resize(int width, int height)
{
     //code
    if (height == 0)
        height = 1;
	

	glMatrixMode(GL_PROJECTION); //Current Transformation Matrix
	glLoadIdentity();

	if (width <= height)
	{
		glOrtho(0.0f,
			15.5f,
			0.0f * ((GLfloat)height / (GLfloat)width),
			15.5f * ((GLfloat)height / (GLfloat)width),
			-10.0f,
			10.0f);
	}
	else
	{
		glOrtho(0.0f * ((GLfloat)width / (GLfloat)height),
			15.5f * ((GLfloat)width / (GLfloat)height),
			0.0f,
			15.5f,
			-10.0f,
			10.0f);
	}

    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void draw(void)
{
    //Variable Declarations
	GLfloat materialAmbient[4];
	GLfloat materialDiffuse[4];
	GLfloat materialSpecular[4];
	GLfloat materialShininess;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //3rd test : Compulsory for Depth 3D.

	glMatrixMode(GL_MODELVIEW); //Current Transformation Matrix
	glLoadIdentity();

	//Animation
	if (keyPressed == 1)
	{
		glRotatef(angleForXRotation, 1.0f, 0.0f, 0.0f);
		lightPosition[2] = angleForXRotation;
	}

	if (keyPressed == 2)
	{
		glRotatef(angleForYRotation, 0.0f, 1.0f, 0.0f);
		lightPosition[0] = angleForYRotation;
	}

	if (keyPressed == 3)
	{
		glRotatef(angleForZRotation, 0.0f, 0.0f, 1.0f);
		lightPosition[1] = angleForZRotation;
	}

	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

	//*************** FIRST COLUMN - Precious Stones *****************
	//***** 1st sphere on 1st column, EMERALD *****
	
	// ambient material
	materialAmbient[0] = 0.0215; // r
	materialAmbient[1] = 0.1745; // g
	materialAmbient[2] = 0.0215; // b
	materialAmbient[3] = 1.0f;   // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.07568; // r
	materialDiffuse[1] = 0.61424; // g
	materialDiffuse[2] = 0.07568; // b
	materialDiffuse[3] = 1.0f;    // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.633;    // r
	materialSpecular[1] = 0.727811; // g
	materialSpecular[2] = 0.633;    // b
	materialSpecular[3] = 1.0f;     // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.6 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(1.5f, 14.0f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30); //gluSphere internally calls all the normals


	// ***** 2nd sphere on 1st column, JADE *****

	// ambient material
	materialAmbient[0] = 0.135;  // r
	materialAmbient[1] = 0.2225; // g
	materialAmbient[2] = 0.1575; // b
	materialAmbient[3] = 1.0f;   // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.54; // r
	materialDiffuse[1] = 0.89; // g
	materialDiffuse[2] = 0.63; // b
	materialDiffuse[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.316228; // r
	materialSpecular[1] = 0.316228; // g
	materialSpecular[2] = 0.316228; // b
	materialSpecular[3] = 1.0f;     // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.1 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(1.5f, 11.5f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);


	// ***** 3rd sphere on 1st column, OBSIDIAN *****
	// ambient material
	materialAmbient[0] = 0.05375; // r
	materialAmbient[1] = 0.05;    // g
	materialAmbient[2] = 0.06625; // b
	materialAmbient[3] = 1.0f;    // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.18275; // r
	materialDiffuse[1] = 0.17;    // g
	materialDiffuse[2] = 0.22525; // b
	materialDiffuse[3] = 1.0f;    // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.332741; // r
	materialSpecular[1] = 0.328634; // g
	materialSpecular[2] = 0.346435; // b
	materialSpecular[3] = 1.0f;     // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.3 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(1.5f, 9.0f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);


	// ***** 4th sphere on 1st column, PEARL *****
	// ambient material
	materialAmbient[0] = 0.25;    // r
	materialAmbient[1] = 0.20725; // g
	materialAmbient[2] = 0.20725; // b
	materialAmbient[3] = 1.0f;    // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 1.0;   // r
	materialDiffuse[1] = 0.829; // g
	materialDiffuse[2] = 0.829; // b
	materialDiffuse[3] = 1.0f;  // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.296648; // r
	materialSpecular[1] = 0.296648; // g
	materialSpecular[2] = 0.296648; // b
	materialSpecular[3] = 1.0f;     // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.088 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(1.5f, 6.5f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);


	// ***** 5th sphere on 1st column, RUBY *****
	// ambient material
	materialAmbient[0] = 0.1745;  // r
	materialAmbient[1] = 0.01175; // g
	materialAmbient[2] = 0.01175; // b
	materialAmbient[3] = 1.0f;    // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.61424; // r
	materialDiffuse[1] = 0.04136; // g
	materialDiffuse[2] = 0.04136; // b
	materialDiffuse[3] = 1.0f;    // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.727811; // r
	materialSpecular[1] = 0.626959; // g
	materialSpecular[2] = 0.626959; // b
	materialSpecular[3] = 1.0f;     // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.6 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(1.5f, 4.0f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);


	// ***** 6th sphere on 1st column, TURQUOISE *****
	// ambient material
	materialAmbient[0] = 0.1;     // r
	materialAmbient[1] = 0.18725; // g
	materialAmbient[2] = 0.1745;  // b
	materialAmbient[3] = 1.0f;    // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.396;   // r
	materialDiffuse[1] = 0.74151; // g
	materialDiffuse[2] = 0.69102; // b
	materialDiffuse[3] = 1.0f;    // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.297254; // r
	materialSpecular[1] = 0.30829;  // g
	materialSpecular[2] = 0.306678; // b
	materialSpecular[3] = 1.0f;     // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.1 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(1.5f, 1.5f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);

	//*********************************************************************************

	//*************************** SECOND COLUMN - Metals *****************************

	// ***** 1st sphere on 2nd column, BRASS *****
	// ambient material
	materialAmbient[0] = 0.329412; // r
	materialAmbient[1] = 0.223529; // g
	materialAmbient[2] = 0.027451; // b
	materialAmbient[3] = 1.0f;     // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.780392; // r
	materialDiffuse[1] = 0.568627; // g
	materialDiffuse[2] = 0.113725; // b
	materialDiffuse[3] = 1.0f;     // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.992157; // r
	materialSpecular[1] = 0.941176; // g
	materialSpecular[2] = 0.807843; // b
	materialSpecular[3] = 1.0f;     // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.21794872 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(7.5f, 14.0f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);


	// ***** 2nd sphere on 2nd column, BRONZE *****
	// ambient material
	materialAmbient[0] = 0.2125; // r
	materialAmbient[1] = 0.1275; // g
	materialAmbient[2] = 0.054;  // b
	materialAmbient[3] = 1.0f;   // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.714;   // r
	materialDiffuse[1] = 0.4284;  // g
	materialDiffuse[2] = 0.18144; // b
	materialDiffuse[3] = 1.0f;    // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.393548; // r
	materialSpecular[1] = 0.271906; // g
	materialSpecular[2] = 0.166721; // b
	materialSpecular[3] = 1.0f;     // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.2 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(7.5f, 11.5f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);


	// ***** 3rd sphere on 2nd column, CHROME *****
	// ambient material
	materialAmbient[0] = 0.25; // r
	materialAmbient[1] = 0.25; // g
	materialAmbient[2] = 0.25; // b
	materialAmbient[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.4;  // r
	materialDiffuse[1] = 0.4;  // g
	materialDiffuse[2] = 0.4;  // b
	materialDiffuse[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.774597; // r
	materialSpecular[1] = 0.774597; // g
	materialSpecular[2] = 0.774597; // b
	materialSpecular[3] = 1.0f;     // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.6 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(7.5f, 9.0f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);


	// ***** 4th sphere on 2nd column, COPPER *****
	// ambient material
	materialAmbient[0] = 0.19125; // r
	materialAmbient[1] = 0.0735;  // g
	materialAmbient[2] = 0.0225;  // b
	materialAmbient[3] = 1.0f;    // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.7038;  // r
	materialDiffuse[1] = 0.27048; // g
	materialDiffuse[2] = 0.0828;  // b
	materialDiffuse[3] = 1.0f;    // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.256777; // r
	materialSpecular[1] = 0.137622; // g
	materialSpecular[2] = 0.086014; // b
	materialSpecular[3] = 1.0f;     // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.1 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(7.5f, 6.5f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);


	// ***** 5th sphere on 2nd column, GOLD *****
// ambient material
	materialAmbient[0] = 0.24725; // r
	materialAmbient[1] = 0.1995;  // g
	materialAmbient[2] = 0.0745;  // b
	materialAmbient[3] = 1.0f;    // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.75164; // r
	materialDiffuse[1] = 0.60648; // g
	materialDiffuse[2] = 0.22648; // b
	materialDiffuse[3] = 1.0f;    // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.628281; // r
	materialSpecular[1] = 0.555802; // g
	materialSpecular[2] = 0.366065; // b
	materialSpecular[3] = 1.0f;     // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.4 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(7.5f, 4.0f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);


	// ***** 6th sphere on 2nd column, SILVER *****
	// ambient material
	materialAmbient[0] = 0.19225; // r
	materialAmbient[1] = 0.19225; // g
	materialAmbient[2] = 0.19225; // b
	materialAmbient[3] = 1.0f;    // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.50754; // r
	materialDiffuse[1] = 0.50754; // g
	materialDiffuse[2] = 0.50754; // b
	materialDiffuse[3] = 1.0f;    // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.508273; // r
	materialSpecular[1] = 0.508273; // g
	materialSpecular[2] = 0.508273; // b
	materialSpecular[3] = 1.0f;     // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.4 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(7.5f, 1.5f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);

	//*********************************************************************************

	//*************************** THIRD COLUMN - Plastic *****************************

	// ***** 1st sphere on 3rd column, BLACK *****
	// ambient material
	materialAmbient[0] = 0.0;  // r
	materialAmbient[1] = 0.0;  // g
	materialAmbient[2] = 0.0;  // b
	materialAmbient[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.01; // r
	materialDiffuse[1] = 0.01; // g
	materialDiffuse[2] = 0.01; // b
	materialDiffuse[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.50; // r
	materialSpecular[1] = 0.50; // g
	materialSpecular[2] = 0.50; // b
	materialSpecular[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(13.5f, 14.0f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);


	// ***** 2nd sphere on 3rd column, CYAN *****
	// ambient material
	materialAmbient[0] = 0.0;  // r
	materialAmbient[1] = 0.1;  // g
	materialAmbient[2] = 0.06; // b
	materialAmbient[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.0;        // r
	materialDiffuse[1] = 0.50980392; // g
	materialDiffuse[2] = 0.50980392; // b
	materialDiffuse[3] = 1.0f;       // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.50196078; // r
	materialSpecular[1] = 0.50196078; // g
	materialSpecular[2] = 0.50196078; // b
	materialSpecular[3] = 1.0f;       // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(13.5f, 11.5f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);


	// ***** 3rd sphere on 2nd column, GREEN *****
	// ambient material
	materialAmbient[0] = 0.0;  // r
	materialAmbient[1] = 0.0;  // g
	materialAmbient[2] = 0.0;  // b
	materialAmbient[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.1;  // r
	materialDiffuse[1] = 0.35; // g
	materialDiffuse[2] = 0.1;  // b
	materialDiffuse[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.45; // r
	materialSpecular[1] = 0.55; // g
	materialSpecular[2] = 0.45; // b
	materialSpecular[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(13.5f, 9.0f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);


	// ***** 4th sphere on 3rd column, red *****
	// ambient material
	materialAmbient[0] = 0.0;  // r
	materialAmbient[1] = 0.0;  // g
	materialAmbient[2] = 0.0;  // b
	materialAmbient[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.5;  // r
	materialDiffuse[1] = 0.0;  // g
	materialDiffuse[2] = 0.0;  // b
	materialDiffuse[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.7;  // r
	materialSpecular[1] = 0.6;  // g
	materialSpecular[2] = 0.6;  // b
	materialSpecular[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(13.5f, 6.5f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);


	// ***** 5th sphere on 3rd column, white*****
	// ambient material
	materialAmbient[0] = 0.0;  // r
	materialAmbient[1] = 0.0;  // g
	materialAmbient[2] = 0.0;  // b
	materialAmbient[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.55; // r
	materialDiffuse[1] = 0.55; // g
	materialDiffuse[2] = 0.55; // b
	materialDiffuse[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.70; // r
	materialSpecular[1] = 0.70; // g
	materialSpecular[2] = 0.70; // b
	materialSpecular[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(13.5f, 4.0f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);


	// ***** 6th sphere on 3rd column, yellow plastic *****
// ambient material
	materialAmbient[0] = 0.0;  // r
	materialAmbient[1] = 0.0;  // g
	materialAmbient[2] = 0.0;  // b
	materialAmbient[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.5;  // r
	materialDiffuse[1] = 0.5;  // g
	materialDiffuse[2] = 0.0;  // b
	materialDiffuse[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.60; // r
	materialSpecular[1] = 0.60; // g
	materialSpecular[2] = 0.50; // b
	materialSpecular[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(13.5f, 1.5f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);


	//*********************************************************************************

	//*************************** FOURTH COLUMN - Rubber *****************************

	// ***** 1st sphere on 4th column, black *****
	// ambient material
	materialAmbient[0] = 0.02; // r
	materialAmbient[1] = 0.02; // g
	materialAmbient[2] = 0.02; // b
	materialAmbient[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.01; // r
	materialDiffuse[1] = 0.01; // g
	materialDiffuse[2] = 0.01; // b
	materialDiffuse[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.4;  // r
	materialSpecular[1] = 0.4;  // g
	materialSpecular[2] = 0.4;  // b
	materialSpecular[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(19.5f, 14.0f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);


	// ***** 2nd sphere on 4th column, cyan *****
	// ambient material
	materialAmbient[0] = 0.0;  // r
	materialAmbient[1] = 0.05; // g
	materialAmbient[2] = 0.05; // b
	materialAmbient[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.4;  // r
	materialDiffuse[1] = 0.5;  // g
	materialDiffuse[2] = 0.5;  // b
	materialDiffuse[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.04; // r
	materialSpecular[1] = 0.7;  // g
	materialSpecular[2] = 0.7;  // b
	materialSpecular[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(19.5f, 11.5f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);


	// ***** 3rd sphere on 4th column, green *****
	// ambient material
	materialAmbient[0] = 0.0;  // r
	materialAmbient[1] = 0.05; // g
	materialAmbient[2] = 0.0;  // b
	materialAmbient[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.4;  // r
	materialDiffuse[1] = 0.5;  // g
	materialDiffuse[2] = 0.4;  // b
	materialDiffuse[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.04; // r
	materialSpecular[1] = 0.7;  // g
	materialSpecular[2] = 0.04; // b
	materialSpecular[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(19.5f, 9.0f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);


	// ***** 4th sphere on 4th column, red *****
	// ambient material
	materialAmbient[0] = 0.05; // r
	materialAmbient[1] = 0.0;  // g
	materialAmbient[2] = 0.0;  // b
	materialAmbient[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.5;  // r
	materialDiffuse[1] = 0.4;  // g
	materialDiffuse[2] = 0.4;  // b
	materialDiffuse[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.7;  // r
	materialSpecular[1] = 0.04; // g
	materialSpecular[2] = 0.04; // b
	materialSpecular[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(19.5f, 6.5f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);


	// ***** 5th sphere on 4th column, white *****
	// ambient material
	materialAmbient[0] = 0.05; // r
	materialAmbient[1] = 0.05; // g
	materialAmbient[2] = 0.05; // b
	materialAmbient[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.5;  // r
	materialDiffuse[1] = 0.5;  // g
	materialDiffuse[2] = 0.5;  // b
	materialDiffuse[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.7;  // r
	materialSpecular[1] = 0.7;  // g
	materialSpecular[2] = 0.7;  // b
	materialSpecular[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(19.5f, 4.0f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);


	// ***** 6th sphere on 4th column, YELLOW rubber *****
	// ambient material
	materialAmbient[0] = 0.05; // r
	materialAmbient[1] = 0.05; // g
	materialAmbient[2] = 0.0;  // b
	materialAmbient[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);

	// diffuse material
	materialDiffuse[0] = 0.5;  // r
	materialDiffuse[1] = 0.5;  // g
	materialDiffuse[2] = 0.4;  // b
	materialDiffuse[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	// specular material
	materialSpecular[0] = 0.7;  // r
	materialSpecular[1] = 0.7;  // g
	materialSpecular[2] = 0.04; // b
	materialSpecular[3] = 1.0f; // a
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	// shininess
	materialShininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glLoadIdentity();
	glTranslatef(19.5f, 1.5f, 0.0f);

	gluSphere(quadric, 1.0f, 30, 30);

    glXSwapBuffers(display, window);
}

void update(void)
{
    //code
    if (keyPressed == 1)
	{
		angleForXRotation = angleForXRotation + 1.0f;
		if (angleForXRotation >= 360.0f)
		{
			angleForXRotation = angleForXRotation - 360.0f;
		}
	}

	if (keyPressed == 2)
	{
		angleForYRotation = angleForYRotation + 1.0f;
		if (angleForYRotation >= 360.0f)
		{
			angleForYRotation = angleForYRotation - 360.0f;
		}
	}

	if (keyPressed == 3)
	{
		angleForZRotation = angleForZRotation + 1.0f;
		if (angleForZRotation >= 360.0f)
		{
			angleForZRotation = angleForZRotation - 360.0f;
		}
	}

}

void uninitialize(void)
{
    //local variable declarartions
    GLXContext currentGLXContext = NULL;

    //code
    //Free visualInfo
    if(visualInfo)
    {
        free(visualInfo);
        visualInfo = NULL;
    }

    //Uncurrent the context
    currentGLXContext = glXGetCurrentContext();
    if (currentGLXContext != NULL && currentGLXContext == glxContext)
    {
        glXMakeCurrent(display, None, NULL);
    }

    if(glxContext)
    {
        glXDestroyContext(display, glxContext);
        glxContext = NULL;
    }

    if (window)
    {
        XDestroyWindow(display, window);
    }

    if (colormap)
    {
        XFreeColormap(display, colormap);
    }

    if (display)
    {
        XCloseDisplay(display);
        display = NULL;
    }

    // Close the log file
    if (gpFile)
    {
        fprintf(gpFile, "Program Ended Successfully!\n");
        fclose(gpFile);
        gpFile = NULL;
    }

}

