//Standard header file
#include <stdio.h> //for printf
#include <stdlib.h> //for exit
#include <memory.h> //for memsate

#include <SOIL/SOIL.h>

//X11 header files
#include <X11/Xlib.h>
#include <X11/Xutil.h> //for XVisualInfo and related APIs
#include <X11/XKBlib.h> //for all XWindow APIs

//OpenGL header files
#include <GL/gl.h>
#include <GL/glx.h> //glx : graphic library for x window
#include <GL/glu.h>

//Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600


//Global Variable Declarations
Display *display = NULL;
Colormap colormap;
Window window;
XVisualInfo *visualInfo = NULL; 

//OpenGL global variable
GLXContext glxContext = NULL; 

Bool bFullscreen = False;
Bool bActiveWindow = False;

FILE* gpFile = NULL;

//texture
GLuint texture_kundali = 0;

GLfloat c_angle = 0.0f;



int main(void)
{
    //Local function declarartions
    void toggleFullscreen(void);
    int initialize(void);
    void resize(int, int);
    void draw(void);
    void update(void);
    void uninitialize(void);

    //Local variable declarartions
    int defaultScreen;
   
    XSetWindowAttributes windowAttributes;
    int styleMask;
    Atom windowManagerDelete;
   
    XEvent event;
    KeySym keySym;


    int screenWidth;
    int screenHeight;

    char keys[26];
    
    display = XOpenDisplay(NULL);

    int frameBufferAttributes[] = 
    {
        GLX_DOUBLEBUFFER, True,
        GLX_RGBA,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        GLX_DEPTH_SIZE, 24
    };

    Bool bDone = False;

    int result = 0;


    gpFile = fopen("Log.txt", "w");
    if (gpFile == NULL)
    {
      /*  MessageBox(NULL, TEXT("Log File Cannot Be Opened"), TEXT("Error"), MB_OK | MB_ICONERROR);*/
        fprintf(gpFile, "Program Failed!\n");
        exit(0);
    }
    else
    {
        fprintf(gpFile, "Program Started Successfully!\n");
    }
    //fprintf(gpFile, "Program Started Successfully!\n");

    
    //code
    // first comment : 
    display = XOpenDisplay(NULL);
    if (display == NULL)
    {
        printf("XOpenDisplay() Failed/n");
        uninitialize();
        exit(1);
    }

    //get default screen from above display
    defaultScreen = XDefaultScreen(display);


    //Get visual info from above three
    //memset((void *)&visualInfo, 0, sizeof(XVisualInfo));
    visualInfo = glXChooseVisual(display, defaultScreen, frameBufferAttributes);

    if (visualInfo == NULL)
    {
        printf("XglXChooseVisual() Failed/n");
        uninitialize();
        exit(1);
    }

    //Set window attributes/properties
    memset((void *)&windowAttributes, 0, sizeof(XSetWindowAttributes));
    windowAttributes.border_pixel = 0;
    windowAttributes.background_pixel = XBlackPixel(display, visualInfo->screen);
    windowAttributes.background_pixmap = 0;
    windowAttributes.colormap = XCreateColormap(display,
                                                XRootWindow(display, visualInfo->screen),
                                                visualInfo->visual,
                                                AllocNone);
    
    //Assign this color map to global color map
    colormap = windowAttributes.colormap;


    //Set the style mask
    styleMask = CWBorderPixel | CWBackPixel | CWColormap | CWEventMask;

    //Now finally create the window
    window = XCreateWindow(display,
                           XRootWindow(display, visualInfo->screen),
                           0, 
                           0,
                           WIN_WIDTH, 
                           WIN_HEIGHT,
                           0,
                           visualInfo->depth,
                           InputOutput,
                           visualInfo->visual,
                           styleMask,
                           &windowAttributes);

    if(!window)
    {
        printf("XCreateWindow() Failed /n");
        uninitialize();
        exit(1);
    }

    //Specify to which events this window should respond
    XSelectInput(display, window, ExposureMask | VisibilityChangeMask | StructureNotifyMask | KeyPressMask | ButtonPressMask | PointerMotionMask | FocusChangeMask);
    

    // Specify window manager delete atom
    windowManagerDelete = XInternAtom(display, "WM_DELETE_WINDOW", True);

    //Add/set above atom as protocol for window manager
    XSetWMProtocols(display, window, &windowManagerDelete, 1);

    //Give caption to the window
    XStoreName(display, window, "Smita_Patil");

    //Show/map the window
    XMapWindow(display, window);

    //Centre the window
    screenWidth = XWidthOfScreen((XScreenOfDisplay(display, visualInfo->screen)));
    screenHeight = XHeightOfScreen((XScreenOfDisplay(display, visualInfo->screen)));

    XMoveWindow(display, window, (screenWidth - WIN_WIDTH)/2, (screenHeight - WIN_HEIGHT)/2);

    //OpenGL initialization
    //Initialization
    result = initialize();
   
    if (result == -1)
    {
       // MessageBox(hwnd, TEXT("initialize Function Failed"), TEXT("Error"), MB_OK | MB_ICONERROR);
        fprintf(gpFile, "Initialize Failed /n");
        uninitialize();
        exit(1);
    }
    else if (result == -2)
    {
        fprintf(gpFile, "Initialize Failed /n");
        uninitialize();
        exit(1);
    }

    //Event loop (mhanje windows ch message loop)
    while(bDone == False)
    {
        while(XPending(display))
        {
            XNextEvent(display, &event);
            
            switch (event.type)
            {
                case MapNotify:
                break;

                case FocusIn:
                bActiveWindow = True;
                break;

                case FocusOut:
                bActiveWindow = False;
                break;

                case ConfigureNotify:
                    resize(event.xconfigure.width, event.xconfigure.height);
                    break;

                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                        break;

                        case 2:
                        break;

                        case 3:
                        break;

                        default:
                        break;
                    }
                break;


                case KeyPress:
                keySym = XkbKeycodeToKeysym(display, event.xkey.keycode, 0, 0);

                switch (keySym)
                {
                    case XK_Escape:
                    bDone = True;
                    break;

                    default:
                    break;
                }

                XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                
                switch (keys[0])
                {
                    case 'F':
                    case 'f':
                    if (bFullscreen == False)
                    {
                        toggleFullscreen();
                        bFullscreen = True;
                    }
                    else
                    {
                        toggleFullscreen();
                        bFullscreen = False;
                    }
                    break;

                    default:
                    break;
                }
                break;

                case 33:
                {
                    bDone = True;
                }
                break;

                default:
                break;

            }
        }
        if (bActiveWindow == True)
        {
            draw();

            update();
        }
    }
    

    uninitialize();

    


    return(0);
}

void toggleFullscreen(void)
{
    //Local Variable declarations
    Atom windowManagerStateNormal;
    Atom windowManagerStateFullscreen;
    XEvent event;

    //Code
    windowManagerStateNormal = XInternAtom(display, "_NET_WM_STATE", False);

    windowManagerStateFullscreen = XInternAtom(display, "_NET_WM_STATE_FULLSCREEN", False);

    //memset the event structure and fill it with above two atoms
    memset((void *)&event, 0, sizeof(XEvent));

    event.type = ClientMessage;
    event.xclient.window = window;
    event.xclient.message_type = windowManagerStateNormal;
    event.xclient.format = 32;
    event.xclient.data.l[0] = bFullscreen?0:1; 
    event.xclient.data.l[1] = windowManagerStateFullscreen;

    //send the event
    XSendEvent(display,
               XRootWindow(display, visualInfo->screen),
               False,
               SubstructureNotifyMask,
               &event);

}



int initialize(void)
{
    //local function declarartion
    void resize(int, int);
    //void uninitialize(void);
    GLuint loadGLTexture(const char* path);
    
    //code
    //create openGL context
    glxContext = glXCreateContext(display, visualInfo, NULL, True);

    if(glxContext == NULL)
    {
        printf("In initialize glxCreateContext() failed\n");
        return(-1);
    }

    //make this context as current context
    if (glXMakeCurrent(display, window, glxContext) == False)
    {
        printf("In initialize glXMakeCurrent() failed\n");
        return(-2);
    }

    //Usual openGL code
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    //Enabling Depth
	glShadeModel(GL_SMOOTH); // Jevha pan apan color karu or shade karu, teva to shade smooth kar - Beautification

	glClearDepth(1.0f); //Compulsory step : Depth buffer la clear karayla glClearDepth() vapar ani depth buffer madhlya saglya buffers na 1 bit kar.

	glEnable(GL_DEPTH_TEST); //Compulsory: 8 test paiki hi ek test - GL_DEPTH_TEST enable kar. Ya tests by default disabled astat.

	glDepthFunc(GL_LEQUAL); //Compulsory: glDepthFunc -> Depth sathi konta function vaparu? - LEQUAL (L less than Equal to) with 1.0f (from glClearDepth()).

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); //Optional: Beautification - Jevha Depth enable karta, tevha corner chya goshti circular na dista elliptical distat. This is not good. Mhanun correction kartana te nicest kar.

    
    texture_kundali = loadGLTexture("Vijay_Kundali.bmp");

    glEnable(GL_TEXTURE_2D);

    
    resize(WIN_WIDTH, WIN_HEIGHT);

    return(0);
}

GLuint loadGLTexture(const char* path)
{

    //local variables
    int width, height;
    unsigned char* imageData = NULL;
    GLuint texture;

    //code
    imageData = SOIL_load_image(path, &width, &height, NULL, SOIL_LOAD_RGB);


    //Create OpenGL texture object
    glGenTextures(1, &texture);

    //Bind to the generted texture
    glBindTexture(GL_TEXTURE_2D, texture);

    //Decide image pixel alignment & unpacking
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

    //Set texture parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    //Create multiple mipmap images
    gluBuild2DMipmaps(GL_TEXTURE_2D, 3, width, height, GL_RGB, GL_UNSIGNED_BYTE, (void*)imageData);

    //unbind
    glBindTexture(GL_TEXTURE_2D, 0);

    //Delete image resource
    SOIL_free_image_data(imageData);

    return(texture);

}

void resize(int width, int height)
{
     //code
    if (height == 0)
        height = 1;
	

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, 
		(GLfloat)width / (GLfloat)height, 
		0.1f, 
		100.0f);
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void draw(void)
{
    //code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //3rd test : Compulsory for Depth 3D.

	glMatrixMode(GL_MODELVIEW); //Current Transformation Matrix
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -5.0f);

	glScalef(0.75f, 0.75f, 0.75f);

	glRotatef(c_angle, 1.0f, 0.0f, 0.0f);
	glRotatef(c_angle, 0.0f, 1.0f, 0.0f);
	glRotatef(c_angle, 0.0f, 0.0f, 1.0f);

	glBindTexture(GL_TEXTURE_2D, texture_kundali);

	glBegin(GL_QUADS);

	//front face
	glTexCoord2f(0.0, 0.0);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0, 0.0);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0, 1.0);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0, 1.0);
	glVertex3f(1.0f, -1.0f, 1.0f);

	//Right face
	glTexCoord2f(0.0, 0.0);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0, 0.0);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0, 1.0);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0, 1.0);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//Back face
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0, 0.0);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0, 1.0);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0, 1.0);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//Left face
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(1.0, 0.0);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0, 1.0);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0, 1.0);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	//Top face - consider spiderman lying on top with head on back side
	glTexCoord2f(0.0, 0.0);
	glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0, 0.0);
	glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0, 1.0);
	glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0, 1.0);
	glVertex3f(1.0f, 1.0f, 1.0f);

	//Bottom face - consider spiderman went down to bottom face with head on backside.
	glTexCoord2f(0.0, 0.0);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0, 0.0);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0, 1.0);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0, 1.0);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);


    glXSwapBuffers(display, window);
}

void update(void)
{
    //code
    c_angle = c_angle - 0.2f;
	if (c_angle <= 0.0f)
	{
		c_angle = c_angle + 360.0f;
	}
}

void uninitialize(void)
{
    //local variable declarartions
    GLXContext currentGLXContext = NULL;

    //code
    //Free visualInfo
    if(visualInfo)
    {
        free(visualInfo);
        visualInfo = NULL;
    }

    //Uncurrent the context
    currentGLXContext = glXGetCurrentContext();
    if (currentGLXContext != NULL && currentGLXContext == glxContext)
    {
        glXMakeCurrent(display, None, NULL);
    }

    if(glxContext)
    {
        glXDestroyContext(display, glxContext);
        glxContext = NULL;
    }

    if (window)
    {
        XDestroyWindow(display, window);
    }

    if (colormap)
    {
        XFreeColormap(display, colormap);
    }

    if (display)
    {
        XCloseDisplay(display);
        display = NULL;
    }

    // Close the log file
    if (gpFile)
    {
        fprintf(gpFile, "Program Ended Successfully!\n");
        fclose(gpFile);
        gpFile = NULL;
    }

    if (texture_kundali)
	{
		glDeleteTextures(1, &texture_kundali);
		texture_kundali = 0;
	}

}

