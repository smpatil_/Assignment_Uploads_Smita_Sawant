//Standard header file
#include <stdio.h> //for printf
#include <stdlib.h> //for exit
#include <memory.h> //for memsate
//#include <SOIL/SOIL.h>

//X11 header files
#include <X11/Xlib.h>
#include <X11/Xutil.h> //for XVisualInfo and related APIs
#include <X11/XKBlib.h> //for all XWindow APIs

//OpenGL header files
#include <GL/gl.h>
#include <GL/glx.h> //glx : graphic library for x window
#include <GL/glu.h> 

//Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//Global Variable Declarations
Display *display = NULL;
Colormap colormap;
Window window;
XVisualInfo *visualInfo; 

//OpenGL global variable
GLXContext glxContext = NULL; 

Bool bFullscreen = False;
Bool bActiveWindow = False;

FILE* gpFile = NULL;

//Dynamic Bharat related variables

GLfloat angleRotation = 0.0f;
GLfloat speed = 0.1f;

GLfloat W = 0.0f;
GLfloat H = 0.0f;



GLfloat bTranslate = -1.4f;
Bool bTransDone = False;


GLfloat hXTranslate = -1.7f;
GLfloat hYTranslate = 1.15f;
Bool hTransDone = False;


GLfloat aXTranslate = -1.7f;
GLfloat aYTranslate = -1.45f;
Bool aTransDone = False;


GLfloat rXTranslate = 1.7f;
GLfloat rYTranslate = 1.65f;
Bool rTransDone = False;


GLfloat a2XTranslate = 1.7f;
GLfloat a2YTranslate = -1.35f;
Bool a2TransDone = False;


GLfloat tTranslate = 1.99f;
Bool tTransDone = False;


GLfloat f1xTranslate = -3.6f;

Bool tricolorOnB = False;
Bool tricolorOnH = False;
Bool tricolorOnA = False;
Bool tricolorOnR = False;
Bool tricolorOnA2 = False;
Bool tricolorOnT = False;

GLfloat tailRotation = 0.0f;

//For top and bottom jets
GLfloat jetRotation = 150.0f;
GLfloat jetXTranslate = -1.2f;

int main(void)
{
    //Local function declarartions
    void toggleFullscreen(void);
    int initialize(void);
    void resize(int, int);
    void draw(void);
    void update(void);
    void uninitialize(void);

    //Local variable declarartions
    int defaultScreen;
   
    XSetWindowAttributes windowAttributes;
    int styleMask;
    Atom windowManagerDelete;
   
    XEvent event;
    KeySym keySym;


    int screenWidth;
    int screenHeight;

    char keys[26];
    
    //display = XOpenDisplay(NULL);

    int frameBufferAttributes[] = 
    {
        GLX_DOUBLEBUFFER, True,
        GLX_RGBA,
        GLX_RED_SIZE, 8,
        GLX_GREEN_SIZE, 8,
        GLX_BLUE_SIZE, 8,
        GLX_ALPHA_SIZE, 8,
        None
    };

    Bool bDone = False;

    int result = 0;


    gpFile = fopen("Log.txt", "w");
    if (gpFile == NULL)
    {
        fprintf(gpFile, "Program Failed!\n");
        exit(0);
    }
    else
    {
        fprintf(gpFile, "Program Started Successfully!\n");
    }
    
    //code
    // 1) Open connection with x server and get the display interface
    display = XOpenDisplay(NULL);
    if (display == NULL)
    {
        printf("XOpenDisplay() Failed/n");
        uninitialize();
        exit(1);
    }

    //2) get default screen from above display
    defaultScreen = XDefaultScreen(display);


    //Get visual info from above three
    //memset((void *)&visualInfo, 0, sizeof(XVisualInfo));
    visualInfo = glXChooseVisual(display, defaultScreen, frameBufferAttributes);

    if (visualInfo == NULL)
    {
        printf("XglXChooseVisual() Failed/n");
        uninitialize();
        exit(1);
    }

    //Set window attributes/properties
    memset((void *)&windowAttributes, 0, sizeof(XSetWindowAttributes));
    windowAttributes.border_pixel = 0;
    windowAttributes.background_pixel = XBlackPixel(display, visualInfo->screen);
    windowAttributes.background_pixmap = 0;
    windowAttributes.colormap = XCreateColormap(display,
                                                XRootWindow(display, visualInfo->screen),
                                                visualInfo->visual,
                                                AllocNone);
    
    //Assign this color map to global color map
    colormap = windowAttributes.colormap;


    //Set the style mask
    styleMask = CWBorderPixel | CWBackPixel | CWColormap | CWEventMask;

    //Now finally create the window
    window = XCreateWindow(display,
                           XRootWindow(display, visualInfo->screen),
                           0, 
                           0,
                           WIN_WIDTH, 
                           WIN_HEIGHT,
                           0,
                           visualInfo->depth,
                           InputOutput,
                           visualInfo->visual,
                           styleMask,
                           &windowAttributes);

    if(!window)
    {
        printf("XCreateWindow() Failed /n");
        uninitialize();
        exit(1);
    }

    //Specify to which events this window should respond
    XSelectInput(display, window, ExposureMask | VisibilityChangeMask | StructureNotifyMask | KeyPressMask | ButtonPressMask | PointerMotionMask | FocusChangeMask);
    

    // Specify window manager delete atom
    windowManagerDelete = XInternAtom(display, "WM_DELETE_WINDOW", True);

    //Add/set above atom as protocol for window manager
    XSetWMProtocols(display, window, &windowManagerDelete, 1);

    //Give caption to the window
    XStoreName(display, window, "Smita_Patil");

    //Show/map the window
    XMapWindow(display, window);

    //Centre the window
    screenWidth = XWidthOfScreen((XScreenOfDisplay(display, visualInfo->screen)));
    screenHeight = XHeightOfScreen((XScreenOfDisplay(display, visualInfo->screen)));

    XMoveWindow(display, window, (screenWidth - WIN_WIDTH)/2, (screenHeight - WIN_HEIGHT)/2);

    //OpenGL initialization
    //Initialization
    result = initialize();
   
    if (result == -1)
    {
        fprintf(gpFile, "Initialize Failed \n");
        uninitialize();
        exit(1);
    }
    else if (result == -2)
    {
        fprintf(gpFile, "Initialize Failed /n");
        uninitialize();
        exit(1);
    }

    //Event loop (mhanje windows ch message loop)
    while(bDone == False)
    {
        while(XPending(display))
        {
            XNextEvent(display, &event);
            
            switch (event.type)
            {
                case MapNotify:
                break;

                case FocusIn:
                bActiveWindow = True;
                break;

                case FocusOut:
                bActiveWindow = False;
                break;

                case ConfigureNotify:
                    resize(event.xconfigure.width, event.xconfigure.height);
                    break;

                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                        break;

                        case 2:
                        break;

                        case 3:
                        break;

                        default:
                        break;
                    }
                break;


                case KeyPress:
                keySym = XkbKeycodeToKeysym(display, event.xkey.keycode, 0, 0);

                switch (keySym)
                {
                    case XK_Escape:
                    bDone = True;
                    break;

                    default:
                    break;
                }

                XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                
                switch (keys[0])
                {
                    case 'F':
                    case 'f':
                    if (bFullscreen == False)
                    {
                        toggleFullscreen();
                        bFullscreen = True;
                    }
                    else
                    {
                        toggleFullscreen();
                        bFullscreen = False;
                    }
                    break;

                    default:
                    break;
                }
                break;

                case 33:
                {
                    bDone = True;
                }
                break;

                default:
                break;

            }
        }
        if (bActiveWindow == True)
        {
            draw();

            update();
        }
    }
    

    uninitialize();

    


    return(0);
}

void toggleFullscreen(void)
{
    //Local Variable declarations
    Atom windowManagerStateNormal;
    Atom windowManagerStateFullscreen;
    XEvent event;

    //Code
    windowManagerStateNormal = XInternAtom(display, "_NET_WM_STATE", False);

    windowManagerStateFullscreen = XInternAtom(display, "_NET_WM_STATE_FULLSCREEN", False);

    //memset the event structure and fill it with above two atoms
    memset((void *)&event, 0, sizeof(XEvent));

    event.type = ClientMessage;
    event.xclient.window = window;
    event.xclient.message_type = windowManagerStateNormal;
    event.xclient.format = 32;
    event.xclient.data.l[0] = bFullscreen?0:1; 
    event.xclient.data.l[1] = windowManagerStateFullscreen;

    //send the event
    XSendEvent(display,
               XRootWindow(display, visualInfo->screen),
               False,
               SubstructureNotifyMask,
               &event);

}



int initialize(void)
{
    //local function declarartion
    void resize(int, int);
    //void uninitialize(void);

    
    //code
    //create openGL context
    glxContext = glXCreateContext(display, visualInfo, NULL, True);

    if(glxContext == NULL)
    {
        printf("In initialize glxCreateContext() failed\n");
        return(-1);
    }

    //make this context as current context
    if (glXMakeCurrent(display, window, glxContext) == False)
    {
        printf("In initialize glXMakeCurrent() failed\n");
        return(-2);
    }

    //Usual openGL code
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    resize(WIN_WIDTH, WIN_HEIGHT);

    return(0);
}

void resize(int width, int height)
{
     //code
    if (height == 0)
        height = 1;
	

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, 
		(GLfloat)width / (GLfloat)height, 
		0.1f, 
		100.0f); //45.0f is fovy *** (GLfloat)width / (GLfloat)height is ratio **** 0.1f is near, 100.0f is far.. near and far are not hard coded, they are standard.

    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void draw(void)
{
    void funcB(void);
	void funcH(void);
	void funcA(void);
	void funcR(void);
	void funcA2(void);
	void funcT(void);
	void fighterJet(void);
	void fighterJet2(void);
	void fighterJet3(void);
	void tricolorBand(void);
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW); //Current Transformation Matrix
	glLoadIdentity();


	glTranslatef(bTranslate, 0.0f, -2.0f);
	glScalef(0.35f, 0.3f, 0.3f);
	funcB();

	glLoadIdentity();
	glTranslatef(hXTranslate, hYTranslate, -2.0f);
	glScalef(0.35f, 0.3f, 0.3f);
	funcH();

	glLoadIdentity();
	glTranslatef(aXTranslate, aYTranslate, -2.0f);
	glScalef(0.35f, 0.3f, 0.3f);
	funcA();

	glLoadIdentity();
	glTranslatef(rXTranslate, rYTranslate, -2.0f);
	glScalef(0.35f, 0.3f, 0.3f);
	funcR();


	glLoadIdentity();
	glTranslatef(a2XTranslate, a2YTranslate, -2.0f);
	glScalef(0.35f, 0.3f, 0.3f);
	funcA2();

	glLoadIdentity();
	glTranslatef(tTranslate, 0.0f, -2.0f);
	glScalef(0.35f, 0.3f, 0.3f);
	funcT();

	glLoadIdentity();
	glTranslatef(f1xTranslate, 0.0f, -3.0f);
	fighterJet();


	//For top jet
	if (tTransDone == True)
	{
		glLoadIdentity();
		if (jetRotation <= 270.0f)
		{
			//Reference point 
			glTranslatef(-1.2f, 1.0f, -3.0f);
			glRotatef(jetRotation, 0.0f, 0.0f, 1.0f);

			//steps size
			glTranslatef(1.0f, 0.0f, 0.0f);
			glRotatef(90.0f, 0.0f, 0.0f, 1.0f);

		}
		else if (jetRotation >= 270.0f && jetXTranslate <= 1.2f)
		{
			glTranslatef(jetXTranslate, 0.0f, -3.0f);
		}
		else
		{
			//Reference point 
			glTranslatef(1.2f, 1.0f, -3.0f);
			glRotatef(jetRotation, 0.0f, 0.0f, 1.0f);

			//steps size
			glTranslatef(1.0f, 0.0f, 0.0f);
			glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
		}
		fighterJet();


		//Bottom jet
		glLoadIdentity();


		if (jetRotation <= 270.0f)
		{
			//Reference point 
			glTranslatef(-1.2f, -1.0f, -3.0f);
			glRotatef(-jetRotation, 0.0f, 0.0f, 1.0f);

			//steps size
			glTranslatef(1.0f, 0.0f, 0.0f);
			glRotatef(-90.0f, 0.0f, 0.0f, 1.0f);

		}
		else if (jetRotation >= 270.0f && jetXTranslate <= 1.2f)
		{
			glTranslatef(jetXTranslate, 0.0f, -3.0f);
		}
		else
		{
			//Reference point 
			glTranslatef(1.2f, -1.0f, -3.0f);
			glRotatef(-jetRotation, 0.0f, 0.0f, 1.0f);

			//steps size
			glTranslatef(1.0f, 0.0f, 0.0f);
			glRotatef(-90.0f, 0.0f, 0.0f, 1.0f);
		}
		fighterJet();

	}
	glXSwapBuffers(display, window);

}

void funcB(void)
{



	//vertical top
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glEnd();


	//Vertical bottom
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);
	glEnd();

	//Horizontal top middle
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.75f, 0.0f);
	glVertex3f(0.5f, 0.75f, 0.0f);
	glEnd();


	//Horizontal top middle - tricolor saffron
	/*glBegin(GL_QUADS);
	glColor3f(1.0f, 0.404f, 0.122f);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.2f, 0.75f, 0.0f);
	glVertex3f(0.5f, 0.75f, 0.0f);
	glEnd();*/



	//Horizontal bottom middle
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, -0.75f, 0.0f);
	glVertex3f(0.5f, -0.75f, 0.0f);
	glEnd();


	//centre white top
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.11f, 0.0f);
	glVertex3f(0.2f, 0.11f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glEnd();

	//centre white bottom
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, -0.11f, 0.0f);
	glVertex3f(0.5f, -0.11f, 0.0f);
	glEnd();

	//slant top right
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.67f, 0.8f, 0.0f);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.00f, 0.0f);
	glVertex3f(0.67f, 0.11f, 0.0f);
	glEnd();

	//slant bottom right
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.67f, -0.11f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glVertex3f(0.67f, -0.8f, 0.0f);
	glEnd();


	////Tricolor patch 
	////centre top - saffron
	if (tricolorOnB == True)
	{
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.404f, 0.122f);
		glVertex3f(0.5f, 0.11f, 0.0f);
		glVertex3f(0.2f, 0.11f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glEnd();


		//centre bottom - Green
		glBegin(GL_QUADS);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);

		glColor3f(0.016f, 0.416f, 0.22f);
		glVertex3f(0.2f, -0.11f, 0.0f);
		glVertex3f(0.5f, -0.11f, 0.0f);
		glEnd();
		
	}


}

void funcH(void)
{



	//vertical top left
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glEnd();


	//Vertical bottom left
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);
	glEnd();


	//centre white top
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.11f, 0.0f);
	glVertex3f(0.2f, 0.11f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glEnd();

	//centre white bottom
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, -0.11f, 0.0f);
	glVertex3f(0.5f, -0.11f, 0.0f);
	glEnd();

	
	//vertical top right
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.7f, 1.0f, 0.0f);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.00f, 0.0f);
	glVertex3f(0.7f, 0.0f, 0.0f);
	glEnd();

	//vertical bottom right
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.7f, 0.0f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glVertex3f(0.7f, -1.0f, 0.0f);
	glEnd();


	// Tricolor patch on H
	if (tricolorOnH == True)
	{
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.404f, 0.122f);
		glVertex3f(0.5f, 0.11f, 0.0f);
		glVertex3f(0.2f, 0.11f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glEnd();


		//centre bottom - Green
		glBegin(GL_QUADS);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);

		glColor3f(0.016f, 0.416f, 0.22f);
		glVertex3f(0.2f, -0.11f, 0.0f);
		glVertex3f(0.5f, -0.11f, 0.0f);
		glEnd();

	}


}

void funcA(void)
{


	//vertical top left
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glEnd();


	//Vertical bottom left
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);
	glEnd();


	//Horizontal top middle
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.75f, 0.0f);
	glVertex3f(0.5f, 0.75f, 0.0f);
	glEnd();

	//centre white top
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.11f, 0.0f);
	glVertex3f(0.2f, 0.11f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glEnd();

	
	//centre white bottom
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, -0.11f, 0.0f);
	glVertex3f(0.5f, -0.11f, 0.0f);
	glEnd();




	//vertical top right
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.7f, 1.0f, 0.0f);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.00f, 0.0f);
	glVertex3f(0.7f, 0.0f, 0.0f);
	glEnd();

	//vertical bottom right
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.7f, 0.0f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glVertex3f(0.7f, -1.0f, 0.0f);
	glEnd();

	// Tricolor patch on A
	if (tricolorOnA == True)
	{
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.404f, 0.122f);
		glVertex3f(0.5f, 0.11f, 0.0f);
		glVertex3f(0.2f, 0.11f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glEnd();


		//centre bottom - Green
		glBegin(GL_QUADS);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);

		glColor3f(0.016f, 0.416f, 0.22f);
		glVertex3f(0.2f, -0.11f, 0.0f);
		glVertex3f(0.5f, -0.11f, 0.0f);
		glEnd();

	}


}

void funcR(void)
{



	//vertical top
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glEnd();


	//Vertical bottom
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);
	glEnd();

	//Horizontal top middle
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.75f, 0.0f);
	glVertex3f(0.5f, 0.75f, 0.0f);
	glEnd();


	//centre white top
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.11f, 0.0f);
	glVertex3f(0.2f, 0.11f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glEnd();


	//centre white bottom
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, -0.11f, 0.0f);
	glVertex3f(0.5f, -0.11f, 0.0f);
	glEnd();

	
	//slant top right
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.67f, 0.8f, 0.0f);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.00f, 0.0f);
	glVertex3f(0.67f, 0.11f, 0.0f);
	glEnd();

	//slant bottom right
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.67f, -0.11f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glVertex3f(0.67f, -1.0f, 0.0f);
	glEnd();

	// Tricolor patch on R
	if (tricolorOnR == True)
	{
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.404f, 0.122f);
		glVertex3f(0.5f, 0.11f, 0.0f);
		glVertex3f(0.2f, 0.11f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glEnd();


		//centre bottom - Green
		glBegin(GL_QUADS);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);

		glColor3f(0.016f, 0.416f, 0.22f);
		glVertex3f(0.2f, -0.11f, 0.0f);
		glVertex3f(0.5f, -0.11f, 0.0f);
		glEnd();

	}


}

void funcA2(void)
{



	//vertical top left
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glEnd();


	//Vertical bottom left
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);
	glEnd();


	//Horizontal top middle
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.75f, 0.0f);
	glVertex3f(0.5f, 0.75f, 0.0f);
	glEnd();

	//centre white top
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.11f, 0.0f);
	glVertex3f(0.2f, 0.11f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glEnd();


	//centre white bottom
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, -0.11f, 0.0f);
	glVertex3f(0.5f, -0.11f, 0.0f);
	glEnd();

	
	//vertical top right
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.7f, 1.0f, 0.0f);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.00f, 0.0f);
	glVertex3f(0.7f, 0.0f, 0.0f);
	glEnd();

	//vertical bottom right
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.7f, 0.0f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glVertex3f(0.7f, -1.0f, 0.0f);
	glEnd();

	// Tricolor patch on A2
	if (tricolorOnA2 == True)
	{
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.404f, 0.122f);
		glVertex3f(0.5f, 0.11f, 0.0f);
		glVertex3f(0.2f, 0.11f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glEnd();


		//centre bottom - Green
		glBegin(GL_QUADS);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);

		glColor3f(0.016f, 0.416f, 0.22f);
		glVertex3f(0.2f, -0.11f, 0.0f);
		glVertex3f(0.5f, -0.11f, 0.0f);
		glEnd();

	}
}

void funcT(void)
{



	//vertical top
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glEnd();


	//Vertical bottom
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);
	glEnd();

	//Horizontal top middle
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glVertex3f(-0.3f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(-0.3f, 0.75f, 0.0f);
	glVertex3f(0.5f, 0.75f, 0.0f);
	glEnd();


	////Tricolor patch
	////centre top - saffron
	if (tricolorOnT == True)
	{
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.404f, 0.122f);
		glVertex3f(0.2f, 0.15f, 0.0f);
		glVertex3f(0.0f, 0.15f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);
		glEnd();

		//centre bottom - Green
		glBegin(GL_QUADS);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);

		glColor3f(0.016f, 0.416f, 0.22f);
		glVertex3f(0.0f, -0.15f, 0.0f);
		glVertex3f(0.2f, -0.15f, 0.0f);
		glEnd();
	}

}

void fighterJet(void)
{
	glScalef(0.004f, 0.004f, 0.004f);

	glBegin(GL_TRIANGLE_FAN);

	//main body
	glColor3f(0.259, 0.459, 0.592);
	glVertex3f(0.0f, 0.0f, 1.0f);
	glVertex3f(71.015f, -0.30193f, 0.0f);
	glVertex3f(65.8818f, 2.11352f, 0.0f);
	glVertex3f(60.1451f, 3.62317f, 0.0f);
	glVertex3f(50.1814f, 5.13283f, 0.0f);
	glVertex3f(34.117f, 6.52114f, 0.0f);
	glVertex3f(33.9956f, 11.7387f, 0.0f);
	glVertex3f(26.4726f, 12.3454f, 0.0f);
	glVertex3f(-5.43963f, 36.4919f, 0.0f);
	glVertex3f(-14.7828f, 33.2158f, 0.0f);
	glVertex3f(3.41814f, 9.67596f, 0.0f);
	glVertex3f(-2.52748f, 9.67596f, 0.0f);
	glVertex3f(-21.4564f, 25.5714f, 0.0f);
	glVertex3f(-26.31f, 25.5714f, 0.0f);
	glVertex3f(-23.1552f, 8.94792f, 0.0f);
	glVertex3f(-25.8246f, 6.88516f, 0.0f);
	glVertex3f(-26.4313f, 1.54623f, 0.0f);
	glVertex3f(-35.0464f, 0.090158f, 0.0f);
	glVertex3f(-26.4313f, -1.54623f, 0.0f);
	glVertex3f(-25.8246f, -6.88516f, 0.0f);
	glVertex3f(-23.1552f, -8.94792f, 0.0f);
	glVertex3f(-26.31f, -25.5714f, 0.0f);
	glVertex3f(-21.4564f, -25.5714f, 0.0f);
	glVertex3f(-2.52748f, -9.67596f, 0.0f);
	glVertex3f(3.41814f, -9.67596f, 0.0f);
	glVertex3f(-14.7828f, -33.2158f, 0.0f);
	glVertex3f(-5.43963f, -36.4919f, 0.0f);
	glVertex3f(26.4726f, -12.3454f, 0.0f);
	glVertex3f(33.9956f, -11.7387f, 0.0f);
	glVertex3f(34.117f, -6.52114f, 0.0f);
	glVertex3f(50.1814f, -5.13283f, 0.0f);
	glVertex3f(60.1451f, -3.62317f, 0.0f);
	glVertex3f(65.8818f, -2.11352f, 0.0f);
	glVertex3f(71.015f, -0.30193f, 0.0f);
	glEnd();

	//Extra lines on wings

	glLineWidth(3.5f);
	glBegin(GL_LINES);
	glColor3f(0.063, 0.251, 0.357);
	glVertex3f(-5.43963f, 36.4919f, 0.0f);
	glVertex3f(-14.7828f, 33.2158f, 0.0f);
	glEnd();

	glLineWidth(3.5f);
	glBegin(GL_LINES);
	glColor3f(0.063, 0.251, 0.357);
	glVertex3f(-4.46892f, -36.1903f, 0.0f);
	glVertex3f(-14.405f, -32.800f, 0.0f);
	glEnd();

	//Dots on rear wings
	//Top
	glPushMatrix();
	glEnable(GL_POINT_SMOOTH);
	glPointSize(4.0f);
	glBegin(GL_POINTS);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-4.77371f, 29.9349f, 0.0f);
	glEnd();
	glDisable(GL_POINT_SMOOTH);

	glEnable(GL_POINT_SMOOTH);
	glPointSize(1.9f);
	glBegin(GL_POINTS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-4.77371f, 29.9349f, 0.0f);
	glEnd();
	glDisable(GL_POINT_SMOOTH);

	glEnable(GL_POINT_SMOOTH);
	glPointSize(1.0f);
	glBegin(GL_POINTS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-4.77371f, 29.9349f, 0.0f);
	glEnd();
	glDisable(GL_POINT_SMOOTH);
	

	//Bottom
	glEnable(GL_POINT_SMOOTH);
	glPointSize(4.0f);
	glBegin(GL_POINTS);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-4.77371f, -29.9349f, 0.0f);
	glEnd();
	glDisable(GL_POINT_SMOOTH);

	glEnable(GL_POINT_SMOOTH);
	glPointSize(1.9f);
	glBegin(GL_POINTS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-4.77371f, -29.9349f, 0.0f);
	glEnd();
	glDisable(GL_POINT_SMOOTH);

	glEnable(GL_POINT_SMOOTH);
	glPointSize(1.0f);
	glBegin(GL_POINTS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-4.77371f, -29.9349f, 0.0f);
	glEnd();
	glDisable(GL_POINT_SMOOTH);
	glPopMatrix();

	//cabin
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.063, 0.251, 0.357);
	glVertex3f(53.00f, 0.0f, 0.0f);
	glVertex3f(58.85f, 0.0f, 0.0f);
	glVertex3f(58.1898f, 1.00729f, 0.0f);
	glVertex3f(56.248f, 1.978f, 0.0f);
	glVertex3f(52.8764f, 2.56748f, 0.0f);
	glVertex3f(50.0f, 1.98f, 0.0f);
	glVertex3f(47.9973f, 1.24997f, 0.0f);
	glVertex3f(47.121f, 0.0f, 0.0f);
	glVertex3f(47.9973f, -1.24997f, 0.0f);
	glVertex3f(50.0f, -1.98f, 0.0f);
	glVertex3f(52.8764f, -2.56748f, 0.0f);
	glVertex3f(56.248f, -1.978f, 0.0f);
	glVertex3f(58.1898f, -1.00729f, 0.0f);
	glVertex3f(58.85f, 0.0f, 0.0f);
	glEnd();

	//Letter I on jet
	glLineWidth(1.5f);
	glBegin(GL_LINES);
	glColor3f(0.686, 0.859, 0.984);
	glVertex3f(-3.0f, 5.0f, 0.0f);
	glVertex3f(-9.0f, 5.0f, 0.0f);
	glEnd();

	glLineWidth(1.5f);
	glBegin(GL_LINES);
	glColor3f(0.686, 0.859, 0.984);
	glVertex3f(-6.0f, 5.0f, 0.0f);
	glVertex3f(-6.0f, -5.0f, 0.0f);
	glEnd();

	glLineWidth(1.5f);
	glBegin(GL_LINES);
	glColor3f(0.686, 0.859, 0.984);
	glVertex3f(-3.0f, -5.0f, 0.0f);
	glVertex3f(-9.0f, -5.0f, 0.0f);
	glEnd();


	//Letter A on jet
	glLineWidth(1.5f);
	glBegin(GL_LINES);
	glColor3f(0.686, 0.859, 0.984);
	glVertex3f(10.0f, -5.0f, 0.0f);
	glVertex3f(6.0f, 5.0f, 0.0f);
	glEnd();

	glLineWidth(1.5f);
	glBegin(GL_LINES);
	glColor3f(0.686, 0.859, 0.984);
	glVertex3f(6.0f, 5.0f, 0.0f);
	glVertex3f(2.0f, -5.0f, 0.0f);
	glEnd();

	glLineWidth(1.5f);
	glBegin(GL_LINES);
	glColor3f(0.686, 0.859, 0.984);
	glVertex3f(8.5f, 0.0f, 0.0f);
	glVertex3f(3.5f, 0.0f, 0.0f);
	glEnd();


	//Letter F on jet
	glLineWidth(1.5f);
	glBegin(GL_LINES);
	glColor3f(0.686, 0.859, 0.984);
	glVertex3f(22.0f, 5.0f, 0.0f);
	glVertex3f(15.0f, 5.0f, 0.0f);
	glEnd();

	glLineWidth(1.5f);
	glBegin(GL_LINES);
	glColor3f(0.686, 0.859, 0.984);
	glVertex3f(22.0f, 0.0f, 0.0f);
	glVertex3f(15.0f, 0.0f, 0.0f);
	glEnd();

	glLineWidth(1.5f);
	glBegin(GL_LINES);
	glColor3f(0.686, 0.859, 0.984);
	glVertex3f(15.0f, 5.0f, 0.0f);
	glVertex3f(15.0f, -5.0f, 0.0f);
	glEnd();


	//Exhaust tail

//For exhaust tail rotation

	glPushMatrix();
	if (tailRotation >= 360.0f)
	{
		tailRotation = tailRotation - 270.0f;
	}
	tailRotation = tailRotation + 40.0f;
	glRotatef(tailRotation, 1.0f, 0.0f, 0.0f);
	glBegin(GL_POLYGON);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-33.4127f, 0.22f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-42.0f, 2.65f, 0.0f);

	glColor3f(1.0f, 0.404f, 0.122f);
	glVertex3f(-50.0f, 4.75f, 0.0f);

	glColor3f(0.016f, 0.416f, 0.22f);
	glVertex3f(-50.0f, -0.22f, 0.0f);

	glColor3f(0.016f, 0.416f, 0.22f);
	glVertex3f(-42.0f, -2.65f, 0.0f);

	glColor3f(1.0f, 0.404f, 0.122f);
	glVertex3f(-50.0f, -4.75f, 0.0f);
	glEnd();
	glPopMatrix();

}



void update(void)
{
    //code
	// FOR B
	if (bTranslate <= -0.85)
	{
		bTranslate = bTranslate + 0.0024f;
	}
	else
	{
		bTransDone = True;
	}

	//FOR H

	if (hXTranslate <= -0.55f && bTransDone == True)
	{
		hXTranslate = hXTranslate + 0.0024f;
		hYTranslate = hYTranslate - 0.0024f;
	}
	else
	{
		hTransDone = True;
	}

	//FOR A
	if (hXTranslate <= -0.55f)
	{
		hTransDone = True;
	}
	else if (aXTranslate <= -0.25 && hTransDone == True)
	{
		aXTranslate = aXTranslate + 0.0024f;
		aYTranslate = aYTranslate + 0.0024f;
	}
	else
	{
		aTransDone = True;
	}

	//FOR R
	if (aXTranslate <= -0.25)
	{
		aTransDone = True;
	}
	else if (rXTranslate >= 0.05 && aTransDone == True)
	{
		rXTranslate = rXTranslate - 0.0024f;
		rYTranslate = rYTranslate - 0.0024f;
	}
	else
	{
		rTransDone = True;
	}

	//FOR A2
	if (rXTranslate >= 0.05)
	{
		rTransDone = True;
	}
	else if (a2XTranslate >= 0.35 && rTransDone == True)
	{
		a2XTranslate = a2XTranslate - 0.0024f;
		a2YTranslate = a2YTranslate + 0.0024f;
	}
	else
	{
		a2TransDone = True;
	}

	//FOR T

	if (a2XTranslate >= 0.35)
	{
		a2TransDone = True;
	}
	else if (tTranslate >= 0.75 && a2TransDone == True)
	{
		tTranslate = tTranslate - 0.0024f;
	}
	else
	{
		tTransDone = True;
	}

	//FOR fighter jets on x translation


	if (tTranslate >= 0.75)
	{
		tTransDone = True;
	}
	else if (f1xTranslate >= -4.0f && tTransDone == True)
	{
		f1xTranslate = f1xTranslate + 0.001f;
	}


// For top and bottom jets
	if (tTranslate >= 0.75)
	{
		tTransDone = True;
	}
	else if (jetRotation <= 270.0f && tTransDone == True)
	{
		jetRotation = jetRotation + 0.05f;
	}
	else if (jetRotation >= 270.0f && jetXTranslate <= 1.2f)
	{
		jetXTranslate = jetXTranslate + 0.001f;
	}
	else if (jetRotation <= 400.0f)
	{
		jetRotation = jetRotation + 0.05f;
	}
	


	//Show tricolor band on BHARAT

	if (f1xTranslate > -1.0505f)
	{
		tricolorOnB = True;
	}

	if (f1xTranslate > -0.640f)
	{
		tricolorOnH = True;
	}

	if (f1xTranslate > -0.18f)
	{
		tricolorOnA = True;
	}

	if (f1xTranslate > 0.25f)
	{
		tricolorOnR = True;
	}

	if (f1xTranslate > 0.71f)
	{
		tricolorOnA2 = True;
	}

	if (f1xTranslate > 1.15f)
	{
		tricolorOnT = True;
	}

}

void uninitialize(void)
{
    //local variable declarartions
    GLXContext currentGLXContext = NULL;

    //code
    //Free visualInfo
    if(visualInfo)
    {
        free(visualInfo);
        visualInfo = NULL;
    }

    //Uncurrent the context
    currentGLXContext = glXGetCurrentContext();
    if (currentGLXContext != NULL && currentGLXContext == glxContext)
    {
        glXMakeCurrent(display, None, NULL);
    }

    if(glxContext)
    {
        glXDestroyContext(display, glxContext);
        glxContext = NULL;
    }

    if (window)
    {
        XDestroyWindow(display, window);
    }

    if (colormap)
    {
        XFreeColormap(display, colormap);
    }

    if (display)
    {
        XCloseDisplay(display);
        display = NULL;
    }

    // Close the log file
    if (gpFile)
    {
        fprintf(gpFile, "Program Ended Successfully!\n");
        fclose(gpFile);
        gpFile = NULL;
    }

}

