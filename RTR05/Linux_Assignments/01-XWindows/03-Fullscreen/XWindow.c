//Standard header file
#include <stdio.h> //for printf
#include <stdlib.h> //for exit
#include <memory.h> //for memsate

//X11 header files
#include <X11/Xlib.h>
#include <X11/Xutil.h> //for XVisualInfo and related APIs
#include <X11/XKBlib.h> //for all XWindow APIs

//Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600



//Global Variable Declarations
Display *display = NULL;
Colormap colormap;
Window window;
XVisualInfo visualInfo;

Bool bFullscreen = False;


int main(void)
{
    //Local function declarartions
    void uninitialize(void);
    void toggleFullscreen(void);

    //Local variable declarartions
    int defaultScreen;
    int defaultDepth;
   
    Status status;
    XSetWindowAttributes windowAttributes;
    int styleMask;
    Atom windowManagerDelete;
   
    XEvent event;
    KeySym keySym;


    int screenWidth;
    int screenHeight;

    char keys[26];
    
    display = XOpenDisplay(NULL);
    
    //code
    // first comment : 
    if (display == NULL)
    {
        printf("XOpenDisplay() Failed/n");
        uninitialize();
        exit(1);
    }

    //get default screen from above display
    defaultScreen = XDefaultScreen(display);

     //get default depth from above two
    defaultDepth = XDefaultDepth(display, defaultScreen);

    //Get visual info from above three
    memset((void *)&visualInfo, 0, sizeof(XVisualInfo));
    status = XMatchVisualInfo(display, defaultScreen, defaultDepth, TrueColor, &visualInfo);

    if (status == 0)
    {
        printf("XMatchVisualInfo() Failed/n");
        uninitialize();
        exit(1);
    }

    //Set window attributes/properties
    memset((void *)&windowAttributes, 0, sizeof(XSetWindowAttributes));
    windowAttributes.border_pixel = 0;
    windowAttributes.background_pixel = XBlackPixel(display, visualInfo.screen);
    windowAttributes.background_pixmap = 0;
    windowAttributes.colormap = XCreateColormap(display,
                                                XRootWindow(display, visualInfo.screen),
                                                visualInfo.visual,
                                                AllocNone);
    
    //Assign this color map to global color map
    colormap = windowAttributes.colormap;


    //Set the style mask
    styleMask = CWBorderPixel | CWBackPixel | CWColormap | CWEventMask;

    //Now finally create the window
    window = XCreateWindow(display,
                           XRootWindow(display, visualInfo.screen),
                           0, 
                           0,
                           WIN_WIDTH, 
                           WIN_HEIGHT,
                           0,
                           visualInfo.depth,
                           InputOutput,
                           visualInfo.visual,
                           styleMask,
                           &windowAttributes);

    if(!window)
    {
        printf("XCreateWindow() Failed /n");
        uninitialize();
        exit(1);
    }

    //Specify to which events this window should respond
    XSelectInput(display, window, ExposureMask | VisibilityChangeMask | StructureNotifyMask | KeyPressMask | ButtonPressMask | PointerMotionMask);
    

    // Specify window manager delete atom
    windowManagerDelete = XInternAtom(display, "WM_DELETE_WINDOW", True);

    //Add/set above atom as protocol for window manager
    XSetWMProtocols(display, window, &windowManagerDelete, 1);

    //Give caption to the window
    XStoreName(display, window, "Smita_Patil");

    //Show/map the window
    XMapWindow(display, window);

    //Centre the window
    screenWidth = XWidthOfScreen((XScreenOfDisplay(display, visualInfo.screen)));
    screenHeight = XHeightOfScreen((XScreenOfDisplay(display, visualInfo.screen)));

    XMoveWindow(display, window, (screenWidth - WIN_WIDTH)/2, (screenHeight - WIN_HEIGHT)/2);

    //Event loop (mhanje windows ch message loop)
    while(1)
    {
        XNextEvent(display, &event);
        
        switch (event.type)
        {
            case KeyPress:
            keySym = XkbKeycodeToKeysym(display, event.xkey.keycode, 0, 0);

            switch (keySym)
            {
                case XK_Escape:
                uninitialize();
                exit (0);
                break;

                default:
                break;
            }

            XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
            
            switch (keys[0])
            {
                case 'F':
                case 'f':
                if (bFullscreen == False)
                {
                    toggleFullscreen();
                    bFullscreen = True;
                }
                else
                {
                    toggleFullscreen();
                    bFullscreen = False;
                }
                break;

                default:
                break;
            }
            break;

            case 33:
            {
                uninitialize();
                exit (0);
            }
            break;

            default:
            break;

        }
    }

    uninitialize();

    


    return(0);
}

void toggleFullscreen(void)
{
    //Local Variable declarations
    Atom windowManagerStateNormal;
    Atom windowManagerStateFullscreen;
    XEvent event;

    //Code
    windowManagerStateNormal = XInternAtom(display, "_NET_WM_STATE", False);

    windowManagerStateFullscreen = XInternAtom(display, "_NET_WM_STATE_FULLSCREEN", False);

    //memset the event structure and fill it with above two atoms
    memset((void *)&event, 0, sizeof(XEvent));

    event.type = ClientMessage;
    event.xclient.window = window;
    event.xclient.message_type = windowManagerStateNormal;
    event.xclient.format = 32;
    event.xclient.data.l[0] = bFullscreen?0:1; 
    event.xclient.data.l[1] = windowManagerStateFullscreen;

    //send the event
    XSendEvent(display,
               XRootWindow(display, visualInfo.screen),
               False,
               SubstructureNotifyMask,
               &event);

}

void uninitialize(void)
{

    if (window)
    {
        XDestroyWindow(display, window);
    }

    if (colormap)
    {
        XFreeColormap(display, colormap);
    }

    if (display)
    {
        XCloseDisplay(display);
        display = NULL;
    }

}

