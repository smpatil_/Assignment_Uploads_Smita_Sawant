//Standard header file
#include <stdio.h> //for printf
#include <stdlib.h> //for exit
#include <memory.h> //for memsate

//X11 header files
#include <X11/Xlib.h>
#include <X11/Xutil.h> //for XVisualInfo and related APIs
#include <X11/XKBlib.h> //for all XWindow APIs

//Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600


//Global Variable Declarations
Display *display = NULL;
Colormap colormap;
Window window;

int main(void)
{
    //Local function declarartions
    void uninitialize(void);

    //Local variable declarartions
    int defaultScreen;
    int defaultDepth;
    XVisualInfo visualInfo;
    Status status;
    XSetWindowAttributes windowAttributes;
    int styleMask;
    Atom windowManagerDelete;
    XEvent event;
    KeySym keySym;
    
    display = XOpenDisplay(NULL);
    
    //code
    // first comment : 
    if (display == NULL)
    {
        printf("XOpenDisplay() Failed/n");
        uninitialize();
        exit(1);
    }

    //get default screen from above display
    defaultScreen = XDefaultScreen(display);

     //get default depth from above two
    defaultDepth = XDefaultDepth(display, defaultScreen);

    //Get visual info from above three
    memset((void *)&visualInfo, 0, sizeof(XVisualInfo));
    status = XMatchVisualInfo(display, defaultScreen, defaultDepth, TrueColor, &visualInfo);

    if (status == 0)
    {
        printf("XMatchVisualInfo() Failed/n");
        uninitialize();
        exit(1);
    }

    //Set window attributes/properties
    memset((void *)&windowAttributes, 0, sizeof(XSetWindowAttributes));
    windowAttributes.border_pixel = 0;
    windowAttributes.background_pixel = XBlackPixel(display, visualInfo.screen);
    windowAttributes.background_pixmap = 0;
    windowAttributes.colormap = XCreateColormap(display,
                                                XRootWindow(display, visualInfo.screen),
                                                visualInfo.visual,
                                                AllocNone);
    
    //Assign this color map to global color map
    colormap = windowAttributes.colormap;


    //Set the style mask
    styleMask = CWBorderPixel | CWBackPixel | CWColormap | CWEventMask;

    //Now finally create the window
    window = XCreateWindow(display,
                           XRootWindow(display, visualInfo.screen),
                           0, 
                           0,
                           WIN_WIDTH, 
                           WIN_HEIGHT,
                           0,
                           visualInfo.depth,
                           InputOutput,
                           visualInfo.visual,
                           styleMask,
                           &windowAttributes);

    if(!window)
    {
        printf("XCreateWindow() Failed /n");
        uninitialize();
        exit(1);
    }

    //Specify to which events this window should respond
    XSelectInput(display, window, ExposureMask | VisibilityChangeMask | StructureNotifyMask | KeyPressMask | ButtonPressMask | PointerMotionMask);
    

    // Specify window manager delete atom
    windowManagerDelete = XInternAtom(display, "WM_DELETE_WINDOW", True);

    //Add/set above atom as protocol for window manager
    XSetWMProtocols(display, window, &windowManagerDelete, 1);

    //Give caption to the window
    XStoreName(display, window, "Smita_Patil");

    //Show/map the window
    XMapWindow(display, window);

    //Event loop (mhanje windows ch message loop)
    while(1)
    {
        XNextEvent(display, &event);
        
        switch (event.type)
        {
            case KeyPress:
            keySym = XkbKeycodeToKeysym(display, event.xkey.keycode, 0, 0);

            switch (keySym)
            {
                case XK_Escape:
                uninitialize();
                exit (0);
                break;

                default:
                break;
            }
            break;

            case 33:
            {
                uninitialize();
                exit (0);
            }
            break;

            default:
            break;

        }
    }

    uninitialize();




    return(0);
}

void uninitialize(void)
{

    if (window)
    {
        XDestroyWindow(display, window);
    }

    if (colormap)
    {
        XFreeColormap(display, colormap);
    }

    if (display)
    {
        XCloseDisplay(display);
        display = NULL;
    }

}

