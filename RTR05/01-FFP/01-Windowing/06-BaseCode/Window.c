// Windows header files
#include <windows.h>
#include <stdio.h> //for FileIO
#include <stdlib.h> //for Exit

#include "Window.h"


//Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// Global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM); // LRESULT: Long / CALLBACK : Calling convention / WndProc : Function Name


//Global variable declarations
DWORD dwStyle = 0;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
BOOL gbFullscreen = FALSE;
FILE* gpFile = NULL;

HWND ghwnd = NULL;
BOOL gbActive = FALSE;


// Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hprevInstance, LPSTR lpszCmdLine, int iCmdShow) //windows.h madhe '#pragma argused1' ha macro ahe tyamule hprevInstance ani lpszCmdLine he vaparla nahi tari error ala nahi. Pan Microsoft backward compatibility sathi ajunhi te code madhe hprevInstance ani lpszCmdLine vapartat.
{

	//Window centering
	int screen_width = GetSystemMetrics(SM_CXSCREEN);
	int screen_height = GetSystemMetrics(SM_CYSCREEN);

	int window_width = 800;
	int window_height = 600;

	int window_x = (screen_width - window_width) / 2;
	int window_y = (screen_height - window_height) / 2;

	//Functions declarations >>>> Hi apli functions ahet
	int initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	// local variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Smita_Patil_chi_Window!"); //He process ch naav ahe
	int iResult = 0;
	BOOL bDone = FALSE;


	// code
	gpFile = fopen("Log.txt", "w");
	if (gpFile == NULL)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Opened"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	fprintf(gpFile, "Program Started Successfully!\n");

	// WNDCLASSEX declarations
	wndclass.cbSize = sizeof(WNDCLASSEX); //cbSize : count of bytes
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; //lpfnWndProc: long pointer to function WndProc
	wndclass.hInstance = hInstance; 
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH); // hbrBackground : handle 0f background brush. mhanje window la konta color dyaycha tyacha handle
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));//hIcon: handle to icon
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW); //hCursor : handle to cursor
	wndclass.lpszClassName = szAppName; //lpsz : long pointer string zero terminated
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// Register WNDCLASSEX
	RegisterClassEx(&wndclass);

	// Create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("SMITA_PATIL"), //He window ch naav ahe
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		window_x,
		window_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//Initialization
	iResult = initialize();

	if (iResult != 0)
	{
		MessageBox(hwnd, TEXT("initialize Function Failed"), TEXT("Error"), MB_OK | MB_ICONERROR);
		DestroyWindow(hwnd);
	}

	// Show the window
	ShowWindow(hwnd, iCmdShow);

	// Paint/redraw the window
	//UpdateWindow(hwnd); >>> Removed because it was used for WM_PAINT and WM_PAINT is not used for real time rendering

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);


	// Game Loop >>>>>> Hi loop aplyala ashichi ashi DirectX madhe vapraychi ahe
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActive == TRUE)
			{
				//Render
				display();

				// Update
				update();
			}
		}
	}

	//Uninitialize
	uninitialize();

	return((int)msg.wParam);

}

// Call back function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Local function declarations
	void ToggleFullscreen(void);
	void resize(int, int);

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActive = TRUE;
		break;

	case WM_KILLFOCUS:
		gbActive = FALSE;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam)); //He height ani width sangta
		break;

	case WM_ERASEBKGND:
		return(0);
	
	case WM_KEYDOWN:
		switch (LOWORD(wParam))
			{
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			}
		break;
	
	case WM_CHAR:
		switch (LOWORD(wParam))
			{
			case 'F':
			case 'f':
				if (gbFullscreen == FALSE)
				{
					ToggleFullscreen();
					gbFullscreen = TRUE;
				}
				else
				{
					ToggleFullscreen();
					gbFullscreen = FALSE;
				}
				break;
			}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//Local variable declarations 
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

int initialize(void)
{
	//function declarations
	// Code

	return(0);
}

void resize(int width, int height)
{
	// Code
	if (height <= 0)
		height = 1;
}

void display(void)
{
	// Code

}

void update(void)
{
	// Code

}

void uninitialize(void)
{
	//Function declarations
	void ToggleFullscreen(void);

	// Code
	// If application is exitting in fullscreen
	if (gbFullscreen == TRUE)
	{
		ToggleFullscreen();
		gbFullscreen = FALSE;
	}

	// destroy window
	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	// Close the log file
	if (gpFile)
	{
		fprintf(gpFile, "Program Ended Successfully!\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

