// Windows header files
#include <windows.h> //Win32 SDK API
#include <stdio.h> //for FileIO
#include <stdlib.h> //for Exit

#define _USE_MATH_DEFINES 1
#include <math.h>

//OpenGL Header Files
#include<gl/GL.h> // GL.h hi file Smita chya laptop chya C:\Program Files (x86)\Windows Kits\10\Include\10.0.22000.0\um\gl ya path la ahe

#include<GL/glu.h>

#include "OGL.h"


//Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//Link with openGL library
#pragma comment(lib, "OpenGL32.lib") //Hi lib sangte ki me OpenGL program karnar asun..........

#pragma comment(lib, "glu32.lib") //Mala opengl utility (GLU) function vapraych ahe

// Global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


//Global variable declarations
DWORD dwStyle          = 0;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
BOOL gbFullscreen      = FALSE;
FILE* gpFile           = NULL;

HWND ghwnd    = NULL;
BOOL gbActive = FALSE;

//OpenGL related variables
HDC ghdc   = NULL;
HGLRC ghrc = NULL;

GLfloat c_angle = 0.0f;

GLfloat identityMatrix[16];
GLfloat translationMatrix[16];
GLfloat scaleMatrix[16];
GLfloat rotationMatrix_X[16];
GLfloat rotationMatrix_Y[16];
GLfloat rotationMatrix_Z[16];


// Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hprevInstance, LPSTR lpszCmdLine, int iCmdShow) //windows.h madhe '#pragma argused' ha macro ahe tyamule hprevInstance ani lpszCmdLine he vaparla nahi tari error ala nahi. Pan Microsoft backward compatibility sathi ajunhi te code madhe hprevInstance ani lpszCmdLine vapartat.
{

	//Window centering
	int screen_width  = GetSystemMetrics(SM_CXSCREEN);
	int screen_height = GetSystemMetrics(SM_CYSCREEN);

	int window_width  = 800;
	int window_height = 600;

	int window_x      = (screen_width - window_width) / 2;
	int window_y      = (screen_height - window_height) / 2;

	//Functions declarations >>>> Hi apli functions ahet
	int initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	// local variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Smita_Patil_chi_Window!"); //He process ch naav ahe
	int iResult = 0;
	BOOL bDone = FALSE;


	// code
	gpFile = fopen("Log.txt", "w");
	if (gpFile == NULL)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Opened"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	fprintf(gpFile, "Program Started Successfully!\n");

	// WNDCLASSEX declarations
	wndclass.cbSize        = sizeof(WNDCLASSEX);
	wndclass.style         = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra    = 0;
	wndclass.cbWndExtra    = 0;
	wndclass.lpfnWndProc   = WndProc;
	wndclass.hInstance     = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon         = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName  = NULL;
	wndclass.hIconSm       = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// Register WNDCLASSEX
	RegisterClassEx(&wndclass);

	// Create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("SMITA_PATIL"), //He window ch naav ahe
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		window_x,
		window_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//Initialization
	iResult = initialize();

	if (iResult != 0)
	{
		MessageBox(hwnd, TEXT("initialize Function Failed"), TEXT("Error"), MB_OK | MB_ICONERROR);
		DestroyWindow(hwnd);
	}

	// Show the window
	ShowWindow(hwnd, iCmdShow);

	// Paint/redraw the window
	//UpdateWindow(hwnd); >>> Removed because it was used for WM_PAINT and WM_PAINT is not used for real time rendering

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);


	// Game Loop >>>>>> Hi loop aplyala ashichi ashi DirectX madhe vapraychi ahe
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActive == TRUE)
			{
				//Render
				display();

				// Update
				update();
			}
		}
	}

	//Uninitialize
	uninitialize();

	return((int)msg.wParam);

}

// Call back function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Local function declarations
	void ToggleFullscreen(void);
	void resize(int, int);

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActive = TRUE;
		break;

	case WM_KILLFOCUS:
		gbActive = FALSE;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam)); //He height ani width sangta
		break;

	case WM_ERASEBKGND:
		return(0);
	
	case WM_KEYDOWN:
		switch (LOWORD(wParam))
			{
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			}
		break;
	
	case WM_CHAR:
		switch (LOWORD(wParam))
			{
			case 'F':
			case 'f':
				if (gbFullscreen == FALSE)
				{
					ToggleFullscreen();
					gbFullscreen = TRUE;
				}
				else
				{
					ToggleFullscreen();
					gbFullscreen = FALSE;
				}
				break;
			}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//Local variable declarations 
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

int initialize(void)
{
	//function declarations
	// Code
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	void resize(int, int);

	//Initialization of PIXELFORMATDESCRIPTOR
	pfd.nSize      = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion   = 1;
	pfd.dwFlags    = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits   = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits  = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;//1st change of depth - 3D

	//Get the DC
	ghdc = GetDC(ghwnd);
	if (ghdc == NULL)
	{
		fprintf(gpFile, "GetDC() Failed/n");
		return(-1);
	}

	//Tell OS 
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed\n");
		return(-2);
	}

	//Set the obtained pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() Failed\n");
		return(-3);
	}

	//Tell windows graphics (bridging) library to give me openGL compatible context from this device context
	//Create openGL context from device context
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() Failed\n");
		return(-4);
	}

	//Now ghdc will end its role and will give control to ghrc to do further drawing
	//Make rendering context current
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() Failed\n");
		return(-5);
	}

	//2nd step of Depth - 3D (3 compulsory - 2 beautification)
	//Enabling Depth
	glShadeModel(GL_SMOOTH); // Jevha pan apan color karu or shade karu, teva to shade smooth kar - Beautification

	glClearDepth(1.0f); //Compulsory step : Depth buffer la clear karayla glClearDepth() vapar ani depth buffer madhlya saglya buffers na 1 bit kar.

	glEnable(GL_DEPTH_TEST); //Compulsory: 8 test paiki hi ek test - GL_DEPTH_TEST enable kar. Ya tests by default disabled astat.

	glDepthFunc(GL_LEQUAL); //Compulsory: glDepthFunc -> Depth sathi konta function vaparu? - LEQUAL (L less than Equal to) with 1.0f (from glClearDepth()).

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); //Optional: Beautification - Jevha Depth enable karta, tevha corner chya goshti circular na dista elliptical distat. This is not good. Mhanun correction kartana te nicest kar.

	// Set the clear color of window to blue.
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);


	//Initialization of matrices
	//Identity matric
	identityMatrix[0] = 1.0f;
	identityMatrix[1] = 0.0f;
	identityMatrix[2] = 0.0f;
	identityMatrix[3] = 0.0f;
	
	identityMatrix[4] = 0.0f;
	identityMatrix[5] = 1.0f;
	identityMatrix[6] = 0.0f;
	identityMatrix[7] = 0.0f;

	identityMatrix[8] = 0.0f;
	identityMatrix[9] = 0.0f;
	identityMatrix[10] = 1.0f;
	identityMatrix[11] = 0.0f;
	
	identityMatrix[12] = 0.0f;
	identityMatrix[13] = 0.0f;
	identityMatrix[14] = 0.0f;
	identityMatrix[15] = 1.0f;
	
	//Translation matrix
	translationMatrix[0] = 1.0f;
	translationMatrix[1] = 0.0f;
	translationMatrix[2] = 0.0f;
	translationMatrix[3] = 0.0f;
	
	translationMatrix[4] = 0.0f;
	translationMatrix[5] = 1.0f;
	translationMatrix[6] = 0.0f;
	translationMatrix[7] = 0.0f;

	translationMatrix[8] = 0.0f;
	translationMatrix[9] = 0.0f;
	translationMatrix[10] = 1.0f;
	translationMatrix[11] = 0.0f;
	
	translationMatrix[12] = 0.0f;
	translationMatrix[13] = 0.0f;
	translationMatrix[14] = -5.0f;
	translationMatrix[15] = 1.0f;

	//scale matrix
	scaleMatrix[0] = 0.75f;
	scaleMatrix[1] = 0.0f;
	scaleMatrix[2] = 0.0f;
	scaleMatrix[3] = 0.0f;
	
	scaleMatrix[4] = 0.0f;
	scaleMatrix[5] = 0.75f;
	scaleMatrix[6] = 0.0f;
	scaleMatrix[7] = 0.0f;
	
	scaleMatrix[8] = 0.0f;
	scaleMatrix[9] = 0.0f;
	scaleMatrix[10] = 0.75f;
	scaleMatrix[11] = 0.0f;
	
	scaleMatrix[12] = 0.0f;
	scaleMatrix[13] = 0.0f;
	scaleMatrix[14] = 0.0f;
	scaleMatrix[15] = 1.0f;

	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

void resize(int width, int height)
{
	// Code
	if (height <= 0)
		height = 1;

	glMatrixMode(GL_PROJECTION); //Current Transformation Matrix
	glLoadIdentity();

	gluPerspective(45.0f, 
		(GLfloat)width / (GLfloat)height, 
		0.1f, 
		100.0f); //45.0f is fovy *** (GLfloat)width / (GLfloat)height is ratio **** 0.1f is near, 100.0f is far.. near and far are not hard coded, they are standard.

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void) //He call kela jaen Game Loop madhe
{
	static GLfloat angle;

	// Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //3rd test : Compulsory for Depth 3D.

	glMatrixMode(GL_MODELVIEW); //Current Transformation Matrix
	//glLoadIdentity();

	glLoadMatrixf(identityMatrix);

	//glTranslatef(0.0f, 0.0f, -5.0f);
	glMultMatrixf(translationMatrix);

	//glScalef(0.75f, 0.75f, 0.75f);
	glMultMatrixf(scaleMatrix);

	/*glRotatef(c_angle, 1.0f, 0.0f, 0.0f);
	glRotatef(c_angle, 0.0f, 1.0f, 0.0f);
	glRotatef(c_angle, 0.0f, 0.0f, 1.0f);*/
	angle = c_angle * (M_PI / 180.0f);

	//X rotation matrix
	rotationMatrix_X[0] = 1.0f;
	rotationMatrix_X[1] = 0.0f;
	rotationMatrix_X[2] = 0.0f;
	rotationMatrix_X[3] = 0.0f;
	
	rotationMatrix_X[4] = 0.0f;
	rotationMatrix_X[5] = cos(angle);
	rotationMatrix_X[6] = sin(angle);
	rotationMatrix_X[7] = 0.0f;
	
	rotationMatrix_X[8] = 0.0f;
	rotationMatrix_X[9] = -sin(angle);
	rotationMatrix_X[10] = cos(angle);
	rotationMatrix_X[11] = 0.0f;
	
	rotationMatrix_X[12] = 0.0f;
	rotationMatrix_X[13] = 0.0f;
	rotationMatrix_X[14] = 0.0f;
	rotationMatrix_X[15] = 1.0f;

	//Y rotation matrix
	rotationMatrix_Y[0] = cos(angle);
	rotationMatrix_Y[1] = 0.0f;
	rotationMatrix_Y[2] = -sin(angle);
	rotationMatrix_Y[3] = 0.0f;
	
	rotationMatrix_Y[4] = 0.0f;
	rotationMatrix_Y[5] = 1.0f;
	rotationMatrix_Y[6] = 0.0f;
	rotationMatrix_Y[7] = 0.0f;
	
	rotationMatrix_Y[8] = sin(angle);
	rotationMatrix_Y[9] = 0.0f;
	rotationMatrix_Y[10] = cos(angle);
	rotationMatrix_Y[11] = 0.0f;
	
	rotationMatrix_Y[12] = 0.0f;
	rotationMatrix_Y[13] = 0.0f;
	rotationMatrix_Y[14] = 0.0f;
	rotationMatrix_Y[15] = 1.0f;


	//Z rotation matrix
	rotationMatrix_Z[0] = cos(angle);
	rotationMatrix_Z[1] = sin(angle);
	rotationMatrix_Z[2] = 0.0f;
	rotationMatrix_Z[3] = 0.0f;
	
	rotationMatrix_Z[4] = -sin(angle);
	rotationMatrix_Z[5] = cos(angle);
	rotationMatrix_Z[6] = 0.0f;
	rotationMatrix_Z[7] = 0.0f;
	
	rotationMatrix_Z[8] = 0.0f;
	rotationMatrix_Z[9] = 0.0f;
	rotationMatrix_Z[10] = 1.0f;
	rotationMatrix_Z[11] = 0.0f;
	
	rotationMatrix_Z[12] = 0.0f;
	rotationMatrix_Z[13] = 0.0f;
	rotationMatrix_Z[14] = 0.0f;
	rotationMatrix_Z[15] = 1.0f;

	glMultMatrixf(rotationMatrix_X);
	glMultMatrixf(rotationMatrix_Y);
	glMultMatrixf(rotationMatrix_Z);

	glBegin(GL_QUADS);

	//front face
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	//Right face
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//Back face
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//Left face
	glColor3f(0.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	//Top face - consider spiderman lying on top with head on back side
	glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	//Bottom face - consider spiderman went down to bottom face with head on backside.
	glColor3f(  1.0f  ,  1.0f, 0.0f);
	glVertex3f( 1.0f , -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f( 1.0f , -1.0f, 1.0f);

	glEnd();

	SwapBuffers(ghdc);
}

void update(void) //ithe update cha code tevach yeto jeva animation karaych asta
{
	// Code
	c_angle = c_angle - 0.2f;
	if (c_angle <= 0.0f)
	{
		c_angle = c_angle + 360.0f;
	}

}

void uninitialize(void)
{
	//Function declarations
	void ToggleFullscreen(void);

	// Code
	// If application is exitting in fullscreen
	if (gbFullscreen == TRUE)
	{
		ToggleFullscreen();
		gbFullscreen = FALSE;
	}

	//Make the HDC as current DC
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	// Destroy or delete Rendering context
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	// Release the HDC
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
	}

	// destroy window
	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	// Close the log file
	if (gpFile)
	{
		fprintf(gpFile, "Program Ended Successfully!\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

