// Windows header files
#include <windows.h> //Win32 SDK API
#include <stdio.h> //for FileIO
#include <stdlib.h> //for Exit

//OpenGL Header Files
#include<gl/GL.h> // GL.h hi file Smita chya laptop chya C:\Program Files (x86)\Windows Kits\10\Include\10.0.22000.0\um\gl ya path la ahe

#include<GL/glu.h>

#include "OGL.h"


//Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//Link with openGL library
#pragma comment(lib, "OpenGL32.lib") //Hi lib sangte ki me OpenGL program karnar asun..........

#pragma comment(lib, "glu32.lib") //Mala opengl utility (GLU) function vapraych ahe

#pragma comment(lib, "winmm.lib")

// Global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


//Global variable declarations
DWORD dwStyle = 0;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
BOOL gbFullscreen = FALSE;
FILE* gpFile = NULL;

HWND ghwnd = NULL;
BOOL gbActive = FALSE;

//OpenGL related variables
HDC ghdc = NULL;
HGLRC ghrc = NULL;


GLfloat angleRotation = 0.0f;
GLfloat speed = 0.1f;

GLfloat W = 0.0f;
GLfloat H = 0.0f;



GLfloat bTranslate = -1.4f;
BOOL bTransDone = FALSE;


GLfloat hXTranslate = -1.7f;
GLfloat hYTranslate = 1.15f;
BOOL hTransDone = FALSE;


GLfloat aXTranslate = -1.7f;
GLfloat aYTranslate = -1.45f;
BOOL aTransDone = FALSE;


GLfloat rXTranslate = 1.7f;
GLfloat rYTranslate = 1.65f;
BOOL rTransDone = FALSE;


GLfloat a2XTranslate = 1.7f;
GLfloat a2YTranslate = -1.35f;
BOOL a2TransDone = FALSE;


GLfloat tTranslate = 1.99f;
BOOL tTransDone = FALSE;


GLfloat f1xTranslate = -3.6f;

//GLfloat tricolorTranslate = -5.0f;

BOOL tricolorOnB = FALSE;
BOOL tricolorOnH = FALSE;
BOOL tricolorOnA = FALSE;
BOOL tricolorOnR = FALSE;
BOOL tricolorOnA2 = FALSE;
BOOL tricolorOnT = FALSE;

//GLfloat f1xTranslate = -1.4f;
BOOL jetTransDone = FALSE;
//BOOL jet2TransDone = FALSE;
//BOOL jet3TransDone = FALSE;

tailRotation = 0.0f;

//For top and bottom jets
GLfloat jetRotation = 150.0f;
GLfloat jetXTranslate = -1.2f;

// Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hprevInstance, LPSTR lpszCmdLine, int iCmdShow) //windows.h madhe '#pragma argused' ha macro ahe tyamule hprevInstance ani lpszCmdLine he vaparla nahi tari error ala nahi. Pan Microsoft backward compatibility sathi ajunhi te code madhe hprevInstance ani lpszCmdLine vapartat.
{

	//Window centering
	int screen_width = GetSystemMetrics(SM_CXSCREEN);
	int screen_height = GetSystemMetrics(SM_CYSCREEN);

	int window_width = 800;
	int window_height = 600;

	int window_x = (screen_width - window_width) / 2;
	int window_y = (screen_height - window_height) / 2;

	//Functions declarations >>>> Hi apli functions ahet
	int initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	// local variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Smita_Patil_chi_Window!"); //He process ch naav ahe
	int iResult = 0;
	BOOL bDone = FALSE;


	// code
	gpFile = fopen("Log.txt", "w");
	if (gpFile == NULL)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Opened"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	fprintf(gpFile, "Program Started Successfully!\n");

	// WNDCLASSEX declarations
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// Register WNDCLASSEX
	RegisterClassEx(&wndclass);

	// Create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("SMITA_PATIL"), //He window ch naav ahe
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		window_x,
		window_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//Initialization
	iResult = initialize();

	if (iResult != 0)
	{
		MessageBox(hwnd, TEXT("initialize Function Failed"), TEXT("Error"), MB_OK | MB_ICONERROR);
		DestroyWindow(hwnd);
	}

	// Show the window
	ShowWindow(hwnd, iCmdShow);

	// Paint/redraw the window
	//UpdateWindow(hwnd); >>> Removed because it was used for WM_PAINT and WM_PAINT is not used for real time rendering

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);


	// Game Loop >>>>>> Hi loop aplyala ashichi ashi DirectX madhe vapraychi ahe
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActive == TRUE)
			{
				//Render
				display();

				// Update
				update();
			}
		}
	}

	//Uninitialize
	uninitialize();

	return((int)msg.wParam);

}

// Call back function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Local function declarations
	void ToggleFullscreen(void);
	void resize(int, int);

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActive = TRUE;
		break;

	case WM_KILLFOCUS:
		gbActive = FALSE;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam)); //He height ani width sangta
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'F':
		case 'f':
			if (gbFullscreen == FALSE)
			{
				ToggleFullscreen();
				gbFullscreen = TRUE;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = FALSE;
			}
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//Local variable declarations 
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

int initialize(void)
{
	//function declarations
	// Code
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	void resize(int, int);

	//for sound play
	PlaySound(MAKEINTRESOURCE(IDR_SOUND), GetModuleHandle(NULL), SND_RESOURCE | SND_ASYNC);

	//Initialization of PIXELFORMATDESCRIPTOR
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	//Get the DC
	ghdc = GetDC(ghwnd);
	if (ghdc == NULL)
	{
		fprintf(gpFile, "GetDC() Failed/n");
		return(-1);
	}

	//Tell OS 
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed\n");
		return(-2);
	}

	//Set the obtained pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() Failed\n");
		return(-3);
	}

	//Tell windows graphics (bridging) library to give me openGL compatible context from this device context
	//Create openGL context from device context
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() Failed\n");
		return(-4);
	}

	//Now ghdc will end its role and will give control to ghrc to do further drawing
	//Make rendering context current
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() Failed\n");
		return(-5);
	}

	// Set the clear color of window to blue.
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

void resize(int width, int height)
{
	// Code
	if (height <= 0)
		height = 1;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f); //45.0f is fovy *** (GLfloat)width / (GLfloat)height is ratio **** 0.1f is near, 100.0f is far.. near and far are not hard coded, they are standard.

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void) //He call kela jaen Game Loop madhe
{
	// Code

	void funcB(void);
	void funcH(void);
	void funcA(void);
	void funcR(void);
	void funcA2(void);
	void funcT(void);
	void fighterJet(void);
	void fighterJet2(void);
	void fighterJet3(void);
	void tricolorBand(void);
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW); //Current Transformation Matrix
	glLoadIdentity();


	glTranslatef(bTranslate, 0.0f, -2.0f);
	glScalef(0.35f, 0.3f, 0.3f);
	funcB();

	glLoadIdentity();
	glTranslatef(hXTranslate, hYTranslate, -2.0f);
	glScalef(0.35f, 0.3f, 0.3f);
	funcH();

	glLoadIdentity();
	glTranslatef(aXTranslate, aYTranslate, -2.0f);
	glScalef(0.35f, 0.3f, 0.3f);
	funcA();

	glLoadIdentity();
	glTranslatef(rXTranslate, rYTranslate, -2.0f);
	glScalef(0.35f, 0.3f, 0.3f);
	funcR();


	glLoadIdentity();
	glTranslatef(a2XTranslate, a2YTranslate, -2.0f);
	glScalef(0.35f, 0.3f, 0.3f);
	funcA2();

	glLoadIdentity();
	glTranslatef(tTranslate, 0.0f, -2.0f);
	glScalef(0.35f, 0.3f, 0.3f);
	funcT();

	glLoadIdentity();
	glTranslatef(f1xTranslate, 0.0f, -3.0f);
	fighterJet();


	//For top jet
	if (tTransDone == TRUE)
	{
		glLoadIdentity();
		if (jetRotation <= 270.0f)
		{
			//Reference point 
			glTranslatef(-1.2f, 1.0f, -3.0f);
			glRotatef(jetRotation, 0.0f, 0.0f, 1.0f);

			//steps size
			glTranslatef(1.0f, 0.0f, 0.0f);
			glRotatef(90.0f, 0.0f, 0.0f, 1.0f);

		}
		else if (jetRotation >= 270.0f && jetXTranslate <= 1.2f)
		{
			glTranslatef(jetXTranslate, 0.0f, -3.0f);
		}
		else
		{
			//Reference point 
			glTranslatef(1.2f, 1.0f, -3.0f);
			glRotatef(jetRotation, 0.0f, 0.0f, 1.0f);

			//steps size
			glTranslatef(1.0f, 0.0f, 0.0f);
			glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
		}
		fighterJet();


		//Bottom jet
		glLoadIdentity();


		if (jetRotation <= 270.0f)
		{
			//Reference point 
			glTranslatef(-1.2f, -1.0f, -3.0f);
			glRotatef(-jetRotation, 0.0f, 0.0f, 1.0f);

			//steps size
			glTranslatef(1.0f, 0.0f, 0.0f);
			glRotatef(-90.0f, 0.0f, 0.0f, 1.0f);

		}
		else if (jetRotation >= 270.0f && jetXTranslate <= 1.2f)
		{
			glTranslatef(jetXTranslate, 0.0f, -3.0f);
		}
		else
		{
			//Reference point 
			glTranslatef(1.2f, -1.0f, -3.0f);
			glRotatef(-jetRotation, 0.0f, 0.0f, 1.0f);

			//steps size
			glTranslatef(1.0f, 0.0f, 0.0f);
			glRotatef(-90.0f, 0.0f, 0.0f, 1.0f);
		}
		fighterJet();

	}
SwapBuffers(ghdc);
}

void funcB(void)
{



	//vertical top
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glEnd();


	//Vertical bottom
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);
	glEnd();

	//Horizontal top middle
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.75f, 0.0f);
	glVertex3f(0.5f, 0.75f, 0.0f);
	glEnd();


	//Horizontal top middle - tricolor saffron
	/*glBegin(GL_QUADS);
	glColor3f(1.0f, 0.404f, 0.122f);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.2f, 0.75f, 0.0f);
	glVertex3f(0.5f, 0.75f, 0.0f);
	glEnd();*/



	//Horizontal bottom middle
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, -0.75f, 0.0f);
	glVertex3f(0.5f, -0.75f, 0.0f);
	glEnd();


	//centre white top
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.11f, 0.0f);
	glVertex3f(0.2f, 0.11f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glEnd();

	//centre white bottom
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, -0.11f, 0.0f);
	glVertex3f(0.5f, -0.11f, 0.0f);
	glEnd();

	//slant top right
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.67f, 0.8f, 0.0f);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.00f, 0.0f);
	glVertex3f(0.67f, 0.11f, 0.0f);
	glEnd();

	//slant bottom right
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.67f, -0.11f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glVertex3f(0.67f, -0.8f, 0.0f);
	glEnd();


	////Tricolor patch 
	////centre top - saffron
	if (tricolorOnB == TRUE)
	{
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.404f, 0.122f);
		glVertex3f(0.5f, 0.11f, 0.0f);
		glVertex3f(0.2f, 0.11f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glEnd();


		//centre bottom - Green
		glBegin(GL_QUADS);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);

		glColor3f(0.016f, 0.416f, 0.22f);
		glVertex3f(0.2f, -0.11f, 0.0f);
		glVertex3f(0.5f, -0.11f, 0.0f);
		glEnd();
		
	}


}

void funcH(void)
{



	//vertical top left
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glEnd();


	//Vertical bottom left
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);
	glEnd();


	//centre white top
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.11f, 0.0f);
	glVertex3f(0.2f, 0.11f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glEnd();

	//centre white bottom
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, -0.11f, 0.0f);
	glVertex3f(0.5f, -0.11f, 0.0f);
	glEnd();

	
	//vertical top right
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.7f, 1.0f, 0.0f);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.00f, 0.0f);
	glVertex3f(0.7f, 0.0f, 0.0f);
	glEnd();

	//vertical bottom right
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.7f, 0.0f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glVertex3f(0.7f, -1.0f, 0.0f);
	glEnd();


	// Tricolor patch on H
	if (tricolorOnH == TRUE)
	{
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.404f, 0.122f);
		glVertex3f(0.5f, 0.11f, 0.0f);
		glVertex3f(0.2f, 0.11f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glEnd();


		//centre bottom - Green
		glBegin(GL_QUADS);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);

		glColor3f(0.016f, 0.416f, 0.22f);
		glVertex3f(0.2f, -0.11f, 0.0f);
		glVertex3f(0.5f, -0.11f, 0.0f);
		glEnd();

	}


}

void funcA(void)
{


	//vertical top left
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glEnd();


	//Vertical bottom left
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);
	glEnd();


	//Horizontal top middle
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.75f, 0.0f);
	glVertex3f(0.5f, 0.75f, 0.0f);
	glEnd();

	//centre white top
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.11f, 0.0f);
	glVertex3f(0.2f, 0.11f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glEnd();

	
	//centre white bottom
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, -0.11f, 0.0f);
	glVertex3f(0.5f, -0.11f, 0.0f);
	glEnd();




	//vertical top right
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.7f, 1.0f, 0.0f);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.00f, 0.0f);
	glVertex3f(0.7f, 0.0f, 0.0f);
	glEnd();

	//vertical bottom right
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.7f, 0.0f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glVertex3f(0.7f, -1.0f, 0.0f);
	glEnd();

	// Tricolor patch on A
	if (tricolorOnA == TRUE)
	{
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.404f, 0.122f);
		glVertex3f(0.5f, 0.11f, 0.0f);
		glVertex3f(0.2f, 0.11f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glEnd();


		//centre bottom - Green
		glBegin(GL_QUADS);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);

		glColor3f(0.016f, 0.416f, 0.22f);
		glVertex3f(0.2f, -0.11f, 0.0f);
		glVertex3f(0.5f, -0.11f, 0.0f);
		glEnd();

	}


}

void funcR(void)
{



	//vertical top
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glEnd();


	//Vertical bottom
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);
	glEnd();

	//Horizontal top middle
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.75f, 0.0f);
	glVertex3f(0.5f, 0.75f, 0.0f);
	glEnd();


	//centre white top
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.11f, 0.0f);
	glVertex3f(0.2f, 0.11f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glEnd();


	//centre white bottom
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, -0.11f, 0.0f);
	glVertex3f(0.5f, -0.11f, 0.0f);
	glEnd();

	
	//slant top right
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.67f, 0.8f, 0.0f);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.00f, 0.0f);
	glVertex3f(0.67f, 0.11f, 0.0f);
	glEnd();

	//slant bottom right
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.67f, -0.11f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glVertex3f(0.67f, -1.0f, 0.0f);
	glEnd();

	// Tricolor patch on R
	if (tricolorOnR == TRUE)
	{
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.404f, 0.122f);
		glVertex3f(0.5f, 0.11f, 0.0f);
		glVertex3f(0.2f, 0.11f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glEnd();


		//centre bottom - Green
		glBegin(GL_QUADS);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);

		glColor3f(0.016f, 0.416f, 0.22f);
		glVertex3f(0.2f, -0.11f, 0.0f);
		glVertex3f(0.5f, -0.11f, 0.0f);
		glEnd();

	}


}

void funcA2(void)
{



	//vertical top left
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glEnd();


	//Vertical bottom left
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);
	glEnd();


	//Horizontal top middle
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.75f, 0.0f);
	glVertex3f(0.5f, 0.75f, 0.0f);
	glEnd();

	//centre white top
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.11f, 0.0f);
	glVertex3f(0.2f, 0.11f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glEnd();


	//centre white bottom
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, -0.11f, 0.0f);
	glVertex3f(0.5f, -0.11f, 0.0f);
	glEnd();

	
	//vertical top right
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.7f, 1.0f, 0.0f);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 0.00f, 0.0f);
	glVertex3f(0.7f, 0.0f, 0.0f);
	glEnd();

	//vertical bottom right
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.7f, 0.0f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glVertex3f(0.7f, -1.0f, 0.0f);
	glEnd();

	// Tricolor patch on A2
	if (tricolorOnA2 == TRUE)
	{
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.404f, 0.122f);
		glVertex3f(0.5f, 0.11f, 0.0f);
		glVertex3f(0.2f, 0.11f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glEnd();


		//centre bottom - Green
		glBegin(GL_QUADS);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.5f, 0.0f, 0.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);

		glColor3f(0.016f, 0.416f, 0.22f);
		glVertex3f(0.2f, -0.11f, 0.0f);
		glVertex3f(0.5f, -0.11f, 0.0f);
		glEnd();

	}
}

void funcT(void)
{



	//vertical top
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glEnd();


	//Vertical bottom
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);
	glEnd();

	//Horizontal top middle
	glBegin(GL_QUADS);
	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glVertex3f(-0.3f, 1.0f, 0.0f);

	glColor3f(0.443, 0.439, 0.431);
	glVertex3f(-0.3f, 0.75f, 0.0f);
	glVertex3f(0.5f, 0.75f, 0.0f);
	glEnd();


	////Tricolor patch
	////centre top - saffron
	if (tricolorOnT == TRUE)
	{
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.404f, 0.122f);
		glVertex3f(0.2f, 0.15f, 0.0f);
		glVertex3f(0.0f, 0.15f, 0.0f);

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);
		glEnd();

		//centre bottom - Green
		glBegin(GL_QUADS);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.2f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);

		glColor3f(0.016f, 0.416f, 0.22f);
		glVertex3f(0.0f, -0.15f, 0.0f);
		glVertex3f(0.2f, -0.15f, 0.0f);
		glEnd();
	}

}

void fighterJet(void)
{
	glScalef(0.004f, 0.004f, 0.004f);

	glBegin(GL_TRIANGLE_FAN);

	//main body
	glColor3f(0.259, 0.459, 0.592);
	glVertex3f(0.0f, 0.0f, 1.0f);
	glVertex3f(71.015f, -0.30193f, 0.0f);
	glVertex3f(65.8818f, 2.11352f, 0.0f);
	glVertex3f(60.1451f, 3.62317f, 0.0f);
	glVertex3f(50.1814f, 5.13283f, 0.0f);
	glVertex3f(34.117f, 6.52114f, 0.0f);
	glVertex3f(33.9956f, 11.7387f, 0.0f);
	glVertex3f(26.4726f, 12.3454f, 0.0f);
	glVertex3f(-5.43963f, 36.4919f, 0.0f);
	glVertex3f(-14.7828f, 33.2158f, 0.0f);
	glVertex3f(3.41814f, 9.67596f, 0.0f);
	glVertex3f(-2.52748f, 9.67596f, 0.0f);
	glVertex3f(-21.4564f, 25.5714f, 0.0f);
	glVertex3f(-26.31f, 25.5714f, 0.0f);
	glVertex3f(-23.1552f, 8.94792f, 0.0f);
	glVertex3f(-25.8246f, 6.88516f, 0.0f);
	glVertex3f(-26.4313f, 1.54623f, 0.0f);
	glVertex3f(-35.0464f, 0.090158f, 0.0f);
	glVertex3f(-26.4313f, -1.54623f, 0.0f);
	glVertex3f(-25.8246f, -6.88516f, 0.0f);
	glVertex3f(-23.1552f, -8.94792f, 0.0f);
	glVertex3f(-26.31f, -25.5714f, 0.0f);
	glVertex3f(-21.4564f, -25.5714f, 0.0f);
	glVertex3f(-2.52748f, -9.67596f, 0.0f);
	glVertex3f(3.41814f, -9.67596f, 0.0f);
	glVertex3f(-14.7828f, -33.2158f, 0.0f);
	glVertex3f(-5.43963f, -36.4919f, 0.0f);
	glVertex3f(26.4726f, -12.3454f, 0.0f);
	glVertex3f(33.9956f, -11.7387f, 0.0f);
	glVertex3f(34.117f, -6.52114f, 0.0f);
	glVertex3f(50.1814f, -5.13283f, 0.0f);
	glVertex3f(60.1451f, -3.62317f, 0.0f);
	glVertex3f(65.8818f, -2.11352f, 0.0f);
	glVertex3f(71.015f, -0.30193f, 0.0f);
	glEnd();

	//Extra lines on wings

	glLineWidth(3.5f);
	glBegin(GL_LINES);
	glColor3f(0.063, 0.251, 0.357);
	glVertex3f(-5.43963f, 36.4919f, 0.0f);
	glVertex3f(-14.7828f, 33.2158f, 0.0f);
	glEnd();

	glLineWidth(3.5f);
	glBegin(GL_LINES);
	glColor3f(0.063, 0.251, 0.357);
	glVertex3f(-4.46892f, -36.1903f, 0.0f);
	glVertex3f(-14.405f, -32.800f, 0.0f);
	glEnd();

	//Dots on rear wings
	//Top
	glPushMatrix();
	glEnable(GL_POINT_SMOOTH);
	glPointSize(4.0f);
	glBegin(GL_POINTS);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-4.77371f, 29.9349f, 0.0f);
	glEnd();
	glDisable(GL_POINT_SMOOTH);

	glEnable(GL_POINT_SMOOTH);
	glPointSize(1.9f);
	glBegin(GL_POINTS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-4.77371f, 29.9349f, 0.0f);
	glEnd();
	glDisable(GL_POINT_SMOOTH);

	glEnable(GL_POINT_SMOOTH);
	glPointSize(1.0f);
	glBegin(GL_POINTS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-4.77371f, 29.9349f, 0.0f);
	glEnd();
	glDisable(GL_POINT_SMOOTH);
	

	//Bottom
	glEnable(GL_POINT_SMOOTH);
	glPointSize(4.0f);
	glBegin(GL_POINTS);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-4.77371f, -29.9349f, 0.0f);
	glEnd();
	glDisable(GL_POINT_SMOOTH);

	glEnable(GL_POINT_SMOOTH);
	glPointSize(1.9f);
	glBegin(GL_POINTS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-4.77371f, -29.9349f, 0.0f);
	glEnd();
	glDisable(GL_POINT_SMOOTH);

	glEnable(GL_POINT_SMOOTH);
	glPointSize(1.0f);
	glBegin(GL_POINTS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-4.77371f, -29.9349f, 0.0f);
	glEnd();
	glDisable(GL_POINT_SMOOTH);
	glPopMatrix();

	//cabin
	glBegin(GL_TRIANGLE_FAN);
	glColor3f(0.063, 0.251, 0.357);
	glVertex3f(53.00f, 0.0f, 0.0f);
	glVertex3f(58.85f, 0.0f, 0.0f);
	glVertex3f(58.1898f, 1.00729f, 0.0f);
	glVertex3f(56.248f, 1.978f, 0.0f);
	glVertex3f(52.8764f, 2.56748f, 0.0f);
	glVertex3f(50.0f, 1.98f, 0.0f);
	glVertex3f(47.9973f, 1.24997f, 0.0f);
	glVertex3f(47.121f, 0.0f, 0.0f);
	glVertex3f(47.9973f, -1.24997f, 0.0f);
	glVertex3f(50.0f, -1.98f, 0.0f);
	glVertex3f(52.8764f, -2.56748f, 0.0f);
	glVertex3f(56.248f, -1.978f, 0.0f);
	glVertex3f(58.1898f, -1.00729f, 0.0f);
	glVertex3f(58.85f, 0.0f, 0.0f);
	glEnd();

	//Letter I on jet
	glLineWidth(1.5f);
	glBegin(GL_LINES);
	glColor3f(0.686, 0.859, 0.984);
	glVertex3f(-3.0f, 5.0f, 0.0f);
	glVertex3f(-9.0f, 5.0f, 0.0f);
	glEnd();

	glLineWidth(1.5f);
	glBegin(GL_LINES);
	glColor3f(0.686, 0.859, 0.984);
	glVertex3f(-6.0f, 5.0f, 0.0f);
	glVertex3f(-6.0f, -5.0f, 0.0f);
	glEnd();

	glLineWidth(1.5f);
	glBegin(GL_LINES);
	glColor3f(0.686, 0.859, 0.984);
	glVertex3f(-3.0f, -5.0f, 0.0f);
	glVertex3f(-9.0f, -5.0f, 0.0f);
	glEnd();


	//Letter A on jet
	glLineWidth(1.5f);
	glBegin(GL_LINES);
	glColor3f(0.686, 0.859, 0.984);
	glVertex3f(10.0f, -5.0f, 0.0f);
	glVertex3f(6.0f, 5.0f, 0.0f);
	glEnd();

	glLineWidth(1.5f);
	glBegin(GL_LINES);
	glColor3f(0.686, 0.859, 0.984);
	glVertex3f(6.0f, 5.0f, 0.0f);
	glVertex3f(2.0f, -5.0f, 0.0f);
	glEnd();

	glLineWidth(1.5f);
	glBegin(GL_LINES);
	glColor3f(0.686, 0.859, 0.984);
	glVertex3f(8.5f, 0.0f, 0.0f);
	glVertex3f(3.5f, 0.0f, 0.0f);
	glEnd();


	//Letter F on jet
	glLineWidth(1.5f);
	glBegin(GL_LINES);
	glColor3f(0.686, 0.859, 0.984);
	glVertex3f(22.0f, 5.0f, 0.0f);
	glVertex3f(15.0f, 5.0f, 0.0f);
	glEnd();

	glLineWidth(1.5f);
	glBegin(GL_LINES);
	glColor3f(0.686, 0.859, 0.984);
	glVertex3f(22.0f, 0.0f, 0.0f);
	glVertex3f(15.0f, 0.0f, 0.0f);
	glEnd();

	glLineWidth(1.5f);
	glBegin(GL_LINES);
	glColor3f(0.686, 0.859, 0.984);
	glVertex3f(15.0f, 5.0f, 0.0f);
	glVertex3f(15.0f, -5.0f, 0.0f);
	glEnd();


	//Exhaust tail

//For exhaust tail rotation

	glPushMatrix();
	if (tailRotation >= 360.0f)
	{
		tailRotation = tailRotation - 270.0f;
	}
	tailRotation = tailRotation + 40.0f;
	glRotatef(tailRotation, 1.0f, 0.0f, 0.0f);
	glBegin(GL_POLYGON);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-33.4127f, 0.22f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-42.0f, 2.65f, 0.0f);

	glColor3f(1.0f, 0.404f, 0.122f);
	glVertex3f(-50.0f, 4.75f, 0.0f);

	glColor3f(0.016f, 0.416f, 0.22f);
	glVertex3f(-50.0f, -0.22f, 0.0f);

	glColor3f(0.016f, 0.416f, 0.22f);
	glVertex3f(-42.0f, -2.65f, 0.0f);

	glColor3f(1.0f, 0.404f, 0.122f);
	glVertex3f(-50.0f, -4.75f, 0.0f);
	glEnd();
	glPopMatrix();

}


void update(void) //ithe update cha code tevach yeto jeva animation karaych asta
{
	// Code
	// FOR B
	if (bTranslate <= -0.85)
	{
		bTranslate = bTranslate + 0.0024f;
	}
	else
	{
		bTransDone = TRUE;
	}

	//FOR H

	if (hXTranslate <= -0.55f && bTransDone == TRUE)
	{
		hXTranslate = hXTranslate + 0.0024f;
		hYTranslate = hYTranslate - 0.0024f;
	}
	else
	{
		hTransDone = TRUE;
	}

	//FOR A
	if (hXTranslate <= -0.55f)
	{
		hTransDone = TRUE;
	}
	else if (aXTranslate <= -0.25 && hTransDone == TRUE)
	{
		aXTranslate = aXTranslate + 0.0024f;
		aYTranslate = aYTranslate + 0.0024f;
	}
	else
	{
		aTransDone = TRUE;
	}

	//FOR R
	if (aXTranslate <= -0.25)
	{
		aTransDone = TRUE;
	}
	else if (rXTranslate >= 0.05 && aTransDone == TRUE)
	{
		rXTranslate = rXTranslate - 0.0024f;
		rYTranslate = rYTranslate - 0.0024f;
	}
	else
	{
		rTransDone = TRUE;
	}

	//FOR A2
	if (rXTranslate >= 0.05)
	{
		rTransDone = TRUE;
	}
	else if (a2XTranslate >= 0.35 && rTransDone == TRUE)
	{
		a2XTranslate = a2XTranslate - 0.0024f;
		a2YTranslate = a2YTranslate + 0.0024f;
	}
	else
	{
		a2TransDone = TRUE;
	}

	//FOR T

	if (a2XTranslate >= 0.35)
	{
		a2TransDone = TRUE;
	}
	else if (tTranslate >= 0.75 && a2TransDone == TRUE)
	{
		tTranslate = tTranslate - 0.0024f;
	}
	else
	{
		tTransDone = TRUE;
	}

	//FOR fighter jets on x translation


	if (tTranslate >= 0.75)
	{
		tTransDone = TRUE;
	}
	else if (f1xTranslate >= -4.0f && tTransDone == TRUE)
	{
		f1xTranslate = f1xTranslate + 0.001f;
	}


// For top and bottom jets
	if (tTranslate >= 0.75)
	{
		tTransDone = TRUE;
	}
	else if (jetRotation <= 270.0f && tTransDone == TRUE)
	{
		jetRotation = jetRotation + 0.05f;
	}
	else if (jetRotation >= 270.0f && jetXTranslate <= 1.2f)
	{
		jetXTranslate = jetXTranslate + 0.001f;
	}
	else if (jetRotation <= 400.0f)
	{
		jetRotation = jetRotation + 0.05f;
	}
	


	//Show tricolor band on BHARAT

	if (f1xTranslate > -1.0505f)
	{
		tricolorOnB = TRUE;
	}

	if (f1xTranslate > -0.640f)
	{
		tricolorOnH = TRUE;
	}

	if (f1xTranslate > -0.18f)
	{
		tricolorOnA = TRUE;
	}

	if (f1xTranslate > 0.25f)
	{
		tricolorOnR = TRUE;
	}

	if (f1xTranslate > 0.71f)
	{
		tricolorOnA2 = TRUE;
	}

	if (f1xTranslate > 1.15f)
	{
		tricolorOnT = TRUE;
	}
}

void uninitialize(void)
{
	//Function declarations
	void ToggleFullscreen(void);

	// Code
	// If application is exitting in fullscreen
	if (gbFullscreen == TRUE)
	{
		ToggleFullscreen();
		gbFullscreen = FALSE;
	}

	//Make the HDC as current DC
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	// Destroy or delete Rendering context
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	// Release the HDC
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
	}

	// destroy window
	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	// Close the log file
	if (gpFile)
	{
		fprintf(gpFile, "Program Ended Successfully!\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

